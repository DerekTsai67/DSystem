This project is not developed for open source.<br>
This repo is built as a recruit reference.<br>
You can still take a look here, but it's not provided to work well in every environments.<br>
This project is developed under Windows, and only support Windows now.<br><br>

This repo is not the original one but a reorganized one, making it less complex.<br>
Thus this repo may not be updated.<br>
The original project is a library but not application.<br>
I merged it with the biggest application using the library, an image editor.<br>
There may be some code not used in the application.<br><br>

I develop this library just for interest, without any particular goal.<br>
I like to learn about the basic of things we use everyday, and rewrite it myself even though there's already some libraries available.<br>
All the codes inside this project are written by myself, no copy&pasting.<br>
This project contains common data structures like Array, Bitmap, Stream,<br>
 decoders of image format .bmp, .gif, .png, .jpg, and some of their encoders,<br>
 homemade GUI system, and multi-threading support.<br><br>

I wrote this project with "C with classes" style of C++,<br>
means that I used only virtual function features of C++, but not STL.<br>
I used a rare indentation style because I think it matches the logic of C language.<br>
I will meet the need of any coding style for working.<br><br>

To compile this repo,<br>
you don't need any external library installed except winapi.<br>
Compile all the .cpp files with following 3 command options:<br>
 -std=c++11<br>
 -include (The root directory you put this repo at)\source\DSPaint\DSPaint.h<br>
 -define DS_WINAPI<br>
and link all the .o files with 3 linker options "-lWinmm -lGdi32 -lWs2_32".<br>
If there's any problem to compile, you can use the precompiled executable file DSPaint.exe.<br>
Remark that the executable file must be put at root directory like the precompiled one does, making it able to read its datafiles.<br><br>

The executable file is an image editor.<br>
You can drag an image file onto the executable file to execute it, it will read the image file with my own decoder.<br>
There are some tools to edit image but no manual, just try it yourself.<br>
You can use Ctrl+Z to undo the modification you did if it's possible.<br><br>

This project is not completed.<br>
It's normal for some features to be buggy.<br>