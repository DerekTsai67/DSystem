

static void render(void*);
static const int threadMax=8;
Thread			**renderThread;
struct RenderInfo{
	const DSBitmap *bmp;
	int start,end,high,renderStart,renderEnd;
	bool edited;
	}	**renderInfo;

struct RangeInfo{
	int start,end;
	}	**rangeInfo,**rangeTemp;


static const int	showDisplayerMax=2000;
struct DisplayerInfo{
	Displayer *dis;
	const DSBitmap	*bmp;
	Coord	showHead,showEnd;
	Coord	renderHead,renderEnd;
	bool	edited;
	}	*displayerInfo;
int					showDisplayerIndex;

CrcCheck	*displayerCrc;
unsigned int		lastCrc;
bool	fullRenderBool;
int		fullRenderTime;
static const int	fullRenderPeriod=1200;



extern bool showSense,trashSignatureShow;
extern int 	senseRet;
extern Byte *buffer;

void	initShow()
	{
	showBG();
	renderThread=dalloc<Thread*>(threadMax);
	renderInfo=dalloc<RenderInfo*>(threadMax);
	rangeInfo=dalloc<RangeInfo*>(threadMax);
	rangeTemp=dalloc<RangeInfo*>(threadMax);
	for(int i=0;i<threadMax;i++)
		{
		renderThread[i]=new Thread(render,(void*)i,true);
		renderThread[i]->launch(true);
		renderInfo[i]=dalloc<RenderInfo>(showDisplayerMax);
		rangeInfo[i]=dalloc<RangeInfo>(showDisplayerMax);
		rangeTemp[i]=dalloc<RangeInfo>(showDisplayerMax);
		}
	
	displayerInfo=dalloc<DisplayerInfo>(showDisplayerMax);
	
	fullRenderTime=-fullRenderPeriod-10;
	displayerCrc=new CrcCheck;
	lastCrc=0;
	}

void	destroyShow()
	{
	delete displayerCrc;
	
	dfree(displayerInfo);
	
	for(int i=0;i<threadMax;i++)
		{
		delete renderThread[i];
		dfree(renderInfo[i]);
		dfree(rangeInfo[i]);
		dfree(rangeTemp[i]);
		}
	dfree(renderThread);
	dfree(renderInfo);
	dfree(rangeInfo);
	dfree(rangeTemp);
	}

void showProcess()
	{
	showDisplayerIndex=0;
	head->showOutRec();
	
	displayerCrc->init();
	for(int i=0;i<showDisplayerIndex;i++)
		displayerCrc->put((Byte*)&(displayerInfo[i].dis),4);

	if(ftime-fullRenderTime>=fullRenderPeriod || displayerCrc->get()!=lastCrc ||keySignal.press(KEY_F3))
		{
		fullRenderTime=ftime;
		fullRenderBool=true;
		lastCrc=displayerCrc->get();
		}
		
	if(fullRenderBool)
		{
		for(int i=0;i<showDisplayerIndex;i++)
			DSBuffer->draw(displayerInfo[i].bmp,displayerInfo[i].showHead);
		DSBuffer->toWinDIB(buffer,NULL);
		showBG();
		}
	else
		{
		for(int i=0;i<threadMax;i++)
			renderThread[i]->once();
		for(int i=0;i<threadMax;i++)
			renderThread[i]->waitOnce();
		}
	
	fullRenderBool=false;
	
	if(showSense)
		{
		if(!(senseRet&SENSOR_hold))
			FOR_COORD(c2,Coord(51,51))
				Coord c=cursorPos+c2-Coord(25,25);
				if((c-cursorPos).abs()>25)
					continue;
				Sense *sense=NULL;
				Window *now=head;
				while(now!=NULL)
					{
					sense=now->sense_(c);
					if(sense!=NULL)
						break;
					now=now->getNext();
					}
				DSBuffer->put(c,makeHSL(getHash((int)sense,360),255,getHash((int)sense,128)+192));
			END_FOR_COORD
		else
			FOR_COORD(c2,Coord(51,51))
				Coord c=cursorPos+c2-Coord(25,25);
				if((c-cursorPos).abs()>25)
					continue;
				React *react=NULL;
				Window *now=head;
				while(now!=NULL)
					{
					react=now->react_(c,sense);
					if(react!=NULL)
						break;
					now=now->getNext();
					}
				DSBuffer->put(c,makeHSL(getHash((int)react,360),255,getHash((int)react,128)+192));
			END_FOR_COORD
		}

	showUpdate();

	trashSignatureShow=true;
	}



void showBG()
	{
	DSBuffer->clear(BGC);
	for(int x=0;x<window_size.x;x++)
		DSBuffer->put(Coord(x,100),DS_RGB(255,0,0));
	for(int y=0;y<window_size.y;y++)
		DSBuffer->put(Coord(100,y),DS_RGB(255,0,0));
	for(int x=0;x<window_size.x;x++)
		DSBuffer->put(Coord(x,300),DS_RGB(255,0,0));
	for(int y=0;y<window_size.y;y++)
		DSBuffer->put(Coord(300,y),DS_RGB(255,0,0));
	}

void	fullRender()
	{
	fullRenderTime=-fullRenderPeriod;
	}



void	Displayer::show(Coord p)
	{
	if(pic==NULL)
		return;
	if(showDisplayerIndex==showDisplayerMax)
		error("showDisplayer overflow.");
	
	const DSBitmap *bmp=pic->getBitmapIndex(frameIndex);
	Coord nowPos=(pos+p).fix(pic,NULL);
	Coord nowEnd=nowPos+bmp->getSize();
	
	displayerInfo[showDisplayerIndex].dis=this;
	displayerInfo[showDisplayerIndex].bmp=bmp;
	displayerInfo[showDisplayerIndex].showHead=nowPos;
	displayerInfo[showDisplayerIndex].showEnd=nowEnd;
	displayerInfo[showDisplayerIndex].renderHead=nowPos&lastPos;
	displayerInfo[showDisplayerIndex].renderEnd=nowEnd|lastEnd;
	displayerInfo[showDisplayerIndex].edited=(lastBmp!=bmp)||(lastPos!=nowPos);
	showDisplayerIndex++;
	lastBmp=bmp;
	lastPos=nowPos;
	lastEnd=nowEnd;
	}


int compareRangeHead(const void *a0,const void *b0)
	{
	RangeInfo *a=(RangeInfo*)a0;
	RangeInfo *b=(RangeInfo*)b0;
	if(a->end==0)return 1;
	if(b->end==0)return -1;
	return (a->start>b->start)-(a->start<b->start);
	}

void render(void *i_)
	{
	int threadIndex=(int)i_;
	RenderInfo	*info=renderInfo[threadIndex];
	RangeInfo	*range=rangeInfo[threadIndex];
	RangeInfo	*rangeTemp=::rangeTemp[threadIndex];
	for(int y=window_size.y*threadIndex/threadMax;y<window_size.y*(threadIndex+1)/threadMax;y++)
		{
		DSBuffer->put(Coord(threadIndex*100+50,y),black);

		int rangeTempIndex=0;
		int max=0;
		for(int di=showDisplayerIndex-1;di>=0;di--)
			{
			if(displayerInfo[di].showHead.y>y
			 ||displayerInfo[di].showEnd.y<=y)
				{
				if(displayerInfo[di].renderHead.y<=y
				 &&displayerInfo[di].renderEnd.y>y)
					{
					rangeTemp[rangeTempIndex].start=displayerInfo[di].renderHead.x;
					rangeTemp[rangeTempIndex].end  =displayerInfo[di].renderEnd.x;
					rangeTempIndex++;
					}
				continue;
				}
			
			info[max].bmp=displayerInfo[di].bmp;
			info[max].start=displayerInfo[di].showHead.x;
			info[max].end=displayerInfo[di].showEnd.x;
			info[max].renderStart=displayerInfo[di].renderHead.x;
			info[max].renderEnd=displayerInfo[di].renderEnd.x;
			info[max].high=y-displayerInfo[di].showHead.y;
			info[max].edited=displayerInfo[di].edited||displayerInfo[di].bmp->monoOptimize(info[max].high);
			max++;
			}
		info[max].bmp=DSBuffer;
		info[max].start=0;
		info[max].end=window_size.x;
		info[max].renderStart=0;
		info[max].renderEnd=0;
		info[max].high=y;
		info[max].edited=false;
		max++;
		//for(int i=max-1;i>=0;i--)
			//DSBuffer->draw(Coord(info[i].start,y),info[i].bmp,Coord(0,info[i].high),Coord(info[i].bmp->getSize().x,1));
		
		int rangeIndex=0;
		if(fullRenderBool)
			{
			range[0].start=0;
			range[0].end=window_size.x;
			rangeIndex=1;
			}
		else
			{
			for(int i=0;i<max;i++)
				{
				if(!info[i].edited)
					continue;
				int ai;
				for(ai=0;ai<rangeTempIndex;ai++)
					if(info[i].renderStart>=rangeTemp[ai].start
					 &&info[i].renderStart<=rangeTemp[ai].end
					 &&info[i].renderEnd>rangeTemp[ai].end)
						{
						rangeTemp[ai].end=info[i].renderEnd;
						break;
						}
				if(ai==rangeTempIndex)
					{
					rangeTemp[rangeTempIndex].start=info[i].renderStart;
					rangeTemp[rangeTempIndex].end=info[i].renderEnd;
					rangeTempIndex++;
					}
				}
			
			
			for(int j=0;j<rangeTempIndex;j++)
				{
				int mi=0;
				for(int ai=1;ai<rangeTempIndex;ai++)
					if(rangeTemp[ai].start<rangeTemp[mi].start)
						mi=ai;
				int ri;
				for(ri=0;ri<rangeIndex;ri++)
					if(rangeTemp[mi].start<=range[ri].end)
						{
						if(rangeTemp[mi].end>range[ri].end)
							range[ri].end=rangeTemp[mi].end;
						break;
						}
				if(ri==rangeIndex)
					{
					range[rangeIndex].start=rangeTemp[mi].start;
					range[rangeIndex].end=rangeTemp[mi].end;
					rangeIndex++;
					}
				rangeTemp[mi].start=window_size.x*5+2000;
				}
			
			for(int ai=0;ai<rangeIndex;ai++)
				if(range[ai].start<0)
					range[ai].start=0;
				else
					break;
			for(int ai=rangeIndex-1;ai>=0;ai--)
				if(range[ai].end>window_size.x)
					range[ai].end=window_size.x;
				else
					break;
			}
		
		int aMax=255*255*255;
		int r,g,b,a;
		int dy=((window_size.x*3+3)&(~0x3))*(window_size.y-y-1);
		bool ed;
		int opt;
		//for(int x=0;x<window_size.x;x++)
		for(int ai=0;ai<rangeIndex;ai++)
		for(int x=range[ai].start;x<range[ai].end;x++)
			{
			r=g=b=0;
			a=aMax/255;
			ed=false;
			opt=0;
			for(int i=0;i<max;i++)
				{
				if(info[i].edited)
					ed=true;
				if(x<info[i].start||x>=info[i].end)
					continue;
				
				Coord p(x-info[i].start,info[i].high);
				if(!ed)
					if(info[i].bmp->getOptimizer(p)>0)
						{
						//opt=info[i].bmp->getOptimizer(p);
						//break;
						}
				DS_RGB c=info[i].bmp->get(p);
				if(c.a==0)
					continue;
				r+=c.r*c.a*a;
				g+=c.g*c.a*a;
				b+=c.b*c.a*a;
				a=a*(255-c.a)/255;
				if(a==0)
					{
					break;
					}
				}
			if(opt>0)
				{
				x+=opt-1;
				continue;
				}
			//DSBuffer->put(Coord(x,y),DS_RGB(r/aMax,g/aMax,b/aMax));
			buffer[dy+x*3  ]=b/aMax;
			if(keySignal.press(KEY_F4))
				buffer[dy+x*3  ]=b/aMax/2+(rand()%127);
			buffer[dy+x*3+1]=g/aMax;
			buffer[dy+x*3+2]=r/aMax;
			}
		}
	}












