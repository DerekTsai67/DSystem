//



extern bool	reportBusy;
void	lockReport();

struct FormatOption{
	bool	leftJustify;
	bool	plusSign;
	bool	spaceSign;
	bool	header;
	bool	zeroPad;
	int		width;
	int		precision;
	int		length;
	int		base,prec;
	bool	cap;
	FormatOption(){
		leftJustify=plusSign=spaceSign=header=zeroPad=false;
		width=0;
		precision=-1;
		length=3;
		base=10;
		prec=0;
		cap=false;
		}
	};


long long int vgetInt(__VALIST va,int length)
	{
	switch(length)
		{
	case 1:
		//return va_arg(va,signed char);
	case 2:
		//return va_arg(va,short);
	case 3:
		return va_arg(va,int);
	case 4:
		return va_arg(va,long);
	case 8:
		return va_arg(va,long long);
	default:
		error("switchOut.");
		}
	return 0;
	}
	
unsigned long long int vgetUnsignedInt(__VALIST va,int length)
	{
	switch(length)
		{
	case 1:
		//return va_arg(va,unsigned char);
	case 2:
		//return va_arg(va,unsigned short);
	case 3:
		return va_arg(va,unsigned int);
	case 4:
		return va_arg(va,unsigned long);
	case 8:
		return va_arg(va,unsigned long long);
	default:
		error("switchOut.");
		}
	return 0;
	}



void	putNumberRec(Array<char> &format,long long int i,FormatOption &option,int prec)
	{
	if(i>=option.base)
		putNumberRec(format,i/option.base,option,prec-1);
	else
		{
		//for(int i=0;i<prec;i++)
		//	format.addItem('0');
		if(i==0)
			return;
		}
	i%=option.base;
	if(i<10)
		format.addItem('0'+i);
	else if(option.cap)
		format.addItem('A'+i-10);
	else
		format.addItem('a'+i-10);
	}

void	putNumber(Array<char> &format,long long int i,FormatOption &option)
	{
	//int wo=format.getNumber();
	
	if(i<0)
		{
		format.addItem('-');
		i=-i;
		}
	else if(option.plusSign)
		format.addItem('+');
	else if(option.spaceSign)
		format.addItem(' ');
	
	if(option.header)
		{
		format.addItem('0');
		switch(option.base)
			{
		case 2:
			format.addItem('b');
			break;
		case 8:
			format.addItem('o');
			break;
		case 10:
			format.addItem('d');
			break;
		case 16:
			format.addItem('x');
			break;
		default:
			format.addItem('?');
			break;
			}
		}
		
	int po=format.getNumber();
	
	putNumberRec(format,i,option,option.prec);
	
	int now=format.getNumber();
	if(now-po<option.precision)
		for(int i=option.precision-(now-po);i>0;i--)
			format.addItem('0',po);
	
	/*now=format.getNumber();
	if(now-wo<option.width)
		for(int i=option.width-(now-wo);i>0;i--)
			format.addItem(option.zeroPad?'0':' ',option.leftJustify?-1:wo);*/
	}

ConstString	vformat(ConstString str,__VALIST va)
	{
	ConstString specifier("duxXfFcsp%SCRb"); //Not supported: ioeEgGaAn
	if(str.search("%")==-1)
		return str;
	
	Array<char> format(str.getLength()+100);
	for(int i=0;i<str.getLength();)
		{
		if(str[i]!='%')
			{
			format.addItem(str[i]);
			i++;
			continue;
			}
		i++;
		ConstString flag=str+i;
		int si=flag.search(specifier);
		if(si==-1)
			{
			format.addItem('%');
			continue;
			}
		
		FormatOption option;
		int fi=0;
		while(true)
			{
			if(flag[fi]=='-')
				option.leftJustify=true;
			else if(flag[fi]=='+')
				option.plusSign=true;
			else if(flag[fi]==' ')
				option.spaceSign=true;
			else if(flag[fi]=='#')
				option.header=true;
			else if(flag[fi]=='0')
				option.zeroPad=true;
			else
				break;
			fi++;
			}
		if(flag[fi]=='*')
			{
			option.width=va_arg(va,int);
			fi++;
			}
		else
			while(flag[fi]>='0'&&flag[fi]<='9')
				{
				option.width=option.width*10+flag[fi]-'0';
				fi++;
				}
		if(flag[fi]=='.')
			{
			fi++;
			option.precision=0;
			if(flag[fi]=='*')
				{
				option.precision=va_arg(va,int);
				fi++;
				}
			else
				while(flag[fi]>='0'&&flag[fi]<='9')
					{
					option.precision=option.precision*10+flag[fi]-'0';
					fi++;
					}
			}
		if(flag[fi]=='h')
			{
			option.length=2;
			fi++;
			if(flag[fi]=='h')
				{
				option.length=1;
				fi++;
				}
			}
		else if(flag[fi]=='l')
			{
			option.length=4;
			fi++;
			if(flag[fi]=='l')
				{
				option.length=8;
				fi++;
				}
			}
		else if(flag[fi]=='L')
			{
			option.length=16;
			fi++;
			}

		
		if(option.precision==-1)
			{
			if(str[i+si]=='f'||str[i+si]=='F')
				option.precision=6;
			else if(str[i+si]=='s')
				option.precision=255;
			else
				option.precision=1;
			}
			
		int wo=format.getNumber();
		
		switch(str[i+si])
			{
		case 'd':
			{
			long long int i;
			if(option.length==8)
				i=va_arg(va,long long int);
			else
				i=va_arg(va,int);
			putNumber(format,i,option);
			}
			break;
		case 'u':
			putNumber(format,va_arg(va,unsigned int),option);
			break;
		case 'X':
			option.cap=true;
		case 'x':
			option.base=16;
			putNumber(format,va_arg(va,unsigned int),option);
			break;
		case 'F':
			option.cap=true;
		case 'f':
			{
			double	f=va_arg(va,double);
			if(f<0)
				{
				format.addItem('-');
				f=-f;
				}
			else if(option.plusSign)
				format.addItem('+');
			else if(option.spaceSign)
				format.addItem(' ');
			option.plusSign=option.spaceSign=false;
			
			long long int i=f;
			FormatOption option2=option;
			option2.header=false;
			option2.width=0;
			option2.precision=1;
			putNumber(format,i,option2);
			f-=i;

			format.addItem('.');
			
			f*=powInt(10,option.precision);
			i=f+0.5;
			option2.precision=option.precision;
			putNumber(format,i,option2);
			while(format[format.getNumber()-1]=='0')
				format.loseItemIndex(format.getNumber()-1);
			if(format[format.getNumber()-1]=='.')
				format.addItem('0');
			break;
			}
		case 'c':
			format.addItem(va_arg(va,int));
			break;
		case 's':
			{
			const char *s=va_arg(va,const char*);
			for(int i=0;s[i]!=0&&i<option.precision;i++)
				format.addItem(s[i]);
			break;
			}
		case 'p':
			option.cap=true;
			option.base=16;
			option.precision=8;
			putNumber(format,va_arg(va,unsigned int),option);
			break;
		case '%':
			format.addItem('%');
			break;
		case 'S':
			{
			ConstString s;
			if(str[i]=='+')
				s=va_arg(va,String);
			else
				s=va_arg(va,ConstString);
			for(int i=0;i<s.getLength();i++)
				format.addItem(s[i]);
			break;
			}
		case 'C':
			{
			Coord c=va_arg(va,Coord);
			format.addItem('(');
			putNumber(format,c.x,option);
			format.addItem(',');
			putNumber(format,c.y,option);
			format.addItem(')');
			break;
			}
		case 'R':
			{
			DS_RGB c=va_arg(va,DS_RGB);
			option.width=3;
			format.addItem('(');
			putNumber(format,c.r,option);
			format.addItem(',');
			putNumber(format,c.g,option);
			format.addItem(',');
			putNumber(format,c.b,option);
			format.addItem(',');
			putNumber(format,c.a,option);
			format.addItem(')');
			break;
			}
		case 'b':
			option.base=2;
			option.header=!option.header;
			putNumber(format,va_arg(va,int),option);
			break;
		default:
			error("switchOut %%%c",str[i+si]);
			}
		
		int now=format.getNumber();
		if(now-wo<option.width)
			for(int i=option.width-(now-wo);i>0;i--)
				format.addItem(option.zeroPad?'0':' ',option.leftJustify?-1:wo);
		i+=si+1;
		}
	
	String ret(format);
	ret.regisTemp();
	return ret;
	}
	
ConstString	format(ConstString str,...)
	{
	va_list va;
	va_start(va,str);
	ConstString ret=vformat(str,va);
	va_end(va);
	return ret;
	}




time_t laggingClock=0;
bool	lagging()
	{
	return false;
	static long long int lastTime=-1;
	static bool lag=false;
	if(lastTime!=ftime)
		{
		lastTime=ftime;
		laggingClock=clock();
		return false;
		}
	if(lag)
		return true;
	lag=clock()-laggingClock>=CLOCKS_PER_SEC*5;
	if(lag)
		printf("===Lagging===\n");
	return lag;
	}

void	vreport(ConstString prefix,ConstString str,__VALIST va)
	{
	lockReport();
	ThreadLock lock(3);
	
	prefix.print();
	bool endLine=true;
	str=vformat(str,va);
	if(str[str.getLength()-1]=='\\')
		{
		str-=1;
		endLine=false;
		}
	str.print();
	//vprintf(str,va);
	if(endLine)
		printf("\n");
	
	reportBusy=false;
	}

void	error_(ConstString func,ConstString str,...)
	{
	//lockReport();
	printf("error [%s]:  ",func.getRaw());
	va_list va;
	//printf("error [%s]:  ",func);
	va_start(va,str);
	//vreport(format("error [%S]:  ",func),str,va);
	vreport("",str,va);
	//vprintf(str,va);
	//printf("\n");
	//reportBusy=false;
	va_end(va);
	//_beep(1000,1000);
	getch();
	exit(1);
	}

/*
void	throwError(ConstString str,...)
	{
	VSTRING
	report("%s",str);
	throw str;
	}*/
void	warning(ConstString str,...)
	{
	//lockReport();
	va_list va;
	//printf("warning : ");
	va_start(va,str);
	vreport(format("warning [%s]:  ","func"),str,va);
	//vprintf(str,va);
	//printf("\n");
	//reportBusy=false;
	va_end(va);

	//_beep(1000,100);
	static const char *skip[100];
	static int sm=0;
	int i;
	for(i=0;i<sm;i++)
		if(str==skip[i])
			break;
	if(i==sm)
		switch(getch())
			{
		case '0':exit(0);
		case 's':
			if(sm>=100)
				error("warningSkip overflow.");
			skip[sm++]=str;
			}
	laggingClock=clock();
	}

int reportRank=0;
void	report(ConstString str,...)
	{
	if(lagging()||reportOff)
		return;
	//lockReport();
	//for(int i=0;i<reportRank;i++)
	//	printf("  ");
	String indent(reportRank*2);
	indent.setDelete();
	for(int i=0;i<indent.getLength();i++)
		indent[i]=' ';
	va_list va;
	va_start(va,str);
	vreport(indent,str,va);
	//vprintf(str,va);
	//printf("\n");
	//reportBusy=false;
	va_end(va);
	}
void	report(int d,ConstString str,...)
	{
	if(lagging()||reportOff)
		{
		reportRank+=d;
		return;
		}
	/*lockReport();
	for(int i=0;i<(d<0?reportRank+d:reportRank);i++)
		printf("  ");
	for(int i=0;i<d;i++)
		;//printf("  ");
	for(int i=0;i>d;i--)
		printf("  ");*/
	String indent(reportRank*2);
	indent.setDelete();
	for(int i=0;i<indent.getLength();i++)
		indent[i]=' ';
	reportRank+=d;
	va_list va;
	va_start(va,str);
	vreport(indent,str,va);
	//vprintf(str,va);
	//printf("\n");
	//reportBusy=false;
	va_end(va);
	}

void	lockReport()
	{
	if(!reportBusy)
		{
		reportBusy=true;
		return;
		}
	int startTime=clock();
	while(reportBusy)
		{
		if(clock()-startTime>3*CLOCKS_PER_SEC)
			return;
		sleep(1);
		}
	reportBusy=true;
	}














ConstString	viewData(ConstString str)
	{
	return viewData(str.getRaw(),str.getLength());
	}

ConstString	viewData(const void *data_,int size)
	{
	argCheck(data_==NULL);
	if(size==-1)
		size=getStringLength((const char*)data_);
	
	const Byte *data=(const Byte*)data_;
	const int retMax=1000;
	//char *ret=dallocTemp<char>(retMax+20);
	Array<char> format(100);
	//int ri=0;

	int wordCount=0;
	for(int i=0;i<size;i++)
		if(data[i]>=0x20&&data[i]<=0x7E)
			wordCount++;
			
	FormatOption option;
	option.base=16;
	option.cap=true;
	option.precision=2;

	if(size==0)
		{
		format.addItem('[');
		format.addItem(']');
		}
	else if(wordCount>=size*4/5)
		{
		format.addItem('\"');
		for(int i=0;i<size;i++)
			{
			switch(data[i])
				{
			case '\\':
				//format.addItem('\\');
				format.addItem('\\');
				break;
			case '\"':
				//format.addItem('\\');
				format.addItem('\"');
				break;
			case '\n':
				format.addItem('\\');
				format.addItem('n');
				break;
			case '\r':
				format.addItem('\\');
				format.addItem('r');
				break;
			case '\t':
				format.addItem('\\');
				format.addItem('t');
				break;
			default:
				if(data[i]>=0x20&&data[i]<=0x7E)
					format.addItem(data[i]);
				else
					{
					format.addItem('\\');
					putNumber(format,data[i],option);
					}
				break;
				}
			if(format.getNumber()>=retMax)
				{
				format.addItem('\\');
				format.addItem('.');
				format.addItem('.');
				format.addItem('.');
				break;
				}
			}
		format.addItem('\"');
		}
	else
		{
		format.addItem('[');
		for(int i=0;i<size;i++)
			{
			putNumber(format,data[i],option);
			format.addItem(' ');
			
			if(format.getNumber()>=retMax)
				{
				format.addItem('.');
				format.addItem('.');
				format.addItem('.');
				format.addItem(' ');
				break;
				}
			}
		format.loseItemIndex(format.getNumber()-1);
		format.addItem(']');
		}
	
	String ret(format);
	ret.regisTemp();
	return ret;
	}






