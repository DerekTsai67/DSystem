


void	initSocket();
void	destroySocket();

class IOputSocket;

typedef unsigned int SOCKET;


class ListenSocket{
	public:
	ListenSocket(int port);
	~ListenSocket();
	private:
SOCKET	s;
	
	public:
IOputSocket*	listen(bool block);
	};


class IOputSocket:public Input,public Output{
	public:
	IOputSocket(SOCKET s);
	~IOputSocket();
	private:
SOCKET	s;

	public:
int		receive();
	private:
int		errorNum;
	
	public://Input
int		getRaw(Byte *dest,int size);
bool	emptyRaw();

	public://Output
int		putRaw(const Byte *data,int size);
	};










