
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0501
#include <winsock2.h>
#include <ws2tcpip.h>

int socketOn=false;

void initSocket()
	{
	if(socketOn)
		return;
	WSADATA wsaData;
	int ret=WSAStartup(MAKEWORD(2,2),&wsaData);
	if(ret!=0)
		error("WSAStartup : %d",ret);
	}
	
void destroySocket()
	{
	if(!socketOn)
		return;
	WSACleanup();
	}



	ListenSocket::ListenSocket(int port)
	{
	initSocket();
	
	int ret;

	struct addrinfo hints={0},*result=NULL;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	char portString[10];
	sprintf(portString,"%d",port);
	ret=getaddrinfo(NULL,portString,&hints,&result);
	if(ret!=0)
		error("getaddrinfo : %d",ret);

	s=socket(result->ai_family,result->ai_socktype,result->ai_protocol);
	if(s==INVALID_SOCKET)
		error("socket() : %d",WSAGetLastError());

	ret=bind(s,result->ai_addr, (int)result->ai_addrlen);
	if(ret!=0)
		error("bind : %d",ret);
		//{cout<<"bind error:"<<ret<<endl;freeaddrinfo(result);closesocket(s);return 3;}

	freeaddrinfo(result);

	if(::listen(s,1)==SOCKET_ERROR)
		error("listen : %d",WSAGetLastError());

	unsigned long arg=1;
	ret=ioctlsocket(s,FIONBIO,&arg);
	if(ret!=NO_ERROR)
		error("ioctlsocket : %d",ret);
	}

	ListenSocket::~ListenSocket()
	{
	closesocket(s);
	}

IOputSocket*	ListenSocket::listen(bool block)
	{
	while(true)
		{
		SOCKET as=accept(s,NULL,NULL);
		if(as==INVALID_SOCKET&&WSAGetLastError()==WSAEWOULDBLOCK)
			{
			if(block)
				{
				sleep(2);
				continue;
				}
			else
				return NULL;
			}
		if(as==INVALID_SOCKET)
			error("accept : %d",WSAGetLastError());

		unsigned long arg=1;
		int ret=ioctlsocket(as,FIONBIO,&arg);
		//report("ioctl ed");
		if(ret!=NO_ERROR)
			error("ioctlsocket : %d",ret);
		return new IOputSocket(as);
		}
	}




	IOputSocket::IOputSocket(SOCKET as):Input(NULL,2000,false),Output(0,false)
	{
	s=as;
	errorNum=0;
	}

	IOputSocket::~IOputSocket()
	{
	closesocket(s);
	}

int		IOputSocket::receive()
	{
	if(errorNum<0)
		return errorNum;
	int ret=recv(s,(char*)Input::byteBuffer+Input::byteIndex,Input::byteMax-Input::byteIndex,0);
	if(ret==0)
		{
		errorNum=-1;
		return -1;
		}
	if(ret<0&&WSAGetLastError()==WSAEWOULDBLOCK)
		return 0;
	if(ret<0)
		{
		int errCode=WSAGetLastError();
		if(errCode==WSAEWOULDBLOCK)
			return 0;
		if(errCode!=WSAECONNRESET)
			report("WSA error : %d.",WSAGetLastError());
		errorNum=-2;
		return -2;
		}
	Input::byteIndex+=ret;
	return ret;
	}



int		IOputSocket::getRaw(Byte *dest,int size)
	{
	return 0;
	}

bool	IOputSocket::emptyRaw()
	{
	return true;
	}



int		IOputSocket::putRaw(const Byte *data,int size)
	{
	int errCount=0;
	while(send(s,(const char *)data,size,0)==SOCKET_ERROR)
		{
		int errCode=WSAGetLastError();
		if(errCode==WSAEWOULDBLOCK)
			{
			sleep(3);
			continue;
			}
		if(errCode==WSAECONNABORTED)
			errCount=5;
		else if(errCode!=WSAECONNRESET)
			report("Send failed. %d %p %3d %d",errCount,data,size,errCode);
		//if(errCode==WSAECONNRESET&&errCount>=3)
		//	break;
		errCount++;
		if(errCount>=5)
			{
			dead=true;
			return size;
			error("Dead");
			}
		sleep(10);
		}
	return size;
	}










