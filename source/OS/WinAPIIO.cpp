
#ifdef DS_WINAPI

#if defined(UNICODE) && !defined(_UNICODE)
	#define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
	#define UNICODE
#endif

//#include <tchar.h>
#include <windows.h>

HWND hwnd;
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);
unsigned char *buffer;
BITMAPINFO info;
DSBitmap *DSBuffer;
bool	painting;
HMIDIOUT midiHandle;

const int	mutexHandleMax=20;
HANDLE	*mutexHandle=NULL;
bool	mutexOn=false;

char className[100]="DSystem";
char titleName[100]="DSystem";

void	initIO()
	{
	WNDCLASSEX windowClass;
	
#ifdef UNICODE
	wchar_t *WclassName=dalloc<wchar_t>(110);
	wchar_t *WtitleName=dalloc<wchar_t>(110);
	mbstowcs(WclassName,className,110);
	mbstowcs(WtitleName,titleName,110);
#else
	char *WclassName=className;
	char *WtitleName=titleName;
#endif
	
	windowClass.hInstance=GetModuleHandle(NULL);
	windowClass.lpszClassName=WclassName;
	windowClass.lpfnWndProc=WindowProcedure;
	windowClass.style=CS_DBLCLKS;
	windowClass.cbSize=sizeof(WNDCLASSEX);
	windowClass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	windowClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	windowClass.hCursor=LoadCursor(NULL,IDC_ARROW);
	windowClass.lpszMenuName=NULL;
	windowClass.cbClsExtra=0;
	windowClass.cbWndExtra=0;
	windowClass.hbrBackground=(HBRUSH)COLOR_BACKGROUND;
	
	if(!RegisterClassEx(&windowClass))
		error("RegisterClassEx");
	
	/* The class is registered, let's create the program*/
	hwnd = CreateWindowEx (
		0,						/* Extended possibilites for variation */
		WclassName,				/* Classname */
		WtitleName,				/* Title Text */
		WS_OVERLAPPEDWINDOW,	/* default window */
		CW_USEDEFAULT,			/* Windows decides the position */
		CW_USEDEFAULT,			/* where the window ends up on the screen */
		window_size.x,			/* The programs width */
		window_size.y,			/* and height in pixels */
		HWND_DESKTOP,			/* The window is a child-window to desktop */
		NULL,					/* No menu */
		GetModuleHandle(NULL),	/* Program Instance handler */
		NULL					/* No Window Creation data */
		);
	
	
	/* Make the window visible on the screen */
	ShowWindow(hwnd,SW_SHOWDEFAULT);

	mutexHandle=dalloc<HANDLE>(mutexHandleMax);
	for(int i=0;i<mutexHandleMax;i++)
		mutexHandle[i]=CreateMutex(NULL,false,NULL);
	mutexOn=true;
	
	buffer=dalloc<Byte>(((window_size.x*3+3)&(~3))*window_size.y);
	memset(&info,0,sizeof(BITMAPINFO));
	info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	info.bmiHeader.biWidth = window_size.x;
	info.bmiHeader.biHeight = window_size.y;
	info.bmiHeader.biPlanes = 1;
	info.bmiHeader.biBitCount = 24;
	info.bmiHeader.biCompression = BI_RGB;
	//info.bmiHeader.biClrUsed = 256;
	DSBuffer=new DSBitmap(window_size);
	
	int ret=midiOutOpen(&midiHandle,0,0,0,CALLBACK_NULL);
	if(ret!=MMSYSERR_NOERROR)
		error("midiOutOpen failed. %d",ret);
	
	painting=false;
	
#ifdef UNICODE
	dfree(WclassName);
	dfree(WtitleName);
#endif
	}

void	destroyIO()
	{
	dfree(buffer);
	delete DSBuffer;
	midiOutClose(midiHandle);
	
	mutexOn=false;
	for(int i=0;i<mutexHandleMax;i++)
		CloseHandle(mutexHandle[i]);
	dfree(mutexHandle);
	}


void showUpdate()
	{
	//DSBuffer->toWinDIB(buffer,&info);
	painting=true;
	if(!RedrawWindow(hwnd,NULL,NULL,RDW_INVALIDATE|RDW_INTERNALPAINT))
		{
		if(GetLastError()!=1400)
			error("RedrawWindow.");
		}
	}






//========================================================================================================================================================


void	sleep(int time)
	{
	Sleep(time);
	}
	
unsigned long WINAPI threadLauncher(void *arg)
	{
	Thread *thread=(Thread*)arg;
	VoidPointerFunction func=thread->func;
	void *data=thread->data;
	for(int i=0;;i=(i+1)%2)
		{
		if(thread->status==Thread::Pause)
			{
			thread->status=Thread::Paused;
			SuspendThread(thread->OSHandle);
			i=0;
			}
		if(thread->status==Thread::Terminate)
			break;

		switch(i)
			{
		case 0:
			if(thread->status!=Thread::Paused)
				{
				func(data);
				thread->fpsCount++;
				if(thread->status==Thread::ProcessOnce)
					thread->status=Thread::Pause;
				}
			break;

		case 1:
			sleep(thread->delay);
			break;

		default:
			error("switchOut.");
			}
		}
	thread->status=Thread::Terminated;
	return 0;
	}

void	Thread::launchOS(bool pause)
	{
	OSHandle=CreateThread(NULL,0,threadLauncher,this,pause?CREATE_SUSPENDED:0,NULL);
	}
void	Thread::resumeOS()
	{
	if(ResumeThread(OSHandle)!=1)
		error("Not suspended.");
	}

	ThreadLock::ThreadLock(ConstString name):ThreadLock(9)
	{
	}
	ThreadLock::ThreadLock(int i)
	{
	if(!mutexOn)
		{
		index=-1;
		return;
		}
	
	index=i;
	index%=mutexHandleMax;
	while(index<0)
		index+=mutexHandleMax;
	WaitForSingleObject(mutexHandle[index],INFINITE);
	//WaitForSingleObject(mutexHandle[index],INFINITE);
	}
	ThreadLock::~ThreadLock()
	{
	if(index!=-1)
		ReleaseMutex(mutexHandle[index]);
	}
	
void	changeDirectory(ConstString path)
	{
	argCheck(path==NULL);
	int i=path.getLength()-1;
	while(path[i]!='/'&&path[i]!='\\'&&i>=0)i--;
	if(i<0)
		error("Not directory.");
	char *dir=dalloc<char>(i+2);
	memcpy(dir,path,i+1);
	dir[i+1]=0;

#ifdef UNICODE
	wchar_t *Wdir=dalloc<wchar_t>(i+10);
	mbstowcs(Wdir,dir,i+10);
	if(!SetCurrentDirectory(Wdir))
		error("Failed. %d",GetLastError());
	dfree(Wdir);
#else
	if(!SetCurrentDirectory(dir))
		error("Failed. %d",GetLastError());
#endif

	dfree(dir);
	}


Array<char*>*	getFileList(ConstString directory)
	{
	argCheck(directory==NULL);
	WIN32_FIND_DATAA data;
	Array<char*> *ret=new Array<char*>(20);

	int dm=directory.getLength();
	char *dir=dalloc<char>(dm+5);
	memcpy(dir,directory,dm);
	if(dir[dm-1]!='\\')
		dir[dm++]='\\';
	dir[dm++]='*';
	dir[dm]=0;

	HANDLE h=FindFirstFile(dir,&data);
	if(!verify(".",data.cFileName))
		error("Not .");
	FindNextFile(h,&data);
	if(!verify("..",data.cFileName))
		error("Not ..");

	
	while(FindNextFile(h,&data))
		{
		//report("file %s \t%08X",data.cFileName,data.dwFileAttributes);
		if(data.dwFileAttributes!=0x20)
			continue;

		int cm=getStringLength(data.cFileName);
		char *path=new char[dm+cm+5];
		memcpy(path,dir,dm);
		memcpy(path+dm-1,data.cFileName,cm);
		path[dm+cm-1]=0;
		ret->addItem(path);
		}
	dfree(dir);
	return ret;
	}



bool	isOSClipboardBitmapAvailable()
	{
	OpenClipboard(hwnd);
	int e=0;
	do	{
		e=EnumClipboardFormats(e);
		if(e==CF_DIB)
			return true;
		}
	while(e!=0);
	CloseClipboard();
	return false;
	}

DSBitmap*	getOSClipboardBitmap(int safeFail)
	{
	OpenClipboard(hwnd);
	HANDLE h=GetClipboardData(CF_DIB);
	if(h==NULL)
		{
		safeError("Unavailable.");
		return NULL;
		}
	//report("getBitmap %08X %S",h,viewData(h,500));
	if(*((long*)h)!=40)
		h=*((HANDLE*)h);
	Datafile *df=loadBMP(new InputArray((Byte*)h,-1,false),0,true);
	CloseClipboard();
	
	DSBitmap *bmp=df->getPic(-1)->getBitmap()->clone();
	delete df;
	return bmp;
	}
	
void	setOSClipboardBitmap(const DSBitmap *bmp)
	{
	argCheck(bmp==NULL);
	OpenClipboard(hwnd);
	EmptyClipboard();
	
	HGLOBAL hglbCopy;
	LPTSTR lptstrCopy;
	int size=bmp->getSize().area()*4+100;
	hglbCopy = GlobalAlloc(GMEM_MOVEABLE,size);
	lptstrCopy=(char*)GlobalLock(hglbCopy); 
	bmp->saveBMP(new OutputArray((Byte*)lptstrCopy,size),(new PictureOption())->setSkipHeader(true));
	GlobalUnlock(hglbCopy); 
	SetClipboardData(CF_DIB, hglbCopy); 
	
	CloseClipboard();
	}
	
void	setOSClipboardText(ConstString str)
	{
	OpenClipboard(hwnd);
	EmptyClipboard();
	
	HGLOBAL hglbCopy;
	LPTSTR lptstrCopy;
	hglbCopy = GlobalAlloc(GMEM_MOVEABLE,str.getLength()+1);
	lptstrCopy=(char*)GlobalLock(hglbCopy); 
	memcpy(lptstrCopy,str,str.getLength()); 
	lptstrCopy[str.getLength()]=0;
	GlobalUnlock(hglbCopy); 
	SetClipboardData(CF_TEXT, hglbCopy); 
	
	CloseClipboard();
	}









//========================================================================================================================================================

void	Sound::initResource()
	{
	//header2=GlobalLock(GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,sizeof(WAVEHDR)));
	header2=dalloc<WAVEHDR>(1);
	if(header2==NULL)
		error("GlobalLock failed.1");

	//buffer2=(char*)GlobalLock(GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,length*channel*byteRate));
	buffer2=dalloc<char>(length*channel*byteRate);
	if(buffer2==NULL)
		error("GlobalLock failed.2");
	}

void	Sound::deleteResource()
	{
	//GlobalFree(buffer2);
	//GlobalFree(header2);
	dfree(header2);
	dfree(buffer2);
	}


void	Sound::play()
	{
	int ret=-1;
	HWAVEOUT handle;
	WAVEFORMATEX waveFormat={WAVE_FORMAT_PCM,(WORD)channel,(DWORD)sampleRate,(DWORD)(sampleRate*byteRate),(WORD)(channel*byteRate),(WORD)(byteRate*8),(WORD)(length*channel*byteRate)};
	if(waveOutOpen(&handle,WAVE_MAPPER,&waveFormat,0,0,CALLBACK_NULL)!=MMSYSERR_NOERROR)
		error("waveOutOpen failed.");

	for(int l=0;l<length;l++)
	for(int c=0;c<channel;c++)
	for(int b=0;b<byteRate;b++)
		{
		buffer2[l*channel*byteRate+c*byteRate+b]=(data[c][l]>>(b*8))&0xFF;
		}


	//WAVEHDR hdr={buffer2,(DWORD)(length*channel*byteRate),0,0,WHDR_BEGINLOOP|WHDR_ENDLOOP,10,NULL,0};
	WAVEHDR *header3=(WAVEHDR*)header2;
	header3->lpData = buffer2;
    header3->dwBufferLength = (DWORD)(length*channel*byteRate);
    header3->dwBytesRecorded=0;
    header3->dwUser=0;
    header3->dwFlags = WHDR_BEGINLOOP|WHDR_ENDLOOP;
    header3->dwLoops = 3;
    header3->lpNext=NULL;
    header3->reserved=0;

	if((ret=waveOutPrepareHeader(handle,header3,sizeof(WAVEHDR)))!=MMSYSERR_NOERROR)
		error("waveOutPrepare failed. %d",ret);




	if((ret=waveOutWrite(handle,header3,sizeof(WAVEHDR)))!=MMSYSERR_NOERROR)
		error("waveOutWrite failed. %d",ret);


/*
	if(waveOutClose(handle)!=MMSYSERR_NOERROR)
		error("waveOutClose failed.");*/
	}









//========================================================================================================================================================


#define KS_MAX 256
#define MS_MAX 3

int *rawKeyState,*rawMouseState;
Coord rawCursorPos;

void	initInput()
	{
	rawKeyState=dalloc<int>(KS_MAX);
	rawMouseState=dalloc<int>(MS_MAX);
	rawCursorPos=zeroCoord;
	}
void	destroyInput()
	{
	dfree(rawKeyState);
	dfree(rawMouseState);
	}

int		keyTrans(int i);
int		keyTrans2(int i);

void	pullInput()
	{
	MSG message;
	while(PeekMessage(&message,NULL,0,0,PM_REMOVE))
		{
		TranslateMessage(&message);
		DispatchMessage(&message);
		}
	}

void	readKeys(Coord &cp,KeySignal &ks)
	{
	cp=rawCursorPos;
	ks.renew(rawMouseState,rawKeyState,keyTrans,keyTrans2);
	}


LRESULT CALLBACK WindowProcedure(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
	{
	//printf("%d : %d %d\n",message,wParam,(int)lParam);
	switch (message)
		{
		case WM_KEYDOWN:
			if(lParam&0x40000000)
				break;
			rawKeyState[wParam]=1;
			break;
		case WM_KEYUP:
			rawKeyState[wParam]=0;
			break;
			
		case WM_LBUTTONDOWN:
		case WM_LBUTTONDBLCLK:
			rawMouseState[MOUSE_LEFT]=1;
			break;
		case WM_LBUTTONUP:
			rawMouseState[MOUSE_LEFT]=0;
			break;
		case WM_RBUTTONDOWN:
		case WM_RBUTTONDBLCLK:
			rawMouseState[MOUSE_RIGHT]=1;
			break;
		case WM_RBUTTONUP:
			rawMouseState[MOUSE_RIGHT]=0;
			break;
		case WM_MBUTTONDOWN:
		case WM_MBUTTONDBLCLK:
			rawMouseState[MOUSE_MIDDLE]=1;
			break;
		case WM_MBUTTONUP:
			rawMouseState[MOUSE_MIDDLE]=0;
			break;
			
		case WM_MOUSEMOVE:
			rawCursorPos.x=lParam&0xFFFF;
			rawCursorPos.y=lParam>>16;
			if(wParam& 1)
				rawMouseState[MOUSE_LEFT]=1;
			else
				rawMouseState[MOUSE_LEFT]=0;
			if(wParam& 2)
				rawMouseState[MOUSE_RIGHT]=1;
			else
				rawMouseState[MOUSE_RIGHT]=0;
			if(wParam&16)
				rawMouseState[MOUSE_MIDDLE]=1;
			else
				rawMouseState[MOUSE_MIDDLE]=0;
			{
			int qi=cursorTrackQueueIndex;
			if(qi!=-1)
				{
				if(cursorTrackIndex[qi]<cursorTrackMax)
					cursorTrack[qi][cursorTrackIndex[qi]++]=rawCursorPos;
				}
			}
			break;
			
		case WM_PAINT:
			{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hwnd,&ps);
			if(hdc==NULL)
				error("BeginPaint");
			StretchDIBits(hdc,0,0,window_size.x,window_size.y,0,0,window_size.x,window_size.y, buffer, &info, DIB_RGB_COLORS, SRCCOPY);
			
	/*SHFILEINFOA shInfo;
	SHGetFileInfoA("E:\\1.ico",0,&shInfo,sizeof(SHFILEINFOA),SHGFI_ICON|SHGFI_SHELLICONSIZE);
	
	DrawIcon(hdc, 10, 20, shInfo.hIcon); 
	if(!DestroyIcon(shInfo.hIcon))
		error("!Destroy Icon.");*/
		
			painting=false;
			EndPaint(hwnd,&ps);
			}
			break;
		case WM_DESTROY:
			{
			extern bool exiting;
			exiting=true;
			//PostQuitMessage(0);
			}
			break;
		default:
			//printf("%d : %d %d\n",message,wParam,(int)lParam);
		case WM_NCHITTEST://132
		case WM_SETCURSOR://32
		case WM_NCMOUSEMOVE://160
			return DefWindowProc (hwnd, message, wParam, lParam);
		}
	
	return 0;
	}


int keyTrans(int i)
	{
	if(i>=KEY_A&&i<=KEY_Z)
		return 65+i-KEY_A;
	if(i>=KEY_N0&&i<=KEY_N9)
		return VK_NUMPAD0+i-KEY_N0;
	if(i>=KEY_F1&&i<=KEY_F12)
		return VK_F1+i-KEY_F1;
	if(i>=KEY_U1&&i<=KEY_U9)
		return 49+i-KEY_U1;
	switch(i)
		{
		case KEY_U0:		return 48;
		case KEY__ :		return 189;

		case KEY_COMMA:		return 188;
		case KEY_DOT:		return 190;
		case KEY_SLASH:		return 191;
		case KEY_COLON:		return 186;
		case KEY_QUOTE:		return 222;
		case KEY_LBRACKET:	return 219;
		case KEY_RBRACKET:	return 221;
		case KEY_TILDE:		return 192;
		case KEY_EQUAL:		return 187;
		case KEY_BACKSLASH:	return 220;

		case KEY_UP:		return VK_UP;
		case KEY_DOWN:		return VK_DOWN;
		case KEY_RIGHT:		return VK_RIGHT;
		case KEY_LEFT:		return VK_LEFT;

		case KEY_BACK:		return VK_BACK;
		case KEY_ENTER:		return VK_RETURN;
		case KEY_SPACE:		return VK_SPACE;
		case KEY_ESC:		return VK_ESCAPE;
		case KEY_TAB:		return VK_TAB;
		case KEY_CAPS:		return VK_CAPITAL;

		case KEY_DELETE:	return VK_DELETE;
		case KEY_INSERT:	return VK_INSERT;
		case KEY_HOME:		return VK_HOME;
		case KEY_END:		return VK_END;
		case KEY_PUP:		return VK_PRIOR;
		case KEY_PDOWN:		return VK_NEXT;

		case KEY_PLUS:		return VK_ADD;
		case KEY_MINUS:		return VK_SUBTRACT;
		case KEY_TIMES:		return VK_MULTIPLY;
		case KEY_DEVIDE:	return VK_DIVIDE;
		case KEY_NDOT:		return VK_DECIMAL;
		case KEY_NUMLOCK:	return VK_NUMLOCK;
		case KEY_PAUSE:		return VK_PAUSE;
		case KEY_SCROLL:	return VK_SCROLL;
		}
	error("undefined key: %d.",i);
	return 0;
	}

int keyTrans2(int i)
	{
	switch(0x0100<<i)
		{
		case KEY_CTRL:	return VK_CONTROL;
		case KEY_ALT:	return VK_MENU;
		case KEY_SHIFT:	return VK_SHIFT;
		}
	error("undefined key2: %d.",i);
	return 0;
	}

#endif


//============================================================================================




void midiTest()
	{
	HMIDIOUT handle;
	int ret=midiOutOpen(&handle,0,0,0,CALLBACK_NULL);
	if(ret!=MMSYSERR_NOERROR)
		error("midi 1 %d",ret);

	midiOutShortMsg(handle,0x000052C0);
	for(int i=0;i<7;i++)
		{
		midiOutShortMsg(handle,0x007F0090|((i+0x3F)<<8));
		sleep(200);
		}
		sleep(2000);
	midiOutClose(handle);
	}



void	putMidiMessage(int s,int a,int b)
	{
	argCheck(((s&(~0x7F))!=0x80)||((a&(~0x7F))!=0)||((b&(~0x7F))!=0));
	midiOutShortMsg(midiHandle,s|(a<<8)|(b<<16));
	}



