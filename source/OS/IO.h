

#ifdef DS_ALLEGRO
struct ALLEGRO_BITMAP;
#define Buffer ALLEGRO_BITMAP
#endif

void	initIO();
void	destroyIO();

void	initShow();
void	destroyShow();

void	showBG();
void	showUpdate();


void	sleep(int delay);
//void	Thread::launchOS();
//void	Thread::resumeOS();

void	changeDirectory(ConstString path);
Array<char*>*	getFileList(ConstString directory);

bool		isOSClipboardBitmapAvailable();
DSBitmap*	getOSClipboardBitmap(int safeFail=0);
void		setOSClipboardBitmap(const DSBitmap *bmp);
void		setOSClipboardText(ConstString str);

void	initInput();
void	destroyInput();

void	pullInput();
void	readKeys(Coord &cp,KeySignal &ks);
/*
bool	keyPress(int x);
bool	keyDown(int x);
bool	keyUp(int x);
void	makeDown(int x);
void	keyCut(int x);

bool	mousePress(int x);
bool	mouseDown(int x);
bool	mouseUp(int x);*/




void	midiTest();
void	putMidiMessage(int statusByte,int a,int b);


