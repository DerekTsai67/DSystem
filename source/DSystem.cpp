

Coord	window_size(1024,576);
DS_RGB	BGC(49,23,243);

Window	*head,*base,*maxmized;
Object	*nowOwner;
Object	*nowObject,*focusObject;
Datafile	*DSData,*buttonData,*choosedButtonData,*hollowData;
KeySignal keySignal;
Font	*font,*smallFont;
long long int	ftime;
int 	delayTime;
Coord	dCursor,cursorPos,lastCursorPos,maxmizedSize;
Coord	*cursorTrack[cursorTrackQueueMax];
int		cursorTrackQueueIndex,cursorTrackIndex[cursorTrackQueueMax];
bool	reportOff=false;
Sense	*nonSense,*nowSense,*sense;
React	*nonReact,*react;
int 	senseRet;
void	nonFunction(){}

LinkedList<Object>		*trashObject;
LinkedList<ObjectState>	*trashObjectState;
bool	trashSignatureShow;

int Sense::count;
int Object::count;
int DSBitmap::count;
int MonoBitmap::count;
int InputFile::count;
int OutputFile::count;
int dallocCount;
Array<TempAllocInfo>	*tempAlloc;



bool	reportBusy;
void	lockReport();


void mainProcess();
void showProcess();
Thread	*mainThread,*showThread;
bool showSense;


void*	operator new(unsigned int size)
	{
	//static int count=0;
	void *ret=dalloc<Byte>(size);
	//report("[%3d]%08X -%d",count++,ret,size);
	return ret;
	}

void	operator delete(void *data)
	{
	//static int count=0;
	//report("[%3d]%08X -delete",count++,data);
	dfree(data);
	}

void initDSystem(ConstString path)
	{
	reportBusy=false;
	Sense::count=0;
	Object::count=0;
	DSBitmap::count=0;
	MonoBitmap::count=0;
	InputFile::count=0;
	OutputFile::count=0;
	dallocCount=0;
	tempAlloc=new Array<TempAllocInfo>(10);
	ftime=0;
	report("\n  ----Initing DSystem----");
	if(path!=NULL)
		changeDirectory(path);
	
	//initMath();
	//initIO();
	//initShow();
	//initSocket();
	setShowDest(NULL);

	DSData=new Datafile("datafile\\DSBase.dsd");
	font=new Font("datafile\\fontHalf.dsd");
	font->addDatafile(new Datafile("datafile\\fontFull1.dsd"));
	font->addDatafile(new Datafile("datafile\\fontFull2.dsd"));
	font->addDatafile(new Datafile("datafile\\fontFull3.dsd"));
	font->addDatafile(new Datafile("datafile\\fontFull4.dsd"));
	smallFont=new Font("datafile\\smallFont.dsd");
	buttonData=new Datafile("datafile\\Button.dsd");
	choosedButtonData=new Datafile("datafile\\ChoosedButton.dsd");
	hollowData=new Datafile("datafile\\Hollow.dsd");
	
	head=new TopWindow();
	trashObject=new LinkedList<Object>(false);
	trashObjectState=new LinkedList<ObjectState>(false);
	trashSignatureShow=false;
	nonSense=new NonSense();
	nonReact=new React();
	dCursor=lastCursorPos=zeroCoord;
	focusObject=NULL;
	base=maxmized=NULL;
	delayTime=10;

 	addThread(mainThread=new Thread(mainProcess,true));
 	addThread(showThread=new Thread(showProcess,true));

	report("  ----DSystem Initted----\n");
	}

inline void debugMessage(ConstString str)
	{
	//report(str);
	}


time_t lastClock=clock();
long long int	lastTime=ftime;

bool exiting=false;

void DSystem()
	{
	report("\n  ==== DSystem Start ====\n");
	
	try{
	initInput();
	initIO();
	initShow();
	
	sense=nonSense;
	react=nonReact;
	senseRet=0;
	lastClock=clock();
	lastTime=ftime;
	
	for(int i=0;i<cursorTrackQueueMax;i++)
		{
		cursorTrack[i]=dalloc<Coord>(cursorTrackMax+5);
		cursorTrackIndex[i]=0;
		}
	cursorTrackQueueIndex=-1;

	showThread->launch(true);
	mainThread->setDelay(delayTime);
	mainThread->launch();

	while(!exiting)
		{
		pullInput();
		if(delayTime!=0&&cursorTrackQueueIndex==-1)
			sleep(delayTime);
		}

	mainThread->terminate(true);
	showThread->terminate(true);
	FOR_LL(now,thread,Thread)
		now->terminate();
	END_FOR_LL
	thread.clear();
	report("End of main loop.\n");

	for(int i=0;i<cursorTrackQueueMax;i++)
		dfree(cursorTrack[i]);
	
	destroyShow();
	destroyIO();
	destroyInput();
	}//try
	catch(exception &e)
		{
		error(e.what());
		}
	catch(...)
		{
		error("Unknown exception catched.");
		}
	}



void	endCheck()
	{
	thread.clear();
	
	delete nonSense;
	delete nonReact;
	delete trashObjectState;
	delete trashObject;
	head->recDel();
	delete head;
	
	delete DSData;
	delete font;
	delete smallFont;
	delete buttonData;
	delete choosedButtonData;
	delete hollowData;
	
	destroySocket();
	
	if(Object::count!=0)
		error("Object last : %d",Object::count);
	if(Sense::count!=0)
		error("Sense last : %d",Sense::count);
	if(DSBitmap::count!=0)
		error("Bitmap last : %d",DSBitmap::count);
	if(MonoBitmap::count!=0)
		error("MonoBitmap last : %d",MonoBitmap::count);
	if(InputFile::count!=0)
		error("InputFile last : %d",InputFile::count);
	if(OutputFile::count!=0)
		error("OutputFile last : %d",OutputFile::count);
	
	FOR_ARRAY(i,*tempAlloc)
		dfree(tempAlloc->getItem(i).data);
	delete tempAlloc;
	
	if(dallocCount!=0)
		{
		printf("dalloc last : %d\n",dallocCount);
		getch();
		exit(1);
		}

	printf("End of DSystem.\n\n");
	}

void	mainProcess()
	{
	debugMessage("MainLoop start.");
	Window *now;

	readKeys(cursorPos,keySignal);
	dCursor=cursorPos-lastCursorPos;
	if(keySignal.press(KEY_CTRL|KEY_X)
	 ||keySignal.press(KEY_ALT|KEY_F4)
	 ||keySignal.press(KEY_ESC))
		{
		exiting=true;
		return;
		}
	if(keySignal.down(KEY_F12))
		screenShot();
	if(keySignal.press(KEY_F8))
		showSense=true;
	else
		showSense=false;
	if(keySignal.press(KEY_F5))
		report("focus:%s",focusObject==NULL?"NULL":typeid(*focusObject).name());

	debugMessage("MainLoop input.");
	Object *nowObject=focusObject;
	while(nowObject!=NULL)
		{
		if(nowObject==base)
			break;
		nowObject->input();
		keySignal.reduceFocus();
		if(nowObject->owner==nowObject)
			break;
		nowObject=nowObject->owner;
		}
	if(base!=NULL)
		{
		base->input();
		keySignal.reduceFocus();
		}

	debugMessage("MainLoop sensing.");
	now=head;
	if(!(senseRet&SENSOR_hold))
		{
		while(now!=NULL)
			{
			nowSense=now->sense_(cursorPos);
			if(nowSense!=NULL)
				break;
			now=now->getNext();
			}

		debugMessage("MainLoop sense refresh.");
		if(nowSense==NULL)
			nowSense=nonSense;
		if(sense==NULL)
			error("sense==NULL");

		if(sense!=nowSense)
			{
			sense->pointed(cursorPos,false);
			sense=nowSense;
			sense->pointed(cursorPos,true);
			}
		if(react!=nonReact)
			{
			react->pointed(cursorPos,false);
			react=nonReact;
			}
		}
	else
		{
		React *nowReact=NULL;
		while(now!=NULL)
			{
			nowReact=now->react_(cursorPos,sense);
			if(nowReact!=NULL)
				break;
			now=now->getNext();
			}

		if(nowReact==NULL)
			nowReact=nonReact;

		if(react!=nowReact)
			{
			react->pointed(cursorPos,false);
			react=nowReact;
			react->setSense(sense);
			react->pointed(cursorPos,true);
			}
		react->press(cursorPos);
		}
	if(sense==NULL)
		error("sense==NULL");

	debugMessage("MainLoop sense process.");
	sense->show();
	senseRet=sense->process(cursorPos,keySignal);

	debugMessage("MainLoop act.");
	now=head;
	while(now!=NULL)
		{
		now->act_();
		now=now->getNext();
		}

	debugMessage("MainLoop check.");
	now=head;
	while(now!=NULL)
		{
		now->check(cursorPos-now->pos);
		/*if(now->lifetime==0)
			{
			now->deleteType(OBJECT_visible);
			now->deleteType(OBJECT_sensable);
			now->deleteType(OBJECT_actable);
			}
		if(now->lifetime>LIFETIME_LIVING)
			now->lifetime--;
		if(now->lifetime==LIFETIME_DELETE)
			{
			Window *now_=now->getNext();
			deleteWindow(now);
			delete now;
			now=now_;
			continue;
			}*/
		if(now->killState==Object::killing)
			{
			extern LinkedList<Object>	*trashObject;
			extern bool	trashSignatureShow;
			deleteWindow(now);
			trashObject->addItem(now);
			trashSignatureShow=false;
			}
		now=now->getNext();
		}
	
	{
	ThreadLock lock(2);
	FOR_ARRAY(i,*tempAlloc)
		{
		TempAllocInfo info=tempAlloc->getItem(i);
		if(clock()-info.setTime>CLOCKS_PER_SEC)
			{
			dfree(info.data);
			tempAlloc->loseItemIndex(i);
			i--;
			}
		}
	//tempAlloc->clear();
	}
	
	extern bool painting;
	if(showThread->isPaused()&&!painting)
		{
		head->showRec(true);
		showThread->once();
		}

	debugMessage("MainLoop timeCheck.");
	while(clock()-lastClock>=CLOCKS_PER_SEC)
		{
		int sec=(clock()-lastClock)/CLOCKS_PER_SEC;
		if(sec>=2)
			{
			report("Paused %d sec.",sec-1);
			lastClock+=CLOCKS_PER_SEC*(sec-1);
			}
		if(!reportOff)
			{
			lockReport();
			printf("fps");
			FOR_LL(now,thread,Thread)
				printf(" %3d",now->getFps());
			END_FOR_LL
			printf(" ; cursor %d,%d %d\n",cursorPos.x,cursorPos.y,dallocCount);
			reportBusy=false;
			}
		lastClock+=CLOCKS_PER_SEC;
		}
	if(trashSignatureShow)
		{
		trashObject->clear();
		trashObjectState->clear();
		}
	ftime++;
	lastCursorPos=cursorPos;
	debugMessage("MainLoop end.");
	}





bool	addWindow(Window *n)
	{
	if(n==NULL)
		error("Add NULL Window.");
	report("Add Window %d as %s",n->getID(),typeid(*n).name());
	n->next=head->next;
	head->next=n;
	nowOwner=NULL;
	return true;
	}

void	deleteWindow(Window *n)
	{
	if(n==NULL)
		error("Delete NULL Window.");
	Window *now=head;
	while(now->next!=n)now=now->next;
	now->next=n->next;
	//n->next=NULL;
	//delete n;
	}

void	refreshWindow(Window *w)
	{
	if(w==NULL)
		return;
	showThread->waitOnce();
	nowOwner=w;
	w->refresh();
	}

void	refreshWindow()
	{
	showThread->waitOnce();
	Window *now=head;
	while(now!=NULL)
		{
		nowOwner=now;
		now->refresh();
		now->reShow();
		now=now->getNext();
		}
	fullRender();
	}

void	maxmizeWindow(Window *w)
	{
	if(maxmized!=NULL)
		{
		//maxmized->reSize(maxmizedSize);
		maxmized->addType(OBJECT_movable);
		}
	maxmized=w;
	if(maxmized!=NULL)
		{
		maxmizedSize=maxmized->getSize();
		//maxmized->reSize(window_size);
		maxmized->move(-maxmized->getPosition());
		maxmized->deleteType(OBJECT_movable);
		}
	}

Position	autoLocate(Position p0,Coord dp,int bn,Coord dp2)
	{
	if(dp==zeroCoord)
		error("autoLocate with dp=0.");
	if(dp2==zeroCoord)
		dp2=dp*bn;
	bool *mask=dalloc<bool>(bn);

	Window *now=head;
	while(now!=NULL)
		{
		int n=(dp.x!=0?(now->getPosition()-p0).x/dp.x:(now->getPosition()-p0).y/dp.y);
		if(now->getPosition()==p0+dp*n)
			if(n>=0&&n<bn)
				mask[n]=true;
		now=now->getNext();
		}
	for(int i=0;i<bn;i++)
		if(!mask[i])
			{
			dfree(mask);
			return p0+dp*i;
			}
	dfree(mask);
	return autoLocate(Position(p0+dp2),dp,bn,dp2);
	}


void screenShot()
	{
	char path[50];
	int i;
	for(i=0;i<1000;i++)
		{
		sprintf(path,".\\ScreenShot\\ScreenShot_%d.png",i);
		FILE *ft=fopen(path,"rb");
		if(ft==NULL)
			break;
		fclose(ft);
		}
	if(i==0)
		system("mkdir .\\ScreenShot");
	showThread->waitOnce();
	DSBitmap *bmp=DSBuffer->clone();
	bmp->savePNG(new OutputFile(path));
	delete bmp;
	}



int readhex(ConstString str)
	{
	int ret=0,i=0;
	for(i=0;i<str.getLength();i++)
		{
		if(str[i]>='0'&&str[i]<='9')
			ret=ret*16+str[i]-'0';
		else if(str[i]>='A'&&str[i]<='F')
			ret=ret*16+str[i]-'A'+10;
		else if(str[i]>='a'&&str[i]<='f')
			ret=ret*16+str[i]-'a'+10;
		else
			break;
		}
	return ret;
	}

int readnum(ConstString str)
	{
	int ret=0,i=0;
	for(i=0;str[i]>='0'&&str[i]<='9'&&i<str.getLength();i++)
		{
		ret=ret*10+str[i]-'0';
		}
	return ret;
	}

void writehex(char *str,unsigned int num,int max,char c)
	{
	int i=0;
	for(i=max-1;i>=0;i--)
		if(num==0&&i!=max-1)
			str[i]=c;
		else
			{
			str[i]=(num%16)+'0';
			if(str[i]>'9')
				str[i]=str[i]-'0'-10+'A';
			num/=16;
			}
	}

void writenum(char *str,unsigned int num,int max,char c)
	{
	int i=0;
	for(i=max-1;i>=0;i--)
		if(num==0&&i!=max-1)
			str[i]=c;
		else
			{
			str[i]=(num%10)+'0';
			num/=10;
			}
	}




int		unpackYMDHMS(int y,int m,int d,int h,int m2,int s)
	{
	return unpackYMD(y,m,d)+unpackHMS(h,m2,s);
	}

int		unpackYMD(int y,int m,int d)
	{
	argCheck(y<1970||y>=2038||m<1||m>12||d<1||d>31);
	y-=1970;
	m--;
	d--;
	int md[]={31,28,31,30,31,30,31,31,30,31,30,31};
	d+=y*365;
	d+=(y+1)/4;
	if((y+2)%4==0)
		md[1]++;
	for(int i=0;i<m;i++)
		d+=md[i];
	return d*24*60*60;
	}

int		unpackHMS(int h,int m,int s)
	{
	argCheck(h<0||h>=24||m<0||m>=60||s<0||s>=60);
	m+=h*60;
	s+=m*60;
	return s;
	}







