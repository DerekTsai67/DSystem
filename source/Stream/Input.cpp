
using namespace dlib;






	Input::Input(Input *i,int max,bool close)
	{
	in=i;
	type=0;
	byteMaxOrigin=byteMaxRequired=byteMax=max+5;
	if(byteMax!=0)
		byteBufferOrigin=byteBuffer=dalloc<Byte>(byteMax);
	else
		byteBufferOrigin=byteBuffer=NULL;
	byteIndex=0;
	remainSize=0;
	bitBuffer=0;
	bitIndex=0;
	closeSource=close;
	safeFail=0;
	position=0;
	originInPosition=0;
	if(in!=NULL)
		originInPosition=in->getPosition();
	}

	Input::~Input()
	{
	if(byteBuffer!=NULL)
		{
		if(byteIndex!=0)
			safeError("Input::byteBuffer last. %S",viewData(byteBuffer,byteIndex));
		if(bitIndex!=0)
			safeError("Input::bitBuffer last. %d %04X",bitIndex,(int)bitBuffer);
		dfree(byteBufferOrigin);
		}
	if(closeSource&&in!=NULL)
		delete in;
	}

void	Input::setType(int t)
	{
	type|=t;
	}

void	Input::setRemainSize(int s)
	{
	argCheck(s<=0||byteMax==0);
	remainSize=s;
	byteMaxOrigin=byteMax=remainSize*2+byteMaxRequired;
	Byte *newBuffer=dalloc<Byte>(remainSize+byteMaxOrigin);
	memcpy(newBuffer+remainSize,byteBuffer,byteIndex);
	dfree(byteBuffer);
	byteBufferOrigin=newBuffer;
	byteBuffer=byteBufferOrigin+remainSize;
	}

void	Input::setSafeFail(int s)
	{
	safeFail=s;
	}

int 	Input::getSafeFail()
	{
	return safeFail;
	}

void	Input::setPosition(int pos)
	{
	error("Unavailable to setPosition.");
	}

long long	Input::getPosition()const
	{
	return position;
	}

long long	Input::getInSize()const
	{
	argCheck(in==NULL);
	return in->getPosition()-originInPosition;
	}


void	Input::dump(Output *out)
	{
	::dump(new InputEmptyMask(this),out);
	}




int 	Input::getInt_(int size,int endianSetting)
	{
	argCheck(size<0||size>4);
	int temp=0;
	getBytes(&temp,size);

	if((endianSetting&bigEndian)&&size>1)
		{
		Byte temp2[8];
		memcpy(temp2,&temp,size);
		for(int j=0;j<size;j++)
			memcpy((Byte*)&temp+j,temp2+size-1-j,1);
		}
	return temp;
	}

unsigned int	Input::getUnsignedInt_(int size,int endianSetting)
	{
	argCheck(size<0||size>4);
	unsigned int temp=0;
	getBytes(&temp,size);

	if((endianSetting&bigEndian)&&size>1)
		{
		Byte temp2[8];
		memcpy(temp2,&temp,size);
		for(int j=0;j<size;j++)
			memcpy((Byte*)&temp+j,temp2+size-1-j,1);
		}
	return temp;
	}

Coord	Input::getCoord_(int size,int endianSetting)
	{
	argCheck(size<1||size>4);
	int x=getInt(size);
	int y=getInt(size);
	return Coord(x,y);
	}

DS_RGB	Input::getRGB()
	{
	int r=getInt(1);
	int g=getInt(1);
	int b=getInt(1);
	return DS_RGB(r,g,b);
	}

DS_RGB	Input::getBGR()
	{
	int b=getInt(1);
	int g=getInt(1);
	int r=getInt(1);
	return DS_RGB(r,g,b);
	}

double	Input::getDouble()
	{
	double ret=0.0;
	getBytes(&ret,8);
	return ret;
	}
	
void	Input::checkBits()
	{
	if(bitIndex!=0)
		safeError("bits last.");
	bitBuffer=0;
	bitIndex=0;
	}
void	Input::alignBits_(int endianSetting)
	{
	if(byteMax-byteIndex<bitIndex/8)
		error("bitPeek return overflow. %d %d %d",byteMax,byteIndex,bitIndex);
	if(endianSetting&dlib::bitLeft)
		{
		bitBuffer<<=bitIndex&7;
		while(bitIndex>=8)
			{
			memcpy(byteBuffer+byteIndex,(Byte*)(&bitBuffer)+7,1);
			byteIndex++;bitIndex-=8;
			}
		}
	else
		{
		bitBuffer>>=bitIndex&7;
		memcpy(byteBuffer+byteIndex,&bitBuffer,bitIndex/8);
		byteIndex+=bitIndex/8;
		}
	bitBuffer=0;
	bitIndex=0;
	}

unsigned int	Input::getBits_(int num,int endianSetting)
	{
	if(num<0||num>=24)
		error("Unsupported getBits num %d",num);

	if(endianSetting&dlib::bitLeft)
		{
		while(bitIndex<num)
			{
			bitBuffer|=(unsigned long long)getUnsignedInt(1)<<(56-8-bitIndex);
			bitIndex+=8;
			}

		unsigned long long ret=bitBuffer&((((unsigned long long)1<<num)-1)<<(56-num));
		bitBuffer<<=num;
		bitIndex-=num;
		return ret>>(56-num);
		}
	else
		{
		while(bitIndex<num)
			{
			if(bitIndex&0x7)
				bitBuffer|=getUnsignedInt(1)<<bitIndex;
			else
				getBytes(((Byte*)&bitBuffer)+(bitIndex>>3),1);
			bitIndex+=8;
			}

		unsigned int ret=bitBuffer&((1<<num)-1);
		bitBuffer>>=num;
		bitIndex-=num;
		return ret;
		}
	}

unsigned int	Input::getPeekBits_(int num,int endianSetting)
	{
	if(num<0||num>=24)
		error("Unsupported getPeekBits num %d",num);

	if(endianSetting&dlib::bitLeft)
		{
		while(bitIndex<num)
			{
			unsigned long long next=0;
			if(getBytes(&next,1,true)==0)
				break;
			bitBuffer|=next<<(56-8-bitIndex);
			bitIndex+=8;
			}
		unsigned long long ret=bitBuffer&((((unsigned long long)1<<num)-1)<<(56-num));
		return ret>>(56-num);
		}
	else
		{
		while(bitIndex<num)
			{
			if(bitIndex&0x7)
				{
				unsigned long long next=0;
				if(getBytes(&next,1,true)==0)
					break;
				bitBuffer|=next<<bitIndex;
				}
			else
				if(getBytes(((Byte*)&bitBuffer)+(bitIndex>>3),1,true)==0)
					break;
			bitIndex+=8;
			}
		unsigned int ret=bitBuffer&((1<<num)-1);
		return ret;
		}
	}

void	Input::getIgnoreBits_(int num,int endianSetting)
	{
	if(num<0||num>=24)
		error("Unsupported getIgnoreBits num %d",num);

	if(endianSetting&dlib::bitLeft)
		{
		while(bitIndex<num)
			{
			bitBuffer|=(unsigned long long)getUnsignedInt(1)<<(56-8-bitIndex);
			bitIndex+=8;
			}
		bitBuffer<<=num;
		bitIndex-=num;
		}
	else
		{
		while(bitIndex<num)
			{
			if(bitIndex&0x7)
				bitBuffer|=getUnsignedInt(1)<<bitIndex;
			else
				getBytes(((Byte*)&bitBuffer)+(bitIndex>>3),1);
			bitIndex+=8;
			}
		bitBuffer>>=num;
		bitIndex-=num;
		}
	}

int 	Input::getFixedString(String &dest,int num)
	{
	if(num==-1)
		num=dest.getLength();
	if(num>dest.getLength()||num<0)
		error("String overflow.");
	return getBytes(dest.getRaw(),num);
	}
int 	Input::getString(String &dest,bool localSafeFail)
	{
	argCheck(dest==NULL);

	int i;
	for(i=0;i<dest.getLength();i++)
		{
		getBytes(&dest[i],1);
		if(dest[i]==0)
			break;
		}
	if(i==dest.getLength()&&!localSafeFail)
		safeFatalError("getString overflow.");
	dest.cut(i);
	return i;
	}

int 	Input::getString(char *dest,int size,bool localSafeFail)
	{
	argCheck(dest==NULL||size<=0);

	int i;
	for(i=0;i<size;i++)
		{
		getBytes(dest+i,1);
		if(dest[i]==0)
			break;
		}
	if(i==size&&!localSafeFail)
		safeFatalError("getString overflow.");
	return i;
	}

bool	Input::getVerify(ConstString v)
	{
	argCheck(v==NULL);

	int i=v.getLength();
	if(i>100)
		error("Verify overflow.");

	char t[100];
	getBytes(t,i);
	return ConstString(t,i)==v;
	}

void	Input::getIgnore(int size)
	{
	argCheck(size<0);
	unsigned char *trash=dalloc<unsigned char>(size);
	getBytes(trash,size);
	dfree(trash);
	}

bool	Input::empty()
	{
	return byteIndex==0&&/*bitIndex==0&&*/emptyRaw();
	if(byteIndex>0)
		return false;
	Byte temp;
	if(getBytes(&temp,1,true)==0)
		return true;
	byteBuffer[byteIndex++]=temp;
	return false;
	}

int 	Input::getBytes(void *dest_,int size,bool unsaturated)
	{
	argCheck(dest_==NULL||size<0);
	Byte *dest=(Byte*)dest_;
	int originSize=size;
	while(size>0)
		{
		if(byteIndex>0)
			{
			int num=size>byteIndex?byteIndex:size;
			memcpy(dest,byteBuffer,num);
			byteBuffer+=num;byteIndex-=num;byteMax-=num;
			dest+=num;size-=num;position+=num;
			if(byteIndex==0)
				{
				if(byteMax<byteMaxRequired)
					{
					memcpy(byteBufferOrigin,byteBuffer-remainSize,remainSize);
					byteBuffer=byteBufferOrigin+remainSize;
					byteMax=byteMaxOrigin;
					}
				}
			if(size==0)
				break;
			}
		if(in!=NULL&&in->getSafeFail()!=0)
			{
			if(in->getSafeFail()>safeFail)
				safeFail=in->getSafeFail();
			}
		if(safeFail>=3)
			{
			if(unsaturated)
				return originSize-size;
			memset(dest,0,size);
			return originSize;
			}
		if(emptyRaw())
			{
			if(!unsaturated)
				safeFatalError("getRaw from empty Input. %d %d %s",size,byteIndex,typeid(*this).name())
			else
				return originSize-size;
			continue;
			}
		int n=getRaw(dest,size);
		if(n<0)
			error("n<0.");
		dest+=n;size-=n;position+=n;
		}
	return originSize;
	}

void	Input::unget(void *data,int size)
	{
	argCheck(data==NULL||size<0);
	if(byteIndex+size>byteMax)
		error("byteBuffer overflow.");
	memcpy(byteBuffer+byteIndex,data,size);
	byteIndex+=size;
	position-=size;
	}

Text*	Input::getEnd(bool localSafeFail)
	{
	if(isType(this,InputEmptyMask))
		return NULL;
	char temp[16];
	int r=getBytes(temp,16,true);
	if(r!=0)
		{
		if(!localSafeFail)
			safeError("getEnd last.");
		report("Data last %S",viewData(temp,r));

		Text *last=new Text();
		while(r!=0)
			{
			last->add(temp,r);
			r=getBytes(temp,16,true);
			}

		if(!localSafeFail)
			{
			delete last;
			return NULL;
			}
		return last;
		}
	return NULL;
	}













	InputEmptyMask::InputEmptyMask(Input *i):Input(i,0,false)
	{
	argCheck(i==NULL);
	}


int 	InputEmptyMask::getRaw(Byte *dest,int size)
	{
	return in->getBytes(dest,size,true);
	}
bool	InputEmptyMask::emptyRaw()
	{
	return in->empty();
	}












