

Datafile*	loadPNG(Input *originIn,int safeFail)
	{
	argCheck(originIn==NULL);
	report(1,"Loading PNG.");
	int startClock=clock();
	if(!originIn->getVerify("\211PNG\r\n\032\n"))
		{
		safeError("Verify failed.");
		report(-1,"Loading aborted.");
		delete originIn;
		return NULL;
		}


	Datafile *datafile=new Datafile();
	Picture *pic=NULL;
	DSBitmap *buffer=NULL;
	DS_RGB *palette=NULL;

	Text	*infoText=new Text("==PNG File Information==\n");

	Coord size=Coord(-2,0);
	int bitDepth=0,colorType=0,interlace=0;
	int pm=0;
	DS_RGB transColor=transparent,backColor=transparent;

	int frameCount=-1,delay=-1;
	Coord localSize=zeroCoord,offset=zeroCoord;
	int dispose=1,blend=0;

	InputPNG *in=new InputPNG(originIn,false);
	const int endianSetting=dlib::bigEndian;
	originIn->setSafeFail(safeFail);
	in->setSafeFail(safeFail);
	while(safeFail<3)
		{
		char blockName[4];
		int length=in->getChunk(blockName);
		infoText->add(blockName,4);
		infoText->addString(" length=%d\n",length);
		//report("Chunk %4s %4d %d %d %d",blockName,length,originIn->getPosition(),pic==NULL?-1:pic->getFrameNumber());
		if(verify(blockName,"IHDR"))
			{
			if(length!=13)
				safeFatalError("IHDR with size %d != 13.",length);
			if(size.x!=-2)
				safeFatalError("IHDR repeated.");


			size=in->getCoord(4);
			if(!(size>zeroCoord))
				safeFatalError("Unexpected size %d*%d.",size.x,size.y);
			bitDepth=in->getInt(1);
			if(bitDepth>9||bitDepth<=0||8%bitDepth!=0)
				safeFatalError("Unsupported bitDepth %d.",bitDepth);
				//safeFatalError("Undefined bitDepth %d.",bitDepth);
			colorType=in->getInt(1);
			if((colorType|0x6)!=6&&colorType!=3)
				safeFatalError("Undefined color type.");
			if(in->getInt(1)!=0)
				safeFatalError("Undefined compression method.");
			if(in->getInt(1)!=0)
				safeError("Undefined filter method.");
			interlace=in->getInt(1);
			if(interlace>1||interlace<0)
				safeError("Undefined interlace method.");
			report("IHDR %d*%d bitDepth:%d colorType:%d%s.",size.x,size.y,bitDepth,colorType,interlace==1?" interlaced":"");
			infoText->addString("    %d*%d bitDepth:%d colorType:%d%s.\n",size.x,size.y,bitDepth,colorType,interlace==1?" interlaced":"");
			}
			else if (size.x==-2)
				safeFatalError("No IHDR at first.")
		else if(verify(blockName,"PLTE"))
			{
			if(length%3!=0)
				safeError("PLTE with size %d %%3!=0.",length);
			if(palette!=NULL)
				{
				safeError("PLTE appears twice.");
				dfree(palette);
				}
			if(colorType==0||colorType==4)
				safeError("PLTE with colorType = %d.",colorType);

			palette=dalloc<DS_RGB>(length/3);

			for(int i=0;i<length/3;i++)
				palette[i]=in->getRGB();
			pm=length/3;
			report("PLTE [%d] loaded.",pm);
			infoText->addString("    [%d] loaded.\n",pm);
			}
		else if(verify(blockName,"IDAT"))
			{
			if(pic!=NULL)
				{
				safeError("Multiple IDAT.");
				continue;
				}

			DSBitmap *bmp=loadPNGBitmap(in,size,bitDepth,colorType,interlace,palette,pm,infoText,safeFail);
			if(bmp==NULL)
				{
				safeFatalError("loadBitmap failed.");
				continue;
				}

			if(transColor.a==255)
				bmp->convertAlpha(transColor);
			if(delay==-1)
				{
				if(frameCount!=-1)
					{
					report("Default image exist.");
					infoText->addString("    Default image exist.\n");
					}
				delay=1;
				}
			pic=new Picture(bmp,delay);
			
			if(dispose==0)
				buffer=bmp->clone();
			if(frameCount!=-1)
				pic->setTimeType(Picture::Realtime);
			}
		else if(verify(blockName,"IEND"))
			;
		else if(verify(blockName,"acTL"))
			{
			frameCount=in->getInt(4);
			int loopTime=in->getInt(4);//loop times
			infoText->addString("    %d frame  %d loop.\n",frameCount,loopTime);
			}
		else if(verify(blockName,"fcTL"))
			{
			//in->getIgnore(4);
			localSize=in->getCoord(4);
			offset=in->getCoord(4);
			int nume=in->getInt(2);
			int deno=in->getInt(2);
			if(nume<0||deno<0)
				safeError("Negative delay.");
			delay=nume<=0?1:(nume*1000/(deno<=0?100:deno));
			if(delay<=0)
				delay=1;
			dispose=in->getInt(1);
			blend=in->getInt(1);
			infoText->addString("    %d,%d + %d*%d  delay:%d (%d/%d) dispose:%d blend:%d.\n",offset.x,offset.y,localSize.x,localSize.y,delay,nume,deno,dispose,blend);
			}
		else if(verify(blockName,"fdAT"))
			{
			//in->getIgnore(4);
			if(pic==NULL)
				{
				safeError("Missing IDAT before fdAT.");
				continue;
				}


			DSBitmap *bmp=loadPNGBitmap(in,localSize,bitDepth,colorType,interlace,palette,pm,infoText,safeFail);
			if(bmp==NULL)
				{
				safeFatalError("loadBitmap failed.");
				continue;
				}
			if(transColor.a==255)
				bmp->convertAlpha(transColor);

			/*if(offset==zeroCoord&&localSize==size&&blend==0)
				{
				pic->addFrame(bmp,delay);
				continue;
				}*/

			void (DSBitmap::*blendFunc)(const DSBitmap *src,Coord p);
			switch(blend)
				{
			default:
				safeError("Undefined blendType.");
			case 0:
				blendFunc=DSBitmap::blit;
				break;
			case 1:
				blendFunc=DSBitmap::draw;
				break;
				}

			if(buffer==NULL)
				{
				buffer=new DSBitmap(size);
				buffer->clear(transparent);
				}

			switch(dispose)
				{
			default:
				safeError("Undefined disposeType.");
			case 0:
				(buffer->*blendFunc)(bmp,offset);
				delete bmp;
				pic->addFrame(buffer->clone(),delay);
				break;
			case 1:
				(buffer->*blendFunc)(bmp,offset);
				delete bmp;
				pic->addFrame(buffer->clone(),delay);
				buffer->clear(transparent,offset,localSize);
				break;
			case 2:
				{
				DSBitmap *temp=buffer->clone();
				(temp->*blendFunc)(bmp,offset);
				delete bmp;
				pic->addFrame(temp,delay);
				break;
				}
				}
			}
		else if(verify(blockName,"bKGD"))
			{
			if(backColor.a!=0)
				safeError("bKGD repeated.");
			switch(colorType)
				{
			case 3:
				if(palette==NULL)
					{
					safeError("bKGD chunk before PLTE.");
					continue;
					}
				int index;
				index=in->getInt(1);
				if(index>=pm||index<0)
					{
					safeError("Palette overflow.");
					continue;
					}
				backColor=palette[index];
				break;
			case 0:
			case 4:
				backColor.r=backColor.g=backColor.b=in->getInt(1);
				backColor.a=255;
				in->getIgnore(1);
				break;
			case 2:
			case 6:
				in->getIgnore(1);
				backColor.r=in->getInt(1);
				in->getIgnore(1);
				backColor.g=in->getInt(1);
				in->getIgnore(1);
				backColor.b=in->getInt(1);
				backColor.a=255;
				break;
			default:
				safeError("Undefined colorType.");
				break;
				}
			report("bKGD chunk");
			infoText->addString("    %d,%d,%d.\n",backColor.r,backColor.g,backColor.b);
			}
		else if(verify(blockName,"tRNS"))
			{
			if(transColor.a==255)
				safeError("tRNS repeated.");
			switch(colorType)
				{
			case 3:
				if(palette==NULL)
					{
					safeError("tRNS chunk before PLTE");
					continue;
					}
				for(int i=0;i<pm&&!in->empty();i++)
					palette[i].a=in->getInt(1);
				break;
			case 0:
				transColor.r=transColor.g=transColor.b=in->getInt(1);
				transColor.a=255;
				in->getIgnore(1);
				break;
			case 2:
				transColor.r=in->getInt(1);
				transColor.r=in->getInt(1);
				transColor.g=in->getInt(1);
				transColor.g=in->getInt(1);
				transColor.b=in->getInt(1);
				transColor.b=in->getInt(1);
				transColor.a=255;
				break;
			case 4:
			case 6:
				safeError("tRNS chunk with alpha channel.");
				break;
			default:
				safeError("Undefined colorType.");
				break;
				}
			report("tRNS chunk");
			if(colorType!=3)
				infoText->addString("    %d,%d,%d.\n",transColor.r,transColor.g,transColor.b);
			}
		else if(verify(blockName,"tEXt"))
			{
			Text *textData=new Text();
			char *text=dalloc<char>(length>85?length:85);
			int keyLen=in->getString(text,85);
			report("tEXt chunk %S",viewData(text,keyLen));
			textData->addLine(text,keyLen);

			in->getBytes(text,length-keyLen-1);
			report(" %S",viewData(text,length-keyLen-1));
			textData->add(text,length-keyLen-1);
			textData->setName("PNG tEXt");
			datafile->addData(textData);
			dfree(text);
			}
		else if(verify(blockName,"zTXt"))
			{
			Text *textData=new Text();
			char *text=dalloc<char>(500);
			int keyLen=in->getString(text,80);
			report("zTXt chunk %S",viewData(text,keyLen));
			textData->addLine(text,keyLen);

			if(in->getInt(1)!=0)
				{
				safeError("Undefined compression method.");
				textData->addString("Undefined compression method.\n");
				textData->setName("PNG zTXt error");
				datafile->addData(textData);
				dfree(text);
				continue;
				}
			Input *zlib=new InputZlib(in,false);
			int s;
			do	{
				s=zlib->getBytes(text,500,true);
				report(" %S",viewData(text,s));
				textData->add(text,s);
				}while(s==500);
			delete zlib;
			textData->setName("PNG zTXt");
			datafile->addData(textData);
			dfree(text);
			}
		else if(verify(blockName,"iTXt"))
			{
			Text *textData=new Text();
			char *text=dalloc<char>(500);
			int keyLen=in->getString(text,80);
			report("iTXt chunk %S",viewData(text,keyLen));
			textData->addLine(text,keyLen);

			int compressFlag=in->getInt(1);
			if(in->getInt(1)!=0)
				{
				safeError("Undefined compression method.");
				textData->addString("Undefined compression method.\n");
				textData->setName("PNG iTXt error");
				datafile->addData(textData);
				dfree(text);
				continue;
				}

			int s;
			do	{
				s=in->getString(text,500,true);
				report("Language tag %S",viewData(text,s));
				textData->add(text,s);
				}while(s==500);
			textData->addString("\n");

			do	{
				s=in->getString(text,500,true);
				report("Translated key %S",viewData(text,s));
				textData->add(text,s);
				}while(s==500);
			textData->addString("\n");

			Input *depress;
			if(compressFlag==0)
				depress=new InputEmptyMask(in);
			else
				depress=new InputZlib(in,false);
			do	{
				s=depress->getBytes(text,500,true);
				report(" %S",viewData(text,s));
				textData->add(text,s);
				}while(s==500);
			delete depress;

			textData->setName("PNG iTXt");
			datafile->addData(textData);
			dfree(text);
			}
		else if(verify(blockName,"tIME"))
			{
			int year=in->getInt(2);
			int month=in->getInt(1);
			int date=in->getInt(1);
			int hour=in->getInt(1);
			int minute=in->getInt(1);
			int second=in->getInt(1);
			infoText->addString("    %4d/%2d/%2d %2d:%2d:%2d.\n",year,month,date,hour,minute,second);
			}
		else
			{
			if(!(blockName[0]&0x20))
				safeError("Unsupported critical chunk %4s",blockName);
			if(!(blockName[1]&0x20))
				;//error("Unsupported public chunk %4s",blockName);
			if(blockName[2]&0x20)
				safeError("Undefined chunkName flag %4s",blockName);
			if(!(blockName[3]&0x20))
				;//Safe to copy

			report("Unsupported chunk %S.",viewData(blockName,4));
			infoText->addString("    Unsupported.\n");
			in->getIgnore(length);
			}

		Text *last=in->getEnd(true);
		if(last!=NULL)
			{
			last->setName("Chunk last");
			last->addString("\nat chunk ");
			last->add(blockName,4);
			datafile->addData(last);
			}
		if(verify(blockName,"IEND"))
			break;
		}
	delete in;

	if(palette!=NULL)
		dfree(palette);

	if(buffer!=NULL)
		delete buffer;

	if(pic==NULL)
		{
		if(safeFail<=1)
			safeError("No IDAT.");
		delete datafile;
		delete infoText;
		delete originIn;
		return NULL;
		}

	Text *last=originIn->getEnd(true);
	if(last!=NULL)
		{
		last->setName("File last");
		datafile->addData(last);
		}

	int overallByteDepth=3;
	for(int fi=0;fi<pic->getFrameNumber();fi++)
		if(!pic->getBitmapIndex(fi)->isFullOpaque())
			{
			overallByteDepth=4;
			break;
			}
	infoText->addString("%lld/%lld Overall compreRate=%5.2f%% in %dbits.\n"
							,originIn->getPosition(),(long long)overallByteDepth*size.area()*pic->getFrameNumber()
							,(double)originIn->getPosition()/overallByteDepth/size.area()/pic->getFrameNumber()*100.0,overallByteDepth*8);
	infoText->addString("Elapsed time:%dms.\n",(clock()-startClock)*1000/CLOCKS_PER_SEC);

	pic->setName("PNG Picture");
	if(backColor.a!=0)
		{
		for(int fi=0;fi<pic->getFrameNumber();fi++)
			{
			DSBitmap *bg=new DSBitmap(size);
			bg->clear(backColor);
			pic->getFrame(fi)->enableLayer();
			((FolderLayer*)pic->getFrame(fi)->getLayer())->addLayer(new SimpleLayer(bg));
			}
		}

	if(safeFail>=2)
		infoText->addString("safeFail=%d\n",safeFail);



	infoText->setName("PNG Information");
	datafile->addData(pic);
	datafile->addData(infoText);

	report(-1,"Loaded PNG.");
	delete originIn;
	return datafile;
	}


int		comparePaletteCount(const void *a_,const void *b_)
	{
	PaletteInfo *a=(PaletteInfo*)a_;
	PaletteInfo *b=(PaletteInfo*)b_;
	return (a->count>b->count)-(a->count<b->count);
	}
int		comparePaletteColor(const void *a_,const void *b_)
	{
	PaletteInfo *a=(PaletteInfo*)a_;
	PaletteInfo *b=(PaletteInfo*)b_;
	return compareColor(&(a->color),&(b->color));
	}

void	DSBitmap::savePNG(Output *out,PictureOption *option)const
	{
	argCheck(out==NULL);
	
	Picture *pic=new Picture(this->clone());
	pic->savePNG(out,option);
	delete pic;
	return;
	/*report(1,"saving PNG.");

	out->putString("\211PNG\r\n\032\n");

	const int endianSetting=dlib::bigEndian;
	int bitDepth=8,colorType=0,interlace=0;
	
	PaletteInfo rawPalette[257];
	DS_RGB palette[257];
	int paletteMax=0;
	for(int y=0;y<size.y;y++)
	for(int x=0;x<size.x;x++)
		{
		if(data[y][x].r!=data[y][x].g||data[y][x].r!=data[y][x].b)
			colorType|=2;
		if(data[y][x].a!=255)
			colorType|=4;
		if(paletteMax<257)
			{
			int pi;
			for(pi=0;pi<paletteMax;pi++)
				if(data[y][x].same(rawPalette[pi].color))
					break;
			if(pi==paletteMax)
				{
				rawPalette[paletteMax].count=0;
				rawPalette[paletteMax++].color=data[y][x];
				}
			rawPalette[pi].count++;
			
			if(x==0)
				sort(rawPalette,paletteMax,sizeof(PaletteInfo),comparePaletteCount,true);
			}
		if(colorType==6)
			{y=size.y;break;}
		}

	if(colorType==0)
		{
		bitDepth=1;
		for(int y=0;y<size.y;y++)
		for(int x=0;x<size.x;x++)
			while(data[y][x].r*((1<<bitDepth)-1)%255!=0)
				{
				bitDepth*=2;
				if(bitDepth>=8)
					{y=size.y;x=size.x;break;}
				}
		}

	if(paletteMax<257)
		{
		int paletteDepth=1;
		while(paletteMax>(1<<paletteDepth))
			paletteDepth*=2;
		if(colorType!=0||paletteDepth<bitDepth)
			{
			colorType=3;
			bitDepth=paletteDepth;
			}
		}

	out=new OutputPNG(out);
	((OutputPNG*)out)->putChunk("IHDR");
	out->putCoord(size,4);
	out->putInt(bitDepth,1);
	out->putInt(colorType,1);
	out->putInt(0,1);//compression
	out->putInt(0,1);//filter
	out->putInt(interlace,1);//interlace
	if(colorType==3)
		{
		sort(rawPalette,paletteMax,sizeof(PaletteInfo),comparePaletteCount,true);
		for(int pi=0;pi<paletteMax;pi++)
			{
			palette[pi]=rawPalette[pi].color;
			}
		((OutputPNG*)out)->putChunk("PLTE");
		for(int i=0;i<paletteMax;i++)
			out->putRGB(palette[i]);
		}

	//((OutputPNG*)out)->putChunk("tEXt");
	//out->putString("Software");
	//out->putInt(0,1);
	//out->putString("D.S.");

	((OutputPNG*)out)->putChunk("IDAT");
	savePNGBitmap(out,bitDepth,colorType,interlace,rawPalette,paletteMax);

	((OutputPNG*)out)->putChunk("IEND");

	delete out;
	report(-1,"saved PNG.");*/
	}

void	Picture::savePNG(Output *out,PictureOption *option)const
	{
	argCheck(out==NULL);
	report(1,"saving PNG.");
	
	if(option==NULL)
		option=new PictureOption();
	if(!option->skipHeader)
		out->putString("\211PNG\r\n\032\n");

	const int endianSetting=dlib::bigEndian;
	//int bitDepth=8,colorType=0,interlace=0;
	
	PaletteInfo rawPalette[257];
	//rawPalette.setSorted(comparePaletteColor);
	DS_RGB palette[257];
	int paletteMax=0;
	if(option->usePalette==false)
		paletteMax=257;
	
	Coord size=getDataSize();
	for(int fi=0;fi<getFrameNumber();fi++)
		{
		const DSBitmap *bmp=frame[fi]->getLayer()->getBitmap();
		for(int y=0;y<size.y;y++)
		for(int x=0;x<size.x;x++)
			{
			DS_RGB c=bmp->get(x,y);
			option->fixStableTransparent(c);
			if(c.r!=c.g||c.r!=c.b)
				if(option->grayScale==AutoLoseless)
					option->grayScale=false;
			if(c.a!=255)
				if(option->alphaChannel==AutoLoseless)
					option->alphaChannel=true;
			if(paletteMax<257)
				{
				int pi;
				for(pi=0;pi<paletteMax;pi++)
					if(c.totalSame(rawPalette[pi].color))
						break;
				if(pi==paletteMax)
					{
					rawPalette[paletteMax].count=0;
					rawPalette[paletteMax++].color=c;
					}
				rawPalette[pi].count++;
				if(x==0)
					sort(rawPalette,paletteMax,sizeof(PaletteInfo),comparePaletteCount,true);
				}
			else if(option->grayScale!=AutoLoseless&&option->alphaChannel!=AutoLoseless)
				{fi=getFrameNumber();y=size.y;break;}
			}
		}
	
	if(option->grayScale==AutoLoseless)
		option->grayScale=true;
	if(option->alphaChannel==AutoLoseless)
		option->alphaChannel=false;
	if(option->interlace==AutoLoseless)
		option->interlace=0;
		
	option->colorType=(!option->grayScale)*2+option->alphaChannel*4;
	
	if(option->colorType==0&&option->usePalette!=true)
		{
		if(option->bitDepth==AutoLoseless)
			{
			option->bitDepth=1;
			for(int fi=0;fi<getFrameNumber();fi++)
				{
				const DSBitmap *bmp=frame[fi]->getLayer()->getBitmap();
				for(int y=0;y<size.y;y++)
				for(int x=0;x<size.x;x++)
					{
					DS_RGB c=bmp->get(x,y);
					option->fixStableTransparent(c);
					int g=c.getGray();
					while(g*((1<<option->bitDepth)-1)%255!=0)
						{
						option->bitDepth*=2;
						if(option->bitDepth>=8)
							{fi=getFrameNumber();y=size.y;x=size.x;break;}
						}
					}
				}
			}
		if(8%option->bitDepth!=0)
			error("Illegal bitDepth.");
		}

	if(paletteMax<257)
		{
		int paletteDepth=1;
		while(paletteMax>(1<<paletteDepth))
			paletteDepth*=2;
		if(option->colorType!=0||paletteDepth<option->bitDepth||option->usePalette==true)
			{
			option->colorType=3;
			option->bitDepth=paletteDepth;
			}
		}
	
	if(option->colorType!=0&&option->colorType!=3)
		option->bitDepth=8;
	
	out=new OutputPNG(out);
	((OutputPNG*)out)->putChunk("IHDR");
	out->putCoord(size,4);
	out->putInt(option->bitDepth,1);
	out->putInt(option->colorType,1);
	out->putInt(0,1);//compression
	out->putInt(0,1);//filter
	out->putInt(option->interlace,1);//interlace

	if(option->colorType==3)
		{
		sort(rawPalette,paletteMax,sizeof(PaletteInfo),comparePaletteCount,true);
		int paletteBorder=1<<option->bitDepth;
		for(int pi=0;pi<paletteBorder;pi++)
			palette[pi]=black;
		for(int pi=0;pi<paletteMax;pi++)
			{
			int ci=(pi+1)/2*(1-(pi%2==0)*2);
			if(ci<0)
				ci+=paletteBorder;
			palette[ci]=rawPalette[pi].color;
			rawPalette[pi].index=ci;
			//report("count[%d]=%d",ci,rawPalette[pi].count);
			}
			
		bool transparentOn=false;
		((OutputPNG*)out)->putChunk("PLTE");
		for(int pi=0;pi<paletteBorder;pi++)
			{
			out->putRGB(palette[pi]);
			if(palette[pi].a!=255)
				transparentOn=true;
			//report("PLTE[%d] %R",pi,palette[pi]);
			}
		if(transparentOn)
			{
			((OutputPNG*)out)->putChunk("tRNS");
			for(int pi=0;pi<paletteBorder;pi++)
				out->putInt(palette[pi].a,1);
			}
		
		option->palette=rawPalette;
		option->paletteMax=paletteMax;
		}

	//((OutputPNG*)out)->putChunk("tEXt");
	//out->putString("Software");
	//out->putInt(0,1);
	//out->putString("D.S.");
	
	if(getFrameNumber()!=1)
		{
		((OutputPNG*)out)->putChunk("acTL");
		out->putInt(getFrameNumber()-(frame[0]->getDelay()==1),4);
		if(getFrameNumber()==2&&frame[0]->getDelay()==1)
			out->putInt(1,4);
		else
			out->putInt(0,4);
		}
	for(int fi=0;fi<getFrameNumber();fi++)
		{
		const DSBitmap *bmp=frame[fi]->getLayer()->getBitmap();
		DSBitmap *temp=NULL;
		if(getFrameNumber()!=1)
			{
			if(fi!=0||frame[fi]->getDelay()!=1)
				{
				((OutputPNG*)out)->putChunk("fcTL");
				out->putCoord(size,4);
				out->putCoord(zeroCoord,4);
				if(getFrameNumber()==2&&frame[0]->getDelay()==1)
					out->putInt(1000,2);
				else
					out->putInt(frame[fi]->getDelay(),2);
				out->putInt(1000,2);
				out->putInt(1,1);
				out->putInt(0,1);
				}
			else if(option->thumbnailSize>zeroCoord)
				{
				Coord thumbSize=bmp->getSize()*Coord(100,100)/option->thumbnailSize;
				if(thumbSize.x>thumbSize.y)
					thumbSize=bmp->getSize()*option->thumbnailSize.x/bmp->getSize().x;
				else
					thumbSize=bmp->getSize()*option->thumbnailSize.y/bmp->getSize().y;
				//report("ts %d,%d ms %d,%d",thumbSize.x,thumbSize.y,mozaicSize.x,mozaicSize.y);
				temp=bmp->clone();
				FOR_COORD(ti,thumbSize)
					long long r=0,g=0,b=0;
					Coord baseSize=ti*bmp->getSize()/thumbSize;
					Coord mozaicSize=(ti+monoCoord)*bmp->getSize()/thumbSize-baseSize;
					FOR_COORD(mi,mozaicSize)
						DS_RGB c=temp->get(baseSize+mi);
						r+=c.r;
						g+=c.g;
						b+=c.b;
					END_FOR_COORD
					DS_RGB c(r/mozaicSize.area(),g/mozaicSize.area(),b/mozaicSize.area());
					FOR_COORD(mi,mozaicSize)
						temp->put(baseSize+mi,c);
					END_FOR_COORD
				END_FOR_COORD
				bmp=temp;
				}
			}
		((OutputPNG*)out)->putChunk(fi==0?"IDAT":"fdAT");
		bmp->savePNGBitmap(out,option->clone());
		if(temp!=NULL)
			delete temp;
		}

	((OutputPNG*)out)->putChunk("IEND");

	delete option;
	delete out;
	report(-1,"saved PNG.");
	}




DSBitmap*	loadPNGBitmap(Input *in,Coord size,int bitDepth,int colorType,int interlace,DS_RGB *palette,int pm,Text *infoText,int safeFail)
	{
	if(colorType==3&&palette==NULL)
		{
		safeFatalError("Palette Data without PLTE.");
		return NULL;
		}

	Input *zlib=new InputZlib(in,false);

	int passNum;
	int *xl,*yl,*xd,*yd;
	int xyl0=1;
	int xyd0=0;
	int xl1[]={8,8,4,4,2,2,1};
	int yl1[]={8,8,8,4,4,2,2};
	int xd1[]={0,4,0,2,0,1,0};
	int yd1[]={0,0,4,0,2,0,1};
	switch(interlace)
		{
	default:
		safeError("Undefined interlace.");
	case 0:
		passNum=1;
		xl=yl=&xyl0;
		xd=yd=&xyd0;
		break;
	case 1:
		passNum=7;
		xl=xl1;
		yl=yl1;
		xd=xd1;
		yd=yd1;
		break;
		}

	DSBitmap *bmp=new DSBitmap(size);
	int bpp=(bitDepth*(1+2*((colorType&3)==2)+((colorType&4)!=0))+7)/8;
	for(int i=0;i<passNum;i++)
		{
		int passWidth=(size.x-xd[i]+xl[i]-1)/xl[i];
		int lineWidth=(passWidth*bitDepth*(1+2*((colorType&3)==2)+((colorType&4)!=0))+7)/8;
		if(lineWidth<=0)
			continue;

		Byte *now=dalloc<Byte>(lineWidth);
		Byte *last=dalloc<Byte>(lineWidth);
		for(int y=yd[i];y<size.y;y+=yl[i])
			{
			int t=zlib->getInt(1);
			zlib->getBytes(now,lineWidth);
			switch(t)
				{
			case 0:
				break;
			case 1:
				for(int x=bpp;x<lineWidth;x++)
					now[x]+=now[x-bpp];
				break;
			case 2:
				for(int x=0;x<lineWidth;x++)
					now[x]+=last[x];
				break;
			case 3:
				for(int x=0;x<bpp;x++)
					now[x]+=last[x]/2;
				for(int x=bpp;x<lineWidth;x++)
					now[x]+=(now[x-bpp]+last[x])/2;
				break;
			case 4:
				for(int x=0;x<bpp;x++)
					now[x]+=last[x];
				for(int x=bpp;x<lineWidth;x++)
					if(abs(last[x]-last[x-bpp])<=abs(now[x-bpp]-last[x-bpp]) && abs(last[x]-last[x-bpp])<=abs(now[x-bpp]+last[x]-last[x-bpp]*2))
						now[x]+=now[x-bpp];
					else if(abs(now[x-bpp]-last[x-bpp])<=abs(now[x-bpp]+last[x]-last[x-bpp]*2))
						now[x]+=last[x];
					else
						now[x]+=last[x-bpp];
				break;
			default:
				safeError("Unsupported filter type %d.",t);
				break;
				}

			switch(colorType)
				{
			case 0:
				for(int x=0;x<passWidth;x++)
					{
					int scale=(now[x*bitDepth/8]>>(8-x*bitDepth%8-bitDepth)) & ((1<<bitDepth)-1);
					scale=scale*255/((1<<bitDepth)-1);
					bmp->put(Coord(x*xl[i]+xd[i],y),DS_RGB(scale,scale,scale));
					}
				break;
			case 3:
				if(palette==NULL)
					error("Palette Data without PLTE.");
				for(int x=0;x<passWidth;x++)
					{
					int pi=(now[x*bitDepth/8]>>(8-x*bitDepth%8-bitDepth)) & ((1<<bitDepth)-1);
					if(pi>=pm||pi<0)
						{
						safeError("Palette overflow.");
						pi=pm-1;
						}
					bmp->put(Coord(x*xl[i]+xd[i],y),palette[pi]);
					}
				break;
			case 4:
				for(int x=0;x<passWidth;x++)
					bmp->put(Coord(x*xl[i]+xd[i],y),DS_RGB(now[x*2],now[x*2],now[x*2],now[x*2+1]));
				break;
			case 2:
				for(int x=0;x<passWidth;x++)
					bmp->put(Coord(x*xl[i]+xd[i],y),DS_RGB(now[x*3],now[x*3+1],now[x*3+2]));
				break;
			case 6:
				for(int x=0;x<passWidth;x++)
					bmp->put(Coord(x*xl[i]+xd[i],y),DS_RGB(now[x*4],now[x*4+1],now[x*4+2],now[x*4+3]));
				break;
			default:
				error("Undefined colorType.");
				}

			Byte *temp=now;
			now=last;
			last=temp;
			}//y
		dfree(now);
		dfree(last);
		}//pass

	bmp->fullOptimize();

	if(infoText!=NULL)
		infoText->addString("    %lld/%lld compreRate=%5.2f%%.\n",zlib->getInSize(),zlib->getPosition(),(double)zlib->getInSize()/zlib->getPosition()*100.0);

	delete zlib;
	return bmp;
	}


void	DSBitmap::savePNGBitmap(Output *out,PictureOption *option)const
	{
	argCheck(out==NULL||option==NULL);
	int bitpp=option->bitDepth*(1+2*((option->colorType&3)==2)+((option->colorType&4)!=0));
	int lineWidth=(size.x*bitpp+7)/8;
	int bm=size.y*(1+lineWidth);
	Output *zlib=new OutputZlib(out,bm+size.y,false);

	int passNum;
	int *xl,*yl,*xd,*yd;
	int xyl0=1;
	int xyd0=0;
	int xl1[]={8,8,4,4,2,2,1};
	int yl1[]={8,8,8,4,4,2,2};
	int xd1[]={0,4,0,2,0,1,0};
	int yd1[]={0,0,4,0,2,0,1};
	switch(option->interlace)
		{
	case 0:
		passNum=1;
		xl=yl=&xyl0;
		xd=yd=&xyd0;
		break;
	case 1:
		passNum=7;
		xl=xl1;
		yl=yl1;
		xd=xd1;
		yd=yd1;
		break;
	default:
		error("undefined interlace");
		break;
		}
	int bpp=(bitpp+7)/8;
	for(int i=0;i<passNum;i++)
		{
		int passWidth=(size.x-xd[i]+xl[i]-1)/xl[i];
		int lineWidth=(passWidth*bitpp+7)/8;
		unsigned char *now=dalloc<unsigned char>(lineWidth);
		unsigned char *last=dalloc<unsigned char>(lineWidth);
		unsigned char *filted[5];
		for(int fi=0;fi<5;fi++)
			filted[fi]=dalloc<unsigned char>(lineWidth);
		for(int y=yd[i];y<size.y;y+=yl[i])
			{
			if(option->bitDepth!=8)
				;//error("Unsupported bitDepth %d.",bitDepth);
			switch(option->colorType)
				{
			case 0:
				memset(now,0,lineWidth);
				for(int x=0;x<passWidth;x++)
					{
					DS_RGB c=data[y][x*xl[i]+xd[i]];
					option->fixStableTransparent(c);
					int gray=c.getGray();
					gray=gray*((1<<option->bitDepth)-1)/255;
					if(gray>((1<<option->bitDepth)-1))
						error("grayScale overflow.");
					now[x*option->bitDepth/8]|=gray<<(8-x*option->bitDepth%8-option->bitDepth);
					}
				break;
			case 3:
				argCheck(option->palette==NULL);
				memset(now,0,lineWidth);
				for(int x=0;x<passWidth;x++)
					{
					int pi;
					for(pi=0;pi<option->paletteMax;pi++)
						if(data[y][x*xl[i]+xd[i]].same(option->palette[pi].color))
							break;
					if(pi==option->paletteMax||pi>((1<<option->bitDepth)-1))
						error("palette overflow.");
					now[x*option->bitDepth/8]|=option->palette[pi].index<<(8-x*option->bitDepth%8-option->bitDepth);
					}
				break;
			case 4:
				for(int x=0;x<passWidth;x++)
					{
					DS_RGB c=data[y][x*xl[i]+xd[i]];
					option->fixStableTransparent(c);
					now[x*2]=c.getGray();;
					now[x*2+1]=c.a;
					}
				break;
			case 2:
				for(int x=0;x<passWidth;x++)
					{
					DS_RGB c=data[y][x*xl[i]+xd[i]];
					option->fixStableTransparent(c);
					now[x*3  ]=c.r;
					now[x*3+1]=c.g;
					now[x*3+2]=c.b;
					}
				break;
			case 6:
				for(int x=0;x<passWidth;x++)
					{
					DS_RGB c=data[y][x*xl[i]+xd[i]];
					option->fixStableTransparent(c);
					now[x*4  ]=c.r;
					now[x*4+1]=c.g;
					now[x*4+2]=c.b;
					now[x*4+3]=c.a;
					}
				break;
			default:
				error("Undefined colorType %d.",option->colorType);
				}

			int filterType=0;
			int filterCount=-1;
			for(int fi=0;fi<5;fi++)
				{
				Byte *ft=filted[fi];
				memcpy(ft,now,lineWidth);
				switch(fi)
					{
				case 0:
					break;
				case 1:
					for(int x=bpp;x<lineWidth;x++)
						ft[x]-=now[x-bpp];
					break;
				case 2:
					for(int x=0;x<lineWidth;x++)
						ft[x]-=last[x];
					break;
				case 3:
					for(int x=0;x<bpp;x++)
						ft[x]-=last[x]/2;
					for(int x=bpp;x<lineWidth;x++)
						ft[x]-=(now[x-bpp]+last[x])/2;
					break;
				case 4:
					for(int x=0;x<bpp;x++)
						ft[x]-=last[x];
					for(int x=bpp;x<lineWidth;x++)
						if(abs(last[x]-last[x-bpp])<=abs(now[x-bpp]-last[x-bpp]) && abs(last[x]-last[x-bpp])<=abs(now[x-bpp]+last[x]-last[x-bpp]*2))
							ft[x]-=now[x-bpp];
						else if(abs(now[x-bpp]-last[x-bpp])<=abs(now[x-bpp]+last[x]-last[x-bpp]*2))
							ft[x]-=last[x];
						else
							ft[x]-=last[x-bpp];
					break;
				default:
					error("unsupported filter type %d",fi);
					break;
					}

				int count=0;
				for(int i=0;i<lineWidth;i++)
					{
					count+=(ft[i]&0x80)?(ft[i]^0xFF)+1:ft[i];
					}
				if(count<filterCount||filterCount==-1)
					{
					filterCount=count;
					filterType=fi;
					}
				}
			
			zlib->putInt(filterType,1);
			zlib->putBytes(filted[filterType],lineWidth);
			
			unsigned char *temp=now;
			now=last;
			last=temp;
			}//y
		dfree(now);
		dfree(last);
		for(int i=0;i<5;i++)
			dfree(filted[i]);
		}//pass
	delete zlib;
	delete option;
	}





















	InputPNG::InputPNG(Input *i,bool close):Input(i,0,close)
	{
	length=in->getUnsignedInt(4);
	in->getBytes(chunkName,4);

	crc=new CrcCheck();
	crc->put((Byte*)chunkName,4);
	newChunk=true;
	apngSequence=0;
	}
	InputPNG::~InputPNG()
	{
	if(newChunk)
		safeError("newChunk last. %c%c%c%c",chunkName[0],chunkName[1],chunkName[2],chunkName[3]);
	if(length!=0)
		{
		safeError("Data last. %d",length);
		in->getIgnore(length);
		}
	if(~crc->get()!=in->getUnsignedInt(4))
		safeError("CRC checksum failed.");
	delete crc;
	}

int 	InputPNG::getRaw(Byte *dest,int size)
	{
	argCheck(dest==NULL||size<0);
	int n=length>0x0FFFFFFF?0x0FFFFFFF:length;
	n=n>size?size:n;
	in->getBytes(dest,n);
	length-=n;
	crc->put(dest,n);

	if(length==0)
		{
		if(~crc->get()!=in->getUnsignedInt(4))
			safeError("CRC checksum failed.");

		length=in->getUnsignedInt(4);
		char nextName[5];
		in->getBytes(nextName,4);
		nextName[4]=0;
		crc->init();
		crc->put((Byte*)nextName,4);
		newChunk=true;

		//if(n!=size&&!verify(chunkName,nextName))
		//	error("getRaw over different chunk. %d %d %s %c%c",n,size,nextName,chunkName[0],chunkName[1]);
		if(verify(nextName,"fcTL")||verify(nextName,"fdAT"))
			{
			crc->put((apngSequence>>24)&0xFF);
			crc->put((apngSequence>>16)&0xFF);
			crc->put((apngSequence>> 8)&0xFF);
			crc->put((apngSequence    )&0xFF);
			if(in->getInt(4)!=apngSequence++)
				safeError("APNG sequence wrong.");
			length-=4;
			}
		if(verify(chunkName,nextName)&&(verify(chunkName,"IDAT")||verify(chunkName,"fdAT")))
			{
			newChunk=false;
			}
		else
			memcpy(chunkName,nextName,4);
		}
	return n;
	}
bool	InputPNG::emptyRaw()
	{
	return length==0||newChunk;
	}

int 	InputPNG::getChunk(char *ret)
	{
	if(!newChunk)
		{
		safeError("chunk last. %d",length);
		terminateChunk();
		}
	memcpy(ret,chunkName,4);
	newChunk=false;
	return length;
	}
void	InputPNG::terminateChunk()
	{
	getIgnore(length);
	}








	OutputPNG::OutputPNG(Output *o,bool close):OutputMask(o,32768,close)
	{
	crc=new CrcCheck();
	apngSequence=0;
	}

	OutputPNG::~OutputPNG()
	{
	terminateChunk();
	delete crc;
	}

int	OutputPNG::putRaw(const Byte *data,int size)
	{
	int n=size>byteMax-byteIndex?byteMax-byteIndex:size;
	memcpy(byteBuffer+byteIndex,data,n);
	byteIndex+=n;
	crc->put(data,n);
	if(size>n)
		{
		if(chunkName!="IDAT"&&chunkName!="fdAT")
			error("PNG chunk overflow.");
		putChunk(chunkName);
		}
	return n;
	}

void	OutputPNG::putChunk(ConstString name)
	{
	terminateChunk();

	if(name!=chunkName)
		chunkName.copyFrom(name);
	crc->init();
	crc->put((Byte*)chunkName.getRaw(),4);

	if(chunkName=="fcTL"||chunkName=="fdAT")
		putInt(apngSequence++,4);
	}

void	OutputPNG::terminateChunk()
	{
	if(chunkName==NULL)
		return;
	out->putInt(byteIndex,4);
	out->putBytes(chunkName.getRaw(),4);
	out->putBytes(byteBuffer,byteIndex);
	out->putInt(~crc->get(),4);
	byteIndex=0;
	}













