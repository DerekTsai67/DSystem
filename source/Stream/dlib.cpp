

extern int dallocCount;


void	regisTemp(void *p)
	{
	argCheck(p==NULL);
	ThreadLock lock(2);
	TempAllocInfo info;
	info.setTime=clock();
	info.data=p;
	tempAlloc->addItem(info);
	}

char*	dcloneString(const char *src)
	{
	return dclone<char>(src,getStringLength(src)+1);
	}


void dfree(void *ptr)
	{
	ThreadLock lock(1);
	if(ptr==NULL)
		error("dfree NULL");
	dallocCount--;
	free(ptr);
	}

bool	verify(ConstString s1,ConstString s2,bool noRemain)
	{
	if(noRemain)
		return s1==s2;
	for(int i=0;i<s2.getLength();i++)
		if(s1[i]!=s2[i])
			return false;
	return true;
	}




