




void	regisTemp(void *p);
char*	dcloneString(const char *src);
void	dfree(void *ptr);


template <class Type>
Type* dalloc(int s)
	{
	argCheck(s<0);
	ThreadLock lock(1);
	if(s==0)s=1;
	extern int dallocCount;
	dallocCount++;
	Type* ret=(Type*)calloc(s,sizeof(Type));
	if(ret==NULL)
		error("dalloc failed with length %d.",s);
	return ret;
	}

	
template <class Type>
Type* dclone(const Type *src,int s)
	{
	argCheck(src==NULL||s<0);
	if(s==0)s=1;
	Type *ret=dalloc<Type>(s);
	memcpy(ret,src,s*sizeof(Type));
	return ret;
	}

template <class Type>
Type* dallocTemp(int s)
	{
	Type* ret=dalloc<Type>(s);
	regisTemp(ret);
	return ret;
	}


bool	verify(ConstString s1,ConstString s2,bool noRemain=false);

namespace dlib
	{
	enum {
		noCheck		=0x0001,
		bigEndian	=0x0002,
		bitLeft		=0x0004,
		space		=0x0008
		};

	}


