



int 	getSuffixType(ConstString name)
	{
	String suffix;
	suffix.copyFrom(getSuffix(name));
	suffix.setDelete();
	for(int i=0;i<suffix.getLength();i++)
		if(suffix[i]>='A'&&suffix[i]<='Z')
			suffix[i]-='A'-'a';
	
	if(suffix==".dsd")
		return datafileDSD;
	if(suffix==".txt")
		return textTXT;
	if(suffix==".bmp")
		return bitmapBMP;
	if(suffix==".ico")
		return bitmapICO;
	if(suffix==".cur")
		return pictureCUR;
	if(suffix==".png"||suffix==".apng")
		return bitmapPNG;
	if(suffix==".jpg"||suffix==".jpeg")
		return bitmapJPG;
	if(suffix==".gif")
		return pictureGIF;
	if(suffix==".wav")
		return soundWAV;
	if(suffix==".midi")
		return midiMIDI;
	
	report("Unknown suffix %S .",viewData(suffix));
	return unknownSuffix;
	}

Datafile*	loadFile(ConstString name,int safeFail)
	{
	argCheck(name==NULL);
	int suffixType=getSuffixType(name);
	if(suffixType==datafileDSD)
		{
		Datafile *df=new Datafile(name);
		df->load();
		return df;
		}

	Input *in=new InputFile(name);
	return loadFile(in,suffixType,safeFail);
	}

Datafile*	loadFile(Input *in,int suffixType,int safeFail)
	{
	argCheck(in==NULL);

	switch(suffixType)
		{
	//case datafileDSD:
	//	return loadDSD(in,safeFail);
	case textTXT:
		return loadTXT(in,safeFail);
	case bitmapBMP:
		return loadBMP(in,safeFail);
	case bitmapICO:
	case pictureCUR:
		return loadICO(in,safeFail);
	case bitmapPNG:
		return loadPNG(in,safeFail);
	case bitmapJPG:
		return loadJPG(in,safeFail);
	case pictureGIF:
		return loadGIF(in,safeFail);
	case soundWAV:
		return loadWAV(in,safeFail);
	case midiMIDI:
		return loadMIDI(in,safeFail);
	default:
		safeError("Unknown suffixType.");
		return NULL;
		}
	}

Data*	loadData(ConstString name,int safeFail)
	{
	argCheck(name==NULL);
	InputFile *in=new InputFile(name);
	int suffixType=getSuffixType(name);

	if(suffixType>bitmapStart&&suffixType<pictureEnd)
		{
		Picture *data=loadPicture(in,suffixType,safeFail);
		if(data==NULL)
			return NULL;
		data->setName(name);
		return data;
		}

	if(suffixType>soundStart&&suffixType<soundEnd)
		{
		Sound *data=loadSound(in,suffixType,safeFail);
		if(data==NULL)
			return NULL;
		data->setName(name);
		return data;
		}

	safeError("Unsupported suffix %d.",suffixType);
	return NULL;
	}

bool	Data::savePath(ConstString p,int safeFail)const
	{
	return save(new OutputFile(p),getSuffixType(p),safeFail);
	}

Picture*	loadPicture(Input *in,int suffixType,int safeFail)
	{
	argCheck(in==NULL);

	Datafile *df=loadFile(in,suffixType,safeFail);
	if(df==NULL)
		return NULL;

	Picture *pic=df->getPic(-1);
	if(pic==NULL)
		{
		safeError("Unsupported suffix %d.",suffixType);
		return NULL;
		}

	pic=pic->clone();
	delete df;
	return pic;
	}

bool	Picture::save(Output *out,int suffixType,int safeFail)const
	{
	argCheck(out==NULL);
	switch(suffixType)
		{
	case pictureGIF:
		saveGIF(out);
		break;
	case bitmapPNG:
		savePNG(out);
		break;
	default:
		if(suffixType>bitmapStart&&suffixType<bitmapEnd)
			return getBitmap()->save(out,suffixType,safeFail);
		safeError("Unsupported suffix %d.",suffixType);
		delete out;
		return false;
		}
	return true;
	}

DSBitmap*	loadBitmap(Input *in,int suffixType,int safeFail)
	{
	argCheck(in==NULL);

	Datafile *df=loadFile(in,suffixType,safeFail);
	if(df==NULL)
		return NULL;

	Picture *pic=df->getPic(-1);
	if(pic==NULL)
		{
		safeError("Unsupported suffix %d.",suffixType);
		return NULL;
		}

	DSBitmap *bmp=pic->getBitmap()->clone();
	delete df;
	return bmp;
	}

bool	DSBitmap::save(Output *out,int suffixType,int safeFail)const
	{
	argCheck(out==NULL);

	switch(suffixType)
		{
	case bitmapBMP:
		saveBMP(out);
		break;
	case bitmapICO:
		saveICO(out);
		break;
	case bitmapPNG:
		savePNG(out);
		break;
	case bitmapJPG:
		saveJPG(out);
		break;
	case pictureGIF:
		saveGIF(out);
		break;
	default:
		safeError("Unsupported suffix %d.",suffixType);
		delete out;
		return false;
		}
	return true;
	}

Sound*	loadSound(Input *in,int suffixType,int safeFail)
	{
	argCheck(in==NULL);

	Datafile *df=loadFile(in,suffixType,safeFail);
	if(df==NULL)
		return NULL;

	Sound *sound=df->getSound(-1);
	if(sound==NULL)
		{
		safeError("Unsupported suffix %d.",suffixType);
		return NULL;
		}

	sound=sound->clone();
	delete df;
	return sound;
	}

bool	Sound::save(Output *out,int suffixType,int safeFail)const
	{
	argCheck(out==NULL);

	switch(suffixType)
		{
	case soundWAV:
		saveWAV(out);
		break;
	default:
		safeError("Unsupported suffix %d.",suffixType);
		delete out;
		return false;
		}
	return true;
	}

bool	Text::save(Output *out,int suffixType,int safeFail)const
	{
	argCheck(out==NULL);

	switch(suffixType)
		{
	case textTXT:
		saveTXT(out);
		break;
	default:
		safeError("Unsupported suffix %d.",suffixType);
		delete out;
		return false;
		}
	return true;
	}

/*
Datafile*	loadDSD(Input *in,int safeFail)
		{
		Datafile *df=new Datafile(in);
		df->load();
		return df;
		}*/







	PictureOption::PictureOption()
	{
	bitDepth=AutoLoseless;
	grayScale=AutoLoseless;
	alphaChannel=AutoLoseless;
	colorType=0;
	usePalette=AutoLoseless;
	interlace=0;
	palette=NULL;
	paletteMax=0;
	stableTransparent=false;
	skipHeader=false;
	thumbnailSize=zeroCoord;
	}
	
	PictureOption::~PictureOption()
	{
	//if(palette!=NULL)
	//	dfree(palette);
	}
PictureOption*	PictureOption::clone()const
	{
	PictureOption *po=new PictureOption;
	memcpy(po,this,sizeof(PictureOption));
	return po;
	}

PictureOption*	PictureOption::setBitDepth(int a)
	{
	bitDepth=a;
	return this;
	}
PictureOption*	PictureOption::setGrayScale(int a)
	{
	grayScale=a;
	return this;
	}
PictureOption*	PictureOption::setAlphaChannel(int a)
	{
	alphaChannel=a;
	return this;
	}
PictureOption*	PictureOption::setUsePalette(int a)
	{
	usePalette=a;
	return this;
	}
PictureOption*	PictureOption::setInterlace(int a)
	{
	interlace=a;
	return this;
	}
PictureOption*	PictureOption::setStableTransparent(int a)
	{
	stableTransparent=a;
	return this;
	}
void	PictureOption::fixStableTransparent(DS_RGB &c)
	{
	if(c.a==0&&!stableTransparent)
		c=DS_RGB(0,0,0,0);
	}
PictureOption*	PictureOption::setSkipHeader(int a)
	{
	skipHeader=a;
	return this;
	}
PictureOption*	PictureOption::setThumbnailSize(Coord a)
	{
	thumbnailSize=a;
	return this;
	}




/*DS_RGB	PictureOption::fixPixel(const DSBitmap *bmp,int x,int y)
	{
	Coord p(x,y);
	if(thumbnailSize>zeroCoord)
		{
		p=p*thumbnailSize/bmp->getSize();
		}
	DS_RGB c=bmp->get(x,y);
	if(c.a==0&&!stableTransparent)
		c=DS_RGB(0,0,0,0);
	}*/










