

bool		isSuffix(ConstString path,ConstString suf);
ConstString getSuffix(ConstString path);
int 		getSuffix(ConstString path,char* out,int outSize);
ConstString	getName(ConstString path);
void		getName(ConstString path,char *out);
ConstString	getDirectory(ConstString path);
void		getDirectory(ConstString path,char *out);


int 	copyString(char *dest,const char *src,int max);
int		getStringLength(const char *str);


