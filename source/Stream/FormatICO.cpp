




Datafile*	loadICO(Input *in,int safeFail)
	{
	argCheck(in==NULL);
	report(1,"Loading ICO.");
	in->setSafeFail(safeFail);
	
	
	if(in->getInt(2)!=0)
		{
		safeFatalError("Verify 0 Failed.");
		delete in;
		return NULL;
		}
		
	Datafile *df=new Datafile();
	Text *infoText=new Text("==ICO File Information==\n");
	
	int iconType=in->getInt(2);
	if(iconType<1||iconType>2)	//1=icon 2=cursor
		{
		safeError("Undefined iconType %d.",iconType);
		iconType=1;
		}
	int iconNum=in->getInt(2);
	if(iconNum<1)
		{
		safeError("iconNum=%d",iconNum);
		iconNum=1;
		}
	infoText->addString("Loading ICO (%s) [%d]\n",iconType==2?"cursor":"icon",iconNum);
	
	struct IconInfo{
		Coord	size;
		int		paletteNum;
		Coord	cursorPos;
		int		plainNum;
		int		bitDepth;
		int		byteSize;
		int		offset;
		} *iconInfo;
	iconInfo=dalloc<IconInfo>(iconNum);
	
	for(int iconIndex=0;iconIndex<iconNum;iconIndex++)
		{
		iconInfo[iconIndex].size=in->getCoord(1);
		if(iconInfo[iconIndex].size.x==0)
			iconInfo[iconIndex].size.x=256;
		if(iconInfo[iconIndex].size.y==0)
			iconInfo[iconIndex].size.y=256;
		iconInfo[iconIndex].paletteNum=in->getInt(1);
		if(iconInfo[iconIndex].paletteNum==0)
			iconInfo[iconIndex].paletteNum=256;
		if(in->getInt(1)!=0)
			safeError("Not reserved.");
		if(iconType==1)
			{
			iconInfo[iconIndex].plainNum=in->getInt(2);
			iconInfo[iconIndex].bitDepth=in->getInt(2);
			}
		else
			iconInfo[iconIndex].cursorPos=in->getCoord(2);
		iconInfo[iconIndex].byteSize=in->getInt(4);
		iconInfo[iconIndex].offset=in->getInt(4);
		}
		
	for(int iconIndex=0;iconIndex<iconNum;iconIndex++)
		{
		report("0at [%d] %08X %08X",iconIndex,in->getPosition(),iconInfo[iconIndex].offset);
		if(in->getPosition()<iconInfo[iconIndex].offset)
			in->getIgnore(iconInfo[iconIndex].offset-in->getPosition());
		
		report("1at %08X",in->getPosition());
		
		int peek=in->getInt(1);
		in->unget(&peek,1);
		report("peek %02X at %08X",peek,in->getPosition());

		Picture *pic;
		if(peek==137)
			{
			pic=loadPicture(new InputEmptyMask(in),bitmapPNG,safeFail);
			if(pic->getSize()!=iconInfo[iconIndex].size)
				safeError("Expect size failed.");
			infoText->addString("[%d]: %d*%d PNG\n\n",iconIndex,iconInfo[iconIndex].size.x,iconInfo[iconIndex].size.y);
			}
		else
			{
			/*Datafile *bmpDf=loadBMP(new InputEmptyMask(in),safeFail,true);
			pic=bmpDf->getPic(-1)->clone();
			DSBitmap *bmp=pic->getEditableBitmap();
			delete bmpDf;*/
			//loadBMP
			//DIB Header
			report("loadBMP %08X",in->getPosition());
			int DIBH;
			if((DIBH=in->getInt(4))!=40)
				safeFatalError("Unsupported DIBH Length. %d",DIBH);
			Coord size=in->getCoord(4);
			size.y/=2;
			if(size!=iconInfo[iconIndex].size)
				safeError("Expect size failed.");
			if(in->getInt(2)!=1)
				safeError("Unexpected LayerNum.");
			int colorDepth=in->getInt(2);
			int compression=in->getInt(4);
			if(compression!=0&&compression!=3)
				safeFatalError("Unsupported Compression.");
			in->getInt(4); //bitmapSize
			in->getInt(4); //image resolution x&y
			in->getInt(4);
			int paletteNum=in->getInt(4);
			in->getInt(4);//Important ColorNum

			if(safeFail>=3&&0)
				{
				delete in;
				return NULL;
				}

			infoText->addString("BMP %d*%d bitDepth:%d.\n",size.x,size.y,colorDepth);

			DS_RGB *palette=NULL;
			if(paletteNum==0&&colorDepth<16)
				paletteNum=1<<colorDepth;
			if(paletteNum!=0)
				{
				if(colorDepth>=16)
					safeError("Unsupported palette with colorDepth >= 16.");
				palette=dalloc<DS_RGB>(paletteNum);
				for(int i=0;i<paletteNum;i++)
					{
					palette[i]=in->getBGR();
					in->getIgnore(1);
					}
				infoText->addString("Palette[%d].\n",paletteNum);
				}
			
			if(compression==3)
				{
				in->getIgnore(12);
				compression=0;
				}
			
			int UD=false;
			if(size.y<0)
				{size.y*=-1;UD=true;}

			DSBitmap *bmp=new DSBitmap(size);
			int endianSetting=dlib::bitLeft;

			switch(colorDepth)
				{
			case 32:
				for(int y=0;y<size.y;y++)
				for(int x=0;x<size.x;x++)
					{
					bmp->put(Coord(x,UD?y:size.y-y-1),in->getBGR());
					int alpha=in->getInt(1);
					bmp->setAlpha(Coord(x,UD?y:size.y-y-1),alpha);
					}
				if(bmp->isFullTransparent())
					{
					bmp->ignoreAlpha();
					report("Full transparent.");
					infoText->addString("Full transparent.\n");
					}
				break;
			case 24:
				for(int y=0;y<size.y;y++)
					{
					for(int x=0;x<size.x;x++)
						bmp->put(Coord(x,UD?y:size.y-y-1),in->getBGR());
					in->getIgnore((4-size.x*3%4)%4);
					}
				break;
			case 1:
			case 4:
			case 8:
				for(int y=0;y<size.y;y++)
					{
					in->checkBits();
					for(int x=0;x<size.x;x++)
						{
						int index=in->getBits(colorDepth);
						if(index>=paletteNum)
							{
							safeError("Palette overflow.");
							index=0;
							}
						bmp->put(Coord(x,UD?y:size.y-y-1),palette[index]);
						}
					in->alignBits();
					in->getIgnore((4-size.x*colorDepth/8%4)%4);
					}
				break;
			case 16:
			default:
				safeFatalError("Unsupported colorDepth %dbpp.",colorDepth);
				}

			//in->getEnd();
			int overallByteDepth=3+!bmp->isFullOpaque();
			infoText->addString("%lld/%lld Overall compreRate=%5.2f%% in %dbits.\n"
									,in->getPosition(),(long long)overallByteDepth*size.area()
									,(double)in->getPosition()/overallByteDepth/size.area()*100.0,overallByteDepth*8);
			if(palette!=NULL)
				dfree(palette);
			report("3at %08X",in->getPosition());
			
			//alpha
			for(int y=0;y<size.y;y++)
				{
				in->checkBits();
				for(int x=0;x<size.x;x++)
					{
					if(in->getBits(1))
						bmp->setAlpha(Coord(x,UD?y:size.y-y-1),0);
					}
				in->alignBits();
				in->getIgnore((4-size.x/8%4)%4);
				}
			report("4at %08X",in->getPosition());
			pic=new Picture(bmp);
			infoText->addString("[%d]: %d*%d BMP\n\n",iconIndex,size.x,size.y);
			}
		pic->setName("ICO Picture");
		df->addData(pic);
		}
	
	
	dfree(iconInfo);
	delete in;

	infoText->setName("ICO Information");
	df->addData(infoText);
	report(-1,"Loaded [%d].",iconNum);
	return df;
	}







void	DSBitmap::saveICO(Output *out,PictureOption *option)const
	{
	argCheck(out==NULL||size>Coord(256));
	report(1,"Saving ICO.");
	
	if(option==NULL)
		option=new PictureOption();
		
	option->bitDepth=32;
	option->usePalette=0;
	option->grayScale=0;
	option->alphaChannel=1;
	
	out->putInt(0,2);
	out->putInt(1,2);//ico cur
	out->putInt(1,2);//Num
	
	out->putCoord(size%Coord(256),1);
	out->putInt(0,1);//palette
	out->putInt(0,1);//reserve
	out->putInt(1,2);//plainNum
	out->putInt(24,2);//bitDepth
	
	IOBuffer *buf=new IOBuffer(size.area()*4+100);
	/*if(size<Coord(256)&&0)
		{
		//DIB Header
		out->putInt(40,4);
		out->putInt(size.x,4);
		out->putInt(size.y*2,4);
		out->putInt(1,2);
		out->putInt(option->bitDepth,2);
		out->putInt(0,4);
		out->putInt(size.area()*option->bitDepth/8,4);
		out->putInt(0,4);
		out->putInt(0,4);//image resolution x&y
		out->putInt(0,4);//palette
		out->putInt(0,4);
		
		//data
		switch(option->bitDepth)
			{
		case 32:
			for(int y=size.y-1;y>=0;y--)
			for(int x=0;x<size.x;x++)
				{
				DS_RGB c=get(x,y);
				if(c.a!=0||option->stableTransparent)
					out->putBGR(c);
				else
					out->putBGR(black);
				out->putInt(c.a,1);
				}
			break;
		case 24:
			for(int y=size.y-1;y>=0;y--){
			for(int x=0;x<size.x;x++)
				out->putBGR(get(x,y));
			if((4-size.x*3%4)%4!=0)
				out->putInt(0,(4-size.x*3%4)%4);
			}
			break;
		case 1:
		case 4:
		case 8:
		case 16:
		default:
			error("Unsupported colorDepth.");
			}
		for(int y=size.y-1;y>=0;y--)
			{
			int endianSetting=dlib::bitLeft;
			out->checkBits();
			for(int x=0;x<size.x;x++)
				out->putBits(get(x,y).a<128,1);
			out->alignBits();
			out->putInt(0,(4-size.x/8%4)%4);
			}
		}
	else*/
		savePNG(new OutputEmptyMask(buf),option);
	
	out->putInt(buf->getNumber(),4);
	out->putInt(22,4);

	buf->dump(new OutputEmptyMask(out));
	delete buf;
	delete out;
	report(-1,"Saved %d*%d.",size.x,size.y);
	}















