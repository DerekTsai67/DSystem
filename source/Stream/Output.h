

#define putInt(x,y) 		putInt_(x,y,endianSetting)
#define putCoord(x,y) 		putCoord_(x,y,endianSetting)
#define putBits(x,y) 		putBits_(x,y,endianSetting)

class Output{
	public:
	 Output(int byteMax,bool close);
virtual	~Output();
void	setType(int t);
void	putInt_(int data,int size,int endianSetting);
void	putCoord_(Coord data,int size,int endianSetting);
void	putRGB(DS_RGB data);
void	putBGR(DS_RGB data);
void	putDouble(double d);
void	checkBits();
void	alignBits_(int endianSetting);
void	putBits_(int data,int num,int endianSetting);
void	putString(ConstString data,...);
void	putDecimal(int d);
void	putText(ConstString data,...);
void	putEndLine();
void	putBytes(const void *data,int size);
virtual int 	putRaw(const Byte *data,int size)=0;

	protected:
int 	type;
int 	byteMax;
unsigned char	*byteBuffer;
unsigned long long	bitBuffer;
int 	bitIndex,byteIndex;
bool	closeSource;
bool	dead;
	};


class OutputMask:public Output{
	public:
	 OutputMask(Output *o,int max,bool close);
virtual	~OutputMask();
	protected:
Output	*out;
	};

class OutputEmptyMask:public OutputMask{
	public:
	 OutputEmptyMask(Output *o);
int 	putRaw(const Byte *data,int size);
	};







