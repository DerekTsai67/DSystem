




void	dump(Input *in,Output *out);



class OutputStd:public Output{
	public:
	 OutputStd();
	~OutputStd();
int 	putRaw(const Byte *data,int size);
	private:
int num;
	};




class InputBorder:public Input{
	public:
	 InputBorder(Input *in,Byte *border,int borderMax);
	~InputBorder();
void	cutBorder();
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
	private:
Byte	*border;
int		borderMax;
Queue<Byte>	*queue;
	};




class InputArray:public Input{
	public:
	InputArray(Byte *arr,int num,bool closeSource=true);
	~InputArray();
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
	private:
Byte	*data;
int		index,max;
	};
	
class OutputArray:public Output{
	public:
	OutputArray(Byte *arr,int max);
int 	putRaw(const Byte *data,int size);
	private:
Byte	*data;
int		index,max;
	};








class IOBuffer:public Input,public Output{
	public:
	IOBuffer(int num);
	~IOBuffer();
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
void	setPosition(int pos);
int 	putRaw(const Byte *data,int size);

int 	getNumber();
void	cutBuffer();


	private:
Byte	*buffer;
int		inIndex,outIndex,dataMax;
	};





