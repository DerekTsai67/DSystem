





	InputB64::InputB64(Input *i,bool close):Input(i,3,close)
	{
	end=false;
	}

int 	InputB64::getRaw(Byte *dest,int size)
	{
	if(in->empty())
		{
		end=true;
		return 0;
		}
	int num=0;
	for(int i=0;i<4;i++)
		{
		int c=0;
		int t=in->getInt(1);
		if(t>='A'&&t<='Z')
			c=t-'A';
		else if(t>='a'&&t<='z')
			c=t-'a'+26;
		else if(t>='0'&&t<='9')
			c=t-'0'+52;
		else if(t=='+')
			c=62;
		else if(t=='/')
			c=63;
		else if(t=='=')
			end=true;
		else
			{
			end=true;
			safeError("Unexpected character %S.",viewData(&t,1));
			}

		num|=c<<((3-i)*6);
		}
	memcpy(byteBuffer+byteIndex,(Byte*)&num+2,1);
	byteIndex++;
	memcpy(byteBuffer+byteIndex,(Byte*)&num+1,1);
	byteIndex++;
	memcpy(byteBuffer+byteIndex,(Byte*)&num,1);
	byteIndex++;
	return 0;
	}

bool	InputB64::emptyRaw()
	{
	return end;
	}


















