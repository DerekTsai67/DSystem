


#define SYNC

struct JPGComponent{
	int id;
	Coord size,factor,factorMax,zigzagMax;
	int quantiIndex;
	int dcIndex,acIndex;
	int ***zigzag;
	double ***coefficient;
	int lastCode;
	};

struct DctData{
	JPGComponent *component;
	int componentNumber;
	double **cosTable2;
	int **quantiTable;
	Coord maxFactor;
	Coord size;
	DSBitmap *bmp;
	int index;
	int *synchro;
	};

bool	skipDepress=false;

const int dctThreadNumber=8;


void	inverseDCT1(void *data_);
void	inverseDCT2(void *data_);

const int zigzagIndex[8][8]={
	{ 0, 1, 5, 6,14,15,27,28},
	{ 2, 4, 7,13,16,26,29,42},
	{ 3, 8,12,17,25,30,41,43},
	{ 9,11,18,24,31,40,44,53},
	{10,19,23,32,39,45,52,54},
	{20,22,33,38,46,51,55,60},
	{21,34,37,47,50,56,59,61},
	{35,36,48,49,57,58,62,63}
	};


Datafile*	loadJPG(Input *originIn,int safeFail)
	{
	argCheck(originIn==NULL);
	report(1,"Loading JPG...");
	int startClock=clock();

	const int endianSetting=dlib::bigEndian|dlib::bitLeft;
	Text *infoText=new Text();
	infoText->addString("==JPG File Information==\n");

	Datafile *datafile=new Datafile();

	InputJPG *in=new InputJPG(originIn,false);
	int segment;
	in->getSegment(&segment);
	if(segment!=0xFFD8)
		error("Verify failed %04x",segment);

	int bitDepth=0;
	Coord size=zeroCoord;

	int componentNumber=0;
	typedef JPGComponent Component;
	Component *component=NULL;
	Coord maxFactor=zeroCoord;

	int successiveHigh=0,successiveLow=0;

	int interleaveNumber=0;
	int *interleaveIndex=NULL;
	int zigzagStart=0,zigzagEnd=63;
	int mcuInterval=0;
	Coord mcuIndex=zeroCoord,mcuMax=zeroCoord;

	int *quantiTable[4];
	for(int i=0;i<4;i++)
		quantiTable[i]=NULL;

	HuffTree *huffTreeTable[2][4];
	for(int x=0;x<2;x++)
	for(int y=0;y<4;y++)
		huffTreeTable[x][y]=NULL;

	while(segment!=0xFFD9)
		{
		ConstString markerName[]={
			"SOF0 baseline"
			,"SOF1 Ext sequential huffman","SOF2 Progressive huffman","SOF3 Lossless huffman"
			,"DHT huffTable"
			,"SOF5 Diffsequen huffman","SOF6 Diffprog huffman","SOF7 Diffless huffman"
			,"JPG reserved"
			,"SOF9 Ext sequential arithmetic","SOF10 Progressive arithmetic","SOF11 Lossless arithmetic"
			,"DAC arithCode"
			,"SOF13 Diffsequen arithmetic","SOF14 Diffprog arithmetic","SOF15 Diffless arithmetic"
			,"RST0 restart","RST1 restart","RST2 restart","RST3 restart","RST4 restart","RST5 restart","RST6 restart","RST7 restart"
			,"SOI","EOI","SOS","DQT","DNL","DRI","DHP","EXP"
			,"APP0","APP1","APP2","APP3","APP4","APP5","APP6","APP7","APP8","APP9","APP10","APP11","APP12","APP13","APP14","APP15"
			,"JPG0","JPG1","JPG2","JPG3","JPG4","JPG5","JPG6","JPG7","JPG8","JPG9","JPG10","JPG11","JPG12","JPG13","COM","XXX"
			};
		int length=in->getSegment(&segment);
		if(mcuIndex==zeroCoord||!(segment==0xFF00||(segment>=0xFFD0&&segment<=0xFFD7)))
			{
			if(segment>=0xFFC0&&segment<=0xFFFF)
				{
				report("seg %S %d",markerName[segment-0xFFC0],length);
				infoText->addString("Segment %S length=%d\n",markerName[segment-0xFFC0],length);
				}
			else
				{
				report("seg %04X %d",segment,length);
				infoText->addString("Segment %04X length=%d\n",segment,length);
				}
			}
		switch(segment)
			{
		case 0xFFD8://SOI Start of image
			safeError("reSOI");
			break;

		case 0xFFC0://SOF Start of frame
		case 0xFFC1:
		case 0xFFC2:
			if(bitDepth!=0)
				{
				safeError("Repeated SOF.");
				in->terminateSegment();
				break;
				}
			bitDepth=in->getInt(1);
			if(bitDepth!=8)
				safeFatalError("Unsupported bitDepth.");
			size.y=in->getInt(2);
			size.x=in->getInt(2);
			if(!(size>zeroCoord))
				safeFatalError("Unsupported size.");
			if(size>Coord(5000,5000))
				skipDepress=true;

			componentNumber=in->getInt(1);
			if(componentNumber<1)
				safeFatalError("Unsupported componentNumber.");
			if(componentNumber!=1&&componentNumber!=3)
				safeError("Unsupported componentNumber. %d",componentNumber);
			component=dalloc<Component>(componentNumber);

			maxFactor=zeroCoord;

			if(safeFail>=3)
				break;

			for(int ci=0;ci<componentNumber;ci++)
				{
				component[ci].id=in->getInt(1);
				int temp=in->getInt(1);
				component[ci].factor.x=temp>>4;
				component[ci].factor.y=temp&0xF;
				component[ci].quantiIndex=in->getInt(1);
				if(!(component[ci].factor>zeroCoord&&component[ci].factor<=Coord(4,4))
				 || component[ci].quantiIndex>=4)
				 	{
					safeFatalError("Unexpected factor or quantiIndex.");
					break;
					}
				maxFactor|=component[ci].factor;
				}
			for(int ci=0;ci<componentNumber;ci++)
				{
				component[ci].size=(size*component[ci].factor-monoCoord)/maxFactor+monoCoord;
				component[ci].factorMax=(component[ci].size-monoCoord)/Coord(8,8)+monoCoord;
				component[ci].zigzagMax=((component[ci].size-monoCoord)/(component[ci].factor*8)+monoCoord)*component[ci].factor;
				component[ci].zigzag=dalloc<int**>(component[ci].zigzagMax.y);
				if(!skipDepress)
				component[ci].coefficient=dalloc<double**>(component[ci].zigzagMax.y);
				
				for(int yi=0;yi<component[ci].zigzagMax.y;yi++)
					{
					component[ci].zigzag[yi]=dalloc<int*>(component[ci].zigzagMax.x);
					if(!skipDepress)
					component[ci].coefficient[yi]=dalloc<double*>(component[ci].zigzagMax.x);
					for(int xi=0;xi<component[ci].zigzagMax.x;xi++)
						{
						component[ci].zigzag[yi][xi]=dalloc<int>(64);
						if(!skipDepress)
						component[ci].coefficient[yi][xi]=dalloc<double>(64);
						}
					}
				report("  Component[%d] id=%d factor=%d,%d Tqi=%d.",ci,component[ci].id,component[ci].factor.x,component[ci].factor.y,component[ci].quantiIndex);
				infoText->addString("    Component[%d] id=%d factor=%d,%d Tqi=%d.\n",ci,component[ci].id,component[ci].factor.x,component[ci].factor.y,component[ci].quantiIndex);
				}
			mcuMax=(size-monoCoord)/(maxFactor*8)+monoCoord;
			report("  SOF Precision=%d size=%d*%d component[%d].",bitDepth,size.x,size.y,componentNumber);
			infoText->addString("  SOF Precision=%d size=%d*%d component[%d].\n",bitDepth,size.x,size.y,componentNumber);
			break;

		case 0xFFDA://SOS Start of scan
			if(bitDepth==0)
				{
				safeError("No SOF before SOS.");
				in->terminateSegment();
				break;
				}
			interleaveNumber=in->getInt(1);
			if(interleaveIndex!=NULL)
				dfree(interleaveIndex);
			interleaveIndex=dalloc<int>(interleaveNumber);
			for(int ii=0;ii<interleaveNumber;ii++)
				{
				int id=in->getInt(1);
				int componentIndex=0;
				for(componentIndex=0;componentIndex<componentNumber;componentIndex++)
					if(component[componentIndex].id==id)
						break;
				if(componentIndex>=componentNumber)
					{
					safeFatalError("Unknown component ID.");
					break;
					}
				interleaveIndex[ii]=componentIndex;

				int temp=in->getInt(1);
				if((temp|0x33)!=0x33)
					safeFatalError("Unexpected tableIndex.");
				component[componentIndex].dcIndex=temp>>4;
				component[componentIndex].acIndex=temp&0xF;
				report("  InterComp[%d] id=%d Index=%d/%d.",componentIndex,id,component[componentIndex].dcIndex,component[componentIndex].acIndex);
				infoText->addString("    InterComp[%d] id=%d Index=%d/%d.\n",componentIndex,id,component[componentIndex].dcIndex,component[componentIndex].acIndex);
				}
			zigzagStart=in->getInt(1);
			zigzagEnd=in->getInt(1);
			{
			int temp=in->getInt(1);
			successiveHigh=temp>>4;
			successiveLow=temp&0xF;
			if(successiveHigh==0)
				successiveHigh=bitDepth+3;
			}
			mcuIndex=zeroCoord;
			report("  SOS interNum=%d zigzag=%d~%d sucess=%d~%d.",interleaveNumber,zigzagStart,zigzagEnd,successiveHigh,successiveLow);
			infoText->addString("  SOS interNum=%d zigzag=%d~%d sucess=%d~%d.\n",interleaveNumber,zigzagStart,zigzagEnd,successiveHigh,successiveLow);
			break;

		case 0xFF00://binary data
			{
			if(interleaveNumber<=0)
				{
				safeFatalError("No SOS before Binary Data.");
				int temp;
				while(in->getBytes(&temp,4,true));
				break;
				}
			
			


			for(int ii=0;ii<interleaveNumber;ii++)
				{
				Component &nowComponent=component[interleaveIndex[ii]];
				if((huffTreeTable[0][nowComponent.dcIndex]==NULL&&zigzagStart==0)
				 ||(huffTreeTable[1][nowComponent.acIndex]==NULL&&zigzagEnd>=1))
					safeFatalError("HuffTree not ready. %d %d",nowComponent.dcIndex,nowComponent.acIndex);
				nowComponent.lastCode=0;
				}
				
			bool nonSuccessing=(successiveHigh==bitDepth+3);
			if(!nonSuccessing&&(successiveHigh-successiveLow)!=1)
				safeError("Successing with length!=1.");
			
			Coord mcuMaxEff=mcuMax;
			if(interleaveNumber==1)
				mcuMaxEff=component[interleaveIndex[0]].factorMax;
			
			int mcuIntervalEff=mcuInterval;
			if(mcuIntervalEff==0)
				mcuIntervalEff=mcuMaxEff.area();
			int mcuCount=0;
			
			int runLength=0;

			if(safeFail>=3)
				break;
			in->checkBits();
			for(;mcuIndex.y<mcuMaxEff.y&&mcuCount<mcuIntervalEff;mcuIndex.y++)
				{
				if(mcuIndex.x==mcuMaxEff.x)
					mcuIndex.x=0;
				for(;mcuIndex.x<mcuMaxEff.x&&mcuCount<mcuIntervalEff;mcuIndex.x++)
					{
					for(int ii=0;ii<interleaveNumber;ii++)
						{
						Component &nowComponent=component[interleaveIndex[ii]];
						Coord nf=mcuIndex*nowComponent.factor;
						HuffTree *dcHuffTree=huffTreeTable[0][nowComponent.dcIndex];
						HuffTree *acHuffTree=huffTreeTable[1][nowComponent.acIndex];

						Coord factorMax=nowComponent.factor;
						if(interleaveNumber==1)
							{
							factorMax=monoCoord;
							nf=mcuIndex;
							}
						for(int yi=0;yi<factorMax.y;yi++)
						for(int xi=0;xi<factorMax.x;xi++)
							{
							if(zigzagStart==0)
								{
								if(nonSuccessing)
									{
									int ssss=dcHuffTree->getData(in,endianSetting);
									if(ssss>(successiveHigh-successiveLow))
										safeError("DC SSSS overflow. %d",ssss);
									int code=in->getBits(ssss);
									if(!(code&(1<<(ssss-1))))
										code=(code|~((1<<ssss)-1))+1;
									nowComponent.lastCode+=code<<successiveLow;
									nowComponent.zigzag[nf.y+yi][nf.x+xi][0]=nowComponent.lastCode;
									}
								else
									{
									nowComponent.zigzag[nf.y+yi][nf.x+xi][0]|=in->getBits(1)<<successiveLow;
									}
								}
							for(int zi=zigzagStart+(zigzagStart==0);zi<=zigzagEnd;zi++)
								{
								int run=0,code=0;
								if(runLength>0)
									{
									run=zigzagEnd-zi+100;
									runLength--;
									}
								if(run==0)
									{
									int ssss=acHuffTree->getData(in,endianSetting);
									if(((ssss&0xF)==0)&&ssss!=0xF0)
										{
										ssss>>=4;
										runLength=((1<<ssss)|in->getBits(ssss));
										zi--;
										continue;
										}
									run=ssss>>4;

									ssss&=0xF;
									if(ssss>(successiveHigh-successiveLow))
										safeError("AC SSSS overflow.");
									code=in->getBits(ssss);
									if(!(code&(1<<(ssss-1))))
										code=(code|~((1<<ssss)-1))+1;
									}


								if(nonSuccessing)
									{
									zi+=run;
									run=0;
									}
								else
									{
									while(run>=0&&zi<=zigzagEnd)
										{
										if(nowComponent.zigzag[nf.y+yi][nf.x+xi][zi]!=0)
											{
											if(in->getBits(1))
												{
												if(nowComponent.zigzag[nf.y+yi][nf.x+xi][zi]>0)
													nowComponent.zigzag[nf.y+yi][nf.x+xi][zi]+=1<<successiveLow;
												else
													nowComponent.zigzag[nf.y+yi][nf.x+xi][zi]-=1<<successiveLow;
												}
											}
										else
											if(run==0)
												break;
											else
												run--;
										zi++;
										}
									}
								if(run==0&&zi<=zigzagEnd)
									nowComponent.zigzag[nf.y+yi][nf.x+xi][zi]=code<<successiveLow;
								}
							}
						}
					mcuCount++;
					}
				}
			in->alignBits();
			//in->terminateSegment();
			if(mcuIndex.y==mcuMaxEff.y)
				interleaveNumber=0;
			}
			break;
		case 0xFFD0:
		case 0xFFD1:
		case 0xFFD2:
		case 0xFFD3:
		case 0xFFD4:
		case 0xFFD5:
		case 0xFFD6:
		case 0xFFD7:
			break;

		case 0xFFD9://EOI End of image
			{
			report("EOI...");
			int dctClock=clock();
			
			
			if(skipDepress)
				{
				Picture *pict=new Picture(new DSBitmap(Coord(300,300)),10);
				pict->setName("JPG Picture");
				datafile->addData(pict);
				infoText->addString("Inverse DCT time:%dms.\n",(clock()-dctClock)*1000/CLOCKS_PER_SEC);
				break;
				}

			Coord zigzagCoord[64];
			FOR_COORD(c,Coord(8,8))
				zigzagCoord[zigzagIndex[c.y][c.x]]=c;
			END_FOR_COORD

			double cosTable[32];
			cosTable[0]=1.0;
			cosTable[1]=0.9807852804;
			cosTable[2]=0.92387953251;
			cosTable[3]=0.8314696123;
			cosTable[4]=0.70710678118;
			cosTable[5]=0.55557023302;
			cosTable[6]=0.38268343236;
			cosTable[7]=0.19509032201;
			cosTable[8]=0.0;
			for(int i=1;i<8;i++)
				{
				//cosTable[i]=cos(PI*i/16.0);
				cosTable[16-i]=cosTable[i]*-1;
				}
			for(int i=0;i<16;i++)
				cosTable[16+i]=cosTable[i]*-1;

			double **cosTable2=dalloc<double*>(64);
			for(int qi=0;qi<64;qi++)
				{
				cosTable2[qi]=dalloc<double>(64);
				for(int ei=0;ei<64;ei++)
					{
					cosTable2[qi][ei]=	cosTable[(zigzagCoord[ei].x*2+1)*zigzagCoord[qi].x%32]*
										cosTable[(zigzagCoord[ei].y*2+1)*zigzagCoord[qi].y%32]/4.0;
					if(!(zigzagCoord[qi]>zeroCoord))
						{
						if(zigzagCoord[qi]==zeroCoord)
							cosTable2[qi][ei]/=2.0;
						else
							cosTable2[qi][ei]/=1.41421356;
						}
					}
				}

			DSBitmap *bmp=new DSBitmap(size);

			Thread **dctThread=dalloc<Thread*>(dctThreadNumber);
			DctData *dctData=dalloc<DctData>(dctThreadNumber);
			int *synchro=dalloc<int>(dctThreadNumber);
			for(int ti=0;ti<dctThreadNumber;ti++)
				{
				dctData[ti].component=component;
				dctData[ti].componentNumber=componentNumber;
				dctData[ti].cosTable2=cosTable2;
				dctData[ti].quantiTable=quantiTable;
				dctData[ti].maxFactor=maxFactor;
				dctData[ti].size=size;
				dctData[ti].bmp=bmp;
				dctData[ti].index=ti;
				dctData[ti].synchro=synchro;
				dctThread[ti]=new Thread(inverseDCT1,dctData+ti,true);
				dctThread[ti]->launch(true);
				dctThread[ti]->once();
				}
			for(int ti=0;ti<dctThreadNumber;ti++)
				dctThread[ti]->waitOnce();
#ifndef SYNC
			for(int ti=0;ti<dctThreadNumber;ti++)
				{
				delete dctThread[ti];
				dctThread[ti]=new Thread(inverseDCT2,dctData+ti,true);
				dctThread[ti]->launch(true);
				dctThread[ti]->once();
				}
			for(int ti=0;ti<dctThreadNumber;ti++)
				dctThread[ti]->waitOnce();
#endif
			for(int ti=0;ti<dctThreadNumber;ti++)
				delete dctThread[ti];
			dfree(dctThread);
			dfree(dctData);
			dfree(synchro);

			for(int qi=0;qi<64;qi++)
				dfree(cosTable2[qi]);
			dfree(cosTable2);
			
			Picture *pic=new Picture(bmp,10);
			pic->setName("JPG Picture");
			datafile->addData(pic);
			infoText->addString("Inverse DCT time:%dms.\n",(clock()-dctClock)*1000/CLOCKS_PER_SEC);
			}
			break;

		case 0xFFDB://DQT Define quantization table
			while(!in->empty())
				{
				int temp=in->getInt(1);
				int depth=(temp>>4)+1;
				int index=temp&0xF;
				if(depth>2||index>=4)
					safeFatalError("Unexpected DQT depth/index.");

				int *&table=quantiTable[index];
				if(table!=NULL)
					dfree(table);
				table=dalloc<int>(64);
				for(int i=0;i<64;i++)
					table[i]=in->getInt(depth);
				}
			break;

		case 0xFFC4://DHT Define huffman table
			while(!in->empty())
				{
				int temp=in->getInt(1);
				int huffClass=temp>>4;
				int huffIndex=temp&0xF;
				if(huffClass>1||huffIndex>=4)
					{
					safeFatalError("Undefined huffTree class/Index.");
					break;
					}
				HuffTree *&huffTree=huffTreeTable[huffClass][huffIndex];
				if(huffTree!=NULL)
					delete huffTree;

				int	*lengthCount=dalloc<int>(16),totalCount=0;
				for(int li=0;li<16;li++)
					totalCount+=(lengthCount[li]=in->getInt(1));
				if(totalCount<=0||totalCount>256)
					{
					safeFatalError("HuffCount overflow.");
					break;
					}

				HuffInfo *huffInfo=dalloc<HuffInfo>(totalCount+1);
				int li=0;
				for(int vi=0;vi<totalCount;vi++)
					{
					while(lengthCount[li]==0&&li<16)li++;
					if(li==16)
						error("li overflow.");

					huffInfo[vi].data=in->getInt(1);
					huffInfo[vi].length=li+1;
					lengthCount[li]--;
					}
				huffInfo[totalCount].data=257;
				huffInfo[totalCount].length=li+1;
				huffTree=new HuffTree(huffInfo,totalCount+1);
				dfree(lengthCount);
				dfree(huffInfo);
				//huffTree->print();
				report("  HuffTable[%d][%d] implied.",huffClass,huffIndex);
				}
			break;

		case 0xFFDD://DRI Define restart interval
			mcuInterval=in->getInt(2);
			break;

		case 0xFFE0://APPn
		case 0xFFE1:
		case 0xFFE2:
		case 0xFFE3:
		case 0xFFE4:
		case 0xFFE5:
		case 0xFFE6:
		case 0xFFE7:
		case 0xFFE8:
		case 0xFFE9:
		case 0xFFEA:
		case 0xFFEB:
		case 0xFFEC:
		case 0xFFED:
		case 0xFFEE:
		case 0xFFEF:
			{
			if(length==0)
				{
				infoText->addString("  Empty APP%d.\n",segment-0xFFE0);
				break;
				}
			String appName(length);
			in->getString(appName,true);
			if(segment==0xFFE0&&appName=="JFIF")
				{
				in->getIgnore(7);
				Coord thumbSize=in->getCoord(1);
				if(thumbSize>zeroCoord)
					{
					DSBitmap *thumb=new DSBitmap(thumbSize);
					FOR_COORD(c,thumbSize)
						thumb->put(c,in->getRGB());
					END_FOR_COORD
					Picture *pic=new Picture(thumb,10);
					pic->setName("JFIF Thumbnail");
					datafile->addData(pic);
					}
				}
			/*else if(segment==0xFFE1&&verify(appName,"Exif"))
				{
				if(in->getPosition()%2==1)
					in->getIgnore(1);
				delete loadTIFF(new InputEmptyMask(in),safeFail);
				}*/
			else
				{
				infoText->addString("  Unknown APP%d:%S.\n",segment-0xFFE0,viewData(appName));
				in->terminateSegment();
				}
			}
			break;

		case 0xFFFE://COM
			{
			String text(length);
			in->getFixedString(text);
			report("COM %S",viewData(text));
			Text *textData=new Text();
			textData->setName("JPG COM");
			textData->addString("==JPG command==\n");
			textData->add(text);
			datafile->addData(textData);
			}
			break;

		default:
			{
			Byte *temp=dalloc<unsigned char>(length);
			in->getBytes(temp,length);
			warning("seg%04X :%S",segment,viewData(temp,length));
			dfree(temp);
			}
			break;
			}
		Text *last=in->getEnd(true);
		if(last!=NULL)
			{
			last->setName("Segment last");
			datafile->addData(last);
			}
		}

	delete in;
	Text *last=originIn->getEnd(true);
	if(last!=NULL)
		{
		last->setName("File last");
		datafile->addData(last);
		}


	if(component!=NULL)
		{
		for(int ci=0;ci<componentNumber;ci++)
			{
			for(int yi=0;yi<component[ci].zigzagMax.y;yi++)
				{
				for(int xi=0;xi<component[ci].zigzagMax.x;xi++)
					{
					dfree(component[ci].zigzag[yi][xi]);
					if(!skipDepress)
					dfree(component[ci].coefficient[yi][xi]);
					}
				dfree(component[ci].zigzag[yi]);
				if(!skipDepress)
				dfree(component[ci].coefficient[yi]);
				}
			dfree(component[ci].zigzag);
			if(!skipDepress)
			dfree(component[ci].coefficient);
			}
		dfree(component);
		}

	if(interleaveIndex!=NULL)
		dfree(interleaveIndex);

	for(int i=0;i<4;i++)
		if(quantiTable[i]!=NULL)
			dfree(quantiTable[i]);

	for(int x=0;x<2;x++)
	for(int y=0;y<4;y++)
		if(huffTreeTable[x][y]!=NULL)
			delete huffTreeTable[x][y];


	infoText->addString("%lld/%lld Overall compreRate=%5.2f%% in 24bits.\n"
							,originIn->getPosition(),(long long)3*size.area()
							,(double)originIn->getPosition()/3/size.area()*100.0);
	infoText->addString("Elapsed time:%dms.\n",(clock()-startClock)*1000/CLOCKS_PER_SEC);
	infoText->setName("JPG Information");

	datafile->addData(infoText);
	delete originIn;
	report(-1,"Loaded.");
	return datafile;
	}




void	inverseDCT1(void *data_)
	{
	DctData *data=(DctData*)data_;

	for(int ci=0;ci<data->componentNumber;ci++)
		{
		JPGComponent &component=data->component[ci];
		int *quanti=data->quantiTable[component.quantiIndex];
		if(quanti==NULL)
			{
			error("Uninstalled quaitiIndex.");
			break;
			}


		for(int zy=component.zigzagMax.y*data->index/dctThreadNumber;zy<component.zigzagMax.y*(data->index+1)/dctThreadNumber;zy++)
		for(int zx=0;zx<component.zigzagMax.x;zx++)
			{
			int *zigzag=component.zigzag[zy][zx];
			double *coefficient=component.coefficient[zy][zx];
			for(int qi=0;qi<64;qi++)
				{
				double amp=zigzag[qi]*quanti[qi];
				if(amp==0.0)
					continue;
				for(int ei=0;ei<64;ei++)
					coefficient[ei]+=data->cosTable2[qi][ei]*amp;
				}
			}
		}
#ifndef SYNC
	}
void	inverseDCT2(void *data_)
	{
	DctData *data=(DctData*)data_;

#else
	data->synchro[data->index]=1;
	for(int i=0;i<dctThreadNumber;i++)
		while(data->synchro[i]==0)
			sleep(2);
#endif

	if(data->componentNumber==1)
		{
		for(int zy=data->size.y*data->index/dctThreadNumber;zy<data->size.y*(data->index+1)/dctThreadNumber;zy++)
		for(int zx=0;zx<data->size.x;zx++)
			{
			int Y=data->component[0].coefficient[zy/8][zx/8][zigzagIndex[zy%8][zx%8]]+128.0;
			if(Y>255)Y=255;
			if(Y<0)Y=0;
			data->bmp->put(Coord(zx,zy),makeYCbCr(Y,128,128));
			}
		}
	if(data->componentNumber==3)
		{
		for(int zy=data->size.y*data->index/dctThreadNumber;zy<data->size.y*(data->index+1)/dctThreadNumber;zy++)
		for(int zx=0;zx<data->size.x;zx++)
			{
			Coord c=Coord(zx,zy);
			Coord cc=c*data->component[0].factor/data->maxFactor;
			double Y= data->component[0].coefficient[cc.y/8][cc.x/8][zigzagIndex[cc.y%8][cc.x%8]]+128.0;
			cc=c*data->component[1].factor/data->maxFactor;
			double Cb=data->component[1].coefficient[cc.y/8][cc.x/8][zigzagIndex[cc.y%8][cc.x%8]];
			cc=c*data->component[2].factor/data->maxFactor;
			double Cr=data->component[2].coefficient[cc.y/8][cc.x/8][zigzagIndex[cc.y%8][cc.x%8]];
			if(Y >255.0)Y =255.0;if(Y <0.0)Y =0.0;
			if(Cb>127.0)Cb=127.0;if(Cb<-128.0)Cb=-128.0;
			if(Cr>127.0)Cr=127.0;if(Cr<-128.0)Cr=-128.0;
			data->bmp->put(c,makeYCbCr(Y,Cb,Cr));
			
		if(0)
		data->bmp->put(c,DS_RGB(	data->component[0].zigzag[cc.y/8][cc.x/8][zigzagIndex[cc.y%8][cc.x%8]] * data->quantiTable[data->component[0].quantiIndex][zigzagIndex[cc.y%8][cc.x%8]]
								,	data->component[0].zigzag[cc.y/8][cc.x/8][zigzagIndex[cc.y%8][cc.x%8]] * data->quantiTable[data->component[0].quantiIndex][zigzagIndex[cc.y%8][cc.x%8]]
								,	data->component[0].zigzag[cc.y/8][cc.x/8][zigzagIndex[cc.y%8][cc.x%8]] * data->quantiTable[data->component[0].quantiIndex][zigzagIndex[cc.y%8][cc.x%8]]
								));
			}
		}
	}





void	DSBitmap::saveJPG(Output *out)const
	{
	error("Incomplated");
	argCheck(out==NULL);
	out->putInt(0xD8FF,2);//SOI
	
	//DQT
	//SOF
	//DHT
	//SOS
	//binary
	
	
	out->putInt(0xD9FF,2);//EOI
	
	delete out;
	}






	InputJPG::InputJPG(Input *i,bool close):Input(i,5,close)
	{
	//segmentIn();
	}

void	InputJPG::segmentIn()
	{
	int temp=in->getInt(1);
	if(temp!=0xFF)
		{
		byteBuffer[byteIndex++]=temp;
		segmentName=0xFF00;
		//report("into binary %04X %02X",segmentName,byteBuffer[0]);
		length=1;
		}
	else
		{
		segmentName=0xFF00+in->getInt(1);
		switch(segmentName)
			{
		case 0xFF00:
			byteBuffer[byteIndex++]=0xFF;
			length=1;
			break;
		case 0xFFD0:
		case 0xFFD1:
		case 0xFFD2:
		case 0xFFD3:
		case 0xFFD4:
		case 0xFFD5:
		case 0xFFD6:
		case 0xFFD7:
		case 0xFFD8:
		case 0xFFD9:
		case 0xFF01:
			length=0;
			break;
		default:
			 length=in->getInt(2)-2;
			 break;
			}
		}
	 }

	InputJPG::~InputJPG()
	{
	if(!empty()&&segmentName!=0xFF00)
		safeError("Data last %d.",length);
	}

int 	InputJPG::getRaw(Byte *dest,int size)
	{
	argCheck(dest==NULL||size<0);
	if(segmentName!=0xFF00)
		{
		int n=length>size?size:length;
		in->getBytes(dest,n);
		length-=n;

		return n;
		}
	else
		{
		int temp=in->getInt(1),temp2=0;
		if(temp==0xFF)
			if((temp2=in->getInt(1))!=0)
				{
				in->unget(&temp,1);
				in->unget(&temp2,1);
				length=0;
				return 0;
				}
		*dest=temp;
		//printf("binary %02X",*dest);
		return 1;
		}
	}

bool	InputJPG::emptyRaw()
	{
	return length==0;
	}

int 	InputJPG::getSegment(int *name)
	{
/*	if(!empty())
		{
		safeError("segment last.%04X %d",segmentName,length);
		terminateSegment();
		}*/
	getEnd();
	segmentIn();
	*name=segmentName;
	return length;
	}

void	InputJPG::terminateSegment()
	{
	if(segmentName!=0xFF00)
		getIgnore(length);
	else
		{
		int temp;
		while(getBytes(&temp,4,true)!=0);
		}
	}




