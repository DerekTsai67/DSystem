




	InputDeflate::InputDeflate(Input *i,bool close):Input(i,258,close)
	{
	setRemainSize(32768);
	state=0;
	liteTree=disTree=NULL;
	noncompressNum=0;

	in->checkBits();
	}

	InputDeflate::~InputDeflate()
	{
	getEnd();
	}



int 	InputDeflate::getRaw(Byte *dest,int size)
	{
	switch(state)
		{
	case 0:
		{
		HuffInfo liteLen[288],disLen[33];
		int li=0,di=0;
		for(int i=0;i<288;i++)
			{
			liteLen[i].length=0;
			liteLen[i].data=i;
			}
		for(int i=0;i<33;i++)
			{
			disLen[i].length=0;
			disLen[i].data=i;
			}
		state=1+in->getBits(1);
		int blockType=in->getBits(2);
		switch(blockType)
			{
		case 0b11:
		default:
			safeFatalError("Undefined compression method 0b11.");
			return 0;
		case 0b00:
			{
			in->alignBits();
			noncompressNum=in->getInt(2);
			int nlen=in->getInt(2);
			if(noncompressNum+nlen!=0xFFFF)
				safeError("Noncompress checksum failed. %d %d",noncompressNum,nlen);
			return 0;
			}
		case 0b01:
			for(int i=0;i<144;i++)
				liteLen[i].length=8;
			for(int i=144;i<256;i++)
				liteLen[i].length=9;
			for(int i=256;i<280;i++)
				liteLen[i].length=7;
			for(int i=280;i<288;i++)
				liteLen[i].length=8;
			li=288;
			for(int i=0;i<32;i++)
				disLen[i].length=5;
			di=32;
			break;
		case 0b10:
			{
			li=in->getBits(5)+257;
			di=in->getBits(5)+1;

			HuffInfo codeLen[19];
			int cli[19]={16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15};
			int cc=in->getBits(4)+4;
			for(int i=0;i<cc;i++)
				{
				codeLen[i].length=in->getBits(3);
				codeLen[i].data=cli[i];
				}
			for(int i=cc;i<19;i++)
				{
				codeLen[i].length=0;
				codeLen[i].data=cli[i];
				}
			sort(codeLen,19,sizeof(HuffInfo),compareHuffInfoData);
			HuffTree *codeTree=new HuffTree(codeLen,19);
			int *huffLen=dalloc<int>(li+di);
			for(int hi=0;hi<li+di;)
				{
				int l;
				l=codeTree->getData(in);

				if(l<16)
					{
					huffLen[hi++]=l;
					continue;
					}
				int n;
				switch(l)
					{
				case 16:
					n=3+in->getBits(2);
					if(hi<=0)
						{
						safeFatalError("Repeat codeLen when hi==0.");
						return 0;
						}
					for(int i=0;i<n;i++)
						huffLen[hi+i]=huffLen[hi-1];
					break;
				case 17:
					n=3+in->getBits(3);
					for(int i=0;i<n;i++)
						huffLen[hi+i]=0;
					break;
				case 18:
					n=11+in->getBits(7);
					for(int i=0;i<n;i++)
						huffLen[hi+i]=0;
					break;
				default:
					error("Undefined HuffLen code %d.",l);
					break;
					}
				hi+=n;
				}
			for(int i=0;i<li;i++)
				liteLen[i].length=huffLen[i];
			for(int i=0;i<di;i++)
				disLen[i].length=huffLen[li+i];
			dfree(huffLen);
			delete codeTree;
			break;}
			}
		liteTree=new HuffTree(liteLen,li);
		disTree=new HuffTree(disLen,di);
		}
	case 1:
	case 2:
		if(liteTree==NULL)
			{
			if(noncompressNum==384)
				safeFatalError("test 384");
			int num=noncompressNum;
			if(num>byteMax)
				num=byteMax;
			in->getBytes(byteBuffer+byteIndex,num);
			byteIndex+=num;
			noncompressNum-=num;
			if(noncompressNum==0)
				{
				in->checkBits();
				break;
				}
			return 0;
			}
		{
		int data;
		data=liteTree->getData(in);
		if(data<256)
			{
			memcpy(byteBuffer+byteIndex,&data,1);
			byteIndex+=1;
			}
		else if (data==256)
			break;
		else
			{
			int len=0;
			if(data<=264)
				len=data-254;
			else if(data<285)
				{
				len=7;
				for(int i=261;i<data;i++)
					len+=1<<((i-261)/4);
				len+=in->getBits((data-261)/4);
				}
			else if(data==285)
				len=258;
			else
				{
				safeFatalError("Undefined literal %d.",data);
				return 0;
				}

			data=disTree->getData(in);

			int dis=0;
			if(data<2)
				dis=data+1;
			else
				{
				dis=3;
				for(int i=2;i<data;i++)
					dis+=1<<(i/2-1);
				dis+=in->getBits(data/2-1);
				}
			if(byteIndex+len>byteMax)
				error("InputDeflate buffer overflow 3.");
			while(len>0)
				{
				int num=len>dis?dis:len;
				memcpy(byteBuffer+byteIndex,byteBuffer+byteIndex-dis,num);
				byteIndex+=num;
				dis+=num;
				len-=num;
				}
			}
		return 0;
		}
	case 3:
	default:
		error("InputDeflate getRaw overflow.");
		}
	if(liteTree!=NULL)
		{
		delete liteTree;
		delete disTree;
		liteTree=disTree=NULL;
		}
	if(state==1)
		state=0;
	else
		{
		state=3;
		in->alignBits();
		}
	return 0;
	}
bool	InputDeflate::emptyRaw()
	{
	return state==3;
	}


	InputZlib::InputZlib(Input *i,bool close):Input(i,0,close)
	{
	int cmf=in->getInt(1);
	int flg=in->getInt(1);
	if((cmf&0x0F)!=8)
		safeFatalError("Undefined compression method %d",cmf&0x0F);
	int bufferSize=1<<((cmf>>4)+8);
	if(bufferSize>32768)
		safeFatalError("Undefined bufferSize %d",bufferSize);
	if((cmf*256+flg)%31!=0)
		safeError("FCHECK checksum failed.");
	if(flg&0x20)
		safeFatalError("FDICT Unsupported. %02x %02x",cmf,flg);

	deflate=new InputDeflate(in,false);

	s1=1;
	s2=0;
	}

	InputZlib::~InputZlib()
	{
	delete deflate;
	int checksum=1;
	if((checksum=in->getInt(4))!=((s2<<16)+s1))
		safeError("Adler checksum failed. %08X %08X",checksum,(s2<<16)+s1);
	}
int 	InputZlib::getRaw(Byte *dest,int size)
	{
	size=deflate->getBytes(dest,size,true);

	for(int i=0;i<size;i++)
		{
		s1=(s1+((unsigned char*)dest)[i])%65521;
		s2=(s2+s1)%65521;
		}
	return size;
	}
bool	InputZlib::emptyRaw()
	{
	return deflate->empty();
	}










	OutputDeflate::OutputDeflate(Output *o,int bm,bool close):OutputMask(o,100000,close)
	{
	lite=dalloc<int>(byteMax);
	dis=dalloc<int>(byteMax/3+10);
	ni=bi=bl=li=di=0;
	out->checkBits();
	}

	OutputDeflate::~OutputDeflate()
	{
	liteProcess(true);

	out->alignBits();
	dfree(lite);
	dfree(dis);
	}

int OutputDeflate::putRaw(const Byte *data,int size)
	{
	int n=size>byteMax-byteIndex-ni?byteMax-byteIndex-ni:size;
	memcpy(byteBuffer+ni+byteIndex,data,n);
	byteIndex+=n;
	if(byteIndex+ni==byteMax)
		liteProcess(false);
	return n;
	}

struct DeflateInfo{
	int pos;
	DeflateInfo ***next;
	};

void	dfreeDeflateInfo(DeflateInfo *info)
	{
	if(info->next!=NULL)
		{
		for(int x=0;x<16;x++)
		if(info->next[x]!=NULL)
			{
			for(int y=0;y<16;y++)
			if(info->next[x][y]!=NULL)
				{
				dfreeDeflateInfo(info->next[x][y]);
				}
			dfree(info->next[x]);
			}
		dfree(info->next);
		}
	dfree(info);
	}


void	OutputDeflate::liteProcess(bool end)
	{
	if(byteIndex==0)
		return;

//#define DIS

#ifdef DIS
	
	const int liteMax=8;
	
	DeflateInfo *head=dalloc<DeflateInfo>(1);
	head->pos=-1;
	head->next=dalloc<DeflateInfo**>(16);

	for(int i=0;i<byteIndex;)
		{
		DeflateInfo *info=head;
		int len=0;
		while(i+len<byteIndex&&len<liteMax)
			{
			Byte data=byteBuffer[ni+i+len];
			if(info->next==NULL)
				{
				info->next=dalloc<DeflateInfo**>(16);
				Byte infoData=byteBuffer[info->pos+len];
				info->next[infoData>>4]=dalloc<DeflateInfo*>(16);
				info->next[infoData>>4][infoData&0xF]=dalloc<DeflateInfo>(1);
				info->next[infoData>>4][infoData&0xF]->pos=info->pos;
				}
			if(info->next[data>>4]==NULL)
				break;
			if(info->next[data>>4][data&0xF]==NULL)
				break;
			if(ni+i-info->next[data>>4][data&0xF]->pos>32768)
				{
				dfreeDeflateInfo(info->next[data>>4][data&0xF]);
				info->next[data>>4][data&0xF]=NULL;
				break;
				}
			info=info->next[data>>4][data&0xF];
			len++;
			}

		int goLen=len;
		int d=ni+i-info->pos;
		if(len<3||(len<11&&d>32)||(len<35&&d>1024))
			{
			lite[li++]=byteBuffer[ni+i];
			goLen=1;
			}
		else
			{
			lite[li++]=len+254;
			dis[di++]=d;
			}

		for(int j=0;j<goLen;j++)
			{
			info=head;
			int k=0;
			while(ni+i+j+k<byteMax&&k<liteMax)
				{
				Byte data=byteBuffer[ni+i+j+k];
				if(info->next==NULL)
					{
					info->next=dalloc<DeflateInfo**>(16);
					Byte infoData=byteBuffer[info->pos+k];
					info->next[infoData>>4]=dalloc<DeflateInfo*>(16);
					info->next[infoData>>4][infoData&0xF]=dalloc<DeflateInfo>(1);
					info->next[infoData>>4][infoData&0xF]->pos=info->pos;
					}
				info->pos=ni+i+j;
				if(info->next[data>>4]==NULL)
					break;
				if(info->next[data>>4][data&0xF]==NULL)
					break;
				if(ni+i+goLen-info->next[data>>4][data&0xF]->pos>32768)
					{
					dfreeDeflateInfo(info->next[data>>4][data&0xF]);
					info->next[data>>4][data&0xF]=NULL;
					break;
					}
				info=info->next[data>>4][data&0xF];
				k++;
				}
			Byte data=byteBuffer[ni+i+j+k];
			if(info->next==NULL)
				info->next=dalloc<DeflateInfo**>(16);
			if(info->next[data>>4]==NULL)
				info->next[data>>4]=dalloc<DeflateInfo*>(16);
			if(info->next[data>>4][data&0xF]==NULL)
				info->next[data>>4][data&0xF]=dalloc<DeflateInfo>(1);
			info->next[data>>4][data&0xF]->pos=ni+i+j;
			}
/*
		Byte data=byteBuffer[ni+i+len];
		if(info->next==NULL)
			info->next=dalloc<DeflateInfo**>(16);
		if(info->next[data>>4]==NULL)
			info->next[data>>4]=dalloc<DeflateInfo*>(16);
		if(info->next[data>>4][data&0xF]==NULL)
			info->next[data>>4][data&0xF]=dalloc<DeflateInfo>(1);
		info=info->next[data>>4][data&0xF];
		info->pos=i;*/



		i+=goLen;
		}
	dfreeDeflateInfo(head);
	ni=byteIndex=0;


#else
	int count=1;
	for(int i=1;i<byteIndex;i++)
		if(byteBuffer[ni+i]==byteBuffer[ni+i-1])
			count++;
		else
			{
			lite[li++]=byteBuffer[ni+i-1];
			count--;
			for(;count>=258;count-=258)
				{
				lite[li++]=258+254;
				dis[di++]=1;
				}
			if(count<3)
				for(int j=0;j<count;j++)
					lite[li++]=byteBuffer[ni+i-1];
			else
				{
				lite[li++]=count+254;
				dis[di++]=1;
				}
			count=1;
			}

	if(ni+count==byteMax)
		{
		lite[li++]=byteBuffer[ni];
		count--;
		for(;count>=258;count-=258)
			{
			lite[li++]=258+254;
			dis[di++]=1;
			}
		}


	ni+=byteIndex-count;
	byteIndex=count;

	if(ni+byteIndex==byteMax)
		{
		memmove(byteBuffer,byteBuffer+ni,byteIndex);
		ni=0;
		bi=0;bl=0;
		}
	if(end)
		{
		lite[li++]=byteBuffer[ni];
		count--;
		for(;count>=258;count-=258)
			{
			lite[li++]=258+254;
			dis[di++]=1;
			}
		if(count<3)
			for(int j=0;j<count;j++)
				lite[li++]=byteBuffer[ni];
		else
			{
			lite[li++]=count+254;
			dis[di++]=1;
			}
		//lite[li++]=256;
		ni=byteIndex=0;
		}
#endif
	blockProcess(end);
	return;

	int *jump=dalloc<int>(259);
	while(true)
		{
		int k=0;

		for(int i=1;i<259;i++)
			{
			int m=(byteIndex-i>259?259:byteIndex-i);
			int j;
			for(j=0;j<m;j++)
				if(byteBuffer[ni+i+j]!=byteBuffer[ni+j%(i)])
					break;
			for(;k<i;k++)
				jump[k]=k;
			for(;k<j;k++)
				jump[k]=i;
			if(j==m)
				break;
			}
		for(;k<259;k++)
			jump[k]=k;
		if(jump[0]==0)
			jump[0]=1;

		int i;
		for(i=bi;i<ni;)
			{
			int m=(byteIndex>258?258:byteIndex);
			int j=byteIndex;
			for(j=(i==bi?bl:0);j<m;j++)
				if(byteBuffer[i+j%(ni-i)]!=byteBuffer[ni+j])
					break;
			if(j>bl)
				{bi=i;bl=j;}
			if(j==258)
				i=ni;
			if(j==m)
				break;
			i+=jump[j];
			}

		if(i>=ni)
			{
			if(bl<3)
				{
				bl=1;
				lite[li++]=byteBuffer[ni];
				}
			else
				{
				lite[li++]=bl+254;
				dis[di++]=ni-bi;
				}
			ni+=bl;
			byteIndex-=bl;
			bi=ni-32768;
			if(bi<0)bi=0;
			bl=0;
			}
		else
			break;
		}
	dfree(jump);

	if(ni+byteIndex==byteMax)
		{
		memmove(byteBuffer,byteBuffer+ni-32768,byteIndex+32768);
		ni=32768;
		bi=0;bl=0;
		}

	if(end)
		{
		if(bl<3)
			{
			for(int i=0;i<bl;i++)
				lite[li++]=byteBuffer[ni+i];
			}
		else
			{
			lite[li++]=bl+254;
			dis[di++]=ni-bi;
			}
		ni+=bl;
		byteIndex-=bl;
		bi=ni-32768;
		if(bi<0)bi=0;
		bl=0;
		lite[li++]=256;
		}
	}

void	OutputDeflate::blockProcess(bool end)
	{
	lite[li++]=256;
	//report("lite[%d]",li);

	HuffInfo liteCount[286],disCount[30];
	for(int i=0;i<286;i++)
		liteCount[i].data=i;
	for(int i=0;i<30;i++)
		disCount[i].data=i;

	for(int i=0;i<li;i++)
		if(lite[i]<0)
			error("lite[%d] %d < 0.",i,lite[i]);
		else if(lite[i]<265)
			{
			liteCount[lite[i]].count++;
			}
		else if(lite[i]<258+254)
			{
			int extraBit=binaryLogarithm(lite[i]-254-3)-3;
			liteCount[261+extraBit*4+((lite[i]-254-3-(1<<(extraBit+2)))>>extraBit)].count++;
			}
		else if(lite[i]==258+254)
			liteCount[285].count++;
		else
			error("lite[%d] %d > 258+254.",i,lite[i]);
	for(int i=0;i<di;i++)
		if(dis[i]<0)
			error("dis[%d] %d < 0.",i,dis[i]);
		else if(dis[i]<4)
			disCount[dis[i]-1].count++;
		else if(dis[i]<=32768)
			{
			int extraBit=binaryLogarithm(dis[i]-1)-2;
			disCount[2+extraBit*2+((dis[i]-1-(1<<(extraBit+1)))>>extraBit)].count++;
			}
		else
			error("dis %d > 32768.",dis[i]);

	int liteCountMax=285,disCountMax=29;
	while(liteCountMax>=0&&liteCount[liteCountMax].count==0)liteCountMax--;
	liteCountMax++;
	while(disCountMax>=0&&disCount[disCountMax].count==0)disCountMax--;
	disCountMax++;
	if(disCountMax<=2)
		{
		disCount[0].count=1;
		disCount[1].count=1;
		disCountMax=2;
		}
	if(liteCountMax<257||disCountMax<1)
		error("Unexpected countMax.");

	HuffTree *liteTree=new HuffTree(liteCount,liteCountMax,15);
	HuffTree *disTree =new HuffTree(disCount,disCountMax,15);
	HuffInfo codeCount[19];
	int last=-1,lastCount=0;
	for(int i=0;i<liteCountMax+disCountMax+1;i++)
		{
		int lcl;
		if(i<liteCountMax)
			lcl=liteTree->getCodeLength(i);
		else if(i<liteCountMax+disCountMax)
			lcl=disTree->getCodeLength(i-liteCountMax);
		else
			lcl=-1;
		if(lcl==last)
			lastCount++;
		else
			{
			if(lastCount==0)
				;
			else if(last>15||last<0)
				error("Unexpected codeLength. %d",last);
			else if(last!=0)
				{
				codeCount[last].count+=1;
				lastCount--;
				codeCount[16].count+=lastCount/6;
				lastCount%=6;
				if(lastCount>2)
					codeCount[16].count+=1;
				else
					codeCount[last].count+=lastCount;
				}
			else
				{
				codeCount[18].count+=lastCount/138;
				lastCount%=138;
				if(lastCount>10)
					codeCount[18].count+=1;
				else if(lastCount>2)
					codeCount[17].count+=1;
				else
					codeCount[last].count+=lastCount;
				}

			last=lcl;
			lastCount=1;
			}
		}

	for(int i=0;i<19;i++)
		codeCount[i].data=i;

	HuffTree *codeTree=new HuffTree(codeCount,19,7);
	const int cli[19]={16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15};
	int cl[19];
	for(int i=0;i<19;i++)
		cl[i]=codeTree->getCodeLength(cli[i]);
	int clm;
	for(clm=18;cl[clm]==0&&clm>4;clm--);
	clm++;

	out->putBits(end,1);
	out->putBits(0b10,2);
	out->putBits(liteCountMax-257,5);
	out->putBits(disCountMax-1,5);
	out->putBits(clm-4,4);

	for(int i=0;i<clm;i++)
		out->putBits(cl[i],3);

	last=-2;
	lastCount=0;
	for(int i=0;i<liteCountMax+disCountMax+1;i++)
		{
		int lcl;
		if(i<liteCountMax)
			lcl=liteTree->getCodeLength(i);
		else if(i<liteCountMax+disCountMax)
			lcl=disTree->getCodeLength(i-liteCountMax);
		else
			lcl=-1;
		if(lcl==last)
			lastCount++;
		else
			{
			if(lastCount==0)
				;
			else if(last!=0)
				{
				codeTree->putCode(last,out);
				lastCount--;
				for(int i=0;i<lastCount/6;i++)
					{
					codeTree->putCode(16,out);
					out->putBits(0b11,2);
					}
				lastCount%=6;
				if(lastCount>2)
					{
					codeTree->putCode(16,out);
					out->putBits(lastCount-3,2);
					}
				else
					for(int i=0;i<lastCount;i++)
						codeTree->putCode(last,out);
				}
			else
				{
				for(int i=0;i<lastCount/138;i++)
					{
					codeTree->putCode(18,out);
					out->putBits(0b1111111,7);
					}
				lastCount%=138;
				if(lastCount>10)
					{
					codeTree->putCode(18,out);
					out->putBits(lastCount-11,7);
					}
				else if(lastCount>2)
					{
					codeTree->putCode(17,out);
					out->putBits(lastCount-3,3);
					}
				else
					for(int i=0;i<lastCount;i++)
						codeTree->putCode(last,out);
				}

			last=lcl;
			lastCount=1;
			}
		}

	int j=0;
	for(int i=0;i<li;i++)
		{
		if(lite[i]<265)
			liteTree->putCode(lite[i],out);
		else if(lite[i]<258+254)
			{
			int extraBit=binaryLogarithm(lite[i]-254-3)-3;
			liteTree->putCode(261+extraBit*4+((lite[i]-254-3-(1<<(extraBit+2)))>>extraBit),out);
			out->putBits((lite[i]-254-3-(1<<(extraBit+2)))&((1<<extraBit)-1),extraBit);
			}
		else if(lite[i]==258+254)
			liteTree->putCode(285,out);
		else
			error("lite[%d] %d > 258+254.",i,lite[i]);
		if(lite[i]>256)
			{
			if(j>=di)
				error("j>=di");
			if(dis[j]<4)
				disTree->putCode(dis[j]-1,out);
			else if(dis[j]<=32768)
				{
				int extraBit=binaryLogarithm(dis[j]-1)-2;
				disTree->putCode(2+extraBit*2+((dis[j]-1-(1<<(extraBit+1)))>>extraBit),out);
				out->putBits((dis[j]-1-(1<<(extraBit+1)))&((1<<extraBit)-1),extraBit);
				}
			else
				error("dis %d > 32768.",dis[j]);
			j++;
			}
		}

	delete liteTree;
	delete disTree;
	delete codeTree;
	li=di=0;
	}









	OutputZlib::OutputZlib(Output *o,int bm,bool close):OutputMask(o,bm,close)
	{
	out->putInt(0x78,1);
	out->putInt(0x9C,1);
	deflate=new OutputDeflate(out,bm,false);
	s1=1;
	s2=0;
	}

	OutputZlib::~OutputZlib()
	{
	delete deflate;
	out->putInt((s2<<16)+s1,4);
	}
int 	OutputZlib::putRaw(const Byte *data,int size)
	{
	deflate->putBytes(data,size);
	for(int i=0;i<size;i++)
		{
		s1=(s1+((unsigned char*)data)[i])%65521;
		s2=(s2+s1)%65521;
		}
	return size;
	}







