




int		getHash(int data,int max)
	{
	srand(data);
	return rand()%max;
	}


int		binaryLogarithm(int a)
	{
	if(a<0)
		error("binaryLogarithm with a<0.");
	int ret=0;
	for(ret=0;a>>ret!=0;ret++);
	return ret;
	}

long long int	powInt(int base,int t)
	{
	long long int ret=1;
	if(t>=8)
		{
		ret=powInt(base,t/4);
		ret=ret*ret*ret*ret;
		t%=4;
		}
	for(int i=0;i<t;i++)
		ret*=base;
	return ret;
	}






	CrcCheck::CrcCheck()
	{
	crcTable=dalloc<unsigned long>(256);
	for(int i=0;i<256;i++)
		{
		unsigned long c=i;
		for(int j=0;j<8;j++)
			c=(c&1)?(0xEDB88320^(c>>1)):(c>>1);
		crcTable[i]=c;
		}
	value=0xFFFFFFFF;
	}

	CrcCheck::~CrcCheck()
	{
	dfree(crcTable);
	}

void	CrcCheck::init()
	{
	value=0xFFFFFFFF;
	}

void	CrcCheck::put(Byte b)
	{
	value=crcTable[(value^b)&0xFF]^(value>>8);
	}

void	CrcCheck::put(const Byte *arr,int size)
	{
	argCheck(arr==NULL||size<0);
	for(int i=0;i<size;i++)
		put(arr[i]);
	}

unsigned long	CrcCheck::get()
	{
	return value;
	}



