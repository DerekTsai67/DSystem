



	InputFile::InputFile(ConstString name):Input(NULL,0,true)
	{
	argCheck(name==NULL);
	file=fopen(name,"rb");
	if(file==NULL)
		safeFatalError("Open InputFile failed : %S",name);
	byteIndex+=fread(byteBuffer+byteIndex,1,1,file);
	count++;
	}

	InputFile::InputFile(FILE *f,bool cf):Input(NULL,0,cf)
	{
	file=f;
	if(file==NULL)
		safeFatalError("Convert to InputFile failed.");
	byteIndex+=fread(byteBuffer+byteIndex,1,1,file);
	count++;
	}

	InputFile::~InputFile()
	{
	if(closeSource)
		{
		getEnd();
		/*Byte temp[16];
		int ti=0;
		while((ti=fread(temp,1,16,file))>0)
			report("File last %S",viewData(temp,ti));*/
		fclose(file);
		}
	count--;
	}

int 	InputFile::getRaw(Byte *dest,int size)
	{
	if(dest==NULL)
		error("getBytes to NULL.");
	if(file==NULL)
		error("getBytes from NULL file.");
	if(size<=0)
		error("getBytes with size<=0.");

	return fread(dest,1,size,file);
	}

bool	InputFile::emptyRaw()
	{
	return feof(file);
	}
/*
int 	InputFile::getPosition()
	{
	if(safeFail>=3)
		error("InputFile::getPosition while fatal failed.");
	return ftell(file);
	}*/

int 	InputFile::read(void *d,int s,int n)
	{
	if(safeFail>=3)
		error("InputFile::read while fatal failed.");
	return fread(d,s,n,file);
	}









	OutputFile::OutputFile(ConstString name):Output(0,true)
	{
	if(name==NULL)
		error("Open OutputFile to NULL.");
	file=fopen(name,"rb");
	if(file!=NULL)
		{
		fclose(file);
		int i=name.getLength();
		originName=dalloc<char>(i+1);
		tempName=dalloc<char>(i+6);
		memcpy(originName,name,i+1);
		memcpy(tempName,name,i);
		memcpy(tempName+i,".TEMP",6);
		file=fopen(tempName,"w+b");
		}
	else
		{
		originName=tempName=NULL;
		file=fopen(name,"w+b");
		}
	if(file==NULL)
		dead=true;
		//error("Open OutputFile failed : %S",name);
	count++;
	}

	OutputFile::OutputFile(FILE *f,bool cf):Output(0,cf)
	{
	originName=tempName=NULL;
	file=f;
	if(file==NULL)
		dead=true;
		;//error("Convert to OutputFile with NULL FILE.");
	count++;
	}

	OutputFile::~OutputFile()
	{
	if(closeSource&&!dead)
		{
		if(fclose(file)!=0)
			error("fclose failed.");;
		if(originName!=NULL)
			{
			remove(originName);
			rename(tempName,originName);
			dfree(originName);
			dfree(tempName);
			}
		}
	count--;
	}


int 	OutputFile::putRaw(const Byte *data,int size)
	{
	if(fwrite(data,1,size,file)!=(unsigned int)size)
		error("fwrite failed.");
	return size;
	}


