



Datafile*	loadBMP(Input *in,int safeFail,bool onlyDIB)
	{
	argCheck(in==NULL);
	report(1,"Loading BMP.");
	in->setSafeFail(safeFail);
	
	if(!onlyDIB)
		{
		if(!in->getVerify("BM"))
			{
			safeFatalError("Verifying 'BM'.");
			delete in;
			return NULL;
			}

		in->getIgnore(4); //totalSize
		in->getIgnore(4); //reserved
		in->getIgnore(4); //startPosition
		}

	//DIB Header
	int DIBH;
	if((DIBH=in->getInt(4))!=40)
		safeFatalError("Unsupported DIBH Length. %d",DIBH);
	Coord size=in->getCoord(4);
	if(in->getInt(2)!=1)
		safeError("Unexpected LayerNum.");
	int colorDepth=in->getInt(2);
	int compression=in->getInt(4);
	if(compression!=0&&compression!=3)
		safeFatalError("Unsupported Compression.");
	in->getInt(4); //bitmapSize
	in->getInt(4); //image resolution x&y
	in->getInt(4);
	int paletteNum=in->getInt(4);
	in->getInt(4);//Important ColorNum

	if(safeFail>=3)
		{
		delete in;
		return NULL;
		}

	Datafile *df=new Datafile();
	Text	*infoText=new Text("==BMP File Information==\n");
	infoText->addString("%d*%d bitDepth:%d.\n",size.x,size.y,colorDepth);

	DS_RGB *palette=NULL;
	if(paletteNum==0&&colorDepth<16)
		paletteNum=1<<colorDepth;
	if(paletteNum!=0)
		{
		if(colorDepth>=16)
			safeError("Unsupported palette with colorDepth >= 16.");
		palette=dalloc<DS_RGB>(paletteNum);
		for(int i=0;i<paletteNum;i++)
			{
			palette[i]=in->getBGR();
			in->getIgnore(1);
			}
		infoText->addString("Palette[%d].\n",paletteNum);
		}
	
	if(compression==3)
		{
		in->getIgnore(12);
		compression=0;
		}
	
	int UD=false;
	if(size.y<0)
		{size.y*=-1;UD=true;}

	DSBitmap *bmp=new DSBitmap(size);
	
	int endianSetting=dlib::bitLeft;

	switch(colorDepth)
		{
	case 32:
		for(int y=0;y<size.y;y++)
		for(int x=0;x<size.x;x++)
			{
			bmp->put(Coord(x,UD?y:size.y-y-1),in->getBGR());
			int alpha=in->getInt(1);
			bmp->setAlpha(Coord(x,UD?y:size.y-y-1),alpha);
			}
		if(bmp->isFullTransparent())
			{
			bmp->ignoreAlpha();
			report("Full transparent.");
			infoText->addString("Full transparent.\n");
			}
		break;
	case 24:
		for(int y=0;y<size.y;y++)
			{
			for(int x=0;x<size.x;x++)
				bmp->put(Coord(x,UD?y:size.y-y-1),in->getBGR());
			in->getIgnore((4-size.x*3%4)%4);
			}
		break;
	case 16:
		for(int y=0;y<size.y;y++)
			{
			for(int x=0;x<size.x;x++)
				{
				int t=in->getInt(2);
				DS_RGB c;
				c.r=((t>>11)&0x1F)*255/31;
				c.g=((t>>5)&0x3F)*255/63;
				c.b=(t&0x1F)*255/31;
				bmp->put(Coord(x,UD?y:size.y-y-1),c);
				}
			in->getIgnore((4-size.x*2%4)%4);
			}
		break;
	case 1:
	case 4:
	case 8:
		for(int y=0;y<size.y;y++)
			{
			in->checkBits();
			for(int x=0;x<size.x;x++)
				{
				int index=in->getBits(colorDepth);
				if(index>=paletteNum)
					{
					safeError("Palette overflow.");
					index=0;
					}
				bmp->put(Coord(x,UD?y:size.y-y-1),palette[index]);
				}
			in->alignBits();
			in->getIgnore((4-size.x*colorDepth/8%4)%4);
			}
		break;
	default:
		safeError("Unsupported colorDepth %dbpp.",colorDepth);
		}

	//in->getEnd();
	int overallByteDepth=3+!bmp->isFullOpaque();
	infoText->addString("%lld/%lld Overall compreRate=%5.2f%% in %dbits."
							,in->getPosition(),(long long)overallByteDepth*size.area()
							,(double)in->getPosition()/overallByteDepth/size.area()*100.0,overallByteDepth*8);
	delete in;

	if(palette!=NULL)
		dfree(palette);

	Picture *pic=new Picture(bmp);
	pic->setName("BMP Picture");
	infoText->setName("BMP Information");
	df->addData(pic);
	df->addData(infoText);
	report(-1,"Loaded %d*%d %dbpp.",size.x,size.y,colorDepth);
	return df;
	}



void	DSBitmap::saveBMP(Output *out,PictureOption *option)const
	{
	argCheck(out==NULL);
	report(1,"Saving BMP.");
	
	if(option==NULL)
		option=new PictureOption();
	
	if(option->bitDepth==AutoLoseless)
		{
		option->bitDepth=24;
		if(option->alphaChannel==AutoLoseless)
			for(int y=0;y<size.y;y++)
			for(int x=0;x<size.x;x++)
				if(get(x,y).a!=255)
					{
					option->alphaChannel=true;
					y=size.y;x=size.x;
					}
		if(option->alphaChannel==true)
			option->bitDepth=32;
		}

	if(!option->skipHeader)
		{
		out->putString("BM");
		out->putInt(size.area()*option->bitDepth/8+54,4);
		out->putInt(0,4);
		out->putInt(54,4);
		}

	//DIB Header
	out->putInt(40,4);
	out->putInt(size.x,4);
	out->putInt(-size.y,4);
	out->putInt(1,2);
	out->putInt(option->bitDepth,2);
	out->putInt(0,4);
	out->putInt(size.area()*option->bitDepth/8,4);
	out->putInt(0,4);
	out->putInt(0,4);//image resolution x&y
	out->putInt(0,4);//palette
	out->putInt(0,4);

	//palette

	//data
	switch(option->bitDepth)
		{
	case 32:
		for(int y=0;y<size.y;y++)
		for(int x=0;x<size.x;x++)
			{
			DS_RGB c=get(x,y);
			if(c.a!=0||option->stableTransparent)
				out->putBGR(c);
			else
				out->putBGR(black);
			out->putInt(c.a,1);
			}
		break;
	case 24:
		for(int y=0;y<size.y;y++)
			{
			for(int x=0;x<size.x;x++)
				out->putBGR(get(x,y));
			out->putInt(0,(4-size.x*3%4)%4);
			}
		break;
	case 1:
	case 4:
	case 8:
	case 16:
	default:
		error("Unsupported colorDepth.");
		}
	
	delete out;
	report(-1,"Saved %d*%d %dbpp.",size.x,size.y,option->bitDepth);
	delete option;
	}






