
/*
Picture*	loadGIF(Input *in,int safeFail)
	{
	Datafile *df=loadGIF_(in,safeFail);
	if(df==NULL)
		return NULL;
	Picture *pic=df->getPic(-1);
	if(pic==NULL)
		{
		error("No Picture in file.");
		return NULL;
		}
	pic=pic->clone();
	delete df;
	return pic;
	}*/
Datafile*	loadGIF(Input *in,int safeFail)
	{
	argCheck(in==NULL);
	report(1,"Loading GIF.");
	int startClock=clock();
	in->setSafeFail(safeFail);

	if(!in->getVerify("GIF"))
		safeFatalError("Verifying 'GIF'.");
	char v[4];
	in->getBytes(v,3);
	if(!verify(v,"87a")&&!verify(v,"89a"))
		safeFatalError("Verifying '89a'.");
	if(safeFail>=3)
		{
		delete in;
		return NULL;
		}

	Datafile *datafile=new Datafile();
	Picture *pic=NULL;
	DS_RGB *GCT=NULL;

	Text	*infoText=new Text("==GIF File Information==\n");

	Coord globalSize=in->getCoord(2);
	infoText->addString("GlobalSize %d*%d.\n",globalSize.x,globalSize.y);
	unsigned char temp=in->getInt(1);
	bool GCTexist=temp&0x80;
	//int colorResolution=(temp&0x70)>>4;
	//bool GCTsorted=temp&0x08;
	int GCTsize=2<<(temp&0x07);
	in->getIgnore(1);//BCI
	in->getIgnore(1);//aspectRatio

	if(GCTexist)
		{
		GCT=dalloc<DS_RGB>(GCTsize);
		for(int i=0;i<GCTsize;i++)
			GCT[i]=in->getRGB();
		infoText->addString("Global ColorTable [%d].\n",GCTsize);
		}


	DSBitmap *bmp=new DSBitmap(globalSize),*restore=NULL;
	bmp->clear(transparent);
	Text *textData=NULL;
	int transFlag=-1,transIndex=-1;
	int delay=1;
	int originLength=0;

	int introducer=in->getInt(1);
	while(introducer!=0x3B&&safeFail<3)
		{
		//report("intro %02X",temp);
		switch(introducer)
			{
		case 0x21:
			switch(in->getInt(1))
				{
			case 0xF9:
				if(in->getInt(1)!=4)
					safeFatalError("Graphic Control ExBlock with size != 4.");
				if(transFlag!=-1)
					{
					safeError("Duplicated Graphic Control ExBlock.");
					if(restore!=NULL)
						{
						delete restore;
						restore=NULL;
						}
					}
				if(safeFail>=3)
					break;

				transFlag=in->getInt(1);

				delay=in->getInt(2);
				if(delay<=0)
					delay=1;

				if(transFlag&0x01)
					transIndex=in->getInt(1);
				else
					{
					in->getIgnore(1);
					transIndex=-1;
					}

				transFlag=(transFlag&0x1C)>>2;
				if(transFlag==3)
					{
					if(restore!=NULL)
						delete restore;
					restore=bmp->clone();
					}

				if(in->getInt(1)!=0)
					safeFatalError("Graphic Control ExBlock with Terminator != 0.");
				break;
			case 0xFE:
				report(3,"Comment ExBlock");
				textData=new Text();
				temp=in->getInt(1);
				while(temp!=0)
					{
					String s(temp);
					in->getFixedString(s);
					report("%S",viewData(s));
					textData->add(s);
					temp=in->getInt(1);
					}
				textData->setName("GIT Comment");
				datafile->addData(textData);
				report(-3,"==Comment End==");
				break;
			case 0x01:
				report("Unsupported Plain Text ExBlock.");
				textData=new Text();
				temp=in->getInt(1);
				while(temp!=0)
					{
					String s(temp);
					in->getFixedString(s);
					report(" %S",viewData(s));
					textData->add(s);
					temp=in->getInt(1);
					}
				textData->setName("GIF PlainText");
				datafile->addData(textData);
				break;
			case 0xFF:
				if(in->getInt(1)!=11)
					safeFatalError("Application ExBlock with size != 11.");
				textData=new Text();

				char s[255];
				in->getBytes(s,8);
				report(1,"Application ExBlock %S",viewData(s,8));
				textData->addLine(ConstString(s,8));

				in->getBytes(s,3);
				report("%S",viewData(s,3));
				textData->addLine(ConstString(s,3));

				temp=in->getInt(1);
				while(temp!=0)
					{
					in->getBytes(s,temp);
					report("%S",viewData(s,temp));
					textData->add(ConstString(s,temp));
					temp=in->getInt(1);
					}
				textData->setName("GIF Application");
				datafile->addData(textData);
				report(-1,"End.");
				break;
			default:
				safeError("Undefined ExBlock.");
				while((temp=in->getInt(1))!=0)
					in->getIgnore(temp);
				break;
				}
			break;
		case 0x2C:{
			if(bmp==NULL)
				error("Missing Graphic Control ExBlock.");
			Coord pos=in->getCoord(2);
			Coord size=in->getCoord(2);
			if(!(pos>=zeroCoord))
				safeFatalError("Negative local pos.");
			if(!(size>=zeroCoord))
				safeFatalError("Negative local size.");
			if(!((size+pos)<=bmp->getSize()))
				safeFatalError("Local size > global size.");
			infoText->addString("%d,%d + %d*%d transFlag=%d %d0ms.\n",pos.x,pos.y,size.x,size.y,transFlag,delay);

			temp=in->getInt(1);
			DS_RGB *LCT;
			int LCTsize;
			if(temp&0x80)
				{
				LCTsize=2<<(temp&0x07);
				LCT=dalloc<DS_RGB>(LCTsize);
				for(int i=0;i<LCTsize;i++)
					LCT[i]=in->getRGB();
				infoText->addString("    Local ColorTable [%d].\n",LCTsize);
				}
			else
				if(GCT==NULL)
					safeFatalError("No either GCT or LCT.")
				else
					{
					LCT=GCT;
					LCTsize=GCTsize;
					}

			int *yd,*yl,passNum;
			int yd0=0,yl0=1;
			int yd1[]={0,4,2,1};
			int yl1[]={8,8,4,2};
			if(temp&0x40)
				{
				yd=yd1;
				yl=yl1;
				passNum=4;
				}
			else
				{
				yd=&yd0;
				yl=&yl0;
				passNum=1;
				}

			if(safeFail>=3)
				break;

			originLength=in->getInt(1);
			Input *decode=new InputLZW(new InputSimpleBlock(in,false),originLength);
			for(int pi=0;pi<passNum;pi++)
			for(int y=yd[pi];y<size.y;y+=yl[pi])
			for(int x=0;x<size.x;x++)
				{
				int index=decode->getInt(1);
				if(index>=LCTsize)
					{
					if(index!=transIndex)
						safeError("LCT overflowed. %d %d %d",index,LCTsize,transIndex);
					index=transIndex;
					}
				if(index!=transIndex)
					bmp->put(Coord(pos.x+x,pos.y+y),LCT[index]);
				}
			Text *last=decode->getEnd(true);
			if(last!=NULL)
				{
				last->setName("LZW last");
				datafile->addData(last);
				}
			infoText->addString("    %lld/%lld compreRate=%5.2f%%.\n",decode->getInSize(),decode->getPosition(),(double)decode->getInSize()/decode->getPosition()*100.0);
			delete decode;

			if(LCT!=GCT)
				dfree(LCT);

			if(pic==NULL)
				pic=new Picture(bmp,delay*10);
			else
				pic->addFrame(bmp,delay*10);

			switch(transFlag)
				{
			default:
				safeError("transFlag switch out. %d",transFlag);
			case -1:
			case 0:
			case 1:
				bmp=bmp->clone();
				break;
			case 3:
				if(restore==NULL)
					error("No restore bitmap.");
				bmp=restore;
				restore=NULL;
				break;
			case 2:
				bmp=bmp->clone();
				bmp->clear(transparent,pos,size);
				break;
				}
			transFlag=-1;
			if(restore!=NULL)
				error("Unexpected restore bitmap exist.");

			break;}
		default:
			safeFatalError("Undefined introducer %02X.",introducer);
			}

		introducer=in->getInt(1);
		}//while
	delete bmp;
	if(restore!=NULL)
		delete restore;
	if(GCT!=NULL)
		dfree(GCT);

	if(pic==NULL)
		{
		safeFatalError("No image.");
		delete datafile;
		delete infoText;
		return NULL;
		}

	Text *last=in->getEnd(true);
	if(last!=NULL)
		{
		last->setName("File last");
		datafile->addData(last);
		}

	int overallByteDepth=3;
	for(int fi=0;fi<pic->getFrameNumber();fi++)
		if(!pic->getBitmapIndex(fi)->isFullOpaque())
			{
			overallByteDepth=4;
			break;
			}
	infoText->addString("%lld/%lld Overall compreRate=%5.2f%% in %dbits.\n"
							,in->getPosition(),(long long)overallByteDepth*globalSize.area()*pic->getFrameNumber()
							,(double)in->getPosition()/overallByteDepth/globalSize.area()/pic->getFrameNumber()*100.0,overallByteDepth*8);
	infoText->addString("Elapsed time:%dms.\n",(clock()-startClock)*1000/CLOCKS_PER_SEC);


	pic->setTimeType(Picture::Realtime);
	pic->setName("GIF Picture");
	infoText->setName("GIF Information");
	datafile->addData(pic);
	datafile->addData(infoText);
	report(-1,"Loaded %d*%d %dbpc.",globalSize.x,globalSize.y,originLength-1);
	delete in;
	return datafile;
	}

void	DSBitmap::saveGIF(Output *out)const
	{
	if(out==NULL)
		error("saveGIF to NULL.");
	report(1,"Saving GIF.");

	out->putString("GIF89a");

	int transIndex=-1;
	DS_RGB *GCT=dalloc<DS_RGB>(256);
	int **table=dalloc<int*>(size.y);
	for(int i=0;i<size.y;i++)
		table[i]=dalloc<int>(size.x);
	int gi=0;
	for(int y=0;y<size.y;y++)
	for(int x=0;x<size.x;x++)
		{
		int i;
		if(data[y][x].a==0)
			{
			if(transIndex!=-1)
				table[y][x]=transIndex;
			else
				table[y][x]=transIndex=gi++;
			continue;
			}
		for(i=0;i<gi;i++)
			if(data[y][x]==GCT[i])
				break;
		table[y][x]=i;
		if(i!=gi)
			continue;
		if(gi>=256)
			error("GCT overflow.");
		if(data[y][x].a==0)
			transIndex=gi;
		GCT[gi++]=data[y][x];
		}

	out->putCoord(size,2);
	out->putInt(0xF7,1);
	out->putInt(0x00,1);//Background Color Index
	out->putInt(0x00,1);//Aspect Ratio

	for(int i=0;i<256;i++)
		out->putRGB(GCT[i]);

	Output *mask;
	if(transIndex!=-1)
		{
		out->putInt(0x21,1);
		out->putInt(0xF9,1);
		out->putInt(0x04,1);
		out->putInt(0x01,1);
		out->putInt(0x00,2);
		out->putInt(transIndex,1);
		out->putInt(0x00,1);
		}
	out->putInt(0x21,1);
	out->putInt(0xFE,1);
	mask=new OutputSimpleBlock(out,false);
	mask->putString("D.S.");
	delete mask;
	out->putInt(0x2C,1);
	out->putCoord(zeroCoord,2);
	out->putCoord(size,2);
	out->putInt(0,1);
	out->putInt(8,1);
	mask=new OutputLZW(new OutputSimpleBlock(out,false),8);
	for(int y=0;y<size.y;y++)
	for(int x=0;x<size.x;x++)
		mask->putInt(table[y][x],1);
	delete mask;

	out->putInt(0x3B,1);
	dfree(GCT);
	for(int i=0;i<size.y;i++)
		dfree(table[i]);
	dfree(table);

	delete out;
	report(-1,"Saved %d*%d.",size.x,size.y);
	}

void	Picture::saveGIF(Output *out)const
	{
	if(out==NULL)
		error("saveGIF to NULL.");
	report(1,"Saving GIF.");

	out->putString("GIF89a");
	
	struct Table{
	DS_RGB *colorTable;
	int		transIndex,paletteMax;
		};
	
	Table *globalTable=dalloc<Table>(1);
	globalTable->colorTable=dalloc<DS_RGB>(257);
	globalTable->transIndex=-1;
	globalTable->paletteMax=0;
	
	Table	**localTable=dalloc<Table*>(frameMax);
	int		*disposal=dalloc<int>(frameMax+1); //8=clear 4=left
	Coord	*head=dalloc<Coord>(frameMax);
	Coord	*end =dalloc<Coord>(frameMax);
	int		***table=dalloc<int**>(frameMax);
	for(int fi=0;fi<frameMax;fi++)
		{
		localTable[fi]=globalTable;
		disposal[fi]=4;
		table[fi]=dalloc<int*>(dataSize.y);
		for(int y=0;y<dataSize.y;y++)
			table[fi][y]=dalloc<int>(dataSize.x);
		}
	disposal[frameMax]=8;
	
	for(int fi=0;fi<frameMax;fi++)
		{
		const DSBitmap *bmp=getBitmapIndex(fi);
		for(int y=0;y<dataSize.y;y++)
		for(int x=0;x<dataSize.x;x++)
			{
			if(globalTable->paletteMax>=257)
				{fi=frameMax;y=dataSize.y;break;}
			int i;
			if(bmp->data[y][x].a==0)
				{
				if(globalTable->transIndex==-1)
					globalTable->transIndex=globalTable->paletteMax++;
				table[fi][y][x]=globalTable->transIndex;
				continue;
				}
			for(i=0;i<globalTable->paletteMax;i++)
				if(bmp->data[y][x]==globalTable->colorTable[i])
					break;
			table[fi][y][x]=i;
			if(i==globalTable->paletteMax)
				{
				globalTable->colorTable[globalTable->paletteMax]=bmp->data[y][x];
				globalTable->colorTable[globalTable->paletteMax++].a=255;
				}

			}
		}
	
	if(globalTable->paletteMax>=257)
		{
		dfree(globalTable->colorTable);
		dfree(globalTable);
		globalTable=NULL;
		for(int fi=0;fi<frameMax;fi++)
			{
			localTable[fi]=dalloc<Table>(1);
			localTable[fi]->colorTable=dalloc<DS_RGB>(257);
			localTable[fi]->paletteMax=0;
			localTable[fi]->transIndex=-1;
			const DSBitmap *bmp=getBitmapIndex(fi);
			for(int y=0;y<dataSize.y;y++)
			for(int x=0;x<dataSize.x;x++)
				{
				if(localTable[fi]->paletteMax>=257)
					error("LCT overflow.");
				int i;
				if(bmp->data[y][x].a==0)
					{
					if(localTable[fi]->transIndex==-1)
						localTable[fi]->transIndex=localTable[fi]->paletteMax++;
					table[fi][y][x]=localTable[fi]->transIndex;
					continue;
					}
				for(i=0;i<localTable[fi]->paletteMax;i++)
					if(bmp->data[y][x]==localTable[fi]->colorTable[i])
						break;
				table[fi][y][x]=i;
				if(i==localTable[fi]->paletteMax)
					{
					localTable[fi]->colorTable[localTable[fi]->paletteMax]=bmp->data[y][x];
					localTable[fi]->colorTable[localTable[fi]->paletteMax++].a=255;
					}
				}
			if(localTable[fi]->paletteMax>=257)
				error("LCT overflow.");
			}
		}
	
	for(int fi=0;fi<frameMax;fi++)
		{
		if(localTable[fi]->paletteMax<(1<<binaryLogarithm(localTable[fi]->paletteMax-1)))
			if(localTable[fi]->transIndex==-1)
				localTable[fi]->transIndex=localTable[fi]->paletteMax++;
		if(localTable[fi]->transIndex==-1||fi==0)
			{
			disposal[fi]=8;
			}
		else
			{
			for(int y=0;y<dataSize.y;y++)
			for(int x=0;x<dataSize.x;x++)
				{
				if(table[fi  ][y][x]==localTable[fi  ]->transIndex
				 &&table[fi-1][y][x]!=localTable[fi-1]->transIndex)
					{
					disposal[fi]=8;
					y=dataSize.y;break;
					}
				}
			}
		
		const DSBitmap *bmp=getBitmapIndex(fi);
		if(disposal[fi]==8)
			{
			for(head[fi].y=0;head[fi].y<dataSize.y;head[fi].y++)
				{
				int x;
				for(x=0;x<dataSize.x;x++)
					if(bmp->get(x,head[fi].y).a!=0)
						break;
				if(x!=dataSize.x)
					break;
				}
			for(end[fi].y=dataSize.y-1;end[fi].y>=head[fi].y+1;end[fi].y--)
				{
				int x;
				for(x=0;x<dataSize.x;x++)
					if(bmp->get(x,end[fi].y).a!=0)
						break;
				if(x!=dataSize.x)
					break;
				}
			end[fi].y++;
			
			for(head[fi].x=0;head[fi].x<dataSize.x;head[fi].x++)
				{
				int y;
				for(y=head[fi].y;y<end[fi].y;y++)
					if(bmp->get(head[fi].x,y).a!=0)
						break;
				if(y!=end[fi].y)
					break;
				}
			for(end[fi].x=dataSize.x-1;end[fi].x>=head[fi].x+1;end[fi].x--)
				{
				int y;
				for(y=head[fi].y;y<end[fi].y;y++)
					if(bmp->get(end[fi].x,y).a!=0)
						break;
				if(y!=end[fi].y)
					break;
				}
			end[fi].x++;
			}
		else
			{
			for(head[fi].y=0;head[fi].y<dataSize.y;head[fi].y++)
				{
				int x;
				for(x=0;x<dataSize.x;x++)
					if(!localTable[fi]->colorTable[table[fi][head[fi].y][x]].same(localTable[fi-1]->colorTable[table[fi-1][head[fi].y][x]]))
						break;
				if(x!=dataSize.x)
					break;
				}
			for(end[fi].y=dataSize.y-1;end[fi].y>=head[fi].y+1;end[fi].y--)
				{
				int x;
				for(x=0;x<dataSize.x;x++)
					if(!localTable[fi]->colorTable[table[fi][end[fi].y][x]].same(localTable[fi-1]->colorTable[table[fi-1][end[fi].y][x]]))
						break;
				if(x!=dataSize.x)
					break;
				}
			end[fi].y++;
			
			for(head[fi].x=0;head[fi].x<dataSize.x;head[fi].x++)
				{
				int y;
				for(y=head[fi].y;y<end[fi].y;y++)
					if(!localTable[fi]->colorTable[table[fi][y][head[fi].x]].same(localTable[fi-1]->colorTable[table[fi-1][y][head[fi].x]]))
						break;
				if(y!=end[fi].y)
					break;
				}
			for(end[fi].x=dataSize.x-1;end[fi].x>=head[fi].x+1;end[fi].x--)
				{
				int y;
				for(y=head[fi].y;y<end[fi].y;y++)
					if(!localTable[fi]->colorTable[table[fi][y][end[fi].x]].same(localTable[fi-1]->colorTable[table[fi-1][y][end[fi].x]]))
						break;
				if(y!=end[fi].y)
					break;
				}
			end[fi].x++;
			}
		if(!(head[fi]<end[fi]))
			{
			head[fi]=zeroCoord;
			end[fi]=monoCoord;
			}
		}


	out->putCoord(dataSize,2);
	if(globalTable==NULL)
		out->putInt(0x70,1);
	else
		out->putInt(0xF0|(binaryLogarithm(globalTable->paletteMax-1)-1),1);
	out->putInt(0x00,1);//Background Color Index
	out->putInt(0x00,1);//Aspect Ratio
	
	if(globalTable!=NULL)
		{
		globalTable->paletteMax=1<<binaryLogarithm(globalTable->paletteMax-1);
		for(int i=0;i<globalTable->paletteMax;i++)
			out->putRGB(globalTable->colorTable[i]);
		}

	Output *mask;
	out->putInt(0x21,1);
	out->putInt(0xFF,1);
	mask=new OutputSimpleBlock(out,false);
	mask->putString("NETSCAPE2.0");
	((OutputSimpleBlock*)mask)->cutBlock();
	mask->putInt(1,1);
	mask->putInt(0,2);
	delete mask;


	/*out->putInt(0x21,1);
	out->putInt(0xFE,1);
	mask=new OutputSimpleBlock(out,false);
	mask->putString("D.S.");
	delete mask;*/

	for(int fi=0;fi<frameMax;fi++)
		{
		out->putInt(0x21,1);
		out->putInt(0xF9,1);
		out->putInt(0x04,1);
		out->putInt(disposal[fi+1]|(localTable[fi]->transIndex!=-1),1);
		out->putInt(frame[fi]->getDelay()/10,2);
		out->putInt(localTable[fi]->transIndex==-1?0:localTable[fi]->transIndex,1);
		out->putInt(0x00,1);

		out->putInt(0x2C,1);
		out->putCoord(head[fi],2);
		out->putCoord(end[fi]-head[fi],2);
		if(globalTable==NULL)
			{
			localTable[fi]->paletteMax=256;
			out->putInt(0x87,1);
			for(int i=0;i<localTable[fi]->paletteMax;i++)
				out->putRGB(localTable[fi]->colorTable[i]);
			}
		else
			{
			out->putInt(0x00,1);
			}
				
		out->putInt(binaryLogarithm(localTable[fi]->paletteMax-1),1);
		mask=new OutputLZW(new OutputSimpleBlock(out,false),binaryLogarithm(localTable[fi]->paletteMax-1));
		if(localTable[fi]->transIndex!=-1&&fi!=0&&disposal[fi]==4)
			{
			for(int y=head[fi].y;y<end[fi].y;y++)
			for(int x=head[fi].x;x<end[fi].x;x++)
				if(localTable[fi]->colorTable[table[fi][y][x]].same(localTable[fi-1]->colorTable[table[fi-1][y][x]]))
					mask->putInt(localTable[fi]->transIndex,1);
				else
					mask->putInt(table[fi][y][x],1);
			}
		else
			{
			for(int y=head[fi].y;y<end[fi].y;y++)
			for(int x=head[fi].x;x<end[fi].x;x++)
				mask->putInt(table[fi][y][x],1);
			}
		delete mask;
		}

	out->putInt(0x3B,1);
	
	if(globalTable!=NULL)
		{
		dfree(globalTable->colorTable);
		dfree(globalTable);
		}
	else
		{
		for(int fi=0;fi<frameMax;fi++)
			{
			dfree(localTable[fi]->colorTable);
			dfree(localTable[fi]);
			}
		}
	dfree(localTable);
	dfree(disposal);
	dfree(head);
	dfree(end);
	
	for(int fi=0;fi<frameMax;fi++)
		{
		for(int y=0;y<dataSize.y;y++)
			dfree(table[fi][y]);
		dfree(table[fi]);
		}
	dfree(table);

	delete out;
	report(-1,"Saved %d*%d %dFrames.",dataSize.x,dataSize.y,frameMax);
	}









	InputLZW::InputLZW(Input *i,int ol,bool close):Input(i,4100,close)
	{
	originLength=ol;
	codeStart=1<<originLength;
	codeLength=++originLength;
	mask=codeStart*2-1;

	list=dalloc<short*>(4100);
	for(int i=0;i<4100;i++)
		list[i]=dalloc<short>(4100)+1;
	lm=2;
	p=list[2];p[-1]=0;

	breakFlag=false;

	in->checkBits();
	}

	InputLZW::~InputLZW()
	{
	for(int i=0;i<4100;i++)
		dfree(list[i]-1);
	dfree(list);
	}

int 	InputLZW::getRaw(Byte *dest,int size)
	{
	short code=in->getBits(codeLength);

	if(code==codeStart)
		{
		lm=2;p=list[2];p[-1]=0;
		codeLength=originLength;
		mask=codeStart*2-1;
		return 0;
		}
	else if(code==codeStart+1)
		{
		in->alignBits();
		breakFlag=true;
		return 0;
		}
	else
		{
		if(code<codeStart)
			{
			c=&code;cm=1;
			}
		else if(code>codeStart+lm)
			{
			safeError("code>lm %d-%d -> %d.",code,lm,code-codeStart-lm);
			return 0;
			}
		else
			{
			c=list[code-codeStart];cm=c[-1];
			if(code-codeStart==lm)
				cm=p[-1]+1;
			}

		if(lm<0xFFF)
			{
			if(p[-1]!=0)
				{
				p[p[-1]++]=c[0];
				p=list[++lm];
				p[-1]=0;
				if((lm+codeStart)==mask+1&&codeLength<12)
					{
					codeLength++;
					mask=(mask<<1)+1;
					}
				}
			memcpy(p+p[-1],c,cm*sizeof(short));
			p[-1]+=cm;
			}

		int n=cm>size?size:cm;
		for(int i=0;i<n;i++)
			{
			if(c[i]>255)
				error("ci>255");
			((unsigned char*)dest)[i]=(unsigned char)c[i];
			}
		if(cm-n>byteMax-byteIndex)
			error("byteBuffer overflow.");
		for(int i=0;i<cm-n;i++)
			{
			if(c[n+i]>255)
				error("cni>255");
			byteBuffer[byteIndex++]=c[n+i];
			}
		return n;
		}
	}

bool	InputLZW::emptyRaw()
	{
	return breakFlag;
	}


	InputSimpleBlock::InputSimpleBlock(Input *i,bool close):Input(i,255,close)
	{
	num=in->getInt(1);
	}

	InputSimpleBlock::~InputSimpleBlock()
	{
	getEnd();
	}

int 	InputSimpleBlock::getRaw(Byte *dest,int size)
	{
	int n=size>num?num:size;
	in->getBytes(dest,n);
	num-=n;
	if(num==0)
		num=in->getInt(1);
	return n;
	}
bool	InputSimpleBlock::emptyRaw()
	{
	return num==0;
	}








	OutputLZW::OutputLZW(Output *o,int ol,bool close):OutputMask(o,0,close)
	{
	originLength=ol;

	codeStart=1<<originLength;
	codeLength=++originLength;
	
	byteTree=new ByteNode<int>();
	for(int i=0;i<codeStart;i++)
		byteTree->setNext(new ByteNode<int>(i),i);
	nowNode=byteTree;
	nodeIndex=codeStart+2;
	
	out->checkBits();
	out->putBits(codeStart,codeLength);
	}

	OutputLZW::~OutputLZW()
	{
	out->putBits(nowNode->getData(),codeLength);
	out->putBits(codeStart+1,codeLength);
	out->alignBits();
	delete byteTree;
	}

int 	OutputLZW::putRaw(const Byte *data,int size)
	{
	for(int di=0;di<size;di++)
		{
		ByteNode<int> *next=nowNode->getNext(data[di]);
		if(next!=NULL)
			{
			nowNode=next;
			}
		else
			{
			out->putBits(nowNode->getData(),codeLength);
			nowNode->setNext(new ByteNode<int>(nodeIndex++),data[di]);
			nowNode=byteTree->getNext(data[di]);
			
			if(nodeIndex==(1<<12))
				{
				out->putBits(codeStart,codeLength);
				codeLength=originLength;
				
				delete byteTree;
				byteTree=new ByteNode<int>();
				for(int i=0;i<codeStart;i++)
					byteTree->setNext(new ByteNode<int>(i),i);
				nowNode=byteTree->getNext(data[di]);
				nodeIndex=codeStart+2;
				}
			if(nodeIndex==(1<<codeLength)+1)
				{
				if(codeLength>=12)
					error("codeLength overflow.");
				else
					codeLength++;
				}
			}
		}
	return size;
	}






	OutputSimpleBlock::OutputSimpleBlock(Output *o,bool close,int max):OutputMask(o,max,close)
	{

	}
	OutputSimpleBlock::~OutputSimpleBlock()
	{
	cutBlock();
	out->putInt(0,1);
	}

int 	OutputSimpleBlock::putRaw(const Byte *data,int size)
	{
	int n=size>byteMax-byteIndex?byteMax-byteIndex:size;
	memcpy(byteBuffer+byteIndex,data,n);
	byteIndex+=n;

	if(byteIndex==byteMax)
		cutBlock();
	return n;
	}


void	OutputSimpleBlock::cutBlock()
	{
	if(byteIndex==0)
		return;
	out->putInt(byteIndex,1);
	out->putBytes(byteBuffer,byteIndex);
	byteIndex=0;
	}












