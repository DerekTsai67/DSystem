
using namespace dlib;


	Output::Output(int bm,bool close)
	{
	type=0;
	byteMax=bm;
	if(byteMax>0)
		byteBuffer=dalloc<unsigned char>(byteMax);
	else
		byteBuffer=NULL;
	bitBuffer=0;
	bitIndex=byteIndex=0;
	closeSource=close;
	dead=false;
	}

	Output::~Output()
	{
	if(byteBuffer!=NULL)
		{
		if(byteIndex!=0)
			error("Output::byteBuffer last. %d",byteIndex);
		dfree(byteBuffer);
		}
	}

void	Output::setType(int t)
	{
	type|=t;
	}

void	Output::putInt_(int data,int size,int endianSetting)
	{
	if(size==0)
		return;
	if(size>4)
		error("putInt with size > 4.");
	if(size<4&&(data>>(size*8))!=0&&(-data>>(size*8))!=0)
		error("putInt overflow.");
	if(endianSetting&dlib::bigEndian)
		for(int i=0;i<size;i++)
			putBytes((Byte*)(&data)+size-1-i,1);
	else
		putBytes(&data,size);
	}
void	Output::putCoord_(Coord data,int size,int endianSetting)
	{
	putInt(data.x,size);
	putInt(data.y,size);
	}
void	Output::putRGB(DS_RGB data)
	{
	putInt(data.r,1);
	putInt(data.g,1);
	putInt(data.b,1);
	}
void	Output::putBGR(DS_RGB data)
	{
	putInt(data.b,1);
	putInt(data.g,1);
	putInt(data.r,1);
	}
void	Output::putDouble(double data)
	{
	putBytes(&data,8);
	}

void	Output::checkBits()
	{
	if(bitIndex!=0||bitBuffer!=0)
		error("bits last");
	}
void	Output::alignBits_(int endianSetting)
	{
	if(bitIndex!=0)
		{
		if(endianSetting&dlib::bitLeft)
			putBytes((Byte*)&bitBuffer+6,1);
		else
			putBytes(&bitBuffer,1);
		}
	bitIndex=0;
	bitBuffer=0;
	}
void	Output::putBits_(int data,int num,int endianSetting)
	{
	if(data>>num!=0)
		error("putBits overflow. %X %d",data,num);
	if(num>32)
		error("Unsupported bitNum %d.",num);

	if(endianSetting&dlib::bitLeft)
		{
		bitBuffer|=(unsigned long long)data<<(56-bitIndex-num);
		bitIndex+=num;
		while(bitIndex>=8)
			{
			putBytes((Byte*)&bitBuffer+6,1);
			bitBuffer<<=8;
			bitIndex-=8;
			}
		}
	else
		{
		bitBuffer|=data<<bitIndex;
		bitIndex+=num;
		while(bitIndex>=8)
			{
			putBytes(&bitBuffer,1);
			bitBuffer>>=8;
			bitIndex-=8;
			}
		}
	}

void	Output::putDecimal(int d)
	{
	char buffer[25];
	int bi=25;
	if(type&dlib::space)
		putInt(' ',1);
	if(d<0)
		{
		putInt('-',1);
		d*=-1;
		}
	do	{
		buffer[--bi]=d%10+'0';
		d/=10;
	}while(d>0);
	putBytes(buffer+bi,25-bi);
	setType(dlib::space);
	}

void	Output::putText(ConstString data,...)
	{
	if(type&dlib::space)
		putInt(' ',1);
	VSTRING
	putBytes(str.getRaw(),str.getLength());
	setType(dlib::space);
	}

void	Output::putEndLine()
	{
	putString("\r\n");
	type&=~dlib::space;
	}

void	Output::putBytes(const void *data_,int size)
	{
	if(dead)
		return;
	const Byte *data=(Byte*)data_;
	if(data==NULL)
		error("putBytes from NULL");
	while(size>0)
		{
		if(dead)
			return;
		int n=putRaw(data,size);
		data+=n;size-=n;
		}
	}
void	Output::putString(ConstString data,...)
	{
	va_list va;
	va_start(va,data);
	ConstString str=vformat(data,va);
	va_end(va);
	putBytes(str.getRaw(),str.getLength());
	}














	OutputMask::OutputMask(Output *o,int max,bool close):Output(max,close)
	{
	if(o==NULL)
		error("OutputMask to NULL.");
	out=o;
	}

	OutputMask::~OutputMask()
	{
	if(closeSource)
		delete out;
	}

	OutputEmptyMask::OutputEmptyMask(Output *o):OutputMask(o,0,false)
	{
	}
int 	OutputEmptyMask::putRaw(const Byte *data,int size)
	{
	out->putBytes(data,size);
	return size;
	}









