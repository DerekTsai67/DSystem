

class JsonArray;
class JsonObject;
class JsonString;


class JsonType{
	public:
virtual	~JsonType();
virtual void	print()=0;

JsonArray*	getArray();
JsonObject*	getObject();
JsonString*	getString();
	};

class JsonPair{
	public:
	JsonPair(JsonString *js,JsonType *jt);
	~JsonPair();
void	print();
//	private:
JsonString	*name;
JsonType	*data;
	};


class JsonArray:public JsonType{
	public:
//	JsonArray();
	~JsonArray();
LinkedList<JsonObject>*	getLL();
void	print();
	private:
LinkedList<JsonObject>	ll;
	};
	
class JsonObject:public JsonType{
	public:
//	JsonObject();
	~JsonObject();

void		addItem(JsonPair *jp);

LinkedList<JsonPair>*	getLL();
JsonType*	getType(ConstString str);
JsonArray*	getArray(ConstString str);
JsonObject*	getObject(ConstString str);
ConstString	getString(ConstString str);
double		getFloat(ConstString str);
int			getDate(ConstString str);
void		print();
	private:
LinkedList<JsonPair>	ll;
	};
	
class JsonString:public JsonType{
	public:
	JsonString(const Array<char> &arr);
	JsonString();
	~JsonString();
bool	verify(ConstString v);
ConstString	getString();
double	getFloat();
int		getDate();
void	print();
	private:
String	str;
int		index;
	};


class JsonNumber:public JsonType{
	public:
	JsonNumber();
	~JsonNumber();
	private:
	};
class JsonBool:public JsonType{
	public:
	JsonBool();
	~JsonBool();
	private:
	};





JsonType*	loadJSON(Input *in,int safeFail=false);





