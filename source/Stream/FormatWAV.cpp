



/*
Sound*	loadWAV(Input *in,int safeFail)
	{
	Datafile *df=loadWAV_(in,safeFail);
	if(df==NULL)
		return NULL;
	Sound *sound=df->getSound(-1);
	if(sound==NULL)
		{
		error("No Sound in file.");
		return NULL;
		}
	Sound *ret=sound->clone();
	delete df;
	return ret;
	}*/

Datafile*	loadWAV(Input *in,int safeFail)
	{
	argCheck(in==NULL);
	report(1,"Loading WAV.");
	in->setSafeFail(safeFail);

	if(!in->getVerify("RIFF"))
		safeFatalError("Verifying 'RIFF'.");
	if(safeFail>=3)
		{
		delete in;
		return NULL;
		}

	in->getIgnore(4); //fileSize -8
	if(!in->getVerify("WAVEfmt "))
		safeFatalError("Verifying 'WAVEfmt '.");
	if(in->getInt(4)!=16)
		safeFatalError("Unexpected formatSize.");
	if(in->getInt(2)!=1)
		safeFatalError("Unexpected formatCode.");

	int channel=in->getInt(2);
	if(channel<=0)
		safeFatalError("Unexpected channel.");
	int sampleRate=in->getInt(4);//sample/sec
	if(sampleRate<=0)
		safeFatalError("Unexpected sampleRate.");
	in->getIgnore(4);//byte/sec
	in->getIgnore(2);//block align

	int byteRate=in->getInt(2);//bit/sample
	if(byteRate%8!=0)
		safeFatalError("Bit rate %%8!=0.");
	byteRate/=8;
	if(byteRate<=0||byteRate>4)
		safeFatalError("Unsupported byteRate %d.",byteRate);

	if(!in->getVerify("data"))
		safeFatalError("Verifying 'data'.");
	int length=in->getInt(4);
	if(length%(channel*byteRate)!=0)
		safeFatalError("Unsaturated length.");
	length/=channel*byteRate;
	if(safeFail>=3)
		{
		delete in;
		return NULL;
		}

	//raw data
	Sound *sound=new Sound(length,channel,sampleRate,byteRate);
	int roundBit=32-byteRate*8;
	for(int i=0;i<length;i++)
		{
		for(int c=0;c<channel;c++)
			{
			int temp=in->getInt(byteRate);
			sound->setSample(temp<<roundBit>>roundBit,c,i);
			}
		}

	report(-1,"Loaded length=%d  %.2fsec %dbits * %dchannel %.1fkHz",length,(double)length/sampleRate,byteRate*8,channel,sampleRate/1000.0);
	delete in;

	Datafile *df=new Datafile();
	sound->setName("WAV Sound");
	df->addData(sound);
	return df;
	}





void	Sound::saveWAV(Output *out)const
	{
	argCheck(out==NULL);
	report(1,"Saving WAV.");

	out->putString("RIFF");

	out->putInt(length*channel*byteRate+36,4); //fileSize -8
	out->putString("WAVEfmt ");
	out->putInt(16,4);
	out->putInt(1,2);
	out->putInt(channel,2);
	out->putInt(sampleRate,4);//sample/sec
	out->putInt(sampleRate*channel*byteRate,4);//byte/sec
	out->putInt(channel*byteRate,2);//block align
	out->putInt(byteRate*8,2);//bit/sample

	out->putString("data");
	out->putInt(length*channel*byteRate,4);

	//raw data
	int roundMask=(1<<(byteRate*8))-1;
	for(int i=0;i<length;i++)
		for(int c=0;c<channel;c++)
			out->putInt(data[c][i]&roundMask,byteRate);

	report(-1,"Saved length=%d  %.2fsec %dbits * %dchannel %.1fkHz",length,(double)length/sampleRate,byteRate*8,channel,sampleRate/1000.0);
	delete out;
	}








