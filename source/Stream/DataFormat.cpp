




Picture*	loadPictureData(Input *in);
Layer*		loadLayerData(Input *in,Coord size);
DSBitmap*	loadBitmapData(Input *in,Coord size);
Text*		loadTextData(Input *in);


void	saveLayerData(Output *out,Layer *layer);
void	saveBitmapData(Output *out,DSBitmap *bmp);


Datafile	*workingDatafile=NULL;
PointerData	**pointerDataQueue=NULL;
int			pointerDataIndex=0;
static const int	pointerDataMax=100;

void	Datafile::dataLoad()
	{
	report("loadData");
	if(in->empty())
		{
		max=0;
		delete in;
		return;
		}
	if(!in->getVerify("D.S.DataStructure"))
		error("Datafile verify failed.");

	argCheck(workingDatafile!=NULL||pointerDataQueue!=NULL);
	workingDatafile=this;
	pointerDataQueue=dalloc<PointerData*>(pointerDataMax);
	pointerDataIndex=0;

	long fileVersion;
	fileVersion=in->getInt(4);
	max=in->getInt(4);
	if(max>dataMax)
		error("dataCount overflow.");
	switch(fileVersion)
		{
	case 0x0001:
		for(int di=0;di<max;di++)
			{
			int dataType=in->getInt(4);

			int pathMax=in->getInt(4);
			char *path=new char[pathMax+1];
			in->getBytes(path,pathMax);
			path[pathMax]=0;

			switch(dataType)
				{
			case Data::Type_Picture:
				report("loadPicture");
				data[di]=loadPictureData(in);
				break;
			case Data::Type_Text:
				report("loadTXT");
				data[di]=loadTextData(in);
				break;
			case Data::Type_Sound:
				//data[di]=loadSoundData(in);
				//break;
			default:
				error("Undefined dataType %d.",dataType);
				break;
				}
				report("loaded");
			data[di]->setName(path);
			delete [] path;
			}
		break;

	default:
		error("Unsupported fileVersion %04X",fileVersion);
		break;
		}

	delete in;
	
	loaded=true;
	
	for(int i=0;i<max;i++)
		data[i]->setPointer();
	
	if(pointerDataIndex>=pointerDataMax)
		error("PointerData overflow.");
	for(int i=0;i<pointerDataIndex;i++)
		pointerDataQueue[i]->setPointer();
	dfree(pointerDataQueue);
	pointerDataQueue=NULL;
	workingDatafile=NULL;
	}





void	Datafile::dataSave()
	{
	if(workingDatafile!=NULL)
		error("workingDatafile != NULL.");
	workingDatafile=this;
	
	out->putString("D.S.DataStructure");

	long fileVersion=0x0001;
	out->putInt(fileVersion,4);
	out->putInt(max,4);
	switch(fileVersion)
		{
	case 0x0001:
		for(int di=0;di<max;di++)
			{
			out->putInt(data[di]->type,4);
			int pm=0;
			while(data[di]->name[pm]!=0)pm++;
			out->putInt(pm,4);
			out->putBytes(data[di]->name,pm);
			report("saving %d %S",data[di]->type,viewData(data[di]->name,pm));
			data[di]->saveData(out);
			}
		break;

	default:
		error("Undefined fileVersion %04X",fileVersion);
		break;
		}
	delete out;
	workingDatafile=NULL;
	}





Picture*	loadPictureData(Input *in)
	{
	argCheck(in==NULL);

	Picture	*pic=NULL;

	int dataVersion=in->getInt(4);
	switch(dataVersion)
		{
	case 0x0001:
		{
		int picType=in->getInt(4);
		in->getIgnore(4);
		DSBitmap *bmp=loadBitmap(new InputEmptyMask(in),bitmapBMP);
		bmp->fullOptimize();
		pic=new Picture(bmp);

		Coord fixSize;
		switch(picType)
			{
		case Picture::Type_default:
			break;
		case Picture::Type_square:
			fixSize=in->getCoord(4);
			pic->setFixSize(fixSize/2,fixSize/2);
			pic->setSquare(zeroCoord,zeroCoord);
			break;
		case Picture::Type_fractile:
			fixSize=in->getCoord(4);//fracSize
			long arg[3];
			in->getBytes(arg,3*4);
			pic->setFixSize(zeroCoord,zeroCoord);
			pic->setFractile(fixSize,arg[0],arg[1],arg[2]);
			for(int i=0;i<pic->stripMax;i++)
				{
				pic->strip[i].max=in->getInt(4);
				pic->strip[i].time=in->getInt(4);
				}
			break;
		default:
			error("PictureType switchOut.");
			}
		}
		break;
	case 0x0002:
		{
		char ident[5];
		in->getBytes(ident,4);	ident[4]=0;
		DSBitmap *bmp=loadBitmap(new InputEmptyMask(in),bitmapBMP);
		bmp->fullOptimize();
		pic=new Picture(bmp);

		Coord tempSize;
		tempSize=in->getCoord(4);
		pic->setFixSize(tempSize,in->getCoord(4));
		switch(in->getInt(4))
			{
		case Picture::Type_default:
			break;
		case Picture::Type_square:
			tempSize=in->getCoord(4);
			pic->setSquare(tempSize,in->getCoord(4));
			break;
		case Picture::Type_fractile:
			tempSize=in->getCoord(4);
			long arg[3];
			in->getBytes(arg,3*4);
			pic->setFractile(tempSize,arg[0],arg[1],arg[2]);
			for(int i=0;i<pic->stripMax;i++)
				{
				pic->strip[i].max=in->getInt(4);
				pic->strip[i].time=in->getInt(4);
				}
			break;
		default:
			error("PictureType switchOut.");
			}
		}
		break;
	case 0x0003:
	case 0x0004:
		{
		Coord	dataSize=in->getCoord(4);
		int fm=in->getInt(4);
		if(fm<=0||!(dataSize>zeroCoord))
			error("empty Picture.");


		int frameVersion=in->getInt(4);
		for(int fi=0;fi<fm;fi++)
			{
			int delay=in->getInt(4);
			Frame *f=new Frame(loadLayerData(in,dataSize),delay);
			if(pic==NULL)
				pic=new Picture(f);
			else
				pic->addFrame(f);
			
			switch(frameVersion)
				{
			case 0x0001:
				break;
			case 0x0002:
				{
				int nextMax=in->getInt(4);
				int *pi=dalloc<int>(nextMax);
				int *ni=dalloc<int>(nextMax);
				for(int i=0;i<nextMax;i++)
					{
					pi[i]=in->getInt(4);
					ni[i]=in->getInt(4);
					}
				f->setNextFrame(pi,ni,nextMax);
				}
				break;
			default:
				error("Unsupported frameVersion.");
				break;
				}
			}
		pic->setTimeType(in->getInt(4));
		if(dataVersion>=0x0004)
			{
			int pi=in->getInt(4);
			pic->setNextFrame(pi,in->getInt(4));
			}

		Coord tempSize;
		tempSize=in->getCoord(4);
		pic->setFixSize(tempSize,in->getCoord(4));
		switch(in->getInt(4))
			{
		case Picture::Type_default:
			break;
		case Picture::Type_square:
			tempSize=in->getCoord(4);
			pic->setSquare(tempSize,in->getCoord(4));
			break;
		case Picture::Type_fractile:
			tempSize=in->getCoord(4);
			long arg[3];
			in->getBytes(arg,3*4);
			pic->setFractile(tempSize,arg[0],arg[1],arg[2]);
			for(int i=0;i<pic->stripMax;i++)
				{
				pic->strip[i].max=in->getInt(4);
				pic->strip[i].time=in->getInt(4);
				}
			break;
		default:
			error("PictureType switchOut.");
			}
		}
		break;
	default:
		error("Unsupported dataVersion.");
		break;
		}
	return pic;
	}
void		Frame::setPointer()
	{
	if(nextPicIndex==NULL)
		return;
	for(int i=0;i<nextMax;i++)
		nextPic[i]=workingDatafile->getPic(nextPicIndex[i]);
	dfree(nextPicIndex);
	nextPicIndex=NULL;
	}
void		Picture::setPointer()
	{
	if(nextPicIndex==-1)
		return;
	nextPic=workingDatafile->getPic(nextPicIndex);
	for(int i=0;i<frameMax;i++)
		frame[i]->setPointer();
	}



void	Picture::saveData(Output *out)const
	{
	argCheck(out==NULL);

	int dataVersion=0x0004;
	out->putInt(dataVersion,4);
	switch(dataVersion)
		{
	case 0x0001:
		out->putInt(getType(),4);
		out->putString(".bmp");
		getBitmap()->saveBMP(new OutputEmptyMask(out));

		switch(getType())
			{
		case Picture::Type_square:
			out->putCoord((fixHeadSize+fixEndSize+squareHeadSize+squareEndSize)/2,4);
			break;
		case Picture::Type_fractile:
			out->putCoord(fractileSize,4);
			out->putInt(stable,4);
			out->putInt(openning,4);
			out->putInt(ending,4);
			for(int i=0;i<stripMax;i++)
				{
				out->putInt(strip[i].max,4);
				out->putInt(strip[i].time,4);
				}
			break;
			}
		break;
	case 0x0002:
		out->putString(".bmp");
		getBitmap()->saveBMP(new OutputEmptyMask(out));

		out->putCoord(fixHeadSize,4);
		out->putCoord(fixEndSize,4);

		out->putInt(getType(),4);
		switch(getType())
			{
		case Picture::Type_square:
			out->putCoord(squareHeadSize,4);
			out->putCoord(squareEndSize,4);
			break;
		case Picture::Type_fractile:
			out->putCoord(fractileSize,4);
			out->putInt(stable,4);
			out->putInt(openning,4);
			out->putInt(ending,4);
			for(int i=0;i<stripMax;i++)
				{
				out->putInt(strip[i].max,4);
				out->putInt(strip[i].time,4);
				}
			break;
			}
		break;
	case 0x0003:
	case 0x0004:
		{
		out->putCoord(dataSize,4);

		out->putInt(frameMax,4);
		int frameVersion=0x0002;
		out->putInt(frameVersion,4);
		for(int fi=0;fi<frameMax;fi++)
			{
			out->putInt(frame[fi]->getDelay(),4);
			saveLayerData(out,frame[fi]->getLayer());
			
			switch(frameVersion)
				{
			case 0x0001:
				break;
			case 0x0002:
				out->putInt(frame[fi]->getNextFrameNumber(),4);
				for(int i=0;i<frame[fi]->getNextFrameNumber();i++)
					{
					out->putInt(workingDatafile->searchPic(frame[fi]->getNextPic(i)),4);
					out->putInt(frame[fi]->getNextIndex(i),4);
					}
				break;
			default:
				error("Unsupported frameVersion.");
				break;
				}
			}
		out->putInt(timeType,4);
		if(dataVersion>=0x0004)
			{
			out->putInt(workingDatafile->searchPic(nextPic),4);
			out->putInt(nextIndex,4);
			}
		
		out->putCoord(fixHeadSize,4);
		out->putCoord(fixEndSize,4);

		out->putInt(getType(),4);
		switch(getType())
			{
		case Picture::Type_square:
			out->putCoord(squareHeadSize,4);
			out->putCoord(squareEndSize,4);
			break;
		case Picture::Type_fractile:
			out->putCoord(fractileSize,4);
			out->putInt(stable,4);
			out->putInt(openning,4);
			out->putInt(ending,4);
			for(int i=0;i<stripMax;i++)
				{
				out->putInt(strip[i].max,4);
				out->putInt(strip[i].time,4);
				}
			break;
			}
		}
		break;
	default:
		error("Undefined dataVersion.");
		break;
		}
	}



Layer*	loadLayerData(Input *in,Coord size)
	{
	Layer *layer=NULL;
	
	int mask=0x0001,opacity=100,filter=0;
	Coord	deviation=zeroCoord;
	
	int layerVersion=in->getInt(4);
	switch(layerVersion)
		{
	case 0x0001:
		break;
	case 0x0002:
		mask=in->getInt(1);
		opacity=in->getInt(1);
		break;
	case 0x0003:
		mask=in->getInt(1);
		opacity=in->getInt(1);
		filter=in->getInt(1);
		break;
	case 0x0004:
		mask=in->getInt(1);
		opacity=in->getInt(1);
		filter=in->getInt(1);
		deviation=in->getCoord(4);
		break;
	default:
		error("Unsupported layerVersion. %d",layerVersion);
		break;
		}

	int layerType=in->getInt(4);
	switch(layerType)
		{
	case 0x0001:
		layer=new SimpleLayer(loadBitmapData(in,size));
		break;
	case 0x0002:
		{
		FolderLayer *fl=new FolderLayer(size);
		layer=fl;
		int num=in->getInt(4);
		for(int i=0;i<num;i++)
			fl->addLayer(loadLayerData(in,size));
		}
		break;
	case 0x0003:
		{
		int di=in->getInt(4);
		int fi=in->getInt(4);
		int li=in->getInt(4);
		Coord c=in->getCoord(4);
		LinkLayer *ll=new LinkLayer(di,fi,li,c);
		layer=ll;
		if(pointerDataIndex>=pointerDataMax)
			error("PointerData overflow.");
		if(pointerDataQueue==NULL)
			error("pointerDataQueue == NULL.");
		pointerDataQueue[pointerDataIndex++]=ll;
		}
		break;
	case 0x0004:
		{
		DS_RGB color=in->getRGB();
		color.a=in->getUnsignedInt(1);
		layer=new ColorLayer(size,color);
		}
		break;
	default:
		error("Unsupported layerType. %d",layerType);
		break;
		}
	
	layer->setVisible(mask&0x0001);
	layer->setOpacity(opacity);
	layer->setFolding(mask&0x0002);
	layer->setFilterType(filter);
	layer->setExportReserve(mask&0x0004);
	layer->setDeviation(deviation);
	
	return layer;
	}

void	saveLayerData(Output *out,Layer *layer)
	{
	argCheck(out==NULL||layer==NULL);

	int layerVersion=0x0004;
	out->putInt(layerVersion,4);
	switch(layerVersion)
		{
	case 0x0001:
		break;
	case 0x0002:
		out->putInt(layer->getVisible()+(layer->getFolding()<<1),1);
		out->putInt(layer->getOpacity(),1);
		break;
	case 0x0003:
		out->putInt(layer->getVisible()+(layer->getFolding()<<1)+(layer->getExportReserve()<<2),1);
		out->putInt(layer->getOpacity(),1);
		out->putInt(layer->getFilterType(),1);
		break;
	case 0x0004:
		out->putInt(layer->getVisible()+(layer->getFolding()<<1)+(layer->getExportReserve()<<2),1);
		out->putInt(layer->getOpacity(),1);
		out->putInt(layer->getFilterType(),1);
		if(isType(layer,SimpleLayer))
			out->putCoord(zeroCoord,4);
		else
			out->putCoord(layer->getDeviation(),4);
		break;
	default:
		error("Undefined layerVersion.");
		break;
		}
	layer->saveData(out);
	}

void	SimpleLayer::saveData(Output *out)
	{
	out->putInt(0x0001,4);
	saveBitmapData(out,getEditableBitmap());
	}

void	FolderLayer::saveData(Output *out)
	{
	out->putInt(0x0002,4);
	out->putInt(layerMax,4);
	for(int i=0;i<layerMax;i++)
		saveLayerData(out,layer[i]);
	}
	
void	LinkLayer::saveData(Output *out)
	{
	out->putInt(0x0003,4);
	
	Layer *topLayer=target;
	while(topLayer->getOwner(true)!=NULL)
		topLayer=topLayer->getOwner();
	
	for(int di=0;di<workingDatafile->getNumber();di++)
		{
		Picture *pic=workingDatafile->getData(di)->getPic(true);
		if(pic==NULL)
			continue;
		for(int fi=0;fi<pic->getFrameNumber();fi++)
			if(pic->getFrame(fi)->getLayer()==topLayer)
				{
				topLayer->forClear();
				for(int li=0;;li++)
					{
					Layer *nowLayer=topLayer->forNext();
					if(nowLayer==NULL)
						break;
					if(nowLayer==target)
						{
						out->putInt(di,4);
						out->putInt(fi,4);
						out->putInt(li,4);
						out->putCoord(getSize(),4);
						return;
						}
					}
				}
		}
	error("Layer not found.");
	}
	
void	LinkLayer::setPointer()
	{
	argCheck(workingDatafile==NULL);
	Layer *topLayer=workingDatafile->getPic(picIndex)->getFrame(frameIndex)->getLayer();
	topLayer->forClear();
	for(int li=0;;li++)
		{
		Layer *nowLayer=topLayer->forNext();
		if(nowLayer==NULL)
			break;
		if(li==layerIndex)
			{
			setTarget(nowLayer);
			return;
			}
		}
	error("targetLayer not found.");
	}

void	ColorLayer::saveData(Output *out)
	{
	out->putInt(0x0004,4);
	out->putRGB(color);
	out->putInt(color.a,1);
	}
	
DSBitmap*	loadBitmapData(Input *in,Coord size)
	{
	argCheck(in==NULL);
	int bmpVersion=in->getInt(4);
	switch(bmpVersion)
		{
	case 0x0001:
		{
		int bitDepth=in->getInt(1);
		int colorType=in->getInt(1);
		return loadPNGBitmap(in,size,bitDepth,colorType,0,NULL,0);
		}
	default:
		error("Unsupported bmpVersion.");
		break;
		}
	return NULL;
	}

void	saveBitmapData(Output *out,DSBitmap *bmp)
	{
	argCheck(out==NULL);

	int bmpVersion=0x0001;
	out->putInt(bmpVersion,4);
	switch(bmpVersion)
		{
	case 0x0001:
		{
		out->putInt(8,1);
		out->putInt(6,1);
		PictureOption *po=new PictureOption;
		po->bitDepth=8;
		po->colorType=6;
		bmp->savePNGBitmap(out,po);
		}
		break;
	default:
		error("Undefined bmpVersion.");
		break;
		}
	}








Text*	loadTextData(Input *in)
	{
	argCheck(in==NULL);

	Text *txt=new Text();

	int dataVersion=in->getInt(4);
	switch(dataVersion)
		{
	case 0x0001:
		while(true)
			{
			int num=in->getInt(4);
			if(num==-1)
				break;
			String str(num);
			in->getFixedString(str);
			txt->addLine(str);
			}
		txt->deleteLastLine();
		break;
	default:
		error("Unsupported dataVersion. %d",dataVersion);
		break;
		}
	return txt;
	}



void	Text::saveData(Output *out)const
	{
	argCheck(out==NULL);

	int dataVersion=0x0001;
	out->putInt(dataVersion,4);
	switch(dataVersion)
		{
	case 0x0001:
		{
		FOR_ARRAY(i,data)
			{
			out->putInt(data[i]->getNumber(),4);
			out->putBytes(data[i]->getData(),data[i]->getNumber());
			}
		out->putInt(-1,4);
		}
		break;
	default:
		error("Undefined dataVersion.");
		break;
		}
	}










