









void	dump(Input *in,Output *out)
	{
	argCheck(in==NULL);
	if(out==NULL)
		out=new OutputFile("E:\\dump.txt");

	while(true)
		{
		const int bufferSize=4096;
		Byte temp[bufferSize];
		int n=in->getBytes(temp,bufferSize,true);
		out->putBytes(temp,n);
		if(n!=bufferSize)
			break;
		}
		
	delete in;
	delete out;
	}





	OutputStd::OutputStd():Output(0,false)
	{
	num=0;
	}
	OutputStd::~OutputStd()
	{
	//printf("\nOutputStd printed : %d Bytes.\n",num);
	}
int 	OutputStd::putRaw(const Byte *data,int size)
	{
	for(int i=0;i<size;i++)
		//printf("%02X ",((unsigned char *)data)[i]);
		printf("%c",((unsigned char *)data)[i]);
	num+=size;
	return size;
	}





	InputBorder::InputBorder(Input *in,Byte *b,int bm):Input(in,0,true)
	{
	argCheck(b==NULL||bm<=0);
	border=b;
	borderMax=bm;
	queue=new Queue<Byte>(borderMax+10);
	for(int i=0;i<borderMax;i++)
		queue->addItem(in->getInt(1));
	}
	InputBorder::~InputBorder()
	{
	dfree(border);
	delete queue;
	}
	
void	InputBorder::cutBorder()
	{
	if(!empty())
		error("cut before empty.");
	queue->clear();
	for(int i=0;i<borderMax;i++)
		queue->addItem(in->getInt(1));
	}

int 	InputBorder::getRaw(Byte *dest,int size)
	{
	dest[0]=queue->takeItem();
	queue->addItem(in->getInt(1));
	return 1;
	}
bool	InputBorder::emptyRaw()
	{
	for(int i=0;i<borderMax;i++)
		if(queue->peekItem(i)!=border[i])
			return false;
	return true;
	}






	InputArray::InputArray(Byte *arr,int num,bool cs):Input(NULL,100,cs)
	{
	argCheck(arr==NULL||num<-1);
	data=arr;
	index=0;
	max=num;
	}
	InputArray::~InputArray()
	{
	if(closeSource)
		dfree(data);
	}
int 	InputArray::getRaw(Byte *dest,int size)
	{
	if(max==-1)
		{
		memcpy(dest,data+index,size);
		index+=size;
		return size;
		}
	
	int n=100;
	if(n>max-index)
		n=max-index;
	takeMin(n,size);
	memcpy(dest,data+index,n);
	index+=n;
	return n;
	}
bool	InputArray::emptyRaw()
	{
	return index==max;
	}


	OutputArray::OutputArray(Byte *arr,int m):Output(0,true)
	{
	argCheck(arr==NULL||m<-2);
	data=arr;
	index=0;
	max=m;
	}
int 	OutputArray::putRaw(const Byte *src,int size)
	{
	if(max!=-1&&index+size>max)
		{
		error("Overflow.");
		return 0;
		}
	memcpy(data+index,src,size);
	index+=size;
	return size;
	}






	IOBuffer::IOBuffer(int num):Input(NULL,0,false),Output(0,false)
	{
	argCheck(num<=0);
	dataMax=num;
	buffer=dalloc<Byte>(dataMax);
	inIndex=outIndex=0;
	}
	IOBuffer::~IOBuffer()
	{
	dfree(buffer);
	}

int 	IOBuffer::getRaw(Byte *dest,int size)
	{
	int n=outIndex-inIndex;
	if(n>size)
		n=size;
	memcpy(dest,buffer+inIndex,n);
	inIndex+=n;
	return n;
	}
bool	IOBuffer::emptyRaw()
	{
	return inIndex>=outIndex;
	}
	
void	IOBuffer::setPosition(int pos)
	{
	argCheck(pos<0||pos>outIndex);
	inIndex=pos;
	}
	
int 	IOBuffer::putRaw(const Byte *data,int size)
	{
	if(outIndex+size>dataMax)
		{
		while(outIndex+size>dataMax)
			dataMax=dataMax*3/2;
		Byte *nb=dalloc<Byte>(dataMax);
		memcpy(nb,buffer,outIndex);
		dfree(buffer);
		buffer=nb;
		}

	memcpy(buffer+outIndex,data,size);
	outIndex+=size;
	return size;
	}

int 	IOBuffer::getNumber()
	{
	return outIndex-inIndex;
	}

void	IOBuffer::cutBuffer()
	{
	dataMax=outIndex;
	Byte *newBuffer=dalloc<Byte>(dataMax);
	memcpy(newBuffer,buffer,dataMax);
	dfree(buffer);
	buffer=newBuffer;
	}





