

enum	{
	AutoLoseless=-20,
	};

class PictureOption{
	public:
	PictureOption();
	~PictureOption();
PictureOption*	clone()const;
#define PO PictureOption*

PO		setBitDepth(int a);
int		bitDepth;

PO		setGrayScale(int a);
PO		setAlphaChannel(int a);
int		colorType;
int		grayScale;
int		alphaChannel;

PO		setUsePalette(int a);
int		usePalette;

PO		setInterlace(int a);
int		interlace;

PaletteInfo	*palette;
int			paletteMax;

PO		setStableTransparent(int a);
void	fixStableTransparent(DS_RGB &c);
int		stableTransparent;

PO		setSkipHeader(int a);
int		skipHeader;

PO		setThumbnailSize(Coord a);
Coord	thumbnailSize;

//DS_RGB	fixPixel(const DSBitmap *bmp,int x,int y);
	};



enum SuffixType{
	datafileStart,
	datafileDSD,
	datafileEnd,

	textStart,
	textTXT,
	textHTML,
	textEnd,

	bitmapStart,
	bitmapBMP,
	bitmapICO,
	bitmapPNG,
	bitmapJPG,
	bitmapEnd,

	pictureStart,
	pictureCUR,
	pictureGIF,
	pictureEnd,

	soundStart,
	soundWAV,
	soundEnd,

	midiStart,
	midiMIDI,
	midiEnd,

	unknownSuffix,
	suffixTypeCount
	};

int 	getSuffixType(ConstString name);

//Data*		loadData(ConstString name,int safeFail=false);
DSBitmap*	loadBitmap(Input *in,int suffixType,int safeFail=false);
Picture*	loadPicture(Input *in,int suffixType,int safeFail=false);
Sound*		loadSound(Input *in,int suffixType,int safeFail=false);

Datafile*	loadFile(ConstString name,int safeFail=false);
Datafile*	loadFile(Input *in,int suffixType,int safeFail=false);
//Datafile*	loadDSD(Input *in,int safeFail=false);
Datafile*	loadTXT(Input *in,int safeFail=false);
Datafile*	loadBMP(Input *in,int safeFail=false,bool onlyDIB=false);
Datafile*	loadICO(Input *in,int safeFail=false);
Datafile*	loadPNG(Input *in,int safeFail=false);
DSBitmap*	loadPNGBitmap(Input *in,Coord size,int bitDepth,int colorType,int interlace,DS_RGB *palette,int pm,Text *infoText=NULL,int safeFail=0);
Datafile*	loadJPG(Input *in,int safeFail=false);
Datafile*	loadGIF(Input *in,int safeFail=false);
Datafile*	loadTIFF(Input *in,int safeFail=false);
Datafile*	loadWAV(Input *in,int safeFail=false);
Datafile*	loadMIDI(Input *in,int safeFail=false);

#include "FormatJSON.h"













class InputDeflate:public Input{
	public:
	 InputDeflate(Input *i,bool close=true);
	~InputDeflate();
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
	private:
int 	state;
HuffTree *liteTree,*disTree;
int 	noncompressNum;
	};

class InputZlib:public Input{
	public:
	 InputZlib(Input *i,bool close=true);
	~InputZlib();
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
	private:
static const int	endianSetting=dlib::bigEndian;
Input	*deflate;
int s1,s2;
	};

class InputPNG:public Input{
	public:
	 InputPNG(Input *i,bool close=true);
	~InputPNG();
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
int 	getChunk(char *ret);
void	terminateChunk();
	private:
static const int	endianSetting=dlib::bigEndian;
char	chunkName[4];
unsigned int	length;
CrcCheck	*crc;
bool		newChunk;
int			apngSequence;
	};



class OutputDeflate:public OutputMask{
	public:
	 OutputDeflate(Output *o,int bufferSize,bool close=true);
	~OutputDeflate();
int 	putRaw(const Byte *data,int size);
void	liteProcess(bool end);
void	blockProcess(bool end);
	private:
int 	*lite,*dis;
int 	ni,bi,bl,li,di;
	};

class OutputZlib:public OutputMask{
	public:
	 OutputZlib(Output *o,int bufferSize,bool close=true);
	~OutputZlib();
int 	putRaw(const Byte *data,int size);
	private:
static const int	endianSetting=dlib::bigEndian;
Output	*deflate;
int s1,s2;
	};


class OutputPNG:public OutputMask{
	public:
	 OutputPNG(Output *o,bool close=true);
	~OutputPNG();
int 	putRaw(const Byte *data,int size);
void	putChunk(ConstString name);
	private:
static const int	endianSetting=dlib::bigEndian;
void	terminateChunk();
String 	chunkName;
CrcCheck	*crc;
int			apngSequence;
	};



class InputLZW:public Input{
	public:
	 InputLZW(Input *i,int ol,bool close=true);
	~InputLZW();
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
	private:
int 	originLength,codeLength,codeStart,mask;
short	*c,cm,*p,**list,lm;
bool	breakFlag;
	};

class InputSimpleBlock:public Input{
	public:
	 InputSimpleBlock(Input *i,bool close=true);
	~InputSimpleBlock();
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
	private:
int 	num;
	};


class OutputLZW:public OutputMask{
	public:
	 OutputLZW(Output *o,int ol=8,bool close=true);
	~OutputLZW();
int 	putRaw(const Byte *data,int size);
	private:
int 	originLength,codeLength,codeStart;
ByteNode<int>	*byteTree,*nowNode;
int			nodeIndex;
	};

class OutputSimpleBlock:public OutputMask{
	public:
	 OutputSimpleBlock(Output *o,bool close=true,int max=255);
	~OutputSimpleBlock();
int 	putRaw(const Byte *data,int size);
void	cutBlock();
	private:
	};







class InputJPG:public Input{
	public:
	InputJPG(Input *i,bool close=true);
	~InputJPG();
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
int 	getSegment(int *name);
void	terminateSegment();
	private:
void	segmentIn();
static const int	endianSetting=dlib::bigEndian;
int		segmentName;
int		length;
bool	newSegment;
	};



class InputB64:public Input{
	public:
	InputB64(Input *i,bool close=true);
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
	private:
bool	end;
	};
