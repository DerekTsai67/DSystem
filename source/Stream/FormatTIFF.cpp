

class Entry{
	public:
	Entry(int tg,int ty,int ct);
int		tag,type,count,position;
	};

class IFD{
	public:
	IFD(int num);
	~IFD();
void	loadEntry(Input *in,int i,int endianSetting,int safeFail);
int		entryNum,nextPosition;
Entry	**entry;
	};

IFD* loadIFD(Input *in,int endianSetting,int safeFail);



Datafile*	loadTIFF(Input *in,int safeFail)
	{
	argCheck(in==NULL);
	report("Loading TIFF...");
	in->setSafeFail(safeFail);
	
	int ed[2];
	ed[0]=in->getInt(1);
	ed[1]=in->getInt(1);
	if(ed[1]!=ed[0])
		safeFatalError("Verify failed %S.",viewData(ed,2));

	int endianSetting=0;
	if(ed[0]=='M')
		endianSetting|=dlib::bigEndian;
	else if(ed[0]!='I')
		safeFatalError("Verify failed %S.",viewData(ed,2));
	if(in->getInt(2)!=42)
		safeFatalError("Verify 42 failed.");

	int firstPosition=in->getInt(4);
	if(firstPosition!=in->getPosition())
		error("Wrong firstPosition. %d",firstPosition);
	if(safeFail>=3)
		return NULL;
	
	LinkedList<IFD> *ll=new LinkedList<IFD>(false);
	ll->addItem(loadIFD(in,endianSetting,safeFail),true);
	
	while(true)
		{
		warning("w");
		bool ended=true;
		FOR_LL(now,*ll,IFD)
			for(int i=0;i<now->entryNum;i++)
				{
				int pos=now->entry[i]->position;
				if(pos==0)
					continue;
				ended=false;
				if(pos<in->getPosition())
					error("Entry pointed back. %d %d",pos,in->getPosition());
				if(pos==in->getPosition())
					{
					now->loadEntry(in,i,endianSetting,safeFail);
					if(in->getPosition()%2==1)
						in->getIgnore(1);
					break;
					}
				}
			int pos=now->nextPosition;
			if(pos!=0)
				ended=false;
			if(pos<in->getPosition())
				error("NextIFD pointed back. %d %d",pos,in->getPosition());
			if(pos==in->getPosition())
				{
				ll->addItem(loadIFD(in,endianSetting,safeFail),true);
				now->nextPosition=0;
				break;
				}
		END_FOR_LL
		if(ended)
			break;
		}
	
	
	
	
	return NULL;
	}



IFD* loadIFD(Input *in,int endianSetting,int safeFail)
	{
	int num=in->getInt(2);
	IFD *ret=new IFD(num);
	report("EntryNum %d",num);
	for(int i=0;i<num;i++)
		{
		report("entry[%d]",i);
		int tag=in->getInt(2);
		int type=in->getInt(2);
		int count=in->getInt(4);
		ret->entry[i]=new Entry(tag,type,count);
		
		if(type<=0||type>12)
			error("Undefined type. %d",type);
		int typeSize[]={0,1,1,2,4,8,1,1,2,4,8,4,8};
		int size=typeSize[type]*count;
		if(size<=4)
			{
			ret->loadEntry(in,i,endianSetting,safeFail);
			ret->entry[i]->position=0;
			in->getIgnore(4-size);
			}
		else
			ret->entry[i]->position=in->getInt(4);
		}
	ret->nextPosition=in->getInt(4);
	return ret;
	}


	Entry::Entry(int tg,int ty,int ct)
	{
	tag=tg;
	type=ty;
	count=ct;
	position=0;
	}

	IFD::IFD(int num)
	{
	entry=dalloc<Entry*>(num);
	entryNum=num;
	nextPosition=0;
	}
	IFD::~IFD()
	{
	for(int i=0;i<entryNum;i++)
		delete entry[i];
	dfree(entry);
	}

void	IFD::loadEntry(Input *in,int i,int endianSetting,int safeFail)
	{
	
	}










