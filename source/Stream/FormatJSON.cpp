



JsonType*	loadJsonType(Input *in,int safeFail);
JsonArray*	loadJsonArray(Input *in,int safeFail);
JsonObject*	loadJsonObject(Input *in,int safeFail);
JsonString*	loadJsonString(Input *in,int safeFail);

JsonType*	loadJSON(Input *in,int safeFail)
	{
	argCheck(in==NULL);
	if(in->empty())
		{
		safeError("Empty file.");
		return NULL;
		}
	JsonType *ret=loadJsonType(in,safeFail);
	delete in;
	return ret;
	}


JsonType*	loadJsonType(Input *in,int safeFail)
	{
	int c;
	c=in->getInt(1);
	switch(c)
		{
	case '{':
		return loadJsonObject(in,safeFail);
	case '[':
		return loadJsonArray(in,safeFail);
	case '\"':
		return loadJsonString(in,safeFail);
	default:
		error("switchOut. %c",c);
		}
	return NULL;
	}


JsonArray*	loadJsonArray(Input *in,int safeFail)
	{
	JsonArray *ret=new JsonArray;
	
	while(true)
		{
		int c=in->getInt(1);
		switch(c)
			{
		case ',':
			in->getIgnore(1);
		case '{':
			ret->getLL()->addItem(loadJsonObject(in,safeFail),true);
			break;
		case ']':
			return ret;
		default:
			error("switchOut.");
			}
		}
	}


JsonObject*	loadJsonObject(Input *in,int safeFail)
	{
	JsonObject *ret=new JsonObject;
	
	while(true)
		{
		int c=in->getInt(1);
		switch(c)
			{
		case ',':
			in->getIgnore(1);
		case '"':
			{
			JsonString *js=loadJsonString(in,safeFail);
			in->getVerify(":");
			JsonType *jt=loadJsonType(in,safeFail);
			ret->getLL()->addItem(new JsonPair(js,jt),true);
			break;
			}
		case '}':
			return ret;
		default:
			error("switchOut.");
			}
		}
	}


JsonString*	loadJsonString(Input *in,int safeFail)
	{
	Array<char> arr(30);
	bool bk=false;
	while(!bk)
		{
		int c=in->getInt(1);
		switch(c)
			{
		default:
			arr.addItem(c);
			break;
		case '"':
			bk=true;
			break;
			}
		}
	return new JsonString(arr);
	}






	JsonPair::JsonPair(JsonString *js,JsonType *jt)
	{
	argCheck(js==NULL||jt==NULL);
	name=js;
	data=jt;
	}
	JsonPair::~JsonPair()
	{
	delete name;
	delete data;
	}


	JsonType::~JsonType()
	{
	}
JsonArray*	JsonType::getArray()
	{
	typeCheck(this,JsonArray);
	return (JsonArray*)this;
	}
JsonObject*	JsonType::getObject()
	{
	typeCheck(this,JsonObject);
	return (JsonObject*)this;
	}
JsonString*	JsonType::getString()
	{
	typeCheck(this,JsonString);
	return (JsonString*)this;
	}
	
	
	
	JsonArray::~JsonArray()
	{
	}

LinkedList<JsonObject>*	JsonArray::getLL()
	{
	return &ll;
	}
	
	
	JsonObject::~JsonObject()
	{
	}
void	JsonObject::addItem(JsonPair *jp)
	{
	argCheck(jp==NULL);
	ll.addItem(jp,true);
	}

LinkedList<JsonPair>*	JsonObject::getLL()
	{
	return &ll;
	}
JsonType*	JsonObject::getType(ConstString str)
	{
	argCheck(str==NULL);
	FOR_LL(now,*getLL(),JsonPair)
		if(now->name->verify(str))
			return now->data;
	END_FOR_LL
	return NULL;
	}
JsonArray*	JsonObject::getArray(ConstString str)
	{
	JsonType *jt=getType(str);
	typeCheck(jt,JsonArray);
	return (JsonArray*)jt;
	}
JsonObject*	JsonObject::getObject(ConstString str)
	{
	JsonType *jt=getType(str);
	typeCheck(jt,JsonObject);
	return (JsonObject*)jt;
	}
ConstString	JsonObject::getString(ConstString str)
	{
	JsonType *jt=getType(str);
	typeCheck(jt,JsonString);
	return ((JsonString*)jt)->getString();
	}
double		JsonObject::getFloat(ConstString str)
	{
	JsonType *jt=getType(str);
	if(isType(jt,JsonString))
		{
		return jt->getString()->getFloat();
		}
	return 0.0;
	}
int			JsonObject::getDate(ConstString str)
	{
	JsonType *jt=getType(str);
	if(isType(jt,JsonString))
		{
		return jt->getString()->getDate();
		}
	return 0;
	}

void	JsonArray::print()
	{
	report(3,"JSON Array [");
	FOR_LL(now,ll,JsonObject)
		now->print();
	END_FOR_LL
	report(-3,"] JSON Array");
	}
void	JsonObject::print()
	{
	report(3,"JSON Object {");
	FOR_LL(now,ll,JsonPair)
		now->print();
	END_FOR_LL
	report(-3,"} JSON Object");
	}
void	JsonPair::print()
	{
	report("JSON Pair");
	name->print();
	data->print();
	}
void	JsonString::print()
	{
	report("JSON String \"%+S\"",str);
	}


	JsonString::JsonString(const Array<char> &arr):str(arr)
	{
	}
	JsonString::JsonString()
	{
	}
	JsonString::~JsonString()
	{
	}
bool	JsonString::verify(ConstString v)
	{
	return str==v;
	}
ConstString	JsonString::getString()
	{
	return str;
	}
double	JsonString::getFloat()
	{
	double d=0.0;
	sscanf(str,"%lf",&d);
	return d;
	}
int		JsonString::getDate()
	{
	int y,m,d,h,m2,s;
	sscanf(str,"%d-%d-%dT%d:%d:%d.000Z",&y,&m,&d,&h,&m2,&s);
	return unpackYMDHMS(y,m,d,h,m2,s);
	}
	
	
	
	
	
	
