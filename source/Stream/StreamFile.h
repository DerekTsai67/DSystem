

class InputFile:public Input{
	public:
	 InputFile(ConstString name);
	 InputFile(FILE *f,bool closeFile=true);
	~InputFile();
static int		count;
int 			getRaw(Byte *dest,int size);
int 			read(void *d,int s,int n);
bool			emptyRaw();
	private:
FILE	*file;
	};




class OutputFile:public Output{
	public:
	 OutputFile(ConstString name);
	 OutputFile(FILE *f,bool closeFile=true);
	~OutputFile();
static int	count;
int 	putRaw(const Byte *data,int size);
	private:
FILE	*file;
char	*originName,*tempName;
	};
