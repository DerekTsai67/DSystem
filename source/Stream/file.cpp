


bool	isSuffix(ConstString path,ConstString suf)
	{
	argCheck(path==NULL||suf==NULL);
	return getSuffix(path)==suf;
	}
	
ConstString 	getSuffix(ConstString path)
	{
	argCheck(path==NULL);
	int s=path.search(".",true);
	if(s==-1)
		return "";
	return path+s;
	}

int 	getSuffix(ConstString path,char* out,int outSize)
	{
	argCheck(path==NULL||out==NULL);
	ConstString suf=getSuffix(path);
	if(suf.getLength()>outSize)
		suf="";
	memcpy(out,suf,suf.getLength()+1);
	return suf.getLength();
	}

ConstString	getName(ConstString path)
	{
	argCheck(path==NULL);
	int s=path.search("\\/",true);
	if(s==-1)
		return "";
	return path+s+1;
	}
	
void	getName(ConstString path,char *out)
	{
	argCheck(path==NULL||out==NULL);
	ConstString suf=getSuffix(path);
	memcpy(out,suf,suf.getLength()+1);
	}

ConstString	getDirectory(ConstString path)
	{
	argCheck(path==NULL);
	int s=path.search("\\/",true);
	if(s==-1)
		return ".\\";
	return path.cut(s+1);
	}

void	getDirectory(ConstString path,char *out)
	{
	ConstString dic=getDirectory(path);
	memcpy(out,dic.getRaw(),dic.getLength());
	}



int 	copyString(char *dest,const char *src,int max)
	{
	int i;
	for(i=0;i<max;i++)
		if(src[i]==0)
			break;
	if(i==max)
		error("copyString overflow.");
	memcpy(dest,src,i+1);
	return i;
	}

int		getStringLength(const char *str)
	{
	argCheck(str==NULL);
	int i=0;
	while(str[i]!=0)i++;
	return i;
	}
