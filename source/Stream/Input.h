

#define getInt(x) 			getInt_(x,endianSetting)
#define getUnsignedInt(x) 	getUnsignedInt_(x,endianSetting)
#define getCoord(x) 		getCoord_(x,endianSetting)
#define alignBits() 		alignBits_(endianSetting)
#define getBits(x) 			getBits_(x,endianSetting)
#define getPeekBits(x) 		getPeekBits_(x,endianSetting)
#define getIgnoreBits(x) 	getIgnoreBits_(x,endianSetting)

const int endianSetting=0;



class Input{
	public:
	 Input(Input *i,int max,bool close);
virtual	~Input();
void	setType(int t);
void	setRemainSize(int s);
void	setSafeFail(int s);
int 	getSafeFail();
	protected:
Input	*in;
int 	type;
bool	closeSource;
int 	safeFail;

	public:
virtual void	setPosition(int pos);
long long	getPosition()const;
long long	getInSize()const;
	private:
long long	position;
long long	originInPosition;

	public:
int 			getInt_(int size,int endianSetting);
unsigned int	getUnsignedInt_(int size,int endianSetting);
Coord			getCoord_(int size,int endianSetting);
DS_RGB			getRGB();
DS_RGB			getBGR();
double			getDouble();
void			checkBits();
void			alignBits_(int endianSetting);
unsigned int	getBits_(int num,int endianSetting);
unsigned int	getPeekBits_(int num,int endianSetting);
void			getIgnoreBits_(int num,int endianSetting);
int 			getBytes(void *dest,int size,bool unsaturated=false);
virtual int		getRaw(Byte *dest,int size)=0;
int 			getFixedString(String &dest,int num=-1);
int 			getString(String &dest,bool localSafeFail);
int 			getString(char *dest,int size,bool localSafeFail=false);
bool			getVerify(ConstString v);
void			getIgnore(int size);
Text*			getEnd(bool localSafeFail=false);
void			unget(void *data,int size);
bool			empty();
virtual bool	emptyRaw()=0;
	protected:
int 	byteMax,byteMaxOrigin,byteMaxRequired,remainSize;
Byte	*byteBuffer,*byteBufferOrigin;
unsigned long long	bitBuffer;
int 	bitIndex,byteIndex;
	
	public:
void	dump(Output *out=NULL);
	};




class InputEmptyMask:public Input{
	public:
	 InputEmptyMask(Input *i);
int 	getRaw(Byte *dest,int size);
bool	emptyRaw();
	};






