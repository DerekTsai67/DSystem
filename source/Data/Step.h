

template<class Type>
class Step{
	public:
	Step();
virtual	~Step();

	public:
void	addType(int t);
bool	haveType(int t);
enum {
	Type_reverse		=0x0001,
	Type_alwaysReady	=0x0002
	};
	private:
int		type;

	public:
bool	isLegal()const;
	protected:
bool	legal;

	public:
void	setBackup(Type *f);
virtual void	setBackupRaw(Type *f);
//Type*	getBackup()const;
virtual void	clearBackup();
bool	haveBackup()const;

void	setEnable(bool e);
int		getEnable();

void	stepIn();
bool	stepOutReady();
void	stepOut();
virtual void	stepInRaw();
virtual bool	stepOutReadyRaw();
virtual void	stepOutRaw();

ConstString	getName()const;

enum StepState{
	enabled,
	disabled,
	bypassed,
	};

	protected:
bool	enable;
Type	*backup;
ConstString	name;
};


template<class Type>
class StepContainer{
	public:
	StepContainer(Type *&field);
	~StepContainer();
void	addStep(Step<Type> *s);
Step<Type>*	getStep(int index);
int		getNumber();
void	stepBack();
void	stepForward();
void	stepTo(Step<Type> *s,bool multiple=true);
	protected:
LinkedList<Step<Type> >	step;
Type	*&field;

	public:
void	setResponse(Object *o,int ri);
	private:
Object	*responseTarget;
int		responseIndex;
};







