



	Sound::Sound(int n,int c,int s,int b):Data(Type_Sound)
	{
	argCheck(n<=0||c<=0||s<=0||b<=0||b>4);
	length=n;
	channel=c;
	sampleRate=s;
	byteRate=b;
	data=dalloc<long*>(channel);
	for(int i=0;i<channel;i++)
		data[i]=dalloc<long>(length);
	initResource();
	}

	Sound::~Sound()
	{
	for(int i=0;i<channel;i++)
		dfree(data[i]);
	dfree(data);
	deleteResource();
	}

Sound*	Sound::clone()
	{
	Sound *ret=new Sound(length,channel,sampleRate,byteRate);
	for(int i=0;i<channel;i++)
		memcpy(ret->data[i],data[i],length*sizeof(long));
	return ret;
	}


void Sound::setSample(long d,int c,int i)
	{
	if(c>=channel||c<0)
		error("Sound::setSample channel overflow %d",c);
	if(i>=length||i<0)
		error("Sound::setSample index overflow %d",i);
	data[c][i]=d;
	}



int Sound::getLength()
	{
	return length;
	}
int Sound::getChannel()
	{
	return channel;
	}
int Sound::getSample(int c,int i)
	{
	if(c>=channel||c<0)
		error("Sound::getSample channel overflow %d",c);
	if(i>=length||i<0)
		error("Sound::getSample index overflow %d",i);
	return data[c][i];
	}



void	Sound::addSound(Sound *s,int time)
	{
	argCheck(s==NULL);
	time*=sampleRate/60;
	for(int i=0;i<s->length&&(i+time)<length;i++)
		for(int c=0;c<channel;c++)
			data[c][i+time]+=s->data[c%s->channel][i];
	}

void	Sound::multiply(double rate)
	{
	for(int i=0;i<length;i++)
	for(int c=0;c<channel;c++)
		data[c][i]=data[c][i]*rate;
	}





