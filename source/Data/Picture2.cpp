
extern DSBitmap *buffer;



	Picture::Picture(Coord s,DS_RGB rgb):Data(Type_Picture)
	{
	DSBitmap *bmp=new DSBitmap(s);
	bmp->clear(rgb);
	init(new Frame(new SimpleLayer(bmp),10));
	setFixSize(zeroCoord,zeroCoord);
	}

	Picture::Picture(DSBitmap *bmp,int d):Data(Type_Picture)
	{
	argCheck(bmp==NULL||d<=0)
	bmp->fullOptimize();
	init(new Frame(new SimpleLayer(bmp),d));
	setFixSize(zeroCoord,zeroCoord);
	}

	Picture::Picture(DSBitmap *bmp,Coord fh,Coord fe):Data(Type_Picture)
	{
	argCheck(bmp==NULL);
	bmp->fullOptimize();
	init(new Frame(new SimpleLayer(bmp),10));
	setFixSize(fh,fe);
	}

	Picture::Picture(Layer *l,int d):Data(Type_Picture)
	{
	argCheck(l==NULL||d<=0);
	init(new Frame(l,d));
	setFixSize(zeroCoord,zeroCoord);
	}

	Picture::Picture(Frame *f):Data(Type_Picture)
	{
	argCheck(f==NULL);
	init(f);
	setFixSize(zeroCoord,zeroCoord);
	}


/*	Picture::Picture(const Picture *p):Data(Type_Picture)
	{
	init(p->data->clone(),10);
	showSize=p->showSize;
	fixHeadSize=p->fixHeadSize;
	 fixEndSize=p-> fixEndSize;
	typeSize[0]=p->typeSize[0];
	typeSize[1]=p->typeSize[1];
	//memcpy(typeSize,p->typeSize,sizeof(Coord)*2);
	type=p->type;
	}*/

Picture*	Picture::clone()const
	{
	Picture *ret=new Picture(frame[0]->clone());
	ret->setFixSize(fixHeadSize,fixEndSize);
	for(int i=1;i<frameMax;i++)
		ret->addFrame(frame[i]->clone());
	ret->setTimeType(timeType);
	ret->setName(getName());

	switch(type)
		{
	case Type_square:
		ret->setSquare(squareHeadSize,squareEndSize);
		break;
	case Type_fractile:
		ret->setFractile(fractileSize,stable,openning,ending);
		break;
		}
	return ret;
	}

	Picture::~Picture()
	{
	destroyType();
	for(int i=0;i<frameMax;i++)
		delete frame[i];
	dfree(frame);
	}

void	Picture::init(Frame *f)
	{
	dataSize=showSize=f->getSize();
	fixHeadSize=fixEndSize=zeroCoord;
	typeSize[0]=typeSize[1]=Coord(1,3);
	type=Type_default;
	frame=dalloc<Frame*>(1);
	nowFrame=frame[0]=f;
	frameMax=1;
	timeType=Static;
	totalTime=f->getDelay();
	nextPicIndex=-1;
	nextPic=this;
	nextIndex=0;
	}


//==============================================================================


	Frame::Frame(DSBitmap *bmp,int d)
	{
	argCheck(bmp==NULL||d<=0);
	layer=new SimpleLayer(bmp);
	delay=d;
	nextPicIndex=NULL;
	nextPic=dalloc<Picture*>(0);
	nextIndex=dalloc<int>(0);
	nextMax=0;
	}

	Frame::Frame(Layer *l,int d)
	{
	argCheck(l==NULL||d<=0);
	layer=l;
	delay=d;
	nextPicIndex=NULL;
	nextPic=dalloc<Picture*>(0);
	nextIndex=dalloc<int>(0);
	nextMax=0;
	}

	Frame::~Frame()
	{
	delete layer;
	dfree(nextPic);
	dfree(nextIndex);
	}

Frame*	Frame::clone()const
	{
	return new Frame(layer->clone(),delay);
	}

void	Frame::enableLayer()
	{
	if(isType(getLayer(),FolderLayer))
		return;
	FolderLayer *fl=new FolderLayer(getLayer()->getSize());
	fl->addLayer(layer);
	layer=fl;
	}
	/*
void	Frame::disableLayer()
	{
	if(!isType(getLayer(),FolderLayer))
		return;
	FolderLayer *fl=(FolderLayer*)getLayer();
	if(fl->getNumber()!=1)
		return;
	layer=fl->getLayer(0);
	fl->addLayer(layer);
	layer=fl;
	}*/

void	Frame::setLayer(Layer *l)
	{
	argCheck(l==NULL);
	layer=l;
	}
	
Layer*	Frame::getLayer()const
	{
	return layer;
	}

FolderLayer*	Frame::getFolderLayer()
	{
	enableLayer();
	return (FolderLayer*)layer;
	}

void	Frame::setDelay(int d)
	{
	delay=d;
	}

int		Frame::getDelay()const
	{
	return delay;
	}

Coord	Frame::getSize()const
	{
	return getLayer()->getSize();
	}

void	Frame::reSize(Coord s)
	{
	getLayer()->reSize(s);
	}

void		Frame::setNextFrame(int *pi,int *ni,int m)
	{
	dfree(nextPic);
	dfree(nextIndex);
	nextPicIndex=pi;
	nextPic=dalloc<Picture*>(m);
	nextIndex=ni;
	nextMax=m;
	}
	
void	Frame::addNextFrame(Picture *p,int i,int index)
	{
	if(index==-1)
		index=nextMax;
	Picture **np=dalloc<Picture*>(nextMax+1);
	int *ni=dalloc<int>(nextMax+1);
	memcpy(np,nextPic,index*sizeof(Picture*));
	memcpy(ni,nextIndex,index*sizeof(int));
	memcpy(np+index,&p,sizeof(Picture*));
	memcpy(ni+index,&i,sizeof(int));
	memcpy(np+index+1,nextPic+index,(nextMax-index)*sizeof(Picture*));
	memcpy(ni+index+1,nextIndex+index,(nextMax-index)*sizeof(int));
	dfree(nextPic);
	dfree(nextIndex);
	nextPic=np;
	nextIndex=ni;
	nextMax++;
	}
void	Frame::deleteNextFrame(int index)
	{
	argCheck(index<0||index>=nextMax);
	
	Picture **np=dalloc<Picture*>(nextMax-1);
	int *ni=dalloc<int>(nextMax-1);
	memcpy(np,nextPic,index*sizeof(Picture*));
	memcpy(ni,nextIndex,index*sizeof(int));
	memcpy(np+index,nextPic+index+1,(nextMax-index-1)*sizeof(Picture*));
	memcpy(ni+index,nextIndex+index+1,(nextMax-index-1)*sizeof(int));
	dfree(nextPic);
	dfree(nextIndex);
	nextPic=np;
	nextIndex=ni;
	nextMax--;
	}
Picture*	Frame::getNextPic(int i)const
	{
	argCheck(i<0||i>=nextMax);
	return nextPic[i];
	}
int			Frame::getNextIndex(int i)const
	{
	argCheck(i<0||i>=nextMax);
	return nextIndex[i];
	}

int		Frame::getNextFrameNumber()const
	{
	return nextMax;
	}


void	Picture::addFrame(DSBitmap *bmp,int t)
	{
	argCheck(bmp==NULL);
	addFrame(new SimpleLayer(bmp),t);
	}
void	Picture::addFrame(Layer *l,int t)
	{
	argCheck(l==NULL||t<0||l->getSize()!=dataSize);
	if(t==0)
		{
		report("zero delay!");
		t=1;
		}
	addFrame(new Frame(l,t));
	}
void	Picture::addFrame(Frame *f,int index)
	{
	argCheck(f==NULL||f->getSize()!=dataSize||index<-1||index>frameMax);
	if(index==-1)
		index=frameMax;
	//setFrameNumber(frameMax+1);
	Frame **nf=dalloc<Frame*>(frameMax+1);
	memcpy(nf,frame,index*sizeof(Frame*));
	memcpy(nf+index,&f,sizeof(Frame*));
	memcpy(nf+index+1,frame+index,(frameMax-index)*sizeof(Frame*));
	dfree(frame);
	frame=nf;
	frameMax++;
	totalTime+=f->getDelay();
	//report("added %d /%d",f->getDelay(),totalTime);
	}

void	Picture::loseFrame(int index)
	{
	argCheck(index<0||index>=frameMax);
	if(nowFrame==frame[index])
		{
		setFrameIndex(index==0?1:index-1);
		}
	
	totalTime-=frame[index]->getDelay();
	Frame **nf=dalloc<Frame*>(frameMax-1);
	memcpy(nf,frame,index*sizeof(Frame*));
	memcpy(nf+index,frame+index+1,(frameMax-index-1)*sizeof(Frame*));
	dfree(frame);
	frame=nf;
	frameMax--;
	}

void	Picture::deleteFrame(int index)
	{
	argCheck(index<0||index>=frameMax);
	delete frame[index];
	loseFrame(index);
	}

void	Picture::reverseFrame()
	{
	for(int i=0;i<frameMax/2;i++)
		{
		Frame *f=frame[i];
		frame[i]=frame[frameMax-1-i];
		frame[frameMax-1-i]=f;
		}
	}
/*void	Picture::setFrameNumber(int n)
	{
	if(n>frameMax)
		{
		Frame **f=dalloc<Frame*>(n);
		memcpy(f,frame,frameMax*sizeof(Frame*));
		dfree(frame);
		frame=f;
		}
	else
		{
		for(int i=n;i<frameMax;i++)
			delete frame[i];
		}
	frameMax=n;
	}*/

void	Picture::setFrameIndex(int i)
	{
	argCheck(i<0||i>=frameMax);
	nowFrame=frame[i];
	}

void	Picture::setTimeType(int t)
	{
	argCheck(t<0||t>=TimeTypeCount)
	timeType=t;
	}

void	Picture::setDelay(int fi,int delay)
	{
	argCheck(fi<0||fi>=frameMax||delay<=0);
	totalTime-=frame[fi]->getDelay();
	frame[fi]->setDelay(delay);
	totalTime+=frame[fi]->getDelay();
	}

Frame*	Picture::getFrame(int fi)
	{
	argCheck(fi<0||fi>=frameMax);
	return frame[fi];
	}

int 	Picture::getFrameNumber()const
	{
	return frameMax;
	}

int		Picture::getFrameIndex(Frame *f,int safeFail)
	{
	if(f==NULL)
		f=nowFrame;
	for(int i=0;i<frameMax;i++)
		if(f==frame[i])
			return i;
	if(!safeFail)
		error("Frame not exist.");
	return -1;
	}

int 	Picture::getTime()
	{
	switch(timeType)
		{
	case Realtime:
		return clock()*1000/CLOCKS_PER_SEC;
	case FrameDepended:
		return ftime;
	case Static:
		return -1;
	default:
		error("switchOut.");
		}
	return 0;
	}


const DSBitmap*	Picture::getBitmapIndex(int i)const
	{
	argCheck(i<0||i>=frameMax);
	return frame[i]->getLayer()->getBitmap();
	}

const DSBitmap*	Picture::getBitmapTime(int time)const
	{
	time%=totalTime;
	while(time<0)time+=totalTime;
	for(int i=0;i<frameMax;i++)
		{
		time-=frame[i]->getDelay();
		if(time<0)
			return frame[i]->getLayer()->getBitmap();
		}
	error("Pic::getBitmapTime");
	return NULL;
	}

//==============================================================================

void		Picture::setNextFrame(int pi,int ni)
	{
	argCheck(pi<0||ni<0);
	nextPicIndex=pi;
	nextIndex=ni;
	}


void		Picture::setNextFrame(Picture *p,int i)
	{
	argCheck(p==NULL||i<0);
	nextPic=p;
	nextIndex=i;
	}

Picture*	Picture::getNextPic()const
	{
	return nextPic;
	}

int			Picture::getNextIndex()const
	{
	return nextIndex;
	}

void	Picture::goNextFrame(Picture **p,int *index)
	{
	argCheck(p==NULL||index==NULL);
	if(*index>=frameMax-1)
		{
		showThread->waitOnce();
		*p=nextPic;
		*index=nextIndex;
		return;
		}
	*index+=1;
	}
	
//==============================================================================

const DSBitmap*	Picture::getBitmap()const
	{
	return nowFrame->getLayer()->getBitmap();
	}

DSBitmap*	Picture::getEditableBitmap()
	{
	//if(frameMax!=1)
	//	error("getEditableBitmap from an animation.");
	return nowFrame->getLayer()->getEditableBitmap();
	}


void	Picture::reSize(Size s)
	{
	Coord c=s.fix(this);
	dataSize=c;
	setFixSize(fixHeadSize,fixEndSize);
	for(int i=0;i<frameMax;i++)
		frame[i]->reSize(s);
	}

void	Picture::setFixSize(Coord fh,Coord fe)
	{
	if(type!=Type_default&&type!=Type_square)
		error("setFixSize with type.");
	fixHeadSize=fh;
	fixEndSize=fe;
	showSize=dataSize-fixHeadSize-fixEndSize;
	if(!(showSize>=zeroCoord))
		error("SetFixSize overflow.");
	}

Coord	Picture::getSize(Size::Type type)const
	{
	return Size(showSize).fix(this,type);
	}

Coord	Picture::getDataSize()const
	{
	return dataSize;
	}

Coord	Picture::getFixHeadSize()const
	{
	return fixHeadSize;
	}

Coord	Picture::getFixEndSize()const
	{
	return fixEndSize;
	}

Coord	Picture::getSquareHeadSize()const
	{
	if(type!=Type_square)
		error("Wrong type.");
	return squareHeadSize;
	}

Coord	Picture::getSquareEndSize()const
	{
	if(type!=Type_square)
		error("Wrong type.");
	return squareEndSize;
	}

Coord	Picture::getFractileSize()const
	{
	if(type!=Type_fractile)
		error("Wrong type.");
	return fractileSize;
	}


bool	Picture::inside(Position pos)const
	{
	Coord c=pos.fix(NULL,this);
	if(c>=zeroCoord&&c<dataSize)
		return true;
	return false;
	}
//==========================================================================type


int Picture::getType()const
	{
	return type;
	}

void Picture::destroyType()
	{
	switch(type)
		{
	case Type_fractile:
		delete [] strip;
		break;
		}
	type=Type_default;
	}

void Picture::setSquare(Coord sh,Coord se)
	{
	if(type!=Type_default&&type!=Type_square)
		error("setSquare with wrong type : %d",type);
	type=Type_square;

	squareHeadSize=sh;
	squareEndSize=se;
	if(!(showSize>=squareHeadSize+squareEndSize))
		error("setSquare with innerBlock<0.");
	}

void Picture::setFractile(Coord fs,int s,int o,int e)
	{
	if(type!=Type_default)
		error("setFractile with wrong type : %d",type);
	type=Type_fractile;

	fractileSize=fs;
	showSize=fractileSize-fixHeadSize-fixEndSize;

	stripMax=dataSize.y/fractileSize.y;
	int tm=dataSize.x/fractileSize.x;
	strip=new Strip[stripMax];
	for(int i=0;i<stripMax;i++)
		{
		strip[i].type=Strip::Type_default;
		strip[i].max=tm;
		strip[i].time=tm*10;
		}
	stable=s;
	openning=o;
	ending=e;
	setTimeType(FrameDepended);
	}



//=====================================================================pixelDraw
void	Picture::putPixel(Position p,DS_RGB rgb)
	{
	getEditableBitmap()->put(p.fix(NULL,this),rgb);
	}

DS_RGB	Picture::getPixel(Position p)
	{
	return nowFrame->getLayer()->getBitmap()->get(p.fix(NULL,this));
	}

void	Picture::bucketFill(Position p,DS_RGB rgb)
	{
	getEditableBitmap()->bucketFill(p.fix(NULL,this),rgb);
	}

void	Picture::drawLineC(Position p1,Position p2,DS_RGB c1,DS_RGB c2)
	{
	getEditableBitmap()->drawLineC(p1.fix(NULL,this),p2.fix(NULL,this),c1,c2);
	}

void	Picture::drawLineD(Position p1,Coord s,DS_RGB rgb)
	{
	getEditableBitmap()->drawLineD(p1.fix(NULL,this),s,rgb,rgb);
	}

void	Picture::drawRect(Position p,Coord s,DS_RGB rgb)
	{
	getEditableBitmap()->drawRect(p.fix(NULL,this),s,rgb);
	}

void	Picture::drawFilledRect(Position p,Coord s,DS_RGB rgb)
	{
	getEditableBitmap()->drawFilledRect(p.fix(NULL,this),s,rgb);
	}

void Picture::drawCircle(Position p,int radius,int thick,DS_RGB rgb)
	{
	getEditableBitmap()->drawCircle(p.fix(NULL,this),radius,thick,rgb);
	}

//==========================================================================blit

void Picture::move(Coord p,DS_RGB c)
	{
	getEditableBitmap()->move(p,c);
	}

void Picture::clear(DS_RGB rgb)
	{
	getEditableBitmap()->clear(rgb);
	}

void	Picture::ignoreAlpha()
	{error("editing");
	for(int fi=0;fi<frameMax;fi++)
		;//frame[fi]->img->ignoreAlpha();
	}

void Picture::blit(const Picture *pic,Position p)
	{
	getEditableBitmap()->blit(pic->getBitmap(),p.fix(pic,this));
	}

void Picture::blitLine(const Picture *pic,int ti,Position p,Size size)
	{
	Coord ps=pic->showSize+pic->fixHeadSize+pic->fixEndSize;
	Coord rs=size.fix(pic->fixHeadSize+pic->fixEndSize);
	Coord es=rs-pic->fixHeadSize-pic->fixEndSize;
	if(es.y!=pic->showSize.y)
		error("blitLine with different size.y.");
	Coord c=p.fix(pic->fixHeadSize,es,pic->fixEndSize,this);

	DSBitmap *b1=getEditableBitmap();
	const DSBitmap *b2=pic->getBitmap();
	b1->blit(c       ,b2,Coord(ps.x*0,ps.y*ti),ps);
	for(int x=ps.x;x<rs.x-ps.x;x+=ps.x)
		b1->blit(c.x+x	,c.y,b2,ps.x*1,ps.y*ti,ps.x,ps.y);
	b1->blit(c.x+rs.x-ps.x,c.y,b2,ps.x*2,ps.y*ti,ps.x,ps.y);
	}

void Picture::blitSquare(const Picture *pic,Position pos,Size size)
	{
	if(pic->getType()!=Type_square)
		error("blitSquare from not square.");

	Coord rs=size.fix(pic->fixHeadSize+pic->fixEndSize);
	Coord es=rs-pic->fixHeadSize-pic->fixEndSize;
	Coord s0=pic->fixHeadSize+pic->squareHeadSize;
	Coord s1=pic->showSize-pic->squareHeadSize-pic->squareEndSize;
	Coord s2=pic->fixEndSize+pic->squareEndSize;
	Coord s4=rs-s0-s2;
	Coord p0=zeroCoord;
	Coord p1=p0+s0;
	Coord p2=p1+s1;
	Coord p3=p2+s2;
	if(p3!=pic->dataSize)
		error("p3!=data");
	Coord t0=pos.fix(pic->fixHeadSize,es,pic->fixEndSize,this);
	Coord t1=t0+s0;
	Coord t2=t1+s4;
	Coord t3=t2+s2;
	if(t3!=t0+rs)
		error("t3!=data");
	Coord l0=Coord(t1.x>=0?0:-t1.x/s1.x,t1.y>=0?0:-t1.y/s1.y);

	//report("(%d,%d) (%d,%d) (%d,%d) (%d,%d)",s0.x,s0.y,s1.x,s1.y,s2.x,s2.y,s4.x,s4.y);
	//report("(%d,%d) (%d,%d) (%d,%d) (%d,%d)",p0.x,p0.y,p1.x,p1.y,p2.x,p2.y,p3.x,p3.y);
	//report("(%d,%d) (%d,%d) (%d,%d) (%d,%d)",t0.x,t0.y,t1.x,t1.y,t2.x,t2.y,t3.x,t3.y);


	DSBitmap *b1=getEditableBitmap();
	const DSBitmap *b2=pic->getBitmap();
	Coord c;
	int x,y;
	for(c.y=l0.y; c.y*s1.y<s4.y && t1.y+c.y*s1.y<dataSize.y; c.y++)
	for(c.x=l0.x; c.x*s1.x<s4.x && t1.x+c.x*s1.x<dataSize.x; c.x++)
		{
		b1->blit(t1+c*s1,b2,p1,s1&(s4-c*s1));
		}
	for(y=l0.y; (y+1)*s1.y<=s4.y && t1.y+y*s1.y<dataSize.y; y++)
		{
		b1->blit(t0.x,t1.y+s1.y*y,b2,p0.x,p1.y,s0.x,s1.y);
		b1->blit(t2.x,t1.y+s1.y*y,b2,p2.x,p1.y,s2.x,s1.y);
		}
	b1->blit(t0.x,t1.y+s1.y*y,b2,p0.x,p1.y,s0.x,s4.y-s1.y*y);
	b1->blit(t2.x,t1.y+s1.y*y,b2,p2.x,p1.y,s2.x,s4.y-s1.y*y);
	for(x=l0.x; (x+1)*s1.x<=s4.x && t1.x+x*s1.x<dataSize.x; x++)
		{
		b1->blit(t1.x+x*s1.x,t0.y,b2,p1.x,p0.y,s1.x,s0.y);
		b1->blit(t1.x+x*s1.x,t2.y,b2,p1.x,p2.y,s1.x,s2.y);
		}
	b1->blit(t1.x+x*s1.x,t0.y,b2,p1.x,p0.y,s4.x-s1.x*x,s0.y);
	b1->blit(t1.x+x*s1.x,t2.y,b2,p1.x,p2.y,s4.x-s1.x*x,s2.y);
	b1->blit(t0.x,t0.y,b2,p0.x,p0.y,s0.x,s0.y);
	b1->blit(t2.x,t0.y,b2,p2.x,p0.y,s2.x,s0.y);
	b1->blit(t0.x,t2.y,b2,p0.x,p2.y,s0.x,s2.y);
	b1->blit(t2.x,t2.y,b2,p2.x,p2.y,s2.x,s2.y);
	}

Picture* Picture::makeTile(Coord ti)const
	{
	if(getType()!=Type_fractile)
		error("makeTile from not fractile.");
	Coord tileSize=showSize+fixHeadSize+fixEndSize;
	Picture *ret=new Picture(tileSize);
	ret->setFixSize(fixHeadSize,fixEndSize);
	ret->getEditableBitmap()->blit(zeroCoord,getBitmap(),ti*tileSize,tileSize);
	return ret;
	}

Picture* Picture::makeSquare(Size size)const
	{
	if(getType()!=Type_square)
		error("makeSquare from not square.");
	Picture *ret=new Picture(size.fix(fixHeadSize+fixEndSize),DS_RGB(255,200,255));
	ret->setFixSize(fixHeadSize,fixEndSize);
	ret->blitSquare(this,zeroRealPosition,size);
	return ret;
	}

Picture* Picture::makeLine(int i,Size size)const
	{
	if(getType()!=Type_fractile)
		error("makeLine from not fractile.");/*
	if(strip[i].type!=Type_line)
		error("makeLine from not fractile::line.");*/
	Coord s=size.fix(fixHeadSize+fixEndSize);
	if(s.y!=(showSize+fixHeadSize+fixEndSize).y)
		error("makeLine with different size.y.");
	Picture *ret=new Picture(s,DS_RGB(255,200,255));
	ret->blitLine(this,i,zeroRealPosition,size);
	return ret;
	}

Picture* Picture::makeScale(Size size)const
	{
	Picture *ret=new Picture(size.fix(fixHeadSize+fixEndSize),DS_RGB(255,200,255,0));
	ret->drawScale(this,zeroRealPosition,size);
	ret->fixHeadSize=fixHeadSize*size.fix(fixHeadSize+fixEndSize)/dataSize;
	ret->fixEndSize = fixEndSize*size.fix(fixHeadSize+fixEndSize)/dataSize;
	ret->showSize=showSize*size.fix(fixHeadSize+fixEndSize)/dataSize;
	return ret;
	}
//==========================================================================draw
void Picture::drawTo(Picture *pic,Position p)const
	{
	pic->draw(this,p);
	}
void Picture::draw(const Picture *pic,Position p)
	{
	switch(pic->getType())
		{
	case Type_default:
	case Type_square:
	case Type_fractile:
		getEditableBitmap()->draw(pic->getBitmap(),p.fix(pic,this));
		break;
		drawFractile(pic,p,zeroCoord);
		break;
		}
	}
void Picture::drawScale(const Picture *pic,Position p,Size size)
	{
	Coord toSize=size.fix(pic);
	Coord fromSize=pic->dataSize;
	getEditableBitmap()->drawScale(pic->getBitmap(),p.fix(pic->fixHeadSize*toSize/fromSize,pic->showSize*toSize/fromSize,pic->fixEndSize*toSize/fromSize,this),toSize);
	}
void Picture::drawFractile(const Picture *pic,Position p,Coord ti)
	{
	if(pic->getType()!=Type_fractile)
		error("drawFractile from not fractile.");/*
	Strip &s=pic->strip[pic->nowAct];
	int t=((ftime-pic->startTime)/(s.time/s.max))%s.max;
	drawTile(pic,p,pic->showSize,Coord(t,pic->nowAct*0));
	*/
	drawTile(pic,p,pic->fractileSize,ti);
	}
void Picture::drawTile(const Picture *pic,Position p,Coord td,Coord ti)
	{
	getEditableBitmap()->draw(p.fix(pic,this),pic->getBitmap(),ti*td,td);
	}


DSBitmap	*showDest;
extern DSBitmap *DSBuffer;

void	setShowDest(Picture *pic)
	{
	if(pic==NULL)
		showDest=DSBuffer;
	else
		showDest=pic->getEditableBitmap();
	}

void Picture::show(Position pos,Coord s,int index/*,Picture *dest*/)
	{
	if(s!=showSize)
		error("Pic::show with unexpected size. %d,%d %d,%d",s.x,s.y,showSize.x,showSize.y);
	DSBitmap *d;
	//if(dest==NULL)
		d=showDest;
	//else
	//	d=dest->getEditableBitmap();
	
	
	d->draw(pos.fix(this,NULL),getBitmapIndex(index),zeroCoord,showSize+fixHeadSize+fixEndSize);
	}

/*
void	Displayer::show(Coord p)
	{
	if(pic!=NULL)
		switch(pic->getType())
			{
		case Picture::Type_default:
		case Picture::Type_square:
			pic->show(pos+p,pic->showSize,frameIndex);
			break;
		case Picture::Type_fractile:{
			Picture::Strip &s=pic->strip[nowAct];
			int t=((ftime-startTime)/(s.time/s.max))%s.max;
			Coord td=pic->getSize();
			Coord ti=Coord(t,nowAct);
			p+=showPosition;
			showDest->draw(p,pic->getBitmap(),ti*td,td);
			//report("showFrac %d %d %d %d %d",t,ftime,startTime,s.time,s.max);
			
			break;}
			}
	else
		{
		//showDest->draw(p,anime->getBitmap(startTime),zeroCoord,anime->getSize());
		
		
		}
	}
*/


