#ifdef DS_ALLEGRO
struct ALLEGRO_BITMAP;
struct ALLEGRO_COLOR;
#endif
#ifdef DS_WINAPI
struct tagBITMAPINFO;
#endif

int		compareAlpha(const void *av,const void *bv);
int		compareColor(const void *av,const void *bv);

struct DS_RGB{
	public:
	DS_RGB();
	DS_RGB(int R,int G,int B);
	DS_RGB(int R,int G,int B,int A);
	DS_RGB(DS_RGB C,int A);
bool	same(const DS_RGB &c)const;
bool	totalSame(const DS_RGB &c)const;
bool	operator==(const DS_RGB &a)const;
bool	operator!=(const DS_RGB &a)const;
DS_RGB	operator-(const DS_RGB &a)const;

int		getGray()const;
int		getAverage()const;
int		getMax()const;
int		getMin()const;
DS_RGB	getReverse()const;
int		getHue()const;
int		getHWCW()const;
int		getHWCC()const;
int		getHWCB()const;
int		getHSLS()const;
int		getHSLL()const;
int		getHSVS()const;
int		getHSVV()const;
double	getYCCY()const;
double	getYCCb()const;
double	getYCCr()const;
Byte 	b,g,r,a;
#ifdef DS_ALLEGRO
ALLEGRO_COLOR	toAL();
#endif
	};

DS_RGB	makeHWC(int h,int w,int c);
DS_RGB	makeHSL(double h,double s,double l);
DS_RGB	makeHSL(int h,int s,int l);
DS_RGB	makeHSV(double h,double s,double v);
DS_RGB	makeHSV(int h,int s,int v);
DS_RGB	makeYCbCr(int y,int cb,int cr);
DS_RGB	makeYCbCr(double y,double cb,double cr);
DS_RGB	blend(DS_RGB c1,int r1,DS_RGB c2,int r2);

const DS_RGB white			(255,255,255);
const DS_RGB gray			(128,128,128);
const DS_RGB black			(  0,  0,  0);
const DS_RGB red			(255,  0,  0);
const DS_RGB green			(  0,255,  0);
const DS_RGB blue			(  0,  0,255);
const DS_RGB brightBlue		(  0,255,255);
const DS_RGB brightPink 	(255,  0,255);
const DS_RGB yellow			(255,255,  0);
const DS_RGB transparent	(255,  0,  0,0);
const DS_RGB orange			(255,128, 64);



struct PaletteInfo{
	DS_RGB color;
	int count,index;
	};
int		comparePaletteCount(const void *a_,const void *b_);
int		comparePaletteColor(const void *a_,const void *b_);
	

class DSBitmap{
	public:
	 DSBitmap(Coord s);
	~DSBitmap();
DSBitmap*	clone()const;
DSBitmap*	cloneSize(Coord p1,Coord s)const;
DSBitmap*	cloneDiffer(const DSBitmap *ref,Coord *pos=NULL)const;
void		reSize(Coord newSize);
DS_RGB**	getData();
Coord		getSize()const;
static int	count;
	private:
void	init(Coord s);
struct DS_RGB	**data;
Coord	size,dataSize;



	public:
friend class	Picture;
int 	getPalette(DS_RGB *palette,Byte **&paletteMap)const;
bool	save(Output *out,int suffixType,int safeFail=false)const;
void	saveBMP(Output *out,PictureOption *po=NULL)const;
void	saveICO(Output *out,PictureOption *po=NULL)const;
void	savePNG(Output *out,PictureOption *po=NULL)const;
void	savePNGBitmap(Output *out,PictureOption *option)const;
void	saveJPG(Output *out)const;
void	saveGIF(Output *out)const;


	public:
void	edited();
void	edited(int y);
void	fullOptimize();
bool	isFullOpaque()const;
bool	isFullTransparent()const;
bool	monoOptimize(int y)const;
int		getOptimizer(Coord p)const;
	private:
//void	setOptimizer(int y,int x,int num,int alpha);
mutable int		**optimizer;
mutable int		*optimizeTime,nextTime,nowTime;

	public:
void	drawPixel(Coord c,DS_RGB color);
void	put(Coord c,DS_RGB color);
void	put(Coord c,DS_RGB color,int num);
void	setAlpha(Coord c,int a);
DS_RGB	get(int x,int y)const;
DS_RGB	get(Coord c)const;
int		getAlpha(Coord c)const;

	public:
void	bucketFill(Coord c,DS_RGB color);
	private:
struct BucketNode{BucketNode *next;};
void	bucketCenter(Coord s,int side,Coord &c,int i,BucketNode *&now,BucketNode *&last,BucketNode *&origin,bool &b);
void	bucketSide(Coord s,Coord cv,int side,int iv,int max,Coord &c,int i,BucketNode *&now,BucketNode *&last,BucketNode *&origin,bool &b);
void	bucketCorner(Coord s,int i,int ia,int ib,Coord &c,BucketNode *&now,BucketNode *&origin);
void	bucketDraw(Coord s,Coord cv,int max,int skip,BucketNode *now,BucketNode *origin,DS_RGB color);
void	bucketFlow(BucketNode *now,BucketNode *a,BucketNode *b,BucketNode *origin);

	public:
void	clear(DS_RGB);
void	clear(DS_RGB c,Coord p,Coord s);
void	move(Coord p,DS_RGB c);
void	ignoreAlpha();
void	convertAlpha(DS_RGB c);
void	drawLineC(Coord p1,Coord p2,DS_RGB rgb,DS_RGB c2=brightPink);
void	drawLineD(Coord p1,Coord s,DS_RGB rgb,DS_RGB c2=brightPink);
void	drawRect(Coord p1,Coord s,DS_RGB rgb);
void	drawFilledRect(Coord p1,Coord s,DS_RGB rgb);
void	drawCircle(Coord p1,int inRadius,int thick,DS_RGB rgb);

	public:
void	blit(const DSBitmap *src,Coord p);
void	blit(int dpx,int dpy,const DSBitmap *src,int spx,int spy,int dsx,int dsy);
void	blit(Coord dp,const DSBitmap *src,Coord sp,Coord drawSize);

void	draw(const DSBitmap *src,Coord p);
void	draw(Coord dp,const DSBitmap *src,Coord sp,Coord drawSize);
void	drawScale(const DSBitmap *src,Coord dp,Coord drawSize);

void	drawAlpha(const DSBitmap *src);

#ifdef DS_ALLEGRO
	public:
	 DSBitmap(ALLEGRO_BITMAP *alb);
ALLEGRO_BITMAP* toAL();
void toAL(ALLEGRO_BITMAP *buf);
#endif

#ifdef DS_WINAPI
	public:
unsigned char*	toWinDIB(tagBITMAPINFO *info)const;
void			toWinDIB(unsigned char*,tagBITMAPINFO *info)const;
#endif
};


