

struct ALLEGRO_BITMAP;
struct ALLEGRO_COLOR;
class PictureBlock;
class Picture;

void	setShowDest(Picture*);

class Frame{
	public:
	Frame(DSBitmap *bmp,int delay);
	Frame(Layer *layer,int delay);
	~Frame();
Frame*	clone()const;
void	enableLayer();
void	setLayer(Layer *l);
Layer*	getLayer()const;
FolderLayer*	getFolderLayer();
void	setDelay(int delay);
int		getDelay()const;
Coord	getSize()const;
void	reSize(Coord s);
	private:
Layer	*layer;
int 	delay;
	
	public:
void		setNextFrame(int *pi,int *ni,int m);
void		setPointer();
void		addNextFrame(Picture *np,int ni,int index=-1);
void		deleteNextFrame(int fi);
Picture*	getNextPic(int i)const;
int			getNextIndex(int i)const;
int			getNextFrameNumber()const;
	private:
int		*nextPicIndex;
Picture **nextPic;
int		*nextIndex;
int		nextMax;
	};

class Picture:public Data{
	public:
	Picture(Coord s,DS_RGB rgb=DS_RGB(128,128,128));
	Picture(DSBitmap *bmp,Coord fh,Coord fe);
	Picture(DSBitmap *bmp,int delay=10);
	Picture(Layer *l,int delay=10);
	Picture(Frame *f);
Picture*	clone()const;
virtual	~Picture();
void	init(Frame *f);
friend class Font;
friend class Displayer;
friend class Position;
friend class Size;

	public:
friend Picture*	loadPictureData(Input *in);
bool	save(Output *out,int suffixType,int safeFail=false)const;
void	saveData(Output *out)const;
void	saveGIF(Output *out)const;
void	savePNG(Output *out,PictureOption *po=NULL)const;

	public:
const DSBitmap*	getBitmap()const;
DSBitmap*	getEditableBitmap();
	protected:
//DSBitmap	*data;

	public:
void	addFrame(DSBitmap *bmp,int t);
void	addFrame(Layer *l,int t);
void	addFrame(Frame *f,int index=-1);
//void	setFrameNumber(int n);
void	loseFrame(int fi);
void	deleteFrame(int fi);
void	reverseFrame();
void	setFrameIndex(int i);
void	setTimeType(int t);
void	setDelay(int fi,int delay);
Frame*	getFrame(int fi);
int		getFrameIndex(Frame *f=NULL,int safeFail=0);
int 	getFrameNumber()const;
int 	getTime();
const DSBitmap*	getBitmapIndex(int index)const;
const DSBitmap*	getBitmapTime(int time)const;
enum TimeType{
	Realtime,		//1000ms	per sec
	FrameDepended,	//60frame
	Static,
	TimeTypeCount
	//Not Editable
	};
Frame	*nowFrame;
	private:
Frame	**frame;
int 	frameMax;
int 	totalTime;
int 	timeType;
	
	public:
void		setNextFrame(int pi,int ni);
void		setPointer();
void		setNextFrame(Picture *p,int i);
Picture*	getNextPic()const;
int			getNextIndex()const;
void		goNextFrame(Picture **p,int *index);
	private:
int		nextPicIndex;
Picture	*nextPic;
int		nextIndex;


	public:
void	reSize(Size s);
void	setFixSize(Coord fh,Coord fe);
Coord	getSize(Size::Type type=Size::Effective)const;
Coord	getDataSize()const;
Coord	getFixHeadSize()const;
Coord	getFixEndSize()const;
bool	inside(Position pos)const;
	protected:
Coord	dataSize,showSize,fixHeadSize,fixEndSize;


//==========================================================================type
	public:
int 	getType()const;
void	destroyType();
enum Type{
	Type_default,
	Type_square,
	Type_fractile,
	Type_count
	};
	protected:
int 	type;
Coord	typeSize[2];
Coord	&squareHeadSize=typeSize[0],&squareEndSize=typeSize[1],&fractileSize=typeSize[0];

//=====square=====
	public:
void	setSquare(Coord sh,Coord se);
Coord	getSquareHeadSize()const;
Coord	getSquareEndSize()const;
	private:


//====fractile====
	public:
void	setFractile(Coord size,int s,int o,int e);
Coord	getFractileSize()const;
struct Strip{
	enum Type{
		Type_loopAnime,
		Type_onceAnime,
		Type_line,
		Type_switch,
		Type_count,
		Type_default=Type_loopAnime
		} type;
	int max;
	int time;
	};
	private:
Strip	*strip;
int 	stripMax;
int 	stable,openning,ending;



//=========================================================================pixel
	public:
void	putPixel(Position p,DS_RGB rgb);
DS_RGB	getPixel(Position p);
void	bucketFill(Position p,DS_RGB rgb);
void	drawLineC(Position p1,Position p2,DS_RGB rgb,DS_RGB c2=brightPink);
void	drawLineD(Position p1,Coord s,DS_RGB rgb);
void	drawRect(Position p1,Coord s,DS_RGB rgb);
void	drawFilledRect(Position p1,Coord s,DS_RGB rgb);
void	drawCircle(Position p1,int inRadius,int thick,DS_RGB rgb);

//==========================================================================blit
	public:
void	clear(DS_RGB rgb);
void	move(Coord p,DS_RGB c);
void	ignoreAlpha();
void	blit(const Picture *pic,Position p);
void	blitLine(const Picture *pic,int ti,Position p,Size size);
void	blitSquare(const Picture *pic,Position pos,Size size);

Picture*	makeTile(Coord ti)const;
Picture*	makeLine(int i,Size size)const;
Picture*	makeSquare(Size size)const;
Picture*	makeScale(Size size)const;

//==========================================================================draw
	public:
void	draw(const Picture *pic,Position p);
void	drawScale(const Picture *pic,Position p,Size size);
void	drawFractile(const Picture *pic,Position p,Coord ti);
void	drawTo(Picture *pic,Position p)const;
void	drawTile(const Picture *pic,Position p,Coord td,Coord ti);
void	show(Position pos=zeroEffectivePosition,Coord s=zeroCoord,int index=0/*,Picture *dest=NULL*/);




#ifdef DS_ALLEGRO
	public:
	 Picture(ALLEGRO_BITMAP *bmp);
operator ALLEGRO_BITMAP*();
#endif
};





