
class Picture;
class Sound;
class Text;




class Data{
	public:
			Data(int type);
void		init(int type,ConstString p);
virtual		~Data();
friend class Datafile;
void		setName(ConstString n);
ConstString getName()const;

virtual Data*	clone()const;
bool			savePath(ConstString path=NULL,int safeFail=false)const;
virtual bool	save(Output *out,int suffixType,int safeFail=false)const;
virtual	void	saveData(Output *out)const;

virtual void	setPointer();

enum Type{
	Type_Picture=0,
	Type_Sound=1,
	Type_Text=2,
	Type_count,
	Type_nothing
	//Not editable
	};

void*		getType(int target,bool safeFail);
Picture*	getPic(bool safeFail=false);
Sound*		getSound(bool safeFail=false);
Text*		getText(bool safeFail=false);
int 	type;

	protected:
String name;
	};





class PointerData{
	public:
virtual void	setPointer()=0;
	};















