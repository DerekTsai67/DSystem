


//struct ALLEGRO_BITMAP;

struct FontCharInfo{
	FontCharInfo();
	FontCharInfo(Picture *pi,Coord p,Coord s);
	Picture	*pic;
	Coord		pos,size;
	};

class Font{
	 public:
	 Font(ConstString path);
	~Font();
void	addDatafile(Datafile *df);
void	loadDatafile(int i);
	 private:
Array<Datafile*>	datafileArray;
ByteNode<FontCharInfo>	*nodeHead;
Coord	size;
bool	standBy;

	 public:
Picture*	makeString(ConstString str);
Coord		putString(Picture *out,ConstString str,Position pos,int enter=100);
Coord		getSize();
	 private:
void		putChar(DSBitmap *out,ConstString c,int len,Coord pos);
Coord	cursor;

	};






