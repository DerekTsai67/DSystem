

	Line::Line(ConstString str)
	{
	argCheck(str==NULL);
	index=max=str.getLength();
	data=dalloc<char>(max+1);
	memcpy(data,str.getRaw(),str.getLength());
	}

	Line::~Line()
	{
	argCheck(data==NULL);
	dfree(data);
	}

void	Line::add(int i,ConstString str)
	{
	if(i==-1)
		i=index;
	argCheck(str==NULL||i<0);
	int n=str.getLength();
	
	if(max<index+n)
		{
		takeMax(max,10);
		while(max<index+n)
			max*=2;
		char *nd=dalloc<char>(max);
		memcpy(nd,data,i);
		memcpy(nd+i,str.getRaw(),n);
		memcpy(nd+i+n,data+i,index-i);
		dfree(data);
		data=nd;
		}
	else
		{
		memmove(data+i+n,data+i,index-i);
		memcpy(data+i,str.getRaw(),n);
		}
	index+=n;
	}

ConstString	Line::getData()const
	{
	return ConstString(data,index);
	}

int		Line::getNumber()const
	{
	return index;
	}

void	Line::print()
	{
	report("%S",viewData(data,index));
	}






	Text::Text():Data(Type_Text)
	{
	data.addItem(new Line(""));
	resetCursor();
	}

	Text::Text(ConstString t):Text()
	{
	add(t);
	}
	
Text*	Text::clone()const
	{
	Text *text=new Text();
	FOR_ARRAY(i,data)
		text->addLine(data[i]->getData());
	text->setName(getName());
	return text;
	}

	Text::~Text()
	{
	data.deleteAll();
	}


void	Text::add(const char *t,int n)
	{
	add(ConstString(t,n));
	}
void	Text::add(ConstString t)
	{
	argCheck(t==NULL);
	int bk=false,a=false;
	while(!bk)
		{
		int i=t.search("\n");
		if(i==-1)
			{
			i=t.getLength();
			bk=true;
			}
		int b=0;
		if(i>=1&&t[i-1]=='\r')
			b=1;
		if(!a)
			data.getItem(data.getNumber()-1)->add(-1,t.cut(i-b));
		else
			data.addItem(new Line(t.cut(i-b)));
		t+=i+1;
		a=true;
		}
	/*for(i=0;i<=n;i++)
		if((t[i]=='\n'&&(i==0||t[i-1]<0x80))||i==n)
			{
			int b=0;
			if(t[i-1]=='\r')
				b=1;
			if(a==0)
				data.getItem(data.getNumber()-1)->add(-1,t,i-b);
			else
				data.addItem(new Line(t+a,i-a-b));
			a=i+1;
			}*/
	}

void	Text::addLine(const char *t,int n)
	{
	add(t,n);
	add("\n");
	}
void	Text::addLine(ConstString t)
	{
	add(t);
	add("\n");
	}

void	Text::addString(ConstString data,...)
	{
	argCheck(data==NULL);
	VSTRING
	add(str);
	}

void	Text::deleteLastLine()
	{
	if(data.getItem(data.getNumber()-1)->getNumber()!=0)
		error("delete nonEmpty line.");
	data.deleteItemIndex(data.getNumber()-1);
	}


int 	Text::getNumber()
	{
	return data.getNumber();
	}

ConstString	Text::getViewLine(int i)
	{
	return viewData(data[i]->getData());
	}




void	Text::resetCursor()
	{
	cursor=zeroCoord;
	}

int		getUTFLength(char c)
	{
	if((c&0x80)==0)
		return 1;
	else if((c&0xF0)==0xE0)
		return 3;
	else if((c&0xE0)==0xC0)
		return 2;
	return 1;
	}

int		getChar(const char *str,int max,int *l)
	{
	argCheck(str==NULL);
	if(max<=0)
		{
		if(l!=NULL)
			*l=1;
		return '\n';
		}
		
	int len=getUTFLength(str[0]);
	
	if(len>max)
		len=1;
	int ret=0;
	memcpy(&ret,str,len);
	if(l!=NULL)
		*l=len;
	return ret;
	}

int		Text::getChar(int *l)
	{
	if(cursor.y>=data.getNumber())
		{
		if(l!=NULL)
			*l=0;
		return 0;
		}
		
	Line *line=data[cursor.y];
	if(cursor.x>=line->getNumber())
		{
		cursor.y++;
		cursor.x=0;
		if(l!=NULL)
			*l=1;
		return '\n';
		}
	int len=0;
	int ret=::getChar(line->getData().getRaw()+cursor.x,line->getNumber()-cursor.x,&len);
	cursor.x+=len;
	if(l!=NULL)
		*l=len;
	return ret;
	}

void	Text::getVerify(const char *str)
	{
	argCheck(str==NULL);
	if(cursor.y>=data.getNumber())
		error("Verify failed.");
	int n=getStringLength(str);
	for(int i=0;i<n;)
		{
		int l=1;
		if(getChar()!=::getChar(str+i,n-i,&l))
			error("Verify failed.");
		i+=l;
		}
	}

int		Text::getDecimal()
	{
	if(cursor.y>=data.getNumber())
		error("Unavailable.");
	if(cursor.x>=data[cursor.y]->getNumber())
		error("Unavailable.");
	int ret=0;
	for(int i=0;;i++)
		{
		Byte c=data[cursor.y]->getData()[cursor.x+i];
		if(c<'0'||c>'9')
			{
			if(i==0)
				error("Unavailable.");
			cursor.x+=i;
			return ret;
			}
		ret=ret*10+c-'0';
		}
	}





void	Text::print()
	{
	FOR_ARRAY(i,data)
		data[i]->print();
	}










