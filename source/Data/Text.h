

int		getUTFLength(char c);
int		getChar(const char *str,int max,int *l=NULL);


class Line{
	public:
	Line(ConstString str);
	~Line();
void	add(int i,ConstString str);

ConstString	getData()const;
int			getNumber()const;

void	print();
	private:
char	*data;
int 	index;
int 	max;
	};


class Text:public Data{
	public:
	Text();
	Text(ConstString t);
	~Text();
	private:
Array<Line*>	data;
	
	public:
void	add(const char *t,int n);
void	add(ConstString t);
void	addLine(const char *t,int n);
void	addLine(ConstString t);
void	addString(ConstString t,...);
void	deleteLastLine();

ConstString	getViewLine(int i);
int 	getNumber();

Text*	clone()const;
bool	save(Output *out,int suffixType,int safeFail=false)const;
void	saveData(Output *out)const;
void	saveTXT(Output *out)const;

	public:
void	resetCursor();
int		getChar(int *l=NULL);
void	getVerify(const char *str);
int		getDecimal();
	private:
Coord	cursor;
	
	public:
void	print();

	};




















