



	Layer::Layer():Layer(zeroCoord)
	{
	}

	Layer::Layer(Coord size):linker(true)
	{
	if(size==zeroCoord)
		bmp=NULL;
	else
		bmp=new DSBitmap(size);

	owner=NULL;
	edited=true;
	visible=true;
	opacity=100;
	folding=false;
	filterType=FilterType_draw;
	exportReserve=false;
	deviation=zeroCoord;
	fored=true;
	}

	Layer::~Layer()
	{
	FOR_LL(now,linker,LinkLayer)
		now->setTarget(NULL);
	END_FOR_LL
	if(bmp!=NULL)
		delete bmp;
	}

const DSBitmap*	Layer::getBitmap()const
	{
	argCheck(bmp==NULL);
	if(edited)
		{
		refreshBitmap();
		edited=false;
		}
	return bmp;
	}

DSBitmap*	Layer::getEditableBitmap()
	{
	error("No editable Bitmap.");
	return NULL;
	}

bool	Layer::bitmapEditable()
	{
	return false;
	}

Coord	Layer::getSize()const
	{
	return getBitmap()->getSize();
	}

void	Layer::reSize(Coord s)
	{
	if(bmp==NULL)
		return;
	bmp->reSize(s);
	edit();
	}




void	Layer::setOwner(FolderLayer *o)
	{
	owner=o;
	}

FolderLayer*	Layer::getOwner(bool safeFail)const
	{
	if(owner==NULL&&!safeFail)
		error("No owner.");
	return owner;
	}

int		Layer::getIndent()const
	{
	const Layer *now=this;
	int indent=-1;
	while(now!=NULL)
		{
		indent++;
		now=now->getOwner(true);
		}
	return indent;
	}
	
void	Layer::setVisible(bool v)
	{
	visible=v;
	if(getOwner(true)!=NULL)
		getOwner()->edit();
	}
bool	Layer::getVisible()const
	{
	return visible;
	}

void	Layer::setOpacity(int o)
	{
	argCheck(o<0||o>100);
	opacity=o;
	if(getOwner(true)!=NULL)
		getOwner()->edit();
	}
int		Layer::getOpacity()const
	{
	return opacity;
	}
	
void	Layer::setFolding(bool f)
	{
	folding=f;
	}
bool	Layer::getFolding()const
	{
	return folding;
	}

void	Layer::setFilterType(int f)
	{
	argCheck(f<0||f>=FilterType_count);
	filterType=f;
	if(getOwner(true)!=NULL)
		getOwner()->edit();
	}
int		Layer::getFilterType()const
	{
	return filterType;
	}

void	Layer::setExportReserve(bool e)
	{
	exportReserve=e;
	}
bool	Layer::getExportReserve()const
	{
	return exportReserve;
	}

void	Layer::setDeviation(Coord d)
	{
	edit();
	deviation=d;
	}
Coord	Layer::getDeviation()const
	{
	return deviation;
	}


void	Layer::edit()
	{
	if(edited)
		return;
	edited=true;
	if(owner!=NULL)
		owner->edit();
	FOR_LL(now,linker,LinkLayer)
		now->edit();
	END_FOR_LL
	}

void	Layer::refreshBitmap()const
	{}


	SimpleLayer::SimpleLayer(Coord size):Layer(size)
	{
	}

	SimpleLayer::SimpleLayer(DSBitmap *b):Layer(zeroCoord)
	{
	bmp=b;
	}

Layer*	SimpleLayer::clone()const
	{
	SimpleLayer *l=new SimpleLayer(getBitmap()->clone());
	return l;
	}

DSBitmap*	SimpleLayer::getEditableBitmap()
	{
	if(deviation!=zeroCoord)
		{
		bmp->move(deviation,transparent);
		deviation=zeroCoord;
		}
	edit();
	return bmp;
	}

bool	SimpleLayer::bitmapEditable()
	{
	return true;
	}






	LinkLayer::LinkLayer(Layer *l):Layer()
	{
	argCheck(l==NULL);
	target=NULL;
	setTarget(l);
	}

	LinkLayer::LinkLayer(int pi,int fi,int li,Coord c):Layer()
	{
	picIndex=pi;
	frameIndex=fi;
	layerIndex=li;
	tempSize=c;
	target=NULL;
	}

	LinkLayer::~LinkLayer()
	{
	if(target!=NULL)
		target->linker.deleteItem(this);
	}

void	LinkLayer::setTarget(Layer *t)
	{
	if(t==NULL)
		{
		target=NULL;
		return;
		}
		
	//argCheck(NULL);
	if(target!=NULL)
		target->linker.deleteItem(this);
	target=t;
	target->linker.addItem(this);
	edit();
	}

Layer*	LinkLayer::clone()const
	{
	return new LinkLayer(target);
	}
	


const DSBitmap*	LinkLayer::getBitmap()const
	{
	argCheck(target==NULL);
	edited=false;
	return target->getBitmap();
	}
	
Coord	LinkLayer::getSize()const
	{
	if(target==NULL)
		return tempSize;
	return target->getSize();
	}



	ColorLayer::ColorLayer(Coord size,DS_RGB c):Layer(size)
	{
	color=c;
	}
	
Layer*	ColorLayer::clone()const
	{
	return new ColorLayer(getSize(),color);
	}

void	ColorLayer::refreshBitmap()const
	{
	bmp->clear(color);
	}




	FolderLayer::FolderLayer(Coord size):Layer(size)
	{
	layer=dalloc<Layer*>(arrayMax);
	layerMax=0;
	}

	FolderLayer::~FolderLayer()
	{
	for(int i=0;i<layerMax;i++)
		delete layer[i];
	dfree(layer);
	}

Layer*	FolderLayer::clone()const
	{
	FolderLayer *l=new FolderLayer(getSize());
	for(int i=0;i<layerMax;i++)
		l->addLayer(layer[i]->clone());
	return l;
	}

int		FolderLayer::getIndex(Layer *l)
	{
	argCheck(l==NULL);
	for(int i=0;i<layerMax;i++)
		if(layer[i]==l)
			return i;
	error("No layer.");
	return 0;
	}

Layer*	FolderLayer::getLayer(int index)
	{
	argCheck(index<0||index>=layerMax);
	return layer[index];
	}

int		FolderLayer::getNumber()const
	{
	return layerMax;
	}

void	FolderLayer::addLayer(Layer *l,int pos)
	{
	argCheck(l==NULL||pos<-1||pos>layerMax);
	if(pos==-1)
		pos=layerMax;
	if(layerMax>=arrayMax)
		error("Overflow.");

	for(int i=layerMax;i>pos;i--)
		layer[i]=layer[i-1];
	layer[pos]=l;
	layerMax++;
	l->setOwner(this);
	edit();
	}

void	FolderLayer::loseLayer(int index)
	{
	argCheck(index<0||index>=layerMax);
	layerMax--;
	layer[index]->setOwner(NULL);
	for(int i=index;i<layerMax;i++)
		layer[i]=layer[i+1];
	edit();
	}
	
void	FolderLayer::reSize(Coord s)
	{
	for(int i=0;i<layerMax;i++)
		layer[i]->reSize(s);
	DSBitmap *oldBmp=bmp;
	bmp=new DSBitmap(s);
	delete oldBmp;
	edit();
	}

void	FolderLayer::refreshBitmap()const
	{
	bmp->clear(transparent);
	for(int i=layerMax-1;i>=0;i--)
		if(layer[i]->getVisible())
			switch(layer[i]->getFilterType())
				{
			case FilterType_draw:
				bmp->draw(layer[i]->getBitmap(),layer[i]->getDeviation());
				break;
			case FilterType_blit:
				bmp->blit(layer[i]->getBitmap(),layer[i]->getDeviation());
				break;
			case FilterType_alphaFilter:
				bmp->drawAlpha(layer[i]->getBitmap());
				break;
			default:
				error("switchOut.");
				break;
				}
	}





void	FolderLayer::forClear()
	{
	fored=false;
	forIndex=0;
	for(int i=0;i<layerMax;i++)
		layer[i]->forClear();
	}
void	Layer::forClear()
	{
	fored=false;
	}

Layer*	FolderLayer::forNext()
	{
	if(!fored)
		{
		fored=true;
		return this;
		}
	Layer *l;
	while(true)
		{
		if(forIndex==layerMax)
			return NULL;
		l=layer[forIndex]->forNext();
		if(l!=NULL)
			return l;
		forIndex++;
		}
	}
Layer*	Layer::forNext()
	{
	if(fored)
		return NULL;
	fored=true;
	return this;
	}

