





template<class Type>
	Step<Type>::Step()
	{
	type=0;
	legal=true;
	enable=true;
	backup=NULL;
	name="Nameless";
	}

template<class Type>
	Step<Type>::~Step()
	{
	clearBackup();
	}

template<class Type>
void	Step<Type>::addType(int t)
	{
	type|=t;
	}

template<class Type>
bool	Step<Type>::haveType(int t)
	{
	return (type&t)==t;
	}

template<class Type>
bool	Step<Type>::isLegal()const
	{
	return legal;
	}
	
template<class Type>
void	Step<Type>::setBackup(Type *f)
	{
	clearBackup();
	setBackupRaw(f);
	}

template<class Type>
void	Step<Type>::setBackupRaw(Type *f)
	{
	backup=f->clone();
	}

/*template<class Type>
Type*	Step<Type>::getBackup()const
	{
	if(backup==NULL)
		error("No backup.");
	return backup;
	}*/

template<class Type>
void	Step<Type>::clearBackup()
	{
	if(backup!=NULL)
		{
		delete backup;
		backup=NULL;
		}
	}

template<class Type>
bool	Step<Type>::haveBackup()const
	{
	return	backup!=NULL;
	}



template<class Type>
void	Step<Type>::stepIn()
	{
	if(enable!=disabled)
		error("Not disabled.");
	enable=enabled;
	stepInRaw();
	}

template<class Type>
bool	Step<Type>::stepOutReady()
	{
	return enable==enabled&&stepOutReadyRaw();
	}

template<class Type>
void	Step<Type>::stepOut()
	{
	if(enable!=enabled)
		error("Not enabled.");
	enable=disabled;
	stepOutRaw();
	}

template<class Type>
void	Step<Type>::stepInRaw()
	{
	error("Undefined.");
	}

template<class Type>
bool	Step<Type>::stepOutReadyRaw()
	{
	return haveType(Type_alwaysReady);
	}

template<class Type>
void	Step<Type>::stepOutRaw()
	{
	if(haveType(Type_reverse))
		stepInRaw();
	else
		error("Undefined.");
	}





template<class Type>
void	Step<Type>::setEnable(bool e)
	{
	enable=e;
	}

template<class Type>
int		Step<Type>::getEnable()
	{
	return enable;
	}

template<class Type>
ConstString	Step<Type>::getName()const
	{
	return name;
	}





template<class Type>
	StepContainer<Type>::StepContainer(Type *&f):field(f)
	{
	responseTarget=NULL;
	}

template<class Type>
	StepContainer<Type>::~StepContainer()
	{
	}
	
template<class Type>
void	StepContainer<Type>::setResponse(Object *o,int ri)
	{
	argCheck(o==NULL);
	responseTarget=o;
	responseIndex=ri;
	}

template<class Type>
void	StepContainer<Type>::addStep(Step<Type> *s)
	{
	argCheck(s==NULL);
	if(!s->isLegal())
		{
		delete s;
		return;
		}

	s->setBackup(field);
	s->stepIn();
	if(!s->isLegal())
		{
		delete s;
		return;
		}
	
	List<Step<Type> > *list=step.getHead(true);
	while(list!=NULL&&list->data->getEnable()!=Step<Type>::enabled)
		list=list->last;
	if(list==NULL)
		step.clear();
	else
		step.cut(list,true);
	step.addItem(s,true);
	
	if(responseTarget!=NULL)
		responseTarget->response(responseIndex);
	}

template<class Type>
Step<Type>*	StepContainer<Type>::getStep(int index)
	{
	return step.getItem(index);
	}
	
template<class Type>
int		StepContainer<Type>::getNumber()
	{
	return step.getNumber();
	}


template<class Type>
void	StepContainer<Type>::stepBack()
	{
	//stepTo(step.getItem(1,true));
	List<Step<Type> > *list=step.getHead(true);
	while(list!=NULL&&list->data->getEnable()!=Step<Type>::enabled)
		list=list->last;
	if(list==NULL)
		return;
	Step<Type> *s=list->data;
	if(s->stepOutReady())
		s->stepOut();
	if(responseTarget!=NULL)
		responseTarget->response(responseIndex);
	}

template<class Type>
void	StepContainer<Type>::stepForward()
	{
	List<Step<Type> > *list=step.getHead(false);
	while(list!=NULL&&list->data->getEnable()!=Step<Type>::disabled)
		list=list->next;
	if(list==NULL)
		return;
	Step<Type> *s=list->data;
	s->stepIn();
	if(responseTarget!=NULL)
		responseTarget->response(responseIndex);
	}

template<class Type>
void	StepContainer<Type>::stepTo(Step<Type> *s,bool multiple)
	{/*
	Step<Type>	*backupStep=NULL;
	bool front=true;
	FOR_LL_BACK(now,step,Step<Type>,true)
		if(now==s)
			{
			front=false;
			now->setEnable(true);
			}
		if(front)
			{
			if(multiple)
				now->setEnable(false);
			}
		else
			{
			if(multiple)
				now->setEnable(true);
			if(now->haveBackup())
				backupStep=now;
			}
	END_FOR_LL

	if(front)
		error("Step not exist.");
	if(backupStep==NULL)
		error("No backup.");

	Type *old=field;
	field=backupStep->getBackup()->clone();
	void	refreshWindow();
	refreshWindow();
	delete old;

	front=true;
	FOR_LL(now,step,Step<Type>)
		if(!front)
			now->stepIn();
		if(now==backupStep)
			front=false;
		if(now==s)
			break;
	END_FOR_LL*/
	}













