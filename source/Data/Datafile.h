
class Data;

class Datafile{
	public:
	 Datafile();
	 Datafile(ConstString n);
	 ~Datafile();
Datafile*	clone();
int 	getNumber();

void	load(ConstString n=NULL);
void	unload();
void	dataLoad();
void	save(ConstString n=NULL);
void	dataSave();

int		searchPic(Picture *p);

void	addData(Data *d,int pos=-1);
void	loseData(int i);
void	deleteData(Data *d);

Data*		getData(int i);
Data*		operator[](int i);

int			getTypeIndex(int target);
void*		getType(int target,int i);
void*		getType(int target,ConstString name);
int			getPicIndex();
Picture*	getPic(int i);
Picture*	getPic(ConstString name);
Sound*		getSound(int i);
Text*		getText(int i);

	private:
static const int dataMax=500,pathMax=300;
Output	*out;
Input	*in;
Data	**data;
int 	max;
String	path;
bool	loaded;
	};




