

	Datafile::Datafile()
	{
	data=dalloc<Data*>(dataMax);
	max=0;
	loaded=true;

	}

	Datafile::Datafile(ConstString n)
	{
	report("Datafile stand by : %S",n);
	data=dalloc<Data*>(dataMax);
	max=0;
	loaded=false;
	
	path.copyFrom(n);
	path.setDelete();
	}


	Datafile::~Datafile()
	{
	if(!loaded)
		report("Datafile not Used : %+S",path);
	unload();
	dfree(data);
	}

void	Datafile::load(ConstString n)
	{
	if(loaded)
		return;
	if(n==NULL)
		n=path;

	report(1,"Opening datafile : %S",n);

	FILE *file=fopen(n,"rb");
	if(file==NULL)
		{
		//char local[100]="./",suf[20];
		//getName(n,local+2);
		//getSuffix(n,suf,20);
		//sprintf(local,"%s%s",local,suf);
		ConstString local=format("./%S",getName(n));
		file=fopen(local,"rb");
		if(file==NULL)
			error("Opening file %S",local);
		}

	in=new InputFile(file);

	if(isSuffix(n,".dsd"))
		dataLoad();
	else
		error("Unsupported suffix. %S",n);

	//loaded=true;
	report(-1,"Data read : %d",max);
	}

void	Datafile::unload()
	{
	if(!loaded)
		return;
	report(1,"Unloading Datafile : %+S",path);

	for(int i=max-1;i>=0;i--)
		delete data[i];
	max=0;
	loaded=false;
	report(-1,"Completed.");
	}

void	Datafile::save(ConstString n)
	{
	if(n==NULL)
		n=path;

	report(1,"Saving Datafile : %S",n);
	if(!loaded)
		error("Not Loaded.");

	out=new OutputFile(n);

	if(isSuffix(n,".dat"))
		{
		for(int i=0;i<max;i++)
			getPic(i)->getBitmap()->saveBMP(new OutputEmptyMask(out));
		delete out;
		}
	else if(isSuffix(n,".dsd"))
		dataSave();
	else
		error("Unsupported suffix.");

	report(-1,"Saved.");
	}

int		Datafile::searchPic(Picture *p)
	{
	for(int i=0;i<dataMax;i++)
		if(data[i]->getPic(true)==p)
			return i;
	error("Not found.");
	return 0;
	}


Datafile*	Datafile::clone()
	{
	if(!loaded)load();

	Datafile *df=new Datafile();
	df->path.copyFrom(path);
	df->path.setDelete();
	//copyString(df->path,path,pathMax);
	for(int i=0;i<max;i++)
		df->addData(data[i]->clone());
	return df;
	}

Data*	Datafile::operator[](int i)
	{
	return getData(i);
	}

Data*	Datafile::getData(int i)
	{
	if(!loaded)load();
	if(i<0||i>=max)
		error("Datafile OverArrayed.");
	return data[i];
	}

int		Datafile::getTypeIndex(int target)
	{
	if(!loaded)load();
	for(int i=0;i<max;i++)
		{
		void *ret=data[i]->getType(target,true);
		if(ret!=NULL)
			return i;
		}
	return -1;
	}
void*	Datafile::getType(int target,int i)
	{
	if(!loaded)load();
	if(i<-1||i>=max)
		error("Datafile OverArrayed. %d/%d %+S",i,max,path);
	if(i==-1)
		{
		for(int i=0;i<max;i++)
			{
			void *ret=data[i]->getType(target,true);
			if(ret!=NULL)
				return ret;
			}
		return NULL;
		}
	else
		return data[i]->getType(target,false);
	}
void*	Datafile::getType(int target,ConstString name)
	{
	if(!loaded)load();
	argCheck(name==NULL);

	for(int i=0;i<max;i++)
		{
		if(data[i]->getName()==name)
			return data[i]->getType(target,false);
		}
	error("Not found.");
	return NULL;
	}
	
int		Datafile::getPicIndex()
	{
	return getTypeIndex(Data::Type_Picture);
	}
Picture*	Datafile::getPic(int i)
	{
	return (Picture*)getType(Data::Type_Picture,i);
	}
Picture*	Datafile::getPic(ConstString name)
	{
	return (Picture*)getType(Data::Type_Picture,name);
	}

Sound*	Datafile::getSound(int i)
	{
	return (Sound*)getType(Data::Type_Sound,i);
	}

Text*	Datafile::getText(int i)
	{
	return (Text*)getType(Data::Type_Text,i);
	}


int 	Datafile::getNumber()
	{
	if(!loaded)load();
	return max;
	}

void	Datafile::addData(Data *d,int pos)
	{
	if(!loaded)load();
	if(max>=dataMax)
		error("Datafile overflow.");
	if(pos<0||pos>max)
		pos=max;
	for(int i=max;i>pos;i--)
		data[i]=data[i-1];
	data[pos]=d;
	max++;
	}

void	Datafile::loseData(int i)
	{
	if(!loaded)load();
	if(i<0||i>=max)
		error("loseData with wrong index.");
	max--;
	for(;i<max;i++)
		data[i]=data[i+1];
	}
void	Datafile::deleteData(Data *d)
	{
	if(!loaded)load();
	int i;
	for(i=0;i<max;i++)
		if(data[i]==d)
			{
			delete data[i];
			break;
			}
	max--;
	for(;i<max;i++)
		{
		data[i]=data[i+1];
		}
	}



