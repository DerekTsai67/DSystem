

class LinkLayer;
class FolderLayer;



class Layer{
	public:
	Layer();
	Layer(Coord size);
virtual ~Layer();
virtual Layer*			clone()const=0;
virtual void			saveData(Output *out)=0;

virtual const DSBitmap*	getBitmap()const;
virtual DSBitmap*		getEditableBitmap();
virtual bool			bitmapEditable();
virtual	void			refreshBitmap()const;
virtual Coord			getSize()const;
virtual void			reSize(Coord s);
void					edit();
	protected:
mutable DSBitmap	*bmp;
mutable bool	edited;

	public:
virtual void	forClear();
virtual Layer*	forNext();
	protected:
bool	fored;

	public:
LinkedList<LinkLayer>	linker;

	public:
void			setOwner(FolderLayer *o);
FolderLayer*	getOwner(bool safeFail=false)const;
int				getIndent()const;
	protected:
FolderLayer	*owner;


	public:
void	setVisible(bool v);
bool	getVisible()const;
void	setOpacity(int o);
int		getOpacity()const;
void	setFolding(bool f);
bool	getFolding()const;
void	setFilterType(int f);
int		getFilterType()const;
void	setExportReserve(bool e);
bool	getExportReserve()const;
void	setDeviation(Coord d);
Coord	getDeviation()const;

enum{
	FilterType_draw=0,
	FilterType_blit,
	FilterType_alphaFilter,
	FilterType_alphaBlit,
	FilterType_count
	//unchangeable
	};
	protected:
bool	visible;
int		opacity;
bool	folding;
int		filterType;
bool	exportReserve;
Coord	deviation;
	};



class SimpleLayer:public Layer{
	public:
	SimpleLayer(Coord size);
	SimpleLayer(DSBitmap *bmp);
Layer*		clone()const;
void		saveData(Output *out);

DSBitmap*	getEditableBitmap();
bool		bitmapEditable();

	public:
//void	forClear();
//Layer*	forNext();
	};

class LinkLayer:public Layer,public PointerData{
	public:
	LinkLayer(Layer *l);
	LinkLayer(int pi,int fi,int li,Coord s);
	~LinkLayer();
Layer*	clone()const;
void	setTarget(Layer *t);
void	saveData(Output *out);
void	setPointer();
	private:
Layer	*target;
int		picIndex,frameIndex,layerIndex;
Coord	tempSize;

		public:
const DSBitmap*	getBitmap()const;
Coord			getSize()const;
	};

class ColorLayer:public Layer{
	public:
	ColorLayer(Coord size,DS_RGB color);
Layer*	clone()const;
void	saveData(Output *out);
void	refreshBitmap()const;
	private:
DS_RGB	color;
	};

class FolderLayer:public Layer{
	public:
	FolderLayer(Coord size);
	~FolderLayer();
Layer*	clone()const;
void	saveData(Output *out);

int		getIndex(Layer *l);
Layer*	getLayer(int index);
int		getNumber()const;

void	addLayer(Layer *l,int pos=-1);
void	loseLayer(int index);
void	reSize(Coord s);
void	refreshBitmap()const;
	protected:
static const int	arrayMax=100;
Layer**	layer;
int		layerMax;

	public:
void	forClear();
Layer*	forNext();
	private:
int		forIndex;

	};

/*
class ChoiceLayer{
DSBitmap*	getBitmap();
	private:
	};*/



