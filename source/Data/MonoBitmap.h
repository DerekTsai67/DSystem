







class MonoBitmap{
	public:
	MonoBitmap(Coord size);
	~MonoBitmap();
MonoBitmap*	clone();
int**		getData();
Coord		getSize();
static int	count;
	private:
void	init(Coord s);

	public:
void	put(Coord c,int color);
void	put(Coord c,int color,int num);
int 	get(Coord c);
void	clear(int);
void	move(Coord p,int c);

	public:
void	blit(MonoBitmap*,Coord p);
void	blit(int dpx,int dpy,MonoBitmap *src,int spx,int spy,int dsx,int dsy);
void	blit(Coord dp,MonoBitmap *src,Coord sp,Coord drawSize);
void	draw(MonoBitmap*,Coord p);
void	draw(Coord dp,MonoBitmap *src,Coord sp,Coord drawSize);
	private:
int 		**data;
Coord		size;
	};



















