



const int dataCount[][4]={
	{19,16,13,9},
	{34,28,22,16},
	{55,44,34,26},
	{80,64,48,36},
	{108,86,62,46},
	{136,108,76,60},
	{156,124,88,66},
	{194,154,110,86},
	{232,182,132,100},
	{274,216,154,122},
	{324,254,180,140},
	{370,290,206,158},
	{428,334,244,180},
	{461,365,261,197},
	{523,415,295,223},
	{589,453,325,253},
	{647,507,367,283},
	{721,563,397,313},
	{795,627,445,341},
	{861,669,485,385},
	{932,714,512,406},
	{1006,782,568,442},
	{1094,860,614,464},
	{1174,914,664,514},
	{1276,1000,718,538},
	{1370,1062,754,596},
	{1468,1128,808,628},
	{1531,1193,871,661},
	{1631,1267,911,701},
	{1735,1373,985,745},
	{1843,1455,1033,793},
	{1955,1541,1115,845},
	{2071,1631,1171,901},
	{2191,1725,1231,961},
	{2306,1812,1286,986},
	{2434,1914,1354,1054},
	{2566,1992,1426,1096},
	{2702,2102,1502,1142},
	{2812,2216,1582,1222},
	{2956,2334,1666,1276},
	};
const int corrCount[][4]={
	{7,10,13,17},
	{10,16,22,28},
	{15,26,36,44},
	{20,36,52,64},
	{26,48,72,88},
	{36,64,96,112},
	{40,72,108,130},
	{48,88,132,156},
	{60,110,160,192},
	{72,130,192,224},
	{80,150,224,264},
	{96,176,260,308},
	{104,198,288,352},
	{120,216,320,384},
	{132,240,360,432},
	{144,280,408,480},
	{168,308,448,532},
	{180,338,504,588},
	{196,364,546,650},
	{224,416,600,700},
	{224,442,644,750},
	{252,476,690,816},
	{270,504,750,900},
	{300,560,810,960},
	{312,588,870,1050},
	{336,644,952,1110},
	{360,700,1020,1200},
	{390,728,1050,1260},
	{420,784,1140,1350},
	{450,812,1200,1440},
	{480,868,1290,1530},
	{510,924,1350,1620},
	{540,980,1440,1710},
	{570,1036,1530,1800},
	{570,1064,1590,1890},
	{600,1120,1680,1980},
	{630,1204,1770,2100},
	{660,1260,1860,2220},
	{720,1316,1950,2310},
	{750,1372,2040,2430},
	};
const int blockCount[][4]={
	{1,1,1,1},
	{1,1,1,1},
	{1,1,2,2},
	{1,2,2,4},
	{1,2,4,4},
	{2,4,4,4},
	{2,4,6,5},
	{2,4,6,6},
	{2,5,8,8},
	{4,5,8,8},
	{4,5,8,11},
	{4,8,10,11},
	{4,9,12,16},
	{4,9,16,16},
	{6,10,12,18},
	{6,10,17,16},
	{6,11,16,19},
	{6,13,18,21},
	{7,14,21,25},
	{8,16,20,25},
	{8,17,23,25},
	{9,17,23,34},
	{9,18,25,30},
	{10,20,27,32},
	{12,21,29,35},
	{12,23,34,37},
	{12,25,34,40},
	{13,26,35,42},
	{14,28,38,45},
	{15,29,40,48},
	{16,31,43,51},
	{17,33,45,54},
	{18,35,48,57},
	{19,37,51,60},
	{19,38,53,63},
	{20,40,56,66},
	{21,43,59,70},
	{22,45,62,74},
	{24,47,65,77},
	{25,49,68,81}
	};

Picture*	makeQRcode(int version,int errorLevel,ConstString str)
	{
	if((version<1&&version!=-1)||version>40||errorLevel<-1||errorLevel>3||str==NULL)
		error("makeQRcode unexpected argument.");
	int side=17+version*4;
	//int si=0;
	//while(str[si]!=0)si++;
	IOBuffer buffer(4000);
	const int endianSetting=dlib::bitLeft;
	//buffer.Output::setType(dlib::bitLeft);
	//buffer.Input::setType(dlib::bitLeft);
	int countLength=(version>9)+(version>=27);

	if(!putQRint(&buffer,countLength,str))
		if(!putQRchar(&buffer,countLength,str))
			putQRbyte(&buffer,countLength,str);
/*
	putQRchar(&buffer,countLength,"ttp://gyro/",11);
	putQRbyte(&buffer,countLength,(const Byte*)"?id=",4);
	putQRchar(&buffer,countLength,"8E:10:4C:F8:E6:A0",17);*/

	int di=buffer.getNumber()+1;
	if(version==-1&&errorLevel==-1)
		error("No version or errorLevel indicated.");
	if(errorLevel==-1)
		{
		for(errorLevel=3;errorLevel>=0;errorLevel--)
			if(dataCount[version-1][errorLevel]>=di)
				break;
		if(errorLevel==-1)
			error("Unexpected dataLength %d for version %d.",di,version);
		}
	if(version==-1)
		{
		for(version=1;version<=40;version++)
			if(dataCount[version-1][errorLevel]>=di)
				break;
		if(version==41)
			error("Unexpected dataLength %d for errorLevel %d.",di,errorLevel);
		}

	int dataCount=::dataCount[version-1][errorLevel];
	int corrCount=::corrCount[version-1][errorLevel];
	if(di>dataCount)
		error("DataCode overflow.");
	else if(di<dataCount)
		buffer.putBits(0,4);
	buffer.Output::alignBits();
	di=buffer.getNumber();
	for(int i=0;i<dataCount-di;i++)
		buffer.putInt(i%2==0?0xEC:0x11,1);

	int blockCount=::blockCount[version-1][errorLevel];
	if(corrCount%blockCount!=0)
		error("corrCount%block !=0.");
	int corrNum=corrCount/blockCount;
	Byte **data=dalloc<Byte*>(blockCount);
	Byte **corr=dalloc<Byte*>(blockCount);
	for(int i=0;i<blockCount;i++)
		{
		data[i]=dalloc<Byte>((dataCount+i)/blockCount);
		corr[i]=dalloc<Byte>(corrNum);
		buffer.getBytes(data[i],(dataCount+i)/blockCount);
		}
	if(!buffer.empty())
		error("Buffer last.");

	int i2a[256],a2i[256];
	a2i[0]=1;
	for(int i=1;i<256;i++)
		{
		a2i[i]=(a2i[i-1]<<1)&0xFF;
		if(a2i[i-1]&0x80)
			a2i[i]^=0x1D;
		}
	i2a[0]=-1;
	for(int i=0;i<255;i++)
		{
		i2a[a2i[i]]=i;
		}

	int *g=dalloc<int>(corrNum);
	g[0]=1;
	for(int i=1;i<corrNum;i++)
		{
		for(int j=corrNum-2;j>=0;j--)
			if(g[j]!=0)
				g[j+1]^=a2i[(i2a[g[j]]+i)%255];
		g[0]^=a2i[(i2a[1]+i)%255];
		}
	for(int j=0;j<corrNum;j++)
		g[j]=i2a[g[j]];

	for(int i=0;i<blockCount;i++)
		{
		int dataNum=(dataCount+i)/blockCount;
		Byte *a=dalloc<Byte>(dataNum+corrNum);
		for(int j=0;j<dataNum;j++)
			a[j]=data[i][j];

		for(int j=0;j<dataNum;j++)
			{
			int a0=i2a[a[j]];
			if(a0==-1)
				continue;
			for(int k=0;k<corrNum;k++)
				if(g[k]!=-1)
					a[j+1+k]^=a2i[(a0+g[k])%255];
			a[j]=0;
			}
		memcpy(corr[i],a+dataNum,corrNum);

		dfree(a);
		}
	dfree(g);
	for(int i=0;i<dataCount/blockCount+1;i++)
	for(int j=0;j<blockCount;j++)
		if(i<(dataCount+j)/blockCount)
			buffer.putInt(data[j][i],1);
	for(int i=0;i<corrNum;i++)
	for(int j=0;j<blockCount;j++)
		buffer.putInt(corr[j][i],1);
	for(int i=0;i<blockCount;i++)
		{
		dfree(data[i]);
		dfree(corr[i]);
		}
	dfree(data);
	dfree(corr);


	Picture *ret=new Picture(Coord(side,side),yellow);

	int alignNum=version/7+2;
	if(version==1)
		alignNum=0;
	int alignDis=(side-13+2*alignNum-3)/(alignNum-1)/2*2;
	if(version==32)alignDis=26;
	Picture *alignPic=new Picture(Size(5,5),black);
	alignPic->drawRect(Position(1,1),Coord(3,3),white);
	for(int i=1;i<alignNum-1;i++)
		{
		ret->blit(alignPic,Position(6,6+i*alignDis,Position::Center,Position::EffectiveLeftDown));
		ret->blit(alignPic,Position(6+i*alignDis,6,Position::Center,Position::EffectiveRightUp));
		}
	for(int y=0;y<alignNum-1;y++)
	for(int x=0;x<alignNum-1;x++)
		if((x==0&&y==alignNum-1)||(y==0&&x==alignNum-1)||(x==alignNum-1&&y==alignNum-1));
		else
			ret->blit(alignPic,Position(6+x*alignDis,6+y*alignDis,Position::Center,Position::EffectiveRightDown));
	delete alignPic;

	if(version>=7)
		{
		ret->drawFilledRect(Position(10,0,Position::EffectiveLeftUp,Position::EffectiveRightUp),Coord(3,6),green);
		ret->drawFilledRect(Position(0,10,Position::EffectiveLeftUp,Position::EffectiveLeftDown),Coord(6,3),green);
		}

	for(int i=0;i<8;i++)
		{
		ret->putPixel(Position(i,8,Position::EffectiveLeftUp),brightBlue);
		ret->putPixel(Position(8,i,Position::EffectiveLeftUp),brightBlue);
		ret->putPixel(Position(8,i,Position::EffectiveLeftDown),brightBlue);
		ret->putPixel(Position(i,8,Position::EffectiveRightUp),brightBlue);
		}
	ret->putPixel(Position(8,8,Position::EffectiveLeftUp),brightBlue);
	ret->putPixel(Position(8,7,Position::EffectiveLeftDown),black);
	for(int i=0;i<side;i++)
		{
		ret->putPixel(Position(6,i,Position::EffectiveLeftUp),i%2==0?black:white);
		ret->putPixel(Position(i,6,Position::EffectiveLeftUp),i%2==0?black:white);
		}
	for(int j=0;j<3;j++)
		{
		Position::Type type,arr[]={Position::EffectiveLeftUp,Position::EffectiveLeftDown,Position::EffectiveRightUp};
		type=arr[j];
		for(int y=0;y<7;y++)
		for(int x=0;x<7;x++)
			{
			ret->putPixel(Position(x,y,type),black);
			}
		for(int i=1;i<6;i++)
			{
			ret->putPixel(Position(i,1,type),white);
			ret->putPixel(Position(i,5,type),white);
			ret->putPixel(Position(1,i,type),white);
			ret->putPixel(Position(5,i,type),white);
			}
		for(int i=0;i<8;i++)
			{
			ret->putPixel(Position(i,7,type),white);
			ret->putPixel(Position(7,i,type),white);
			}
		}


	for(int x0=side;x0>1;)
	for(int y=side,s=-1;s<=1;s+=2)
	for(y+=s,x0-=2+(x0==7);y>=0&&y<side;y+=s)
	for(int x=x0+1;x>=x0;x--)
	if(x>=0&&ret->getPixel(Position(x,y))==yellow)
		ret->putPixel(Position(x,y),buffer.empty()?blue:buffer.getBits(1)?red:green);


	Picture *masked[8];
	for(int i=0;i<8;i++)
		masked[i]=new Picture(ret->getSize());
	DS_RGB c1=black,c0=white;
	for(int y=0;y<side;y++)
	for(int x=0;x<side;x++)
		{
		DS_RGB p=ret->getPixel(Position(x,y));
		if(p==red||p==green||p==blue)
			{
			masked[0]->putPixel(Position(x,y),((p==red||p==blue)+((x+y)%2==0)==1)?c1:c0);
			masked[1]->putPixel(Position(x,y),((p==red||p==blue)+((x)%2==0)==1)?c1:c0);
			masked[2]->putPixel(Position(x,y),((p==red||p==blue)+((y)%3==0)==1)?c1:c0);
			masked[3]->putPixel(Position(x,y),((p==red||p==blue)+((x+y)%3==0)==1)?c1:c0);
			masked[4]->putPixel(Position(x,y),((p==red||p==blue)+((x/2+y/3)%2==0)==1)?c1:c0);
			masked[5]->putPixel(Position(x,y),((p==red||p==blue)+((x*y)%6==0)==1)?c1:c0);
			masked[6]->putPixel(Position(x,y),((p==red||p==blue)+((x*y%2+x*y%3)%2==0)==1)?c1:c0);
			masked[7]->putPixel(Position(x,y),((p==red||p==blue)+((x*y%3+x*y%2)%2==0)==1)?c1:c0);
			}
		else
			for(int i=0;i<8;i++)
				masked[i]->putPixel(Position(x,y),p);
		}

	int nowScore=-1;
	int mask=3;
	for(int i=0;i<8;i++)
		{
		int score=0;
		int blackCount=0,whiteCount=0;
		for(int y=0;y<side;y++)
			{
			int length=0;
			DS_RGB last=gray;
			for(int x=0;x<=side;x++)
				if(x!=side&&masked[i]->getPixel(Position(x,y))==last)
					length++;
				else
					{
					if(length>5)
						score+=length-2;
					if(last==black)
						blackCount+=length;
					else
						whiteCount+=length;
					if(x!=side)
						last=masked[i]->getPixel(Position(x,y));
					length=1;
					}
			}
		for(int x=0;x<side;x++)
			{
			int length=0;
			DS_RGB last=gray;
			for(int y=0;y<=side;y++)
				if(y!=side&&masked[i]->getPixel(Position(x,y))==last)
					length++;
				else
					{
					if(length>5)
						score+=length-2;
					if(y!=side)
						last=masked[i]->getPixel(Position(x,y));
					length=1;
					}
			}

		for(int y=0;y<side-1;y++)
		for(int x=0;x<side-1;x++)
			if(masked[i]->getPixel(Position(x,y))==masked[i]->getPixel(Position(x+1,y))
			 &&masked[i]->getPixel(Position(x,y))==masked[i]->getPixel(Position(x,y+1))
			 &&masked[i]->getPixel(Position(x,y))==masked[i]->getPixel(Position(x+1,y+1)) )
			score+=3;

		if(blackCount*100/(blackCount+whiteCount)>55)
			score+=blackCount*1000/(blackCount+whiteCount)-550;
		if(whiteCount*100/(blackCount+whiteCount)>55)
			score+=whiteCount*1000/(blackCount+whiteCount)-550;

		if(score<nowScore||nowScore==-1)
			{
			nowScore=score;
			mask=i;
			}
		}

	ret->blit(masked[mask],zeroEffectivePosition);
	for(int i=0;i<8;i++)
		delete masked[i];

	int maskData[5],maskCorr[15],mi=0;
	maskData[0]=errorLevel>=2;
	maskData[1]=errorLevel%2==0;
	maskData[2]=(mask&4)!=0;
	maskData[3]=(mask&2)!=0;
	maskData[4]=(mask&1)!=0;
	memcpy(maskCorr,maskData,5*sizeof(int));
	memset(maskCorr+5,0,10*sizeof(int));
	for(int i=0;i<5;i++)
		if(maskCorr[i])
			{
			maskCorr[i+2]^=1;
			maskCorr[i+5]^=1;
			maskCorr[i+6]^=1;
			maskCorr[i+8]^=1;
			maskCorr[i+9]^=1;
			maskCorr[i+10]^=1;
			}
	memcpy(maskCorr,maskData,5*sizeof(int));
	maskCorr[0]^=1;
	maskCorr[2]^=1;
	maskCorr[4]^=1;
	maskCorr[10]^=1;
	maskCorr[13]^=1;

	for(int i=side-1;i>=0;i--)
		if(ret->getPixel(Position(8,i))==brightBlue)
			ret->putPixel(Position(8,i),maskCorr[mi++]?black:white);
	mi=0;
	for(int i=0;i<side;i++)
		if(ret->getPixel(Position(i,8))==brightBlue)
			ret->putPixel(Position(i,8),maskCorr[mi++]?black:white);

	if(version>=7)
		{
		int verData[6],verCorr[18];
		for(int i=0;i<6;i++)
			verData[i]=verCorr[i]=((version&(1<<(5-i)))!=0);
		memset(verCorr+6,0,12*sizeof(int));
		for(int i=0;i<6;i++)
			if(verCorr[i])
				{
				verCorr[i+1]^=1;
				verCorr[i+2]^=1;
				verCorr[i+3]^=1;
				verCorr[i+4]^=1;
				verCorr[i+7]^=1;
				verCorr[i+10]^=1;
				verCorr[i+12]^=1;
				}
		memcpy(verCorr,verData,6*sizeof(int));
		for(int x=0;x<3;x++)
		for(int y=0;y<6;y++)
			{
			ret->putPixel(Position(y,10-x,Position::EffectiveLeftDown),verCorr[17-y*3-x]?black:white);
			ret->putPixel(Position(10-x,y,Position::EffectiveRightUp),verCorr[17-y*3-x]?black:white);
			}
		}

	report("QRcode generated with version %d errorLevel %d mask %d.",version,errorLevel,mask);
	return ret;
	}


bool putQRint(Output *out,int countLength,ConstString data)
	{
	const int endianSetting=dlib::bitLeft;
	if(out==NULL||data==NULL)
		error("putQRint unexpected argument.");
	int di=data.getLength();
	for(int i=0;i<di;i++)
		if(data[i]<'0'||data[i]>'9')
			return false;

	out->putBits(0b0001,4);
	out->putBits(di,10+countLength*2);
	for(int i=0;i<di/3;i++)
		out->putBits(((int)data[i*3]-'0')*100+((int)data[i*3+1]-'0')*10+((int)data[i*3+2]-'0'),10);
	if(di%3==2)
		out->putBits(((int)data[di-2]-'0')*10+(int)data[di-1]-'0',7);
	if(di%3==1)
		out->putBits((int)data[di-1]-'0',4);

	return true;
	}


bool putQRchar(Output *out,int countLength,ConstString data)
	{
	const int endianSetting=dlib::bitLeft;
	if(out==NULL||data==NULL)
		error("putQRchar unexpected argument.");
	int di=data.getLength();

	int converter[256];
	for(int i=0;i<256;i++)
		converter[i]=-1;
	for(int i='0';i<='9';i++)
		converter[i]=i-'0';
	for(int i='a';i<='z';i++)
		converter[i]=i-'a'+10;
	for(int i='A';i<='Z';i++)
		converter[i]=i-'A'+10;
	converter[' ']=36;
	converter['$']=37;
	converter['%']=38;
	converter['*']=39;
	converter['+']=40;
	converter['-']=41;
	converter['.']=42;
	converter['/']=43;
	converter[':']=44;

	for(int i=0;i<di;i++)
		if(converter[(int)data[i]]==-1)
			return false;

	out->putBits(0b0010,4);
	out->putBits(di,9+countLength*2);
	for(int i=0;i<di/2;i++)
		out->putBits(converter[(int)data[i*2]]*45+converter[(int)data[i*2+1]],11);
	if(di%2!=0)
		out->putBits(converter[(int)data[di-1]],6);
	return true;
	}

//void		putQRkanzi	(Output *out,int countLength,ConstString data);


bool putQRbyte(Output *out,int countLength,ConstString data)
	{
	const int endianSetting=dlib::bitLeft;
	if(out==NULL||data==NULL)
		error("putQRbyte unexpected argument.");

	out->putBits(0b0100,4);
	out->putBits(data.getLength(),8+(countLength!=0)*8);
	for(int i=0;i<data.getLength();i++)
		out->putBits((int)data[i],8);
	return true;
	}


