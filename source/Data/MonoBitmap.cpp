






	MonoBitmap::MonoBitmap(Coord s)
	{
	if(!(s>zeroCoord))
		error("creat Bitmap with negative size.");
	init(s);
	}

void	MonoBitmap::init(Coord s)
	{
	size=s;
	data=dalloc<int*>(size.y);
	for(int i=0;i<size.y;i++)
		data[i]=dalloc<int>(size.x);

	count++;
	}

	MonoBitmap::~MonoBitmap()
	{
	for(int i=0;i<size.y;i++)
		dfree(data[i]);
	dfree(data);
	count--;
	}

MonoBitmap*	MonoBitmap::clone()
	{
	MonoBitmap *n=new MonoBitmap(size);
	n->blit(this,zeroCoord);
	return n;
	}

Coord	MonoBitmap::getSize()
	{
	return size;
	}

int**	MonoBitmap::getData()
	{
	return data;
	}





void	MonoBitmap::put(Coord c,int b)
	{
	if(c>=zeroCoord&&c<size)
		data[c.y][c.x]=b;
	}
void	MonoBitmap::put(Coord c,int color,int num)
	{
	if((!(c>=zeroCoord))||(!(c<size))||num<=0)
		return;
	if(c.x+num>size.x)
		num=size.x-c.x;

	data[c.y][c.x]=color;
	int i;
	for(i=1;i<=num/2;i<<=1)
		memcpy(data[c.y]+c.x+i,data[c.y]+c.x,i*sizeof(int));
	if(i<num)
		memcpy(data[c.y]+c.x+i,data[c.y]+c.x,(num-i)*sizeof(int));
	}

int 	MonoBitmap::get(Coord c)
	{
	if(!(c>=zeroCoord&&c<size))
		error("MonoBitmap::get out of size");
	return data[c.y][c.x];
	}

void	MonoBitmap::clear(int c)
	{
	put(zeroCoord,c,size.x);
	for(int y=1;y<size.y;y++)
		memcpy(data[y],data[0],size.x*sizeof(int));
	}

void	MonoBitmap::move(Coord dp,int c)
	{
	Coord lt=zeroCoord|dp;
	Coord rb=size&(dp+size);
	int dx=lt.x,sx=dp.x>=0?0:-dp.x,lenx=(rb.x-lt.x)*sizeof(int);
	if(dp.y==0)
		for(int y=0;y<size.y;y++)
			{
			memmove(data[y]+dx,data[y]+sx,lenx);
			for(int x=0;x<lt.x;x++)
				data[y][x]=c;
			for(int x=rb.x;x<size.x;x++)
				data[y][x]=c;
			}
	else if(dp.y<0)
		{
		for(int y=0;y<size.y+dp.y;y++)
			{
			memcpy(data[y]+dx,data[y-dp.y]+sx,lenx);
			for(int x=0;x<lt.x;x++)
				data[y][x]=c;
			for(int x=rb.x;x<size.x;x++)
				data[y][x]=c;
			}
		for(int y=size.y+dp.y;y<size.y;y++)
			for(int x=0;x<size.x;x++)
				data[y][x]=c;
		}
	else //if(dp.y>0)
		{
		for(int y=size.y-1;y>=dp.y;y--)
			{
			memcpy(data[y]+dx,data[y-dp.y]+sx,lenx);
			for(int x=0;x<lt.x;x++)
				data[y][x]=c;
			for(int x=rb.x;x<size.x;x++)
				data[y][x]=c;
			}
		for(int y=dp.y-1;y>=0;y--)
			for(int x=0;x<size.x;x++)
				data[y][x]=c;
		}
	}





void	MonoBitmap::blit(MonoBitmap *src,Coord p)
	{
	blit(p,src,zeroCoord,src->size);
	}
void	MonoBitmap::blit(int dpx,int dpy,MonoBitmap *src,int spx,int spy,int dsx,int dsy)
	{
	blit(Coord(dpx,dpy),src,Coord(spx,spy),Coord(dsx,dsy));
	}
void	MonoBitmap::blit(Coord dp,MonoBitmap *src,Coord sp,Coord drawSize)
	{
	if(src==NULL)
		error("blit from NULL.");
	if(src->data==data)
		error("blit to same Bitmap.");


	Coord rb=size&(dp+((src->size-sp)&drawSize));
	Coord lt=zeroCoord|dp;
	Coord div=rb-lt;
	if(!(rb>lt)){
		//warning("blitting nothing");
		return;}
	int **s=src->data+sp.y+(dp.y>=0?0:-dp.y);
	int **d=     data+lt.y;
	int len=div.x*sizeof(int);
	int dd=lt.x;
	int sd=(dp.x>=0?0:-dp.x)+sp.x;

	for(int y=0;y<div.y;y++)
		memcpy(d[y]+dd,s[y]+sd,len);
	}
















