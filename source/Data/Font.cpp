


	Font::Font(ConstString path):datafileArray(10)
	{
	argCheck(path==NULL);
	
	nodeHead=new ByteNode<FontCharInfo>();
	size=zeroCoord;
	standBy=true;
	addDatafile(new Datafile(path));
	loadDatafile(0);
	
	cursor=zeroCoord;
	}
	Font::~Font()
	{
	delete nodeHead;
	datafileArray.deleteAll();
	}

	FontCharInfo::FontCharInfo()
		:pic(NULL),pos(zeroCoord),size(zeroCoord)
	{}
	FontCharInfo::FontCharInfo(Picture *pi,Coord p,Coord s)
		:pic(pi),pos(p),size(s)
	{}


void	Font::addDatafile(Datafile *df)
	{
	argCheck(df==NULL);
	datafileArray.addItem(df);
	}
void	Font::loadDatafile(int i)
	{
	argCheck(i<0||i>=datafileArray.getNumber());
	Datafile *df=datafileArray[i];
	Picture *pic=df->getPic(0);
	Text	*text=df->getText(1);
	text->resetCursor();
	Coord	pos=zeroCoord,size;
	text->getVerify("[");
	size.x=text->getDecimal();
	text->getVerify(",");
	size.y=text->getDecimal();
	text->getVerify("]\n");
	//report("size=%d,%d",size.x,size.y);
	while(true)
		{
		int len=0;
		int c=text->getChar(&len);
		if(c==0)
			break;
		if(c=='\n')
			{
			pos.x=0;
			pos.y+=size.y;
			continue;
			}
		Byte *t=(Byte*)&c;
		
		ByteNode<FontCharInfo> *now=nodeHead->getNext(t,len,true);
		now->setData(FontCharInfo(pic,pos,size));
		pos.x+=size.x;
		
		//report("char %d %02X %02X %02X %02X",len,t[0],t[1],t[2],t[3]);
		}
	if(this->size==zeroCoord)
		this->size=size;
	}


/*enum{
	ENCODE_FREE,
	ENCODE_UTF_3,
	ENCODE_HURIGANA,
	ENCODE_BIG5
	};

int		encodeType(char c)
	{
	switch(c)
		{
		//case '`':return ENCODE_UTF_3;
		case '^':return ENCODE_HURIGANA;
		}
	if((c&0xF000)==0xE000)
		return ENCODE_UTF_3;
	if(c&0x8000)
		return ENCODE_BIG5;
	return ENCODE_FREE;
	}*/




void	Font::putChar(DSBitmap *out,ConstString c,int len,Coord pos)
	{
	if(standBy)
	if(c[0]&0x80)
		{
		FOR_ARRAY(i,datafileArray)
			loadDatafile(i);
		standBy=false;
		}
		
	if(c[0]=='\n')
		{
		cursor.x=0;
		cursor.y+=size.y;
		return;
		}

	ByteNode<FontCharInfo> *node=nodeHead->getNext((Byte*)c.getRaw(),len);
	/*for(int i=0;i<len;i++)
		{
		if(node==NULL)
			break;
		node=node->getNext(c[i]);
		}*/
	
	if(node!=NULL)
		{
		FontCharInfo info=node->getData();
		out->draw(pos+cursor,info.pic->getBitmap(),info.pos,info.size);
		cursor.x+=info.size.x;
		}
	else
		{
		cursor.x+=size.x;
		}
	}
	
/*
union LChar{
	struct {char c3,c2,c1,z;} c;
	long l;
	LChar(){c.z=0;}
	};

int switchLWord(long l,int &ch);

void	Font::putLChar(Picture *out,long l,Position pos,int enter,bool HK)
	{
	int &i=font_i;
	if(l==0){i=0;return;}
	int ch=0;
	int t=switchLWord(l,ch);
	if(!HK){
		out->drawTile(data.getPic(3+ch),pos+Coord(i%enter*size.x,i/enter*size.y*3/2)  ,Coord(size.x*2,size.y)  ,Coord(t%16,t/16));
		i+=2;}
	else
		out->drawTile(data.getPic(2),pos+Coord(i%enter*size.x,i/enter*size.y*3/2)/2,Coord(size.x*2,size.y)/2,Coord(t%16,t/16));
	}

void	Font::putCChar(Picture *out,char c,Position pos,int enter,bool HK)
	{
	int &i=font_i;
	if(c==0){i=0;return;}
	if(c=='\n')
		{i=(i/enter+1)*enter;return;}
	register int t=96;
	switch(c){
		case' ':t--;case'!':t--;case'"':t--;case'#':t--;case'$':t--;case'%':t--;case'&':t--;case'\'':t--;case'(':t--;case')':t--;case'*':t--;case'+':t--;case',':t--;case'-':t--;case'.':t--;case'/':t--;
		case'0':t--;case'1':t--;case'2':t--;case'3':t--;case'4':t--;case'5':t--;case'6':t--;case'7':t--;case'8':t--;case'9':t--;case':':t--;case';':t--;case'<':t--;case'=':t--;case'>':t--;case'?':t--;
		case'@':t--;case'A':t--;case'B':t--;case'C':t--;case'D':t--;case'E':t--;case'F':t--;case'G':t--;case'H':t--;case'I':t--;case'J':t--;case'K':t--;case'L':t--;case'M':t--;case'N':t--;case'O':t--;
		case'P':t--;case'Q':t--;case'R':t--;case'S':t--;case'T':t--;case'U':t--;case'V':t--;case'W':t--;case'X':t--;case'Y':t--;case'Z':t--;case'[':t--;case'\\':t--;case']':t--;case'^':t--;case'_':t--;
		case'`':t--;case'a':t--;case'b':t--;case'c':t--;case'd':t--;case'e':t--;case'f':t--;case'g':t--;case'h':t--;case'i':t--;case'j':t--;case'k':t--;case'l':t--;case'm':t--;case'n':t--;case'o':t--;
		case'p':t--;case'q':t--;case'r':t--;case's':t--;case't':t--;case'u':t--;case'v':t--;case'w':t--;case'x':t--;case'y':t--;case'z':t--;case'{':t--;case'|':t--;case'}':t--;case'~':t--;        t--;
		}
	if(!HK)
		{
		out->drawTile(data.getPic(0),pos+Coord(i%enter*size.x,i/enter*size.y*3/2)  ,size  ,Coord(t%16,t/16));
		i++;
		}
	else
		out->drawTile(data.getPic(1),pos+Coord(i%enter*size.x,i/enter*size.y*3/2)/2,size/2,Coord(t%16,t/16));
	}*/

Coord	measureString(ConstString str,int enter);
Coord	Font::putString(Picture *out,ConstString str,Position posi,int enter)
	{
	argCheck(out==NULL||str==NULL);
	Coord s=measureString(str,enter);
	
	DSBitmap *bmp=out->getEditableBitmap();
	Coord pos=posi.fix(zeroCoord,s*size,zeroCoord,out);
	cursor=zeroCoord;
	int max=str.getLength();
	for(int i=0;i<max;)
		{
		int len=getUTFLength(str[i]);
		putChar(bmp,str+i,len,pos);
		i+=len;
		}
	return cursor;
	/*Coord s;
	measureString(str,s.x,s.y,enter);
	posi=posi.change(zeroCoord,s*size,zeroCoord,out);
	union LChar data;
	int i=0,HKU=0,HKD=0,HKC=-1,HKL=0,HKs=0,&width=size.x;
	putLChar(out,0,zeroEffectivePosition,enter);
	while(str[i]!=0)
		switch(encodeType(str[i]))
			{
		case ENCODE_FREE:
		case ENCODE_UTF_3:
			HKs=0;
			if(HKC!=-1)
				if(HKC<HKD)
					HKs=(HKL-HKD*width)/2;
				else if(HKC<HKD+HKU)
					HKs=(HKL-HKU*width/2)/2+(HKC-HKD)*width/2-HKD*width;
				else
					{font_i+=(HKL-HKD*width+width-1)/width;
					HKC=-1;}
				else
					HKs=0;
			if(encodeType(str[i])==ENCODE_FREE)
				{
				putCChar(out,str[i++],posi+Coord(HKs,0),enter,HKC>=HKD);
				HKC+=(HKC!=-1);
				break;
				}

			data.c.c1=str[i];
			data.c.c2=str[i+1];
			data.c.c3=str[i+2];
			putLChar(out, data.l ,posi+Coord(HKs,0),enter,HKC>=HKD);
			HKC+=(HKC!=-1)*2; i+=3;
			break;

		case ENCODE_HURIGANA:
			HKD=str[i+1]*10+str[i+2]-'0'*11;
			HKU=str[i+3]*10+str[i+4]-'0'*11;
			if(HKD*2<HKU-2)
				HKL=HKU*width/2-width;
			else
				HKL=HKD*width;
			HKC=0;  i+=5;
			break;

		case ENCODE_BIG5:
			i+=2;font_i+=2;
			break;
			}
	return Coord(i%enter,i/enter)*getSize();*/
	}

Coord	measureString(ConstString str,int enter=100)
	{
	int w=0,h=1;
	int nw=0;
	for(int i=0;i<str.getLength();i++)
		{
		/*if(str[i]&0x80||str[i]=='`')
			{
			if(enter-nw<2)
				{
				if(w<nw)
					w=nw;
				h++;
				nw=2;
				i+=3;
				}
			else
				{
				i+=3;
				nw+=2;
				}
			}
		else*/
			if(str[i]=='\n')
				{if(nw>w)w=nw;
				nw=0;h++;}
			else if(enter-nw<1)
				{if(nw>w)w=nw;
				nw=1;h++;}
			else
				nw+=1;
		}
	if(nw>w)w=nw;
	return Coord(w,h);
	}


//==============================================================================


Coord	Font::getSize()
	{
	return Coord(size.x,size.y*3/2);
	}


Picture*	Font::makeString(ConstString str)
	{
	argCheck(str==NULL);
	Coord c=measureString(str);
	Picture *pic=new Picture(c*getSize(),transparent);
	putString(pic,str,zeroEffectivePosition);
	return pic;
	}










