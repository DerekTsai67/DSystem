




	Data::Data(int t)
	{
	init(t,"Nameless");
	}

void	Data::init(int t,ConstString n)
	{
	type=t;
	name.copyFrom(n);
	name.setDelete();
	}

	Data::~Data()
	{
	}

void	Data::setName(ConstString n)
	{
	argCheck(n==NULL);
	name.copyFrom(n);
	}

ConstString 	Data::getName()const
	{
	return name;
	}



Data*	Data::clone()const
	{
	error("%s::clone() Not completed.",typeid(*this).name());
	return NULL;
	}

bool	Data::save(Output *out,int suffixType,int safeFail)const
	{
	safeError("%s::saveFile() Not completed.",typeid(*this).name());
	return false;
	}

void	Data::saveData(Output *out)const
	{
	error("%s::saveData() Not completed.",typeid(*this).name());
	}

void	Data::setPointer()
	{
	}





void*	Data::getType(int target,bool safeFail)
	{
	if(type!=target)
		{
		if(safeFail)
			return NULL;
		error("Converting with wrong Type %d %d.",target,type);
		}
	return this;
	}

Picture*	Data::getPic(bool safeFail)
	{
	return (Picture*)getType(Type_Picture,safeFail);
	}

Sound*	Data::getSound(bool safeFail)
	{
	return (Sound*)getType(Type_Sound,safeFail);
	}

Text*	Data::getText(bool safeFail)
	{
	return (Text*)getType(Type_Text,safeFail);
	}


