


	DS_RGB::DS_RGB()
		:b(0),g(0),r(0),a(255){}
	DS_RGB::DS_RGB(int R,int G,int B)
		:b(B),g(G),r(R),a(255){}
	DS_RGB::DS_RGB(int R,int G,int B,int A)
		:b(B),g(G),r(R),a(A){}
	DS_RGB::DS_RGB(DS_RGB C,int A)
		:b(C.b),g(C.g),r(C.r),a(A){}

bool	DS_RGB::same(const DS_RGB &c)const
	{
	return c.a==a&&((c.r==r&&c.g==g&&c.b==b)||a==0);
	}
	
bool	DS_RGB::totalSame(const DS_RGB &c)const
	{
	return c.r==r&&c.g==g&&c.b==b&&c.a==a;
	}

bool	DS_RGB::operator==(const DS_RGB &a)const
	{
	return a.r==r&&a.g==g&&a.b==b;
	}
bool	DS_RGB::operator!=(const DS_RGB &a)const
	{
	return a.r!=r||a.g!=g||a.b!=b;
	}
DS_RGB	DS_RGB::operator-(const DS_RGB &a)const
	{
	return DS_RGB(r-a.r,g-a.g,b-a.b);
	}
	

int		compareAlpha(const void *av,const void *bv)
	{
	const DS_RGB &a=*(const DS_RGB*)av;
	const DS_RGB &b=*(const DS_RGB*)bv;
	return (a.a>b.a)-(a.a<b.a);
	}
int		compareColor(const void *av,const void *bv)
	{
	const DS_RGB &a=*(const DS_RGB*)av;
	const DS_RGB &b=*(const DS_RGB*)bv;
	if(a.r>b.r)
		return true;
	if(a.r<b.r)
		return false;
	if(a.g>b.g)
		return true;
	if(a.g<b.g)
		return false;
	if(a.b>b.b)
		return true;
	if(a.b<b.b)
		return false;
	return (a.a>b.a)-(a.a<b.a);
	}





int		DS_RGB::getGray()const
	{
	return (r+g+b+1)/3;
	}
	
int		DS_RGB::getAverage()const
	{
	return (r+g+b+1)/3;
	}

int		DS_RGB::getMax()const
	{
	if(r>g&&r>b)
		return r;
	if(g>b)
		return g;
	return b;
	}

int		DS_RGB::getMin()const
	{
	if(r<g&&r<b)
		return r;
	if(g<b)
		return g;
	return b;
	}

DS_RGB	DS_RGB::getReverse()const
	{
	return DS_RGB(255-r,255-g,255-b);
	}

int		DS_RGB::getHue()const
	{
	if(r==g&&g==b)
		return 0;

	int max=getMax();
	int min=getMin();
	int mid,hi,h;

	if(r==max&&g==max)
		{
		mid=max;
		hi=1;
		}
	else if(r==max)
		if(b>g)
			{
			mid=b;
			hi=5;
			}
		else
			{
			mid=g;
			hi=0;
			}
	else if(b==max)
		if(g>r)
			{
			mid=g;
			hi=3;
			}
		else
			{
			mid=r;
			hi=4;
			}
	else if(g==max)
		if(r>b)
			{
			mid=r;
			hi=1;
			}
		else
			{
			mid=b;
			hi=2;
			}
	else;

	h=60*(mid-min)/(max-min);
	if(hi%2==1)
		h=60-h;
	return hi*60+h;
	}

int		DS_RGB::getHWCW()const
	{
	return getMin();
	}
int		DS_RGB::getHWCC()const
	{
	return getMax()-getMin();
	}
int		DS_RGB::getHWCB()const
	{
	return 255-getMax();
	}
int		DS_RGB::getHSLS()const
	{
	if(getHWCC()==0)
		return 0;;
	int l=getHSLL();
	if(l>255)
		l=510-l;
	return (255*getHWCC()+l/2)/l;
	}

int		DS_RGB::getHSLL()const
	{
	return getMax()+getMin();
	}

int		DS_RGB::getHSVS()const
	{
	if(getHSVV()==0)
		return 0;
	return 255*getHWCC()/getHSVV();
	}

int		DS_RGB::getHSVV()const
	{
	return getMax();
	}

double	DS_RGB::getYCCY()const
	{
	double ret=0.299*r+0.587*g+0.114*b;
	if(ret>255.0)
		ret=255.0;
	if(ret<0.0)
		ret=0.0;
	return ret;
	}
double	DS_RGB::getYCCb()const
	{
	double ret=-0.1687*r-0.3313*g+0.5*b;
	if(ret>127.0)
		ret=127.0;
	if(ret<-128.0)
		ret=-128.0;
	return ret;
	}
double	DS_RGB::getYCCr()const
	{
	double ret=0.5*r-0.4187*g-0.0813*b;
	if(ret>127.0)
		ret=127.0;
	if(ret<-128.0)
		ret=-128.0;
	return ret;
	}

DS_RGB	makeHWC(int h,int w,int c)
	{
	h%=360;
	takeMax(w,0);
	takeMin(w,255);
	takeMax(c,0);
	takeMin(c,255-w);
	argCheck(h<0||w<0||c<0||h>=360||w+c>=256);

	int hi=h/60;
	h%=60;
	int v=w+c;
	int a=w+(c*h+30)/60;
	int b=w+c-(c*h+30)/60;
	switch(hi)
		{
	case 0:
		return DS_RGB(v,a,w);
	case 1:
		return DS_RGB(b,v,w);
	case 2:
		return DS_RGB(w,v,a);
	case 3:
		return DS_RGB(w,b,v);
	case 4:
		return DS_RGB(a,w,v);
	case 5:
		return DS_RGB(v,w,b);
	default:
		error("makeHWC switchOut.");
		}
	return black;
	}

DS_RGB	makeHSL(double h,double s,double l)
	{
	return makeHSL((int)(360*h),(int)(255*s),(int)(510*l));
	}

DS_RGB	makeHSL(int h,int s,int l)
	{
	h%=360;
	takeMax(s,0);
	takeMin(s,255);
	takeMax(l,0);
	takeMin(l,510);
	argCheck(h<0||s<0||l<0||h>360||s>255||l>510);
	int c;
	if(l<256)
		c=(l*s+128)/255;
	else
		c=((510-l)*s+128)/255;

	int w=(l-c+1)/2;

	return makeHWC(h,w,c);
	}

DS_RGB	makeHSV(double h,double s,double v)
	{
	return makeHSV((int)(360*h),(int)(255*s),(int)(255*v));
	}

DS_RGB	makeHSV(int h,int s,int v)
	{
	argCheck(h<0||s<0||v<0||h>360||s>255||v>255);

	int c=v*s/255;
	int w=v-c;
	return makeHWC(h,w,c);
	}

DS_RGB	makeYCbCr(int Y,int Cb,int Cr)
	{
	argCheck(Y<0||Cb<0||Cr<0||Y>255||Cb>255||Cr>255);
	Cb-=128;
	Cr-=128;
	int r=Y+Cr*1.402;
	int g=Y-(0.34414*Cb+0.71414*Cr);
	int b=Y+Cb*1.772;
	if(r<0)r=0;if(r>255)r=255;
	if(g<0)g=0;if(g>255)g=255;
	if(b<0)b=0;if(b>255)b=255;
	return DS_RGB(r,g,b);
	}

DS_RGB	makeYCbCr(double Y,double Cb,double Cr)
	{
	argCheck(Y<0.0||Cb<-128.0||Cr<-128.0||Y>255.0||Cb>127.0||Cr>127.0);
	int r=0.5+Y+Cr*1.402;
	int g=0.5+Y-(0.34414*Cb+0.71414*Cr);
	int b=0.5+Y+Cb*1.772;
	if(r<0)r=0;if(r>255)r=255;
	if(g<0)g=0;if(g>255)g=255;
	if(b<0)b=0;if(b>255)b=255;
	return DS_RGB(r,g,b);
	}

DS_RGB blend(DS_RGB c1,int r1,DS_RGB c2,int r2)
	{
	int rm=r1+r2;
	return DS_RGB((c1.r*r1+c2.r*r2)/rm,(c1.g*r1+c2.g*r2)/rm,(c1.b*r1+c2.b*r2)/rm,(c1.a*r1+c2.a*r2)/rm);
	}


//==========================================================================================================================
//==========================================================================================================================
//==========================================================================================================================



	DSBitmap::DSBitmap(Coord s)
	{
	if(!(s>zeroCoord))
		error("creat Bitmap with negative size.");
	init(s);
	}

void	DSBitmap::init(Coord s)
	{
	size=dataSize=s;
	data=dalloc<DS_RGB*>(dataSize.y);
	for(int i=0;i<dataSize.y;i++)
		data[i]=dalloc<DS_RGB>(dataSize.x);

	nextTime=1;
	nowTime=0;
	optimizeTime=dalloc<int>(dataSize.y);
	optimizer=dalloc<int*>(dataSize.y);
	for(int i=0;i<dataSize.y;i++)
		{
		optimizer[i]=dalloc<int>(dataSize.x);
		optimizer[i][0]=-size.x;
		}

	count++;
	}

	DSBitmap::~DSBitmap()
	{
	for(int i=0;i<dataSize.y;i++){
		dfree(data[i]);
		dfree(optimizer[i]);}
	dfree(data);
	dfree(optimizer);
	dfree(optimizeTime);
	count--;
	}



DSBitmap*	DSBitmap::clone()const
	{
	DSBitmap *n=new DSBitmap(size);
	n->blit(this,zeroCoord);
	return n;
	}

DSBitmap*	DSBitmap::cloneSize(Coord pos,Coord s)const	
	{
	DSBitmap *n=new DSBitmap(s);
	n->blit(zeroCoord,this,pos,s);
	return n;
	}

DSBitmap*	DSBitmap::cloneDiffer(const DSBitmap *ref,Coord *pos)const
	{
	argCheck(ref==NULL);
	if(getSize()!=ref->getSize())
		error("Different size.");

	int minY,maxY,minX,maxX;
	for(minY=0;minY<getSize().y;minY++)
		{
		int x=0;
		for(x=0;x<getSize().x;x++)
			if(!get(x,minY).same(ref->get(x,minY)))
				break;
		if(x!=getSize().x)
			break;
		}
	if(minY==getSize().y)
		return NULL;

	for(maxY=getSize().y-1;maxY>minY;maxY--)
		{
		int x=0;
		for(x=0;x<getSize().x;x++)
			if(!get(x,maxY).same(ref->get(x,maxY)))
				break;
		if(x!=getSize().x)
			break;
		}
	maxY++;
		
	for(minX=0;minX<getSize().x;minX++)
		{
		int y=0;
		for(y=minY;y<maxY;y++)
			if(!get(minX,y).same(ref->get(minX,y)))
				break;
		if(y!=maxY)
			break;
		}
	for(maxX=getSize().x-1;maxX>minX;maxX--)
		{
		int y=0;
		for(y=minY;y<maxY;y++)
			if(!get(maxX,y).same(ref->get(maxX,y)))
				break;
		if(y!=maxY)
			break;
		}
	maxX++;
	
	if(minY>=maxY||minX>=maxX)
		error("Negative size.");
	
	if(pos!=NULL)
		*pos=Coord(minX,minY);
	return cloneSize(Coord(minX,minY),Coord(maxX-minX,maxY-minY));
	}
	
	
void	DSBitmap::reSize(Coord newSize)
	{
	Coord newDataSize=newSize|dataSize;
	
	if(newDataSize.x>dataSize.x)
		{
		for(int y=0;y<dataSize.y;y++)
			{
			DS_RGB *newData=dalloc<DS_RGB>(newDataSize.x);
			int *newOptimizer=dalloc<int>(newDataSize.x);
			memcpy(newData,data[y],dataSize.x*sizeof(DS_RGB));
			memcpy(newOptimizer,optimizer[y],dataSize.x*sizeof(int));

			DS_RGB *oldData=data[y];
			int *oldOptimizer=optimizer[y];
			data[y]=newData;
			optimizer[y]=newOptimizer;
			dfree(oldData);
			dfree(oldOptimizer);
			}
		}
	
	if(newDataSize.y>dataSize.y)
		{
		DS_RGB **newData=dalloc<DS_RGB*>(newDataSize.y);
		int **newOptimizer=dalloc<int*>(newDataSize.y);
		int *newOptimizeTime=dalloc<int>(newDataSize.y);
		memcpy(newData,data,dataSize.y*sizeof(DS_RGB*));
		memcpy(newOptimizer,optimizer,dataSize.y*sizeof(int*));
		memcpy(newOptimizeTime,optimizeTime,dataSize.y*sizeof(int));
		
		for(int y=dataSize.y;y<newDataSize.y;y++)
			{
			newData[y]=dalloc<DS_RGB>(newDataSize.x);
			newOptimizer[y]=dalloc<int>(newDataSize.x);
			}

		DS_RGB **oldData=data;
		int **oldOptimizer=optimizer;
		int *oldOptimizeTime=optimizeTime;
		data=newData;
		optimizer=newOptimizer;
		optimizeTime=newOptimizeTime;
		dfree(oldData);
		dfree(oldOptimizer);
		dfree(oldOptimizeTime);
		}
	
	memset(optimizeTime,0,newDataSize.y*sizeof(int));
	nextTime=1;
	nowTime=0;
	size=newSize;
	dataSize=newDataSize;
	}

Coord	DSBitmap::getSize()const
	{
	return size;
	}

DS_RGB**	DSBitmap::getData()
	{
	return data;
	}



void	DSBitmap::edited()
	{
	nowTime=nextTime;
	}

void	DSBitmap::edited(int y)
	{
	argCheck(y<0||y>=size.y);
	optimizeTime[y]=nowTime-1;
	}

bool	DSBitmap::isFullOpaque()const
	{
	for(int y=0;y<size.y;y++)
		{
		monoOptimize(y);
		if(optimizer[y][0]!=size.x)
			return false;
		}
	return true;
	}

bool	DSBitmap::isFullTransparent()const
	{
	for(int y=0;y<size.y;y++)
		{
		monoOptimize(y);
		if(optimizer[y][0]!=-size.x)
			return false;
		}
	return true;
	}

bool	DSBitmap::monoOptimize(int y)const
	{
	argCheck(y<0||y>=size.y);
	if(optimizeTime[y]==nowTime)
		return false;
	memset(optimizer[y],0,size.x*sizeof(int));

	int opc=0;
	for(int x=0;x<size.x;x++)
		{
		DS_RGB &S=data[y][x];
		if(S.a==0)
			if(opc<0)opc--;
			else {
				for(;opc>0;opc--)
					optimizer[y][x-opc]=opc;
				opc=-1;}
		else if(S.a==255)
			if(opc>=0)opc++;
			else{
				for(;opc<0;opc++)
					optimizer[y][x+opc]=opc;
				opc=1;}
		else {
			if(opc>0)
				for(;opc>0;opc--)
					optimizer[y][x-opc]=opc;
			else
				for(;opc<0;opc++)
					optimizer[y][x+opc]=opc;
			opc=0;}
		}
	if(opc>0)
		for(;opc>0;opc--)
			optimizer[y][size.x-opc]=opc;
	else
		for(;opc<0;opc++)
			optimizer[y][size.x+opc]=opc;
	optimizeTime[y]=nowTime;
	nextTime=nowTime+1;
	return true;
	}

void	DSBitmap::fullOptimize()
	{
	if(sizeof(DS_RGB)!=4)
		error("Wrong size.");
	for(int y=0;y<size.y;y++)
		monoOptimize(y);
	}

int		DSBitmap::getOptimizer(Coord p)const
	{
	return optimizer[p.y][p.x];
	}
/*void	DSBitmap::setOptimizer(int y,int x,int num,int alpha)
	{
	argCheck(y<0||y>=size.y||num<=0||x<0||x+num>size.x||alpha<0||alpha>255);

	if(alpha!=255)
		takeMin(optimizer[y][0],x);
	if(alpha!=0)
		takeMax(optimizer[y][0],-x);
	if(optimizer[y][0]>=x&&optimizer[y][0]<x+num&&alpha==255)
		optimizer[y][0]=x+num;
	if(optimizer[y][0]<=-x&&optimizer[y][0]>-x-num&&alpha==0)
		optimizer[y][0]=-x-num;
	optimizerAvailable[y]=false;
	}*/
	
void	DSBitmap::drawPixel(Coord c,DS_RGB color)
	{
	if(color.a==255)
		{
		put(c,color);
		return;
		}
	
	
	{
	DS_RGB &S=color;
	DS_RGB &D=data[c.y][c.x];
	int a=S.a,b=(255-S.a)*D.a;
	D.a=255-(255-S.a)*(255-D.a)/255;
	if(D.a==0)
		return;
	D.r=(a*S.r+b*D.r/255)/D.a;
	D.g=(a*S.g+b*D.g/255)/D.a;
	D.b=(a*S.b+b*D.b/255)/D.a;
	}
	edited(c.y);
	}

void	DSBitmap::put(Coord c,DS_RGB color)
	{
	if(!(c>=zeroCoord&&c<size))
		return;

	data[c.y][c.x]=color;

	edited(c.y);
	//setOptimizer(c.y,c.x,1,color.a);
	}
void	DSBitmap::put(Coord c,DS_RGB color,int num)
	{
	if(c.y<0||!(c<size))
		return;
	if(c.x<0)
		{
		num+=c.x;
		c.x=0;
		}
	if(num<=0)
		return;
	takeMin(num,size.x-c.x);

	data[c.y][c.x]=color;
	int i;
	for(i=1;i<=num/2;i<<=1)
		memcpy(data[c.y]+c.x+i,data[c.y]+c.x,i*sizeof(DS_RGB));
	if(i<num)
		memcpy(data[c.y]+c.x+i,data[c.y]+c.x,(num-i)*sizeof(DS_RGB));

	edited(c.y);
	//setOptimizer(c.y,c.x,num,color.a);
	}
void	DSBitmap::setAlpha(Coord c,int a)
	{
	if(c>=zeroCoord&&c<size)
		data[c.y][c.x].a=a;
	edited(c.y);
	//setOptimizer(c.y,c.x,1,a);
	}

DS_RGB	DSBitmap::get(int x,int y)const
	{
	return get(Coord(x,y));
	}
DS_RGB	DSBitmap::get(Coord c)const
	{
	if(!(c>=zeroCoord&&c<size))
		error("Bitmap::get out of size.");
	return data[c.y][c.x];
	}
int		DSBitmap::getAlpha(Coord c)const
	{
	if(!(c>=zeroCoord&&c<size))
		error("Bitmap::getAlpha out of size.");
	return data[c.y][c.x].a;
	}


void	DSBitmap::clear(DS_RGB c)
	{
	put(zeroCoord,c,size.x);
	for(int y=1;y<size.y;y++)
		memcpy(data[y],data[0],size.x*sizeof(DS_RGB));

	edited();
	/*int o=0;
	if(c.a==255)
		o=size.x;
	if(c.a==0)
		o=-size.x;
	for(int y=0;y<size.y;y++)
		{
		optimizerAvailable[y]=false;
		optimizer[y][0]=o;
		}*/
	}
	
void	DSBitmap::clear(DS_RGB c,Coord p,Coord s)
	{
	Coord lt=zeroCoord|p;
	Coord rb=size&(p+s);
	Coord div=rb-lt;
	if(!(div>zeroCoord))
		return;
	put(lt,c,div.x);
	for(int y=lt.y+1;y<rb.y;y++)
		memcpy(data[y]+lt.x,data[lt.y]+lt.x,div.x*sizeof(DS_RGB));

	edited();
	}

void	DSBitmap::convertAlpha(DS_RGB t)
	{
	FOR_COORD(c,size)
		if(get(c)==t)
			setAlpha(c,0);
	END_FOR_COORD
	}

void	DSBitmap::ignoreAlpha()
	{
	FOR_COORD(c,size)
		setAlpha(c,255);
	END_FOR_COORD
	}

void	DSBitmap::move(Coord dp,DS_RGB c)
	{
	if(dp==zeroCoord)
		return;
	Coord lt=zeroCoord|dp;
	Coord rb=size&(dp+size);
	int dx=lt.x;
	int sx=dp.x>=0?0:-dp.x;
	int lenx=(rb.x-lt.x)*sizeof(DS_RGB);
	if(!(rb>lt))
		{
		clear(c);
		return;
		}
	if(dp.y==0)
		for(int y=0;y<size.y;y++)
			{
			memmove(data[y]+dx,data[y]+sx,lenx);
			for(int x=0;x<lt.x;x++)
				data[y][x]=c;
			for(int x=rb.x;x<size.x;x++)
				data[y][x]=c;
			}
	else if(dp.y<0)
		{
		for(int y=0;y<size.y+dp.y;y++)
			{
			memcpy(data[y]+dx,data[y-dp.y]+sx,lenx);
			for(int x=0;x<lt.x;x++)
				data[y][x]=c;
			for(int x=rb.x;x<size.x;x++)
				data[y][x]=c;
			}
		for(int y=size.y+dp.y;y<size.y;y++)
			for(int x=0;x<size.x;x++)
				data[y][x]=c;
		}
	else //if(dp.y>0)
		{
		for(int y=size.y-1;y>=dp.y;y--)
			{
			memcpy(data[y]+dx,data[y-dp.y]+sx,lenx);
			for(int x=0;x<lt.x;x++)
				data[y][x]=c;
			for(int x=rb.x;x<size.x;x++)
				data[y][x]=c;
			}
		for(int y=dp.y-1;y>=0;y--)
			for(int x=0;x<size.x;x++)
				data[y][x]=c;
		}

	edited();
	/*int o=0;
	if(c.a==255)
		o=size.x;
	if(c.a==0)
		o=-size.x;
	for(int y=0;y<size.y;y++)
		{
		if(y<dp.y||y>=size.y+dp.y)
			optimizer[y][0]=o;
		else
			if(dp.x==0)
				fullOptimize();
			else if(dp.x>0)
				if(c.a==255)
					if(optimizer[y][0]>0)
						optimizer[y][0]=optimizer[y][0]+dp.x;
					else
						optimizer[y][0]=dp.x;
				else if(c.a==0)
					if(optimizer[y][0]<0)
						optimizer[y][0]=optimizer[y][0]-dp.x;
					else
						optimizer[y][0]=-dp.x;
				else
					optimizer[y][0]=0;
			else// if(dp.x<0)
				if(optimizer[y][0]>-dp.x)
					optimizer[y][0]+=dp.x;
				else if(optimizer[y][0]<dp.x)
					optimizer[y][0]-=dp.x;
				else
					fullOptimize();
		}*/
	}



void	DSBitmap::bucketFlow(BucketNode *now,BucketNode *a,BucketNode *b,BucketNode *origin)
	{
	if(a!=NULL)
		{
		if(a->next==a)
			a=NULL;
		else
			while(a->next!=NULL)
				a=a->next;
		}
	if(b!=NULL)
		{
		if(b->next==b)
			b=NULL;
		else
			while(b->next!=NULL)
				b=b->next;
		}

	if(a==NULL)
		now->next=b;
	else if(b==NULL)
		now->next=a;
	else if(a==b)
		now->next=a;
	else if(b==origin)
		{
		now->next=origin;
		a->next=origin;
		}
	else
		{
		now->next=a;
		b->next=a;
		}
	}

void	DSBitmap::bucketCenter(Coord s,int side,Coord &c,int i,BucketNode *&now,BucketNode *&last,BucketNode *&origin,bool &refresh)
	{
	i*=side;
	if(!data[c.y+s.y][c.x+s.x].same(data[c.y][c.x]))
		{
		now[i].next=now+i;
		return;
		}
	bucketFlow(now+i,last+i-side,NULL,origin);
	if(now[i].next==origin)
		refresh=true;
	}

void	DSBitmap::bucketSide(Coord s,Coord cv,int side,int iv,int max,Coord &c,int level,BucketNode *&now,BucketNode *&last,BucketNode *&origin,bool &refresh)
	{
	s+=c;
	int i=level*side;
	for(int j=1;j<max;j++)
		{
		s+=cv;
		i+=iv;
		if(!data[s.y][s.x].same(data[c.y][c.x]))
			{
			now[i].next=now+i;
			continue;
			}
		bucketFlow(now+i,last+(i-side)%(level*8-8),now+i-iv,origin);
		if(now[i].next==origin)
			refresh=true;
		}
	}
void	DSBitmap::bucketCorner(Coord s,int i,int ia,int ib,Coord &c,BucketNode *&now,BucketNode *&origin)
	{
	s+=c;
	if(!(s>=zeroCoord&&s<size))
		return;
	if(!data[s.y][s.x].same(data[c.y][c.x]))
		{
		now[i].next=now+i;
		return;
		}
	bucketFlow(now+i,now+ia,now+ib,origin);
	}
void	DSBitmap::bucketDraw(Coord s,Coord cv,int max,int skip,BucketNode *now,BucketNode *origin,DS_RGB color)
	{
	s+=cv*skip;
	now+=skip;
	max-=skip;
	for(int j=0;j<max;j++)
		{
		BucketNode *n=now;
		if(n->next!=n)
			{
			while(n->next!=NULL)
				n=n->next;
			if(n==origin)
				data[s.y][s.x]=color;
			}
		s+=cv;
		now++;
		}
	}

void	DSBitmap::bucketFill(Coord c,DS_RGB color)
	{
	if(!(c>=zeroCoord&&c<size))
		return;
	if(get(c).same(color))
		return;
	BucketNode *origin,*now,*last;
	LinkedList<BucketNode>	tree(true);
	tree.addItem(origin=dalloc<BucketNode>(1));
	origin[0].next=NULL;
	last=origin;
	Coord d=size-c-Coord(1,1);
	int i;
	for(i=1;;i++)
		{
		bool b=false;
		tree.addItem(now=dalloc<BucketNode>(i*8),true);
		if(i<=c.y)
			{
			bucketCenter(Coord(0,-i),1,c,i,now,last,origin,b);
			bucketSide	(Coord(0,-i),Coord(-1,0),1,-1,i<c.x+1?i:c.x+1,c,i,now,last,origin,b);
			bucketSide	(Coord(0,-i),Coord( 1,0),1, 1,i<d.x+1?i:d.x+1,c,i,now,last,origin,b);
			}
		if(i<=d.x)
			{
			bucketCenter(Coord( i,0),3,c,i,now,last,origin,b);
			bucketSide	(Coord( i,0),Coord(0,-1),3,-1,i<c.y+1?i:c.y+1,c,i,now,last,origin,b);
			bucketSide	(Coord( i,0),Coord(0, 1),3, 1,i<d.y+1?i:d.y+1,c,i,now,last,origin,b);
			bucketCorner(Coord(i,-i),i*2,i*2-1,i*2+1,c,now,origin);
			}
		if(i<=d.y)
			{
			bucketCenter(Coord(0, i),5,c,i,now,last,origin,b);
			bucketSide	(Coord(0, i),Coord(-1,0),5, 1,i<c.x+1?i:c.x+1,c,i,now,last,origin,b);
			bucketSide	(Coord(0, i),Coord( 1,0),5,-1,i<d.x+1?i:d.x+1,c,i,now,last,origin,b);
			bucketCorner(Coord(i, i),i*4,i*4-1,i*4+1,c,now,origin);
			}
		if(i<=c.x)
			{
			bucketCenter(Coord(-i,0),7,c,i,now,last,origin,b);
			bucketSide	(Coord(-i,0),Coord(0,-1),7, 1,i<c.y+1?i:c.y+1,c,i,now,last,origin,b);
			bucketSide	(Coord(-i,0),Coord(0, 1),7,-1,i<d.y+1?i:d.y+1,c,i,now,last,origin,b);
			bucketCorner(Coord(-i,i),i*6,i*6-1,i*6+1,c,now,origin);
			bucketCorner(Coord(-i,-i),0,1,i*8-1,c,now,origin);
			}

		if(!b)
			{
			i++;
			break;
			}
		last=now;
		}

	i=0;
	FOR_LL(now,tree,BucketNode)
		if(i==0)
			{
			data[c.y][c.x]=color;
			i++;
			CONTINUE_LL;
			}
		if(i<=c.y)
			bucketDraw(c+Coord(-i,-i),Coord( 1,0),i*2-(i<d.x+1?0:i-d.x-1),i<c.x?0:i-c.x,now    ,origin,color);
		if(i<=d.x)
			bucketDraw(c+Coord( i,-i),Coord(0, 1),i*2-(i<d.y+1?0:i-d.y-1),i<c.y?0:i-c.y,now+i*2,origin,color);
		if(i<=d.y)
			bucketDraw(c+Coord( i, i),Coord(-1,0),i*2-(i<c.x+1?0:i-c.x-1),i<d.x?0:i-d.x,now+i*4,origin,color);
		if(i<=c.x)
			bucketDraw(c+Coord(-i, i),Coord(0,-1),i*2-(i<c.y+1?0:i-c.y-1),i<d.y?0:i-d.y,now+i*6,origin,color);
			//bucketDraw(c+Coord(1,0),Coord(0,1),1,now+3,origin,color);

		i++;
	END_FOR_LL

	FOR_LL(now,tree,BucketNode)
		dfree(now);
	END_FOR_LL
	edited();
	}






void	DSBitmap::drawLineC(Coord p1,Coord p2,DS_RGB c1,DS_RGB c2)
	{
	drawLineD(p1,p2-p1,c1,c2);
	}

void	DSBitmap::drawLineD(Coord p,Coord s,DS_RGB c1,DS_RGB c2)
	{
	if(c2==brightPink)
		c2=c1;
	if(s==zeroCoord)
		{
		put(p,c1);
		return;
		}
	int mx=abs(s.x),my=abs(s.y);
	if(mx>=my)
		for(int x=0;x<=mx;x++)
			put(p+s*x/mx,blend(c1,mx-x,c2,x));
	else
		for(int y=0;y<=my;y++)
			put(p+s*y/my,blend(c1,my-y,c2,y));
	}

void	DSBitmap::drawRect(Coord p1,Coord s,DS_RGB rgb)
	{
	Coord p2=p1+s;
	put(p1,rgb,p2.x-p1.x);
	put(Coord(p1.x,p2.y-1),rgb,p2.x-p1.x);
	for(int y=p1.y+1;y<p2.y-1;y++)
		{
		put(Coord(p1.x,y),rgb);
		put(Coord(p2.x-1,y),rgb);
		}
	}

void	DSBitmap::drawFilledRect(Coord p1,Coord s,DS_RGB rgb)
	{
	Coord p2=p1+s;
	int d=p2.x-p1.x;
	for(;p1.y<p2.y;p1.y++)
		put(p1,rgb,d);
	}

void	DSBitmap::drawCircle(Coord p1,int radius,int thick,DS_RGB rgb)
	{
	Coord p2;
	thick+=radius;
	int r2=radius*radius,R2=thick*thick;
	for(p2.y=-thick;p2.y<=thick;p2.y++)
	for(p2.x=-thick;p2.x<=thick;p2.x++)
		{
		int r=p2.x*p2.x+p2.y*p2.y;
		if(r>=r2&&r<=R2)
			put(p1+p2,rgb);
		}
	}












void	DSBitmap::blit(const DSBitmap *src,Coord p)
	{
	blit(p,src,zeroCoord,src->size);
	}
void	DSBitmap::blit(int dpx,int dpy,const DSBitmap *src,int spx,int spy,int dsx,int dsy)
	{
	blit(Coord(dpx,dpy),src,Coord(spx,spy),Coord(dsx,dsy));
	}
void	DSBitmap::blit(Coord dp,const DSBitmap *src,Coord sp,Coord drawSize)
	{
	argCheck(src==NULL);
	argCheck(src->data==data);
	
	Coord lt=zeroCoord|(dp+((-sp)|zeroCoord));
	Coord rb=size&(dp+((src->size-sp)&drawSize));
	if(!(rb>lt))return;
	Coord div=rb-lt;
	Coord headCut=lt-dp;
	sp+=headCut;
	
	DS_RGB **s=src->data+sp.y;
	DS_RGB **d=     data+lt.y;
	int len=div.x*sizeof(DS_RGB);

	for(int y=0;y<div.y;y++)
		memcpy(d[y]+lt.x,s[y]+sp.x,len);

	edited();
	}
//==================================================================================


void	DSBitmap::draw(const DSBitmap *src,Coord p)
	{
	draw(p,src,zeroCoord,src->size);
	}
void	DSBitmap::draw(Coord dp,const DSBitmap *src,Coord sp,Coord drawSize)
	{
	argCheck(src==NULL);
	argCheck(src->data==data);

	Coord lt=zeroCoord|dp;
	Coord rb=size&(dp+((src->size-sp)&drawSize));
	if(!(rb>lt))return;
	Coord div=rb-lt;
	Coord headCut=lt-dp;
	sp+=headCut;
	
	DS_RGB **s=src->data+sp.y;
	DS_RGB **d=     data+lt.y;

	int **optr=src->optimizer+sp.y;
	if(sizeof(DS_RGB)!=4)error("optimize error.");
	
	for(int y=0;y<div.y;y++)
		{
		src->monoOptimize(y+sp.y);
		for(int x=0;x<div.x;)
			{
			if(optr[y][x+sp.x]>0){
				int l=optr[y][x+sp.x]>div.x-x?div.x-x:optr[y][x+sp.x];
				memcpy(d[y]+x+lt.x,s[y]+x+sp.x,4*l);
				x+=l;}
			else if(optr[y][x+sp.x]<0)
				{
				x-=optr[y][x+sp.x];
				}
			else
				{
				DS_RGB &S=s[y][x+sp.x];
				DS_RGB &D=d[y][x+lt.x];
				int a=S.a,b=(255-S.a)*D.a;
				D.a=255-(255-S.a)*(255-D.a)/255;
				x++;
				if(D.a==0)
					continue;
				D.r=(a*S.r+b*D.r/255)/D.a;
				D.g=(a*S.g+b*D.g/255)/D.a;
				D.b=(a*S.b+b*D.b/255)/D.a;
				}
			}
		}
	edited();
	}

void	DSBitmap::drawScale(const DSBitmap *src,Coord dp,Coord drawSize)
	{
	argCheck(src==NULL);
	argCheck(src->data==data);

	Coord lt=zeroCoord|dp;
	Coord rb=size&(dp+drawSize);
	if(!(rb>lt))return;
	Coord div=rb-lt;
	Coord headCut=lt-dp;
	
	DS_RGB **s=src->data;
	DS_RGB **d=     data+lt.y;
	for(int y=0;y<div.y;y++)
	for(int x=0;x<div.x;x++)
		{
		DS_RGB &S=s[(y+headCut.y)*src->size.y/drawSize.y][(x+headCut.x)*src->size.x/drawSize.x],&D=d[y][x+lt.x];
		if(S.a==255)
			memcpy(&D,&S,4);
		else
			{
			int a=S.a,b=(255-S.a)*D.a;
			D.a=255-(255-S.a)*(255-D.a)/255;
			if(D.a==0)
				continue;
			D.r=(a*S.r+b*D.r/255)/D.a;
			D.g=(a*S.g+b*D.g/255)/D.a;
			D.b=(a*S.b+b*D.b/255)/D.a;
			}
		}

	edited();
	}


int 	DSBitmap::getPalette(DS_RGB *palette,Byte **&paletteMap)const
	{
	int pi=0;
	paletteMap=dalloc<Byte*>(size.y);
	for(int y=0;y<size.y;y++)
		{
		paletteMap[y]=dalloc<Byte>(size.x);
		for(int x=0;x<size.x;x++)
			{
			int i;
			for(i=0;i<pi;i++)
				if(data[y][x]==palette[i])
					break;
			if(i==pi)
				{
				if(pi>=256)
					{
					for(int j=0;j<=y;j++)
						dfree(paletteMap[j]);
					dfree(paletteMap);
					paletteMap=NULL;
					return -1;
					}
				palette[pi++]=data[y][x];
				}
			paletteMap[y][x]=i;
			}
		}
	return pi;
	}


void	DSBitmap::drawAlpha(const DSBitmap *src)
	{
	argCheck(src==NULL);
	argCheck(src->data==data);
	argCheck(src->getSize()!=size);
	
	DS_RGB **s=src->data;
	DS_RGB **d=     data;

	int **optr=src->optimizer;
	if(sizeof(DS_RGB)!=4)error("optimize error.");
	
	for(int y=0;y<size.y;y++)
		{
		src->monoOptimize(y);
		for(int x=0;x<size.x;)
			{
			if(optr[y][x]>0)
				x+=optr[y][x];
			else if(optr[y][x]<0)
				{
				for(int i=optr[y][x];i<0&&x<size.x;i++,x++)
					d[y][x].a=0;
				}
			else
				{
				DS_RGB &S=s[y][x];
				DS_RGB &D=d[y][x];
				D.a=D.a*S.a/255;
				x++;
				}
			}
		}
	edited();
	}


#ifdef DS_WINAPI
#if defined(UNICODE) && !defined(_UNICODE)
	#define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
	#define UNICODE
#endif

#include <windows.h>

void DSBitmap::toWinDIB(unsigned char* ret,BITMAPINFO *info)const
	{
	argCheck(ret==NULL);

	if(info!=NULL)
		{
		memset(info,0,sizeof(BITMAPINFO));
		info->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		info->bmiHeader.biWidth = size.x;
		info->bmiHeader.biHeight = size.y;
		info->bmiHeader.biPlanes = 1;
		info->bmiHeader.biBitCount = 24;
		info->bmiHeader.biCompression = BI_RGB;
		//info->bmiHeader.biClrUsed = 256;
		}

	int pitch=(size.x*3+3)&(~0x3);
	for(int y=0;y<size.y;y++)
	for(int x=0;x<size.x;x++)
		{
		memcpy(ret+pitch*y+x*3  ,data[size.y-y-1]+x,3);
		continue;
		memcpy(ret+pitch*y+x*3  ,&data[y][x].b,1);
		memcpy(ret+pitch*y+x*3+1,&data[y][x].g,1);
		memcpy(ret+pitch*y+x*3+2,&data[y][x].r,1);

		}
	}
#endif
