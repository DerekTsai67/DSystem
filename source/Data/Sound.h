






class Sound:public Data{
	public:
	 Sound(int sampleCount,int channel,int sampleRate,int byteRate);
	~Sound();
Sound*	clone();
void	initResource();
void	deleteResource();
bool	save(Output *out,int suffixType,int safeFail=0)const;
void	saveWAV(Output *out)const;
int 	getLength();
int 	getChannel();
int 	getSample(int c,int i);
void	setSample(long d,int c,int i);
void	addSound(Sound *s,int time);
void	multiply(double rate);
void	play();
	private:
long 	**data;
void	*header1,*header2;
char	*buffer1,*buffer2;
int 	length,channel,sampleRate,byteRate;
	};



