#ifndef D_SYSTEM
#define D_SYSTEM
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cstring>
#include <cmath>
#include <ctime>
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <conio.h>
using namespace std;

class String;
class ConstString;
class ThreadLock;

struct TempAllocInfo{
	int		setTime;
	void*	data;
	};

#define isType(x,Type)		(dynamic_cast<Type*>(x)!=NULL)
#define typeCheck(x,Type)	{if(!isType(x,Type))error("Unexpected Type. " #x "!=" #Type);}
#define argCheck(x)			{if(x)error("Unexpected argument. " #x);}
#define safeError(x...) {\
						if(!safeFail)\
							error(x);\
						else{\
							report("safeError [%s]:  \\",__FUNCTION__);\
							report(x);\
							if(safeFail<2)safeFail=2;}\
						}
#define safeFatalError(x...) {\
						if(!safeFail)\
							error(x);\
						else{\
							report("safeError [%s]:  \\",__FUNCTION__);\
							report(x);\
							if(safeFail<3)safeFail=3;}\
						}
						
#define VSTRING	va_list va;va_start(va,data);ConstString str=vformat(data,va);va_end(va);
ConstString	format(ConstString str,...);
ConstString	vformat(ConstString str,__VALIST va);

#define error(x...)			error_(__FUNCTION__,x)
void	error_(ConstString func,ConstString str,...);
//void	throwError(ConstString str,...);
void	warning(ConstString str,...);
void	report(int d,ConstString str,...);
void	report(ConstString str,...);
void	vreport(ConstString prefix,ConstString str,__VALIST va);
ConstString	viewData(ConstString str);
ConstString	viewData(const void *str,int size=-1);
#define CD(x) {char dir[300];getDirectory(x,dir);chdir(dir);}
#define PI 3.1415926535

#define takeMin(a,x) {int &a_=(a),x_=(x);if(a_>x_)a_=x_;}
#define takeMax(a,x) {int &a_=(a),x_=(x);if(a_<x_)a_=x_;}


class Thread;
class Coord;

struct DS_RGB;
class Bitmap;
class Picture;
class Datafile;

class Input;
class Output;
class IOBuffer;

class Window;
class Object;
class Sense;
class React;
class Displayer;

template<class A,class B>			class ObjectLinker;
typedef ObjectLinker<Object,Object>	EtcLinker;
typedef int				(*Compare)(const void *,const void *);
typedef void			(*VoidFunction)();
typedef void			(*VoidPointerFunction)(void*);
typedef unsigned char	Byte;

//namespace DS{
#include "Math/DSystemMath.h"

#include "Data/DSystemData.h"
#include "Stream/DSystemStream.h"




void deleteWindow(Window *n);


enum BaseDatafile{
	BDF_TestWindow1,
	BDF_TestWindow2,
	BDF_Scroll,
	BDF_TextBG,
	BDF_TextCursorOld,
	BDF_Button,
	BDF_Block,
	BDF_MenuCursor,
	BDF_hollowBG,
	BDF_DataButton,
	BDF_Window,
	BDF_Click,
	BDF_TextCursor,
	BDF_count
	};



void	initDSystem(ConstString path=NULL);
void	DSystem();
void	endCheck();
void	refreshWindow(Window *w);
void	refreshWindow();
//bool	addWindow(Window *n);
//void	keyProgress(KeySignal keySignal);
void	maxmizeWindow(Window *w);
void	screenShot();

Position autoLocate(Position p0,Coord dp,int bn=10,Coord dp2=zeroCoord);
void	nonFunction();

void	fullRender();


int		unpackYMDHMS(int y,int m,int d,int h,int m2,int s);
int		unpackYMD(int y,int m,int d);
int		unpackHMS(int h,int m,int s);

extern Coord window_size;
extern char titleName[];
extern DS_RGB BGC;
extern DSBitmap *DSBuffer;
extern Window *head,*base;
extern Object *nowOwner;
extern Object *nowObject,*focusObject;
extern Datafile *DSData,*buttonData,*choosedButtonData,*hollowData;
extern Font	*font,*smallFont;
extern long long int ftime;
extern int delayTime;
extern int objectCount;
extern Coord dCursor,cursorPos,lastCursorPos;
const int	cursorTrackQueueMax=3,cursorTrackMax=500;
extern Coord	*cursorTrack[cursorTrackQueueMax];
extern int		cursorTrackQueueIndex,cursorTrackIndex[cursorTrackQueueMax];
extern bool reportOff;
extern Sense *nonSense,*nowSense,*sense;
extern React *nonReact,*react;
extern int dallocCount;
extern Array<TempAllocInfo>	*tempAlloc;
extern Thread *showThread;


//};using namespace DS;
#include "Object/DSystemObject.h"
#include "OS/DSystemOS.h"

#include "Data/Step.hpp"
#include "Math/Array.hpp"
#include "Math/Queue.hpp"
#include "Math/ByteTree.hpp"

extern KeySignal keySignal;


#endif // DSYSTEM
