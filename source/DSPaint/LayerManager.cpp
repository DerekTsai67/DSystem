






	LayerManager::LayerManager():Window(Position(8,8,Position::EffectiveRightDown),DSData->getPic(BDF_Window),Size(192,386))
	{
	SENSE->left=new MoveSensor();
	layerButton=new LinkedList<LayerButton>;
	addEtcObject(menu=new Menu(Position(8,8),Size(150,300),(LinkedList<MenuObject>*)layerButton));
	menu->setDirection(Menu::vertical);
	refresh();
	}

	LayerManager::~LayerManager()
	{

	}


void	LayerManager::rclick(Coord c)
	{
	Popup *p=new Popup(getPosition()+c,this);
	if(!isType(frameData->getLayer(),FolderLayer))
		p->addContext("Enable Layer",3);
	p->addContext("Close",ResponseIndex_close);
	p->endFolder();
	addWindow(p);
	}


void	LayerManager::response(int r)
	{
	switch(r)
		{
	case 3:
		addProcessStep(PictureProcess_enableLayer);
		break;
		
	case ResponseIndex_close:
		kill();
		break;
	case ResponseIndex_shiftSelect:
		{
		bool b=false;
		FOR_LL(now,*layerButton,LayerButton)
			if(now->isMenuSelected()||now->isData(editingLayer))
				{
				if((now->isMenuSelected()&&now->isData(editingLayer))||b)
					{
					now->setMenuSelected(true);
					break;
					}
				b=true;
				}

			if(b)
				now->setMenuSelected(true);
		END_FOR_LL
		}
		break;
	
	case ResponseIndex_copyMulti://copy
		clipboard->clear(Clipboard_Layer);
		FOR_LL(now,*layerButton,LayerButton)
			if(now->isMenuSelected())
				clipboard->addCopy(now->getLayer());
		END_FOR_LL
		refreshWindow();
		break;
	
	case 21:
	case 31://delete selected
		{
		int max=0,index=0;
		FOR_LL(now,*layerButton,LayerButton)
			if(now->isMenuSelected())
				max++;
		END_FOR_LL
		Layer **addLayer=dalloc<Layer*>(max);
		FolderLayer **targetLayer=dalloc<FolderLayer*>(max);
		int *pos=dalloc<int>(max);

		FOR_LL(now,*layerButton,LayerButton)
			if(now->isMenuSelected())
				{
				switch(r)
					{
				case 21:
					//addFrame[index]=now->getFrame()->clone();
					//pos[index]=imgData->getFrameIndex(now->getFrame())+1+index;
					break;
				case 31:
					addLayer[index]=now->getLayer();
					targetLayer[index]=addLayer[index]->getOwner();
					pos[index]=targetLayer[index]->getIndex(addLayer[index]);
					break;
					}
				index++;
				}
		END_FOR_LL
		
		switch(r)
			{
		case 21:
			//stepContainer->addStep(new AddFrameStep(addFrame,pos,max));
			break;
		case 31:
			stepContainer->addStep(new AddLayerStep(addLayer,targetLayer,pos,max,true));
			break;
			}
		}
		break;
		}
	}


void	LayerManager::select(Layer *l)
	{
	FOR_LL(now,*layerButton,LayerButton)
		now->select(now->getLayer()==l);
	END_FOR_LL
	}



void	LayerManager::refresh()
	{
	switch(dataType)
		{
	case Data::Type_Picture:
		{
		Layer *layer=frameData->getLayer();
		layer->forClear();
		//layer->forNext();//skip root layer
		int skipIndent=-1;
		menu->refreshStart();
		while(true)
			{
			Layer *nowLayer=layer->forNext();
			if(nowLayer==NULL)
				break;
			
			if(skipIndent!=-1)
				{
				if(nowLayer->getIndent()>skipIndent)
					continue;
				else
					skipIndent=-1;
				}
			
			LayerButton *now=(LayerButton*)menu->refreshNext(nowLayer);
			if(now==NULL)
				menu->refreshAdd(now=new LayerButton(nowLayer));
			now->setIndent(Coord(nowLayer->getIndent()*8,0));
			
			if(nowLayer->getFolding())
				skipIndent=nowLayer->getIndent();
			}
		menu->refreshEnd();
		menu->fitSize(DSData->getPic(BDF_hollowBG));
		}
		break;
	default:
		kill();
		break;
		}
	}






	LayerManager::LayerButton::LayerButton(Layer *l):MenuObject(DSData->getPic(BDF_DataButton),Size(150,32))
	{
	layer=l;
	defaultReShow=true;
	addDisplayer(foldingDisplayer=new Displayer());
	foldingDisplayer->setPos(Position(6,16,Position::Center,Position::EffectiveLeftUp));
	addEtcObject(visibleButton=new Button(Position(16,8),hollowData,Size(16,16),NULL,2));
	visibleButton->setOwner(this);
	}

	LayerManager::LayerButton::~LayerButton()
	{
	}

Layer*	LayerManager::LayerButton::getLayer()
	{
	return layer;
	}
	
bool	LayerManager::LayerButton::isData(void *l)const
	{
	return layer==l;
	}
	
void	LayerManager::LayerButton::lclick(Coord c)
	{
	if(c.x<12)
		{
		if(isType(layer,FolderLayer))
			{
			layer->setFolding(!layer->getFolding());
			refreshWindow(getOwner());
			}
		if(!isType(layer,SimpleLayer))
			return;
		}

	if(keySignal.press(KEY_CTRL))
		{
		setMenuSelected(!isMenuSelected());
		return;
		}
	if(keySignal.press(KEY_SHIFT))
		{
		getMenuOwner()->clearSelect();
		setMenuSelected(true);
		getOwner()->response(ResponseIndex_shiftSelect);
		return;
		}

	getMenuOwner()->clearSelect();
	setMenuSelected(true);
	setLayer(layer);
	}

void	LayerManager::LayerButton::rclick(Coord c)
	{
	Popup *p=new Popup(getPosition()+c,this);
	p->addFolder("Add");
		p->addContext("Layer",ResponseIndex_add+1);
		p->addContext("Folder",ResponseIndex_add+2);
		p->addContext("Color",ResponseIndex_add+3);
		p->endFolder();
	p->addFolder("Set FilterType");
		p->addContext("Default"		,ResponseIndex_filterType+Layer::FilterType_draw);
		p->addContext("Blit"		,ResponseIndex_filterType+Layer::FilterType_blit);
		p->addContext("Alpha Filter",ResponseIndex_filterType+Layer::FilterType_alphaFilter);
		p->endFolder();
	if(clipboard->avalible(Clipboard_Layer))
		{
		p->addContext("Paste",ResponseIndex_paste);
		p->addContext("Paste Link",ResponseIndex_pasteLink);
		p->addContext("Move",65);
		}
	if(isMenuSelected()&&getMenuOwner()->isMultiSelected())
		{
		p->addContext("Copy",ResponseIndex_copyMulti);
		p->addContext("Clone",21);
		p->addContext("Delete",31);
		}
	else
		{
		p->addContext("Copy",ResponseIndex_copy);
		p->addContext("Clone",ResponseIndex_clone);
		p->addContext("Delete",ResponseIndex_delete);
		}
	p->endFolder();
	addWindow(p);
	downClick=c.y>getSize().y/2;
	}
	
void	LayerManager::LayerButton::input()
	{
	if(keySignal.down(KEY_CTRL|KEY_C))
		response(ResponseIndex_copyMulti);
	if(keySignal.down(KEY_CTRL|KEY_V))
		response(ResponseIndex_paste);
	if(keySignal.down(KEY_DELETE))
		response(31);
	}

void	LayerManager::LayerButton::response(int r)
	{
	switch(r)
		{
	case 2:
		layer->setVisible(!layer->getVisible());
		imgReshow();
		break;
	
	case ResponseIndex_add+1:
		stepContainer->addStep(new AddLayerStep(new SimpleLayer(imgSize),layer,downClick));
		break;
	case ResponseIndex_add+2:
		stepContainer->addStep(new AddLayerStep(new FolderLayer(imgSize),layer,downClick));
		break;
	case ResponseIndex_add+3:
		stepContainer->addStep(new AddLayerStep(new ColorLayer(imgSize,black),layer,downClick));
		break;
	case ResponseIndex_clone:
		stepContainer->addStep(new AddLayerStep(layer->clone(),layer,downClick));
		break;
	case ResponseIndex_delete:
		stepContainer->addStep(new AddLayerStep(layer,layer,false,true));
		break;
	case ResponseIndex_copy:
		clipboard->clear(Clipboard_Layer);
		clipboard->addCopy(layer);
		refreshWindow();
		break;
	
	case ResponseIndex_paste:
	case ResponseIndex_pasteLink:
		{
		if(!clipboard->avalible(Clipboard_Layer))
			break;
		bool legal=true;
		
		Layer **addLayer=dalloc<Layer*>(clipboard->getNumber());
		FolderLayer **targetLayer=dalloc<FolderLayer*>(clipboard->getNumber());
		int *pos=dalloc<int>(clipboard->getNumber());
		
		FolderLayer *fl;
		int p;
		if(isType(layer,FolderLayer)&&downClick)
			{
			fl=(FolderLayer*)layer;
			p=0;
			}
		else
			{
			fl=layer->getOwner();
			p=fl->getIndex(layer)+downClick;
			}
		
		for(int i=0;i<clipboard->getNumber();i++)
			{
			Layer *l=(Layer*)clipboard->getData(i);
			switch(r)
				{
			case ResponseIndex_paste:
				addLayer[i]=l->clone();
				break;
			case ResponseIndex_pasteLink:
				addLayer[i]=new LinkLayer(l);
				{
				Layer *tl=fl;
				while(tl!=NULL)
					{
					if(tl==l)
						legal=false;
					tl=tl->getOwner(true);
					}
				}
				break;
				}
			targetLayer[i]=fl;
			pos[i]=p+i;
			}
		if(legal==false)
			{
			delete new AddLayerStep(addLayer,targetLayer,pos,clipboard->getNumber());
			break;
			}
		stepContainer->addStep(new AddLayerStep(addLayer,targetLayer,pos,clipboard->getNumber()));
		}
		break;
		
	case ResponseIndex_copyMulti:
		getOwner()->response(ResponseIndex_copyMulti);
		break;
	
	case ResponseIndex_filterType+Layer::FilterType_draw:
	case ResponseIndex_filterType+Layer::FilterType_blit:
	case ResponseIndex_filterType+Layer::FilterType_alphaFilter:
		layer->setFilterType(r-ResponseIndex_filterType);
		break;
		}
	}

void	LayerManager::LayerButton::select(bool b)
	{
	reShow();
	}

void	LayerManager::LayerButton::show()
	{
	if(isType(layer,FolderLayer))
		{
		wBuffer->drawFilledRect(zeroEffectivePosition,Coord(12,wBuffer->getSize().y),red);
		foldingDisplayer->setPic(foldingArrow->getPic(layer->getFolding()?"Close":"Open"));
		}
	else if(isType(layer,LinkLayer))
		{
		wBuffer->drawFilledRect(zeroEffectivePosition,Coord(12,wBuffer->getSize().y),yellow);
		foldingDisplayer->setPic(foldingArrow->getPic("Link"));
		}
	else if(isType(layer,ColorLayer))
		{
		wBuffer->drawFilledRect(zeroEffectivePosition,Coord(12,wBuffer->getSize().y),black);
		foldingDisplayer->setPic(NULL);
		}
	else 
		foldingDisplayer->setPic(NULL);
	if(clipboard->isCopyed(layer))
		wBuffer->clear(yellow);
	if(editingLayer==layer)
		wBuffer->clear(blue);
	if(isMenuSelected())
		wBuffer->clear(green);
	visibleButton->setIcon(layer->getVisible()?DSPaintData->getPic(1):NULL);
	wBuffer->blitSquare(DSData->getPic(BDF_hollowBG),Position(44,4),Size(24,24));
	wBuffer->getEditableBitmap()->drawScale(layer->getBitmap(),Coord(44,4),Coord(24,24));
	}







