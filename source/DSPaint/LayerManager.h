


class LayerManager:public Window{
	public:
	LayerManager();
	~LayerManager();
void	rclick(Coord c);
void	response(int r);
void	select(Layer *l);
void	refresh();
	private:
class	LayerButton;
Menu	*menu;
LinkedList<LayerButton>	*layerButton;




class LayerButton:public MenuObject{
	public:
	LayerButton(Layer *l);
	~LayerButton();
Layer*	getLayer();
bool	isData(void *l)const;
void	lclick(Coord c);
void	rclick(Coord c);
void	input();
void	response(int r);
void	select(bool b);
void	show();
	private:
Layer	*layer;
Displayer	*foldingDisplayer;
Button	*visibleButton;
bool	downClick;
	};
	};












