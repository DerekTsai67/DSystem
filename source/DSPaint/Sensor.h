


class DrawSensor:public PullSensor{
	public:
	DrawSensor(int i);
	~DrawSensor();
friend	void drawProcess(void*);
void	down(Coord p);
void	press(Coord p);
void	up(Coord p);
	private:
void	prepareBuffer();
Coord	drawedCursor;
int 	index;
DS_RGB	color;
DSBitmap	*drawBuffer;
};

class BrushSensor:public PullSensor{
	public:
	BrushSensor(int i);
	~BrushSensor();
friend	void brushProcess(void*);
void	down(Coord p);
void	press(Coord p);
void	up(Coord p);
	private:
void	prepareBuffer();
Coord	drawedCursor;
int 	index;
DS_RGB	color;
int		radius;
MonoBitmap	*fillBuffer;
Coord		lastLightPos;
MonoBitmap *lastLight;
DSBitmap	*drawBuffer;
};


class LineSensor:public PullSensor{
	public:
	LineSensor(int i);
	~LineSensor();
void	down(Coord p);
void	press(Coord p);
void	up(Coord p);
	private:
void	prepareBuffer();
int 	index;
Coord	start;
DSBitmap	*drawBuffer;
};


class EraserSensor:public PullSensor{
	public:
void	down(Coord p);
void	press(Coord p);
};

class MoverSensor:public PullSensor{
	public:
void	down(Coord p);
void	press(Coord p);
	private:
Coord	start;
};

class BucketSensor:public PullSensor{
	public:
	BucketSensor(int i);
void	down(Coord p);
	private:
int 	index;
	};

class ColorPickerSensor:public PullSensor{
	public:
	ColorPickerSensor(int i);
void	press(Coord p);
	private:
int 	index;
	};


class EnclosureSensor:public PullSensor{
	public:
	EnclosureSensor();
	~EnclosureSensor();
void	down(Coord p);
void	press(Coord p);
void	up(Coord p);
void	paste(DSBitmap *bmp);
	private:
int			mode;
Coord		base,basePos;
DSBitmap	*drawBuffer;
DSBitmap	*backBuffer;
DSBitmap	*frontBuffer;
	};
	
class CursorSensor:public PullSensor{
	public:
	CursorSensor();
void	down(Coord p);
	private:
	};

class DeviateSensor:public PullSensor{
	public:
	DeviateSensor();
void	down(Coord p);
void	press(Coord p);
void	up(Coord p);
	private:
Coord	base,originDeviation;
	};
