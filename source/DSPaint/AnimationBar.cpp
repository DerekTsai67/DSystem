


static const int barLength=1000;


int 	AnimationBar::clickTime=0,AnimationBar::playTime=0;

	AnimationBar::AnimationBar():Window(Position(48,48),DSData->getPic(BDF_Window),Size(16+barLength+16,16+32+16+32+16))
	{
	SENSE->left=new MoveSensor();
	addEtcObject(delayScroll=new HiddenScroll(Position(20,10),1,2000,10,3));
	addEtcObject(menu=new Menu(Position(16,16+32+16),Size(barLength,32),(LinkedList<MenuObject>*)(frameBlock=new LinkedList<FrameBlock>())));
	refresh();
	playTime=clickTime=0;
	}

	AnimationBar::~AnimationBar()
	{
	}

void	AnimationBar::rclick(Coord c)
	{
	Popup *p=new Popup(getPosition()+c,this);
	//p->addContext("Add frame",FrameProcess_addFrame);
	p->addContext("Reverse",PictureProcess_reverseFrame);
	p->addContext("Unify delay",FrameProcess_unifyDelay);
	if(clipboard->avalible(Clipboard_Frame))
		p->addContext("Set As NextFrame",ResponseIndex_setNextFrame);
	p->addContext("Set TimeType",5);
	p->addContext("Diff",456);
	p->addContext("Close",ResponseIndex_close);
	p->endFolder();
	addWindow(p);
	}

void	AnimationBar::response(int r)
	{
	switch(r)
		{
	case 456:
		stepContainer->addStep(new FrameProcessStep(FrameProcess_diff,frameIndex,false));
		break;
	case 3:
		frameData->setDelay(delayScroll->getValue());
		refreshWindow(this);
		break;
	case 5:
		imgData->setTimeType(Picture::Realtime);
		break;
	case ResponseIndex_setNextFrame:
		if(!clipboard->avalible(Clipboard_Frame))
			break;
		stepContainer->addStep(new SetPicNextFrameStep((Frame*)clipboard->getData(0)));
		reShow();
		break;
		
	case FrameProcess_unifyDelay:
		stepContainer->addStep(new FrameProcessStep(FrameProcess_unifyDelay,frameIndex,false));
		break;
		
	case ResponseIndex_close:
		kill();
		break;
	
	case ResponseIndex_shiftSelect:
		{
		bool b=false;
		FOR_LL(now,*frameBlock,FrameBlock)
			if(now->isMenuSelected()||now->isData(frameData))
				{
				if((now->isMenuSelected()&&now->isData(frameData))||b)
					{
					now->setMenuSelected(true);
					break;
					}
				b=true;
				}

			if(b)
				now->setMenuSelected(true);
		END_FOR_LL
		}
		break;
	
	case ResponseIndex_copyMulti:
		clipboard->clear(Clipboard_Frame);
		FOR_LL(now,*frameBlock,FrameBlock)
			if(now->isMenuSelected())
				clipboard->addCopy(now->getFrame());
		END_FOR_LL
		refreshWindow();
		break;
	
	case ResponseIndex_cloneMulti:
	case ResponseIndex_deleteMulti:
	case ResponseIndex_reverse:
		{
		int max=0,index=0;
		FOR_LL(now,*frameBlock,FrameBlock)
			if(now->isMenuSelected())
				max++;
		END_FOR_LL
		Frame **addFrame=dalloc<Frame*>(max);
		int *pos=dalloc<int>(max);

		FOR_LL(now,*frameBlock,FrameBlock)
			if(now->isMenuSelected())
				{
				switch(r)
					{
				case ResponseIndex_cloneMulti:
					addFrame[index]=now->getFrame()->clone();
					pos[index]=imgData->getFrameIndex(now->getFrame())+1+index;
					break;
				case ResponseIndex_deleteMulti:
					addFrame[index]=now->getFrame();
					pos[index]=imgData->getFrameIndex(now->getFrame());
					break;
				case ResponseIndex_reverse:
					pos[index]=imgData->getFrameIndex(now->getFrame());
					break;
					}
				index++;
				}
		END_FOR_LL
		
		switch(r)
			{
		case ResponseIndex_cloneMulti:
			stepContainer->addStep(new AddFrameStep(addFrame,pos,max));
			break;
		case ResponseIndex_deleteMulti:
			stepContainer->addStep(new AddFrameStep(addFrame,pos,max,true));
			break;
		case ResponseIndex_reverse:
			{
			int *to=dalloc<int>(max);
			for(int i=0;i<max;i++)
				to[i]=pos[0];
			stepContainer->addStep(new MoveFrameStep(pos,to,max));
			dfree(addFrame);
			}
			break;
			}
		}
		break;
		}

	addProcessStep(r);
	}

void	AnimationBar::input()
	{
	if(keySignal.down(KEY_CTRL|KEY_R))
		{
		int m=imgData->getFrameNumber();
		for(int i=0;i<m;i++)
			{
			imgData->addFrame(imgData->getFrame(i)->clone());
			}
		refreshWindow(this);
		}
	if(keySignal.down(KEY_CTRL|KEY_E))
		{
		int m=imgData->getFrameNumber();
		for(int i=m-2;i>=0;i-=2)
			{
			imgData->deleteFrame(i);
			}
		refreshWindow(this);
		}
	if(keySignal.down(KEY_HOME))
		setFrame(0);
	if(keySignal.down(KEY_END))
		setFrame(-1);
	if(keySignal.down(KEY_LEFT))
		setFrame(frameIndex-1);
	if(keySignal.down(KEY_RIGHT))
		setFrame(frameIndex+1);
	static bool autoPlay=false;
	if(keySignal.down(KEY_LEFT)||keySignal.down(KEY_RIGHT))
		{
		playTime=0;
		clickTime=clock();
		//autoPlay=true;
		}
	if(keySignal.press(KEY_RIGHT)||keySignal.press(KEY_LEFT)||autoPlay)
		{
		if(playTime==0)
			{
			if((clock()-clickTime)*1000/CLOCKS_PER_SEC>500)
				playTime=imgData->getTime();
			}
		else
			{
			int time=imgData->getTime();
			if(time-playTime>frameData->getDelay())
				{
				time-=frameData->getDelay();
				playTime+=frameData->getDelay();
				if(keySignal.press(KEY_LEFT))
					setFrame(frameIndex-1);
				else
					setFrame(frameIndex+1);
				}
			}
		}
	}

void	AnimationBar::refreshIndex(int i)
	{
	refreshWindow(this);
	return;
	argCheck(i<0||i>=imgData->getFrameNumber());
	frameBlock->getItem(frameIndex)->activate(false);
	frameBlock->getItem(i)->activate(true);
	frameIndex=i;
	delayScroll->setValue(frameData->getDelay());
	}

void	AnimationBar::refresh()
	{
	switch(dataType)
		{
	case Data::Type_Text:
		kill();
		break;
	case Data::Type_Picture:
		{
		menu->refreshStart();
		for(int i=0;i<imgData->getFrameNumber();i++)
			{
			int l=imgData->getFrame(i)->getDelay()/10;
			if(l<2)l=2;
			
			FrameBlock *now=(FrameBlock*)menu->refreshNext(imgData->getFrame(i));
			if(now==NULL)
				menu->refreshAdd(now=new FrameBlock(l,i));
			else
				{
				now->setIndex(i);
				now->reSize(Size(l,32));
				}

			if(i==frameIndex)
				now->activate(true);
			else
				now->activate(false);
			}
		
		menu->refreshEnd();
		//menu->fitSize(DSData->getPic(BDF_DataButton));
		delayScroll->setValue(frameData->getDelay());
		reShow();
		}
		break;
	default:
		error("switchOut");
		break;
		}
	}

void	AnimationBar::show()
	{
	ConstString str=format("Next:%S[%d]",imgData->getNextPic()->getName(),imgData->getNextIndex());
	font->putString(wBuffer,str,Position(10,10,Position::EffectiveRightUp));
	for(int i=0;i<frameData->getNextFrameNumber()&&i<5;i++)
		{
		ConstString str=format(str,"Next[%d]:%S[%d]",i,frameData->getNextPic(i)->getName(),frameData->getNextIndex(i));
		font->putString(wBuffer,str,Position(100,5+20*i));
		}
	}
	
	
	
	
	

	AnimationBar::FrameBlock::FrameBlock(int w,int i):MenuObject(DSData->getPic(BDF_DataButton),Size(w,32))
	{
	frame=imgData->getFrame(i);
	setIndex(i);
	acting=false;
	rightClick=false;
	}

void	AnimationBar::FrameBlock::setIndex(int i)
	{
	index=i;
	if(imgData->getFrame(index)!=frame)
		error("Wrong frame.");
	}

Frame*	AnimationBar::FrameBlock::getFrame()const
	{
	return frame;
	}
	
bool	AnimationBar::FrameBlock::isData(void *data)const
	{
	return frame==data;
	}
	
void	AnimationBar::FrameBlock::activate(bool b)
	{
	acting=b;
	reShow();
	}

void	AnimationBar::FrameBlock::lclick(Coord c)
	{
	if(keySignal.press(KEY_CTRL))
		{
		setMenuSelected(!isMenuSelected());
		return;
		}
	if(keySignal.press(KEY_SHIFT))
		{
		getMenuOwner()->clearSelect();
		setMenuSelected(true);
		getOwner()->response(ResponseIndex_shiftSelect);
		return;
		}

	getMenuOwner()->clearSelect();
	setMenuSelected(true);
	setFrame(index);
	}

void	AnimationBar::FrameBlock::rclick(Coord c)
	{
	Popup *p=new Popup(getPosition()+c,this);
	p->addContext("Add",ResponseIndex_add);
	if(clipboard->avalible(Clipboard_Frame))
		{
		p->addContext("Paste",ResponseIndex_paste);
		p->addContext("Paste Link",ResponseIndex_pasteLink);
		p->addContext("Move",65);
		p->addContext("Set Next",ResponseIndex_setNextFrame);
		p->addContext("Clear Next",7);
		}
	if(isMenuSelected()&&getMenuOwner()->isMultiSelected())
		{
		p->addContext("Reverse",ResponseIndex_reverse);
		p->addContext("Copy",ResponseIndex_copyMulti);
		p->addContext("Clone",ResponseIndex_cloneMulti);
		p->addContext("Delete",ResponseIndex_deleteMulti);
		}
	else
		{
		p->addContext("Copy",ResponseIndex_copy);
		p->addContext("Clone",ResponseIndex_clone);
		p->addContext("Delete",ResponseIndex_delete);
		}
	p->endFolder();
	addWindow(p);
	rightClick=c.x>getOriginSize().x/2;
	}

void	AnimationBar::FrameBlock::input()
	{
	if(keySignal.down(KEY_CTRL|KEY_C))
		response(ResponseIndex_copyMulti);
	if(keySignal.down(KEY_CTRL|KEY_V))
		response(ResponseIndex_paste);
	if(keySignal.down(KEY_DELETE))
		response(ResponseIndex_deleteMulti);
	}

void	AnimationBar::FrameBlock::response(int r)
	{
	switch(r)
		{
	case 7:
		{
		Frame **f=dalloc<Frame*>(frame->getNextFrameNumber());
		int *p=dalloc<int>(frame->getNextFrameNumber());
		for(int i=0;i<frame->getNextFrameNumber();i++)
			{
			f[i]=frame->getNextPic(i)->getFrame(frame->getNextIndex(i));
			p[i]=i;
			}
		stepContainer->addStep(new AddNextFrameStep(frame,f,p,frame->getNextFrameNumber(),true));
		}
		break;

	case ResponseIndex_add:
		stepContainer->addStep(new AddFrameStep(new Frame(new DSBitmap(imgData->getSize()),100),index+rightClick));
		break;
	case ResponseIndex_clone:
		stepContainer->addStep(new AddFrameStep(frame->clone(),index+rightClick));
		break;
	case ResponseIndex_delete:
		stepContainer->addStep(new AddFrameStep(frame,index,true));
		break;
	case ResponseIndex_copy:
		clipboard->clear(Clipboard_Frame);
		clipboard->addCopy(frame);
		refreshWindow();
		break;
	
	case ResponseIndex_paste:
	case ResponseIndex_pasteLink:
	case ResponseIndex_setNextFrame:
		{
		if(!clipboard->avalible(Clipboard_Frame))
			break;
		Frame **addFrame=dalloc<Frame*>(clipboard->getNumber());
		int *pos=dalloc<int>(clipboard->getNumber());
		for(int i=0;i<clipboard->getNumber();i++)
			{
			Frame *f=(Frame*)clipboard->getData(i);
			switch(r)
				{
			case ResponseIndex_paste:
				addFrame[i]=f->clone();
				pos[i]=index+rightClick+i;
				break;
			case ResponseIndex_pasteLink:
				addFrame[i]=new Frame(new LinkLayer(f->getLayer()),f->getDelay());
				pos[i]=index+rightClick+i;
				break;
			case ResponseIndex_setNextFrame:
				addFrame[i]=f;
				pos[i]=frame->getNextFrameNumber()+i;
				break;
				}
			}
		switch(r)
			{
		case ResponseIndex_paste:
		case ResponseIndex_pasteLink:
			stepContainer->addStep(new AddFrameStep(addFrame,pos,clipboard->getNumber()));
			break;
		case ResponseIndex_setNextFrame:
			stepContainer->addStep(new AddNextFrameStep(frame,addFrame,pos,clipboard->getNumber()));
			getOwner()->reShow();
			break;
			}
		}
		break;
		
	case ResponseIndex_copyMulti:
	case ResponseIndex_cloneMulti:
	case ResponseIndex_deleteMulti:
	case ResponseIndex_reverse:
		getOwner()->response(r);
		break;
		}
	}




void	AnimationBar::FrameBlock::show()
	{
	if(clipboard->isCopyed(frame))
		{
		wBuffer->clear(yellow);
		}
	if(acting)
		{
		wBuffer->clear(red);
		}
	if(isMenuSelected())
		{
		wBuffer->clear(green);
		}

	const DSBitmap *bmp=frame->getLayer()->getBitmap();
	int bx=bmp->getSize().x;
	int by=bmp->getSize().y;
	int sx=min(bx,100);
	
	int wx=wBuffer->getSize().x;
	int wy=wBuffer->getSize().y;
	for(int y=2;y<wy-2;y++)
		{
		int r=0,g=0,b=0,a=0;
		for(int x=0;x<sx;x++)
			{
			DS_RGB color=bmp->get(x*bx/sx,y*by/wy);
			r+=color.r*color.a;
			g+=color.g*color.a;
			b+=color.b*color.a;
			a+=color.a;
			}
		if(a==0)
			continue;
		for(int x=2;x<wx-2;x++)
			wBuffer->putPixel(Position(x,y),DS_RGB(r/a,g/a,b/a));
		}
	}





