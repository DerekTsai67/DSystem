


class Tool;


void	initTool();
void	destroyTool();
void	setTool(int i,Tool *t);
extern Tool		***actingTool,**tool;

enum{
	Tool_mover=1,
	Tool_pencil,
	Tool_eraser,
	Tool_brush,
	Tool_brushEraser,
	Tool_bucket,
	Tool_colorPicker,
	Tool_line,
	Tool_enclosure,
	Tool_deviate,
	Tool_count
	};


class Tool{
	public:
	Tool(Picture *p,int st,Sensor *l,Sensor *r=NULL,Sensor *m=NULL);
	~Tool();
Picture*	getPic();
Sensor*		getSensor(int index);

enum SenseType{
	nonEditing,
	bitmapDrawing,
	senseTypeCount
	};
	protected:
Picture	*pic;
int 	senseType;
Sensor	*sensor[3];
	};

class Toolbar:public Window{
	public:
	Toolbar();
	~Toolbar();
void	input();
void	refresh();
void	response(int ri);
void	show();
	private:
LinkedList<Button>	toolButton;
	};
