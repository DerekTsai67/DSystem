



	StepManager::StepManager():Window(Position(8,230,Position::EffectiveRightUp),DSData->getPic(BDF_Window),Size(16+192+16,16+120+16))
	{
	SENSE->left=new MoveSensor();
	SENSE->right=new SuicideSensor();
	stepBlock=new LinkedList<StepBlock>();
	addEtcObject(menu=new Menu(Position(16,16),Size(20,20),(LinkedList<MenuObject>*)stepBlock));
	menu->setDirection(Menu::vertical);
	refresh();
	}

void	StepManager::refresh()
	{
	menu->refreshStart();
	stepCount=stepContainer->getNumber();
	for(int i=max(0,stepCount-5);i<stepCount;i++)
		{
		StepBlock *now=(StepBlock*)menu->refreshNext(stepContainer->getStep(i));
		if(now==NULL)
			menu->refreshAdd(now=new StepBlock(stepContainer->getStep(i)));
		now->reShow();
		}
	menu->refreshEnd();
	menu->fitSize();
	
	}



	StepManager::StepBlock::StepBlock(Step<Datafile> *s):MenuObject(DSData->getPic(BDF_DataButton),Size(192,24))
	{
	argCheck(s==NULL);
	step=s;
	}
	
bool	StepManager::StepBlock::isData(void *s)const
	{
	return step==s;
	}

void	StepManager::StepBlock::show()
	{
	if(step->getEnable()==Step<Datafile>::disabled)
		wBuffer->clear(gray);
	font->putString(wBuffer,typeid(*step).name(),Position(0,0));
	}













