
static const Coord		borderInterval(16,16);

static const int		hexScrollOutRadius=150;
static const int		hexScrollInRadius=120;
static const int		triScrollRadius=hexScrollInRadius;
static const Position	hexScrollOrigin=Position(150,150)+borderInterval;

static const Position	infoPosition=Position(300+32,0)+borderInterval;
static const Position	rgbScrollPos=Position(300+32-3,150)+borderInterval;
static const int		rgbScrollLength=255+48;

static const Position	hueScrollPos=Position(0,300+16)+borderInterval;
static const int		hueScrollLength=600+32;
static const int		hwcScrollLength=200;
static const int		hwcScrollInterval=hwcScrollLength+16;


static void hexScrollConverter(Coord c,int xm,int ym,int zm,int *x,int *y,int *z)
	{
	if(abs(c.x)<xm*1732/2000&&abs(c.y)<xm-abs(c.x)*1732/1000)
		{
		int angle=(270+360-toAngle(c))%360;
		*x=angle%60;
		*y=angle/60;
		return;
		}
	
	Coord n[6]={Coord(-1000,-1732),Coord(-2000,0),Coord(-1000,1732),Coord(1000,1732),Coord(2000,0),Coord(1000,-1732)};
	bool middle=false;
	for(int i=0;i<6;i++)
		{
		if(c.dot(n[i])<0)
			continue;
		int t=c.dot(n[i].right())+xm*1000;
		if(t<0)
			{
			if(middle)
				{
				*x=0;
				*y=i;
				return;
				}
			continue;
			}
		if(t<xm*2000)
			{
			*x=t*60/xm/2000;
			*y=i;
			return;
			}
		else
			middle=true;
		}
	*x=*y=0;
	}

static void triScrollConverter(Coord c,int xm,int ym,int zm,int *x,int *y,int *z)
	{
	if(xm==0)
		return;
	*y=(-c.y*2+xm)*ym/xm/3;
	*x=(c.x*1732+c.y*1000+xm*1000)*ym/xm/3000;
	*z=(-c.x*1732+c.y*1000+xm*1000)*ym/xm/3000;
	
	if(*x<=0&&*y<=0)
		{
		*x=*y=0;
		*z=ym;
		}
	if(*y<=0&&*z<=0)
		{
		*y=*z=0;
		*x=ym;
		}
	if(*z<=0&&*x<=0)
		{
		*z=*x=0;
		*y=ym;
		}
		
	if(*x<0)
		{
		*x=0;
		*y+=*x/2;
		*z+=*x/2;
		}
	if(*y<0)
		{
		*x+=*y/2;
		*y=0;
		*z+=*y/2;
		}
	if(*z<0)
		{
		*x+=*z/2;
		*y+=*z/2;
		*z=0;
		}
	}

	ColorPanel::ColorPanel(int index):Window(Position(70,70),DSData->getPic(BDF_Window),Size(hueScrollLength+borderInterval.x*2,hueScrollPos.y+32*3+borderInterval.y-16))
	{
	SENSE->left=new MoveSensor();
	SENSE->right=new SuicideSensor();
	triScrollSense=new RegularSense(triScrollSensor=new ScrollSensor(triScrollConverter,hexScrollOrigin,false,triScrollRadius,255,0,0,0,0,0,6,0));
	hexScrollSense=new RegularSense(hexScrollSensor=new ScrollSensor(hexScrollConverter,hexScrollOrigin,false,hexScrollInRadius,0,0,0,0,0,0,8,0));
	
	actingIndex=index;
	color=actingColor[actingIndex];
	addEtcObject(colorTicket=new ColorTicket(Position(borderInterval,Position::RealRightUp,Position::EffectiveRightUp),Size(50,50),color));
	response(10);
	addEtcObject(new Scroll(rgbScrollPos+Coord(  0, 0),Size(rgbScrollLength,16),DSData->getPic(BDF_Scroll),r,0,255,2));
	addEtcObject(new Scroll(rgbScrollPos+Coord(  0,32),Size(rgbScrollLength,16),DSData->getPic(BDF_Scroll),g,0,255,2));
	addEtcObject(new Scroll(rgbScrollPos+Coord(  0,64),Size(rgbScrollLength,16),DSData->getPic(BDF_Scroll),b,0,255,2));
	addEtcObject(new Scroll(rgbScrollPos+Coord(  0,96),Size(rgbScrollLength,16),DSData->getPic(BDF_Scroll),a,0,255,2));
	addEtcObject(new Scroll(hueScrollPos+Coord(  0, 0),Size(hueScrollLength,16),DSData->getPic(BDF_Scroll),hue,0,359,7));
	addEtcObject(new Scroll(hueScrollPos+Coord(  0,32),Size(hwcScrollLength,16),DSData->getPic(BDF_Scroll),HSLS,0,255,3));
	addEtcObject(new Scroll(hueScrollPos+Coord(  0,64),Size(hwcScrollLength,16),DSData->getPic(BDF_Scroll),HSLL,0,510,3));
	addEtcObject(new Scroll(hueScrollPos+Coord(hwcScrollInterval  ,32),Size(hwcScrollLength,16),DSData->getPic(BDF_Scroll),HSVS,0,255,4));
	addEtcObject(new Scroll(hueScrollPos+Coord(hwcScrollInterval  ,64),Size(hwcScrollLength,16),DSData->getPic(BDF_Scroll),HSVV,0,255,4));
	addEtcObject(new Scroll(hueScrollPos+Coord(hwcScrollInterval*2,32),Size(hwcScrollLength,16),DSData->getPic(BDF_Scroll),HWCW,0,255,5));
	addEtcObject(new Scroll(hueScrollPos+Coord(hwcScrollInterval*2,64),Size(hwcScrollLength,16),DSData->getPic(BDF_Scroll),HWCC,0,255,5));
	}

	ColorPanel::~ColorPanel()
	{
	delete triScrollSense;
	delete hexScrollSense;
	}

Sense*	ColorPanel::sense(Coord c)
	{
	Coord hc=c-hexScrollOrigin;
	if(hc.y<=triScrollRadius/2&&hc.y>=-triScrollRadius+abs(hc.x)*1732/1000)
		return triScrollSense;
	
	hc=abs(hc);
	if(hc.y<hexScrollOutRadius-hc.x*1000/1732 && hc.x<hexScrollOutRadius*1732/2000)
	if(hc.y>hexScrollInRadius -hc.x*1000/1732 || hc.x>hexScrollInRadius*1732/2000)
		return hexScrollSense;
	return defaultSense;
	}

void	ColorPanel::response(int ri)
	{
	switch(ri)
		{
	case 2://RGBA
		color=DS_RGB(r,g,b,a);
		break;
	case 3://HSL
		color=makeHSL(hue,HSLS,HSLL);
		color.a=a;
		break;
	case 4://HSV
		color=makeHSV(hue,HSVS,HSVV);
		color.a=a;
		break;
	case 5://HWC
		color=makeHWC(hue,HWCW,HWCC);
		color.a=a;
		break;
	case 6://triScroll
		color=makeHWC(hue,triScrollSensor->getX(),triScrollSensor->getY());
		color.a=a;
		break;
	case 7://Hue
		color=makeHWC(hue,HWCW,HWCC);
		hexScrollSensor->setX(hue%60);
		hexScrollSensor->setY(hue/60);
		color.a=a;
		break;
	case 8://hexScroll
		hue=hexScrollSensor->getX()+hexScrollSensor->getY()*60;
		color=makeHWC(hexScrollSensor->getX()+hexScrollSensor->getY()*60,HWCW,HWCC);
		color.a=a;
		break;
	case 9://other
		if(actingColor[actingIndex].same(color))
			return;
		if(colorSkip)
			actingIndex=0;
		color=actingColor[actingIndex];
		break;
		}
	
	if(ri>=2&&ri<=10)
		{
		r=color.r;
		g=color.g;
		b=color.b;
		a=color.a;
		if(ri!=7&&ri!=8)
			{
			if(ri!=3)
				{
				HSLS=color.getHSLS();
				HSLL=color.getHSLL();
				}
			if(ri!=4)
				{
				HSVS=color.getHSVS();
				HSVV=color.getHSVV();
				}
			//if(ri!=5)
				{
				HWCW=color.getHWCW();
				HWCC=color.getHWCC();
				HWCB=color.getHWCB();
				}
			if(ri!=6)
				{
				//tri
				}
			}
		if(!(ri>=3&&ri<=8))
			{
			if(ri!=7)
				hue=color.getHue();
			if(ri!=8)
				{
				hexScrollSensor->setX(hue%60);
				hexScrollSensor->setY(hue/60);
				//hex
				}
			}
		colorTicket->setColor(color);
		//actingColor[actingIndex]=color;
		setColor(color,actingIndex);
		reShow();
		}
	}

void	ColorPanel::show()
	{
	char rgbString[100];
	sprintf(rgbString,"RGBA:%3d,%3d,%3d,%3d",color.r,color.g,color.b,color.a);
	font->putString(wBuffer,rgbString,infoPosition+Coord(0,20)*0);

	sprintf(rgbString,"HSL :%3d,%3d,%3d",color.getHue(),color.getHSLS(),color.getHSLL());
	font->putString(wBuffer,rgbString,infoPosition+Coord(0,20)*1);

	sprintf(rgbString,"HSV :%3d,%3d,%3d",color.getHue(),color.getHSVS(),color.getHSVV());
	font->putString(wBuffer,rgbString,infoPosition+Coord(0,20)*2);

	sprintf(rgbString,"WCB :%3d,%3d,%3d",HWCW,HWCC,HWCB);
	font->putString(wBuffer,rgbString,infoPosition+Coord(0,20)*3);
	
	for(int x=-hexScrollOutRadius*1732/2000;x<=hexScrollOutRadius*1732/2000;x++)
	for(int y=abs(x)<hexScrollInRadius*1732/2000?hexScrollInRadius-abs(x)*1000/1732:0;y<hexScrollOutRadius-abs(x)*1000/1732;y++)
	for(int z=-1;z<=1;z+=2)
		{
		int hx,hy;
		hexScrollConverter(Coord(x,y*z),hexScrollInRadius,0,0,&hx,&hy,NULL);
		int angle=hx+hy*60;
		wBuffer->putPixel(hexScrollOrigin+Coord(x,y*z),makeHWC(angle,0,255/**60/(60+(hy%2==1?59-hx:hx))*/));
		if(hx==hexScrollSensor->getX()&&hy==hexScrollSensor->getY())
			wBuffer->putPixel(hexScrollOrigin+Coord(x,y*z),black);
		}

	for(int x=-triScrollRadius*1732/2000;x<=triScrollRadius*1732/2000;x++)
	for(int y=triScrollRadius/2;y>=-triScrollRadius+abs(x)*1732/1000;y--)
		{
		int w,c,b;
		triScrollConverter(Coord(x,y),triScrollRadius,255,0,&w,&c,&b);
		wBuffer->putPixel(Position(x,y)+hexScrollOrigin,makeHWC(hue,w,c));
		}
	wBuffer->drawCircle(hexScrollOrigin+Position(triScrollRadius*1732*(HWCW-HWCB)/2000/255,triScrollRadius*((HWCW+HWCB)/2-HWCC)/255,Position::Center,Position::EffectiveLeftUp),5,2,blue);
	}

























































