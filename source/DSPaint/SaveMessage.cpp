


static const Position optionPos(42,140);
static const Size optionSize(80,28);
static const Coord optionDis(90,40);

	SaveMessage::SaveMessage():Window(Position(100,100),DSData->getPic(BDF_Window),Size(480,480)),page(4)
	{
	SENSE->left=new MoveSensor();
	SENSE->right=new SuicideSensor();
	
	path=dalloc<char>(305);
	strcpy(path,"E:\\temp.png");
	addEtcObject(new TextBar(Position(4,4),400,path,300));
	addEtcObject(new Button(Position(4,40),buttonData,Size(20,20),NULL,1));
	
	pictureOption=new PictureOption;
	
	iconMax=30;
	icon=dalloc<Picture*>(iconMax);
	icon[ 0]=font->makeString("BMP");
	icon[ 1]=font->makeString("PNG");
	icon[ 2]=font->makeString("GIF");
	icon[ 3]=font->makeString("JPG");
	icon[ 4]=font->makeString("None");
	icon[ 5]=font->makeString("Auto");
	icon[ 6]=font->makeString("Palette");
	icon[ 7]=font->makeString("RGB");
	icon[ 8]=font->makeString("Auto");
	icon[ 9]=font->makeString("Grayscale");
	icon[10]=font->makeString("Alpha");
	icon[11]=font->makeString("Auto");
	icon[12]=font->makeString("None");
	icon[13]=font->makeString("Stable");
	icon[14]=font->makeString("None");
	icon[15]=font->makeString("bit8");
	icon[16]=font->makeString("Auto");
	icon[17]=font->makeString("4");
	icon[18]=font->makeString("2");
	icon[19]=font->makeString("1");
	icon[20]=font->makeString("Interlace");
	icon[21]=font->makeString("None");
	icon[22]=font->makeString("None");
	icon[23]=font->makeString("Thumb250");
	icon[24]=font->makeString("Thumb125");
	
	addEtcObject(new Button(optionPos+optionDis*Coord(0,-2),buttonData,optionSize,icon[0],10));
	addEtcObject(new Button(optionPos+optionDis*Coord(1,-2),buttonData,optionSize,icon[1],11));
	addEtcObject(new Button(optionPos+optionDis*Coord(2,-2),buttonData,optionSize,icon[2],12));
	addEtcObject(new Button(optionPos+optionDis*Coord(3,-2),buttonData,optionSize,icon[3],13));
	
	addObject(page);
	page[1].addItem(paletteFalseButton=		new Button(optionPos+optionDis*Coord(0,0),buttonData,optionSize,icon[ 4],101));
	page[1].addItem(paletteAutoButton=		new Button(optionPos+optionDis*Coord(1,0),buttonData,optionSize,icon[ 5],102));
	page[1].addItem(paletteTrueButton=		new Button(optionPos+optionDis*Coord(2,0),buttonData,optionSize,icon[ 6],103));
	page[1].addItem(grayFalseButton=		new Button(optionPos+optionDis*Coord(0,1),buttonData,optionSize,icon[ 7],201));
	page[1].addItem(grayAutoButton=			new Button(optionPos+optionDis*Coord(1,1),buttonData,optionSize,icon[ 8],202));
	page[1].addItem(grayTrueButton=			new Button(optionPos+optionDis*Coord(2,1),buttonData,optionSize,icon[ 9],203));
	page[1].addItem(alphaTrueButton=		new Button(optionPos+optionDis*Coord(0,2),buttonData,optionSize,icon[10],301));
	page[1].addItem(alphaAutoButton=		new Button(optionPos+optionDis*Coord(1,2),buttonData,optionSize,icon[11],302));
	page[1].addItem(alphaFalseButton=		new Button(optionPos+optionDis*Coord(2,2),buttonData,optionSize,icon[12],303));
	page[1].addItem(stableTrueButton=		new Button(optionPos+optionDis*Coord(0,3),buttonData,optionSize,icon[13],401));
	page[1].addItem(stableFalseButton=		new Button(optionPos+optionDis*Coord(1,3),buttonData,optionSize,icon[14],402));
	page[1].addItem(bit8Button=				new Button(optionPos+optionDis*Coord(0,4),buttonData,optionSize,icon[15],501));
	page[1].addItem(bitAutoButton=			new Button(optionPos+optionDis*Coord(1,4),buttonData,optionSize,icon[16],502));
	page[1].addItem(bit4Button=				new Button(optionPos+optionDis*Coord(2,4),buttonData,optionSize,icon[17],503));
	page[1].addItem(bit2Button=				new Button(optionPos+optionDis*Coord(3,4),buttonData,optionSize,icon[18],504));
	page[1].addItem(bit1Button=				new Button(optionPos+optionDis*Coord(4,4),buttonData,optionSize,icon[19],505));
	page[1].addItem(interlaceTrueButton=	new Button(optionPos+optionDis*Coord(0,5),buttonData,optionSize,icon[20],601));
	page[1].addItem(interlaceFalseButton=	new Button(optionPos+optionDis*Coord(1,5),buttonData,optionSize,icon[21],602));
	page[1].addItem(thumbNullButton=		new Button(optionPos+optionDis*Coord(0,6),buttonData,optionSize,icon[22],701));
	page[1].addItem(thumb250Button=			new Button(optionPos+optionDis*Coord(1,6),buttonData,optionSize,icon[23],702));
	page[1].addItem(thumb125Button=			new Button(optionPos+optionDis*Coord(2,6),buttonData,optionSize,icon[24],703));
	response(100);
	}

	SaveMessage::~SaveMessage()
	{
	dfree(path);
	for(int i=0;i<iconMax;i++)
		if(icon[i]!=NULL)
			delete icon[i];
	dfree(icon);
	delete pictureOption;
	}

void	SaveMessage::response(int ri)
	{
	switch(ri)
		{
	case 1:
		if(isSuffix(path,".dsd"))
			fileData->save(path);
		else if(isSuffix(path,".png"))
			((Picture*)typeData)->savePNG(new OutputFile(path),pictureOption->clone());
		else
			if(typeData->savePath(path,true))
				head->addDisplayer(new Displayer(DSData->getPic(BDF_Click),Position(cursorPos,Position::Center,Position::EffectiveLeftUp)));
			else
				report("Save failed.");
		break;
	case 10:
	case 11:
	case 12:
	case 13:
		page.setPage(ri-10);
		break;
	default:
		if(ri>=100&&ri<1000)
			{
			switch(ri)
				{
			case 101:
				pictureOption->usePalette=false;
				break;
			case 102:
				pictureOption->usePalette=AutoLoseless;
				break;
			case 103:
				pictureOption->usePalette=true;
				break;
			case 201:
				pictureOption->grayScale=false;
				break;
			case 202:
				pictureOption->grayScale=AutoLoseless;
				break;
			case 203:
				pictureOption->grayScale=true;
				break;
			case 301:
				pictureOption->alphaChannel=true;
				break;
			case 302:
				pictureOption->alphaChannel=AutoLoseless;
				break;
			case 303:
				pictureOption->alphaChannel=false;
				break;
			case 401:
				pictureOption->stableTransparent=true;
				break;
			case 402:
				pictureOption->stableTransparent=false;
				break;
			case 501:
				pictureOption->bitDepth=8;
				break;
			case 502:
				pictureOption->bitDepth=AutoLoseless;
				break;
			case 503:
				pictureOption->bitDepth=4;
				break;
			case 504:
				pictureOption->bitDepth=2;
				break;
			case 505:
				pictureOption->bitDepth=1;
				break;
			case 601:
				pictureOption->interlace=true;
				break;
			case 602:
				pictureOption->interlace=false;
				break;
			case 701:
				pictureOption->thumbnailSize=zeroCoord;
				break;
			case 702:
				pictureOption->thumbnailSize=Coord(250,250);
				break;
			case 703:
				pictureOption->thumbnailSize=Coord(125,125);
				break;
				}
			
			#define setDF(x,y) x->setDatafile(y?choosedButtonData:buttonData)
			setDF(paletteFalseButton,pictureOption->usePalette==false);
			setDF(paletteAutoButton,pictureOption->usePalette==AutoLoseless);
			setDF(paletteTrueButton,pictureOption->usePalette==true);
			setDF(grayFalseButton,pictureOption->grayScale==false);
			setDF(grayAutoButton,pictureOption->grayScale==AutoLoseless);
			setDF(grayTrueButton,pictureOption->grayScale==true);
			setDF(alphaTrueButton,pictureOption->alphaChannel==true);
			setDF(alphaAutoButton,pictureOption->alphaChannel==AutoLoseless);
			setDF(alphaFalseButton,pictureOption->alphaChannel==false);
			setDF(stableTrueButton,pictureOption->stableTransparent==true);
			setDF(stableFalseButton,pictureOption->stableTransparent==false);
			setDF(bit8Button,pictureOption->bitDepth==8);
			setDF(bitAutoButton,pictureOption->bitDepth==AutoLoseless);
			setDF(bit4Button,pictureOption->bitDepth==4);
			setDF(bit2Button,pictureOption->bitDepth==2);
			setDF(bit1Button,pictureOption->bitDepth==1);
			setDF(interlaceTrueButton,pictureOption->interlace==true);
			setDF(interlaceFalseButton,pictureOption->interlace==false);
			setDF(thumbNullButton,pictureOption->thumbnailSize==zeroCoord);
			setDF(thumb250Button,pictureOption->thumbnailSize==Coord(250,250));
			setDF(thumb125Button,pictureOption->thumbnailSize==Coord(125,125));
			break;
			}
		error("switchOut.");
		}
	}







