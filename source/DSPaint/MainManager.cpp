




	MainManager::MainManager():Window(zeroEffectivePosition,DSData->getPic(BDF_Window),Size(window_size.x,48)),toolbar(1),navigator(1)
	{
	addType(OBJECT_alwaysDown);
	addObject(button);
	button.addItem(new Button(Position( 52,4),buttonData,Size(16,16),iconData->getPic(0),ResponseIndex_toolBar));
	button.addItem(new Button(Position( 76,4),buttonData,Size(16,16),iconData->getPic(1),ResponseIndex_navigator));
	button.addItem(new Button(Position(100,4),buttonData,Size(16,16),iconData->getPic(2),ResponseIndex_colorBox));
	button.addItem(new Button(Position(124,4),buttonData,Size(16,16),iconData->getPic(3),ResponseIndex_datafileManager));
	button.addItem(new Button(Position(148,4),buttonData,Size(16,16),iconData->getPic(4),ResponseIndex_animationBar));
	button.addItem(new Button(Position(172,4),buttonData,Size(16,16),iconData->getPic(5),ResponseIndex_saveMessage));
	button.addItem(new Button(Position(196,4),buttonData,Size(16,16),iconData->getPic(6),ResponseIndex_layerManager));
	button.addItem(new Button(Position(220,4),buttonData,Size(16,16),iconData->getPic(7),ResponseIndex_loadMessage));
	button.addItem(new Button(Position(244,4),buttonData,Size(16,16),iconData->getPic(8),ResponseIndex_stepManager));
	response(ResponseIndex_toolBar);
	response(ResponseIndex_navigator);
	if(fileData->getNumber()>1)
		response(ResponseIndex_datafileManager);
	response(ResponseIndex_stepManager);
	lastPos=zeroCoord;
	}

	MainManager::~MainManager()
	{
	//delete ObjectLinker
	}

void	MainManager::act()
	{
	if(imgCoord(cursorPos)!=lastPos)
		{
		lastPos=imgCoord(cursorPos);
		reShow();
		}
	}

void	MainManager::lclick(Coord c)
	{
	if(c>=Coord(4,4)&&c<Coord(44,20))
		response(ResponseIndex_toolBar);
	if(c>=Coord(4,28)&&c<Coord(44,44))
		if(colorPanel==NULL)
			addWindow(colorPanel=new ColorPanel(c.x>=28&&!colorSkip));
	}

void	MainManager::rclick(Coord c)
	{
	if(c>=Coord(28,28)&&c<Coord(44,44))
		{
		colorSkip=!colorSkip;
		if(colorSkip)
			setColor(actingColor[0],0);
		return;
		}
	
	switch(dataType)
		{
	case Data::Type_Picture:
		{
		Popup *p=new Popup(getPosition()+c,this);
		p->addFolder("Gray scalize");
			p->addContext("by RGB",BitmapProcess_grayScalizeRGB);
			p->addContext("by HSL",BitmapProcess_grayScalizeHSL);
			p->addContext("by HSV",BitmapProcess_grayScalizeHSV);
			p->addContext("by YCC",BitmapProcess_grayScalizeYCC);
			p->endFolder();
		p->addFolder("Binarize");
			p->addContext("Simple",BitmapProcess_binarizeSimple);
			p->addContext("Integral",BitmapProcess_binarizeIntegral);
			p->endFolder();
		p->addFolder("Opacify");
			p->addContext("Ignore alpha",BitmapProcess_opacifyIgnore);
			p->addContext("Delete mask",BitmapProcess_deMask);
			p->addContext("Show mask",BitmapProcess_showMask);
			p->endFolder();
		p->addFolder("Reverse color");
			p->addContext("by RGB",BitmapProcess_reverseRGB);
			p->addContext("by Hue",BitmapProcess_reverseHue);
			p->endFolder();
		p->addFolder("Clear");
			p->addContext("to Black",BitmapProcess_clearBlack);
			p->addContext("to White",BitmapProcess_clearWhite);
			p->addContext("to Transparent",BitmapProcess_clearTransparent);
			p->addContext("to Noise",BitmapProcess_clearNoise);
			p->endFolder();
		p->addFolder("Reverse");
			p->addContext("Horizontal",BitmapProcess_reverseHorizontal);
			p->addContext("Vertical",BitmapProcess_reverseVertical);
			p->endFolder();
		p->addFolder("Change type");
			p->addContext("to Default",PictureProcess_setTypeDefault);
			p->addContext("to Square",PictureProcess_setTypeSquare);
			p->endFolder();
		p->endFolder();
		addWindow(p);
		}
		break;

		}

	}

void	MainManager::response(int r)
	{
	switch(r)
		{
	case 1://Window closed
		reShow();
		break;
	case ResponseIndex_toolBar:
		if(toolbar==NULL)
			addWindow(toolbar=new Toolbar());
		break;
	case ResponseIndex_navigator:
		if(navigator==NULL)
			addWindow(navigator=new Navigator());
		break;
	case ResponseIndex_colorBox:
		if(colorBox==NULL)
			addWindow(colorBox=new ColorBox());
		break;
	case ResponseIndex_datafileManager:
		if(datafileManager==NULL)
			addWindow(datafileManager=new DatafileManager());
		break;
	case ResponseIndex_animationBar:
		if(animationBar==NULL)
			addWindow(animationBar=new AnimationBar());
		break;
	case ResponseIndex_saveMessage:
		if(saveMessage==NULL)
			addWindow(saveMessage=new SaveMessage());
		break;
	case ResponseIndex_layerManager:
		if(layerManager==NULL)
			addWindow(layerManager=new LayerManager());
		break;
	case ResponseIndex_loadMessage:
		if(loadMessage==NULL)
			{
			addWindow(loadMessage=new LoadMessage());
			}
		break;
	case ResponseIndex_stepManager:
		if(stepManager==NULL)
			addWindow(stepManager=new StepManager());
		break;
		}

	addProcessStep(r);
	}

void	MainManager::show()
	{
	wBuffer->draw(actingTool[dataType][0]->getPic(),Position( 4, 4));
	wBuffer->draw(actingTool[dataType][1]->getPic(),Position(28, 4));
	
	if(dataType==Data::Type_Picture)
		{
		wBuffer->blitSquare(DSData->getPic(BDF_hollowBG),Position( 4,28,Position::RealLeftUp,Position::EffectiveLeftUp),Size(16,16,Size::Real));
		wBuffer->drawFilledRect(Position( 4,28,Position::RealLeftUp,Position::EffectiveLeftUp).change(DSData->getPic(BDF_hollowBG),wBuffer),Size(16,16,Size::Real).change(DSData->getPic(BDF_hollowBG)),actingColor[0]);
		wBuffer->blitSquare(DSData->getPic(BDF_hollowBG),Position(28,28,Position::RealLeftUp,Position::EffectiveLeftUp),Size(16,16,Size::Real));
		wBuffer->drawFilledRect(Position(28,28,Position::RealLeftUp,Position::EffectiveLeftUp).change(DSData->getPic(BDF_hollowBG),wBuffer),Size(16,16,Size::Real).change(DSData->getPic(BDF_hollowBG)),actingColor[1]);
		if(colorSkip)
			wBuffer->draw(DSPaintData->getPic("ColorSkip"),Position(28,28));
		}

	char str[20];
	sprintf(str,"%2d/%2d",dataIndex,fileData->getNumber());
	font->putString(wBuffer,str,Position(window_size.x-160,0));
	if(dataType==Data::Type_Picture)
		{
		sprintf(str,"%3d/%3d",dataIndex,fileData->getNumber());
		font->putString(wBuffer,str,Position(window_size.x-100,0));
		}
	sprintf(str,"(%4d,%4d)",imgCoord(cursorPos).x,imgCoord(cursorPos).y);
	font->putString(wBuffer,str,Position(window_size.x-160,24));
	}










