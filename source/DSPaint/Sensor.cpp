




	DrawSensor::DrawSensor(int i)
	{
	index=i;
	color=transparent;
	drawBuffer=NULL;
	}
	
void	DrawSensor::prepareBuffer()
	{
	if(drawBuffer!=NULL)
		error("drawBuffer!=NULL.");
	drawBuffer=editingLayer->getEditableBitmap()->clone();
	}
	
	DrawSensor::~DrawSensor()
	{
	if(drawBuffer!=NULL)
		error("drawBuffer!=NULL.");
	}

void	DrawSensor::down(Coord p)
	{
	prepareBuffer();
	for(int i=0;i<cursorTrackQueueMax;i++)
		cursorTrackIndex[i]=0;
	cursorTrackQueueIndex=0;
	if(index!=-1)
		color=actingColor[index];
	editingLayer->getEditableBitmap()->put(imgCoord(p),color);
	drawedCursor=p;
	}
void	DrawSensor::press(Coord p)
	{
	int qi=cursorTrackQueueIndex;
	cursorTrackQueueIndex=(cursorTrackQueueIndex+1)%cursorTrackQueueMax;
	for(int i=0;i<cursorTrackIndex[qi];i++)
		{
		editingLayer->getEditableBitmap()->drawLineC(imgCoord(drawedCursor),imgCoord(cursorTrack[qi][i]),color);
		drawedCursor=cursorTrack[qi][i];
		}
	cursorTrackIndex[qi]=0;
	imgReshow();
	}
void	DrawSensor::up(Coord p)
	{
	cursorTrackQueueIndex=-1;
	stepContainer->addStep(new DrawStep(drawBuffer));
	drawBuffer=NULL;
	}




/*
void	brushProcess(void *arg)
	{
	BrushSensor *owner=(BrushSensor*)arg;
	extern Coord rawCursorPos;
	Coord cursorPos=rawCursorPos;
	
	int rad=owner->radius;
	Coord lt=imgCoord(cursorPos)-Coord(rad,rad)/imgScale-monoCoord;
	Coord rb=imgCoord(cursorPos)+Coord(rad,rad)/imgScale+monoCoord;
	lt|=zeroCoord;
	rb&=imgSize-monoCoord;
	DSBitmap *bmp=editingLayer->getEditableBitmap();
	Coord c;
	for(c.y=lt.y;c.y<=rb.y;c.y++)
	for(c.x=lt.x;c.x<=rb.x;c.x++)
		{
		int f=rad-(rawCoord(c)-cursorPos).abs();
		f=f*300/rad;
		takeMax(f,0);
		takeMin(f,255);
		if(owner->fillBuffer->get(c)<f)
			{
			owner->fillBuffer->put(c,f);
			DS_RGB color=owner->color;
			color.a=color.a*f/255;
			bmp->put(c,owner->drawBuffer->get(c));
			bmp->drawPixel(c,color);
			}
		}
	drawedCursor=cursorPos;
	}*/

extern int brushRadius;
extern int brushDeep;
	BrushSensor::BrushSensor(int i)
	{
	index=i;
	color=transparent;
	drawBuffer=NULL;
	}
	
void	BrushSensor::prepareBuffer()
	{
	if(drawBuffer!=NULL)
		error("drawBuffer!=NULL.");
	drawBuffer=editingLayer->getEditableBitmap()->clone();
	fillBuffer=new MonoBitmap(drawBuffer->getSize());
	}

	BrushSensor::~BrushSensor()
	{
	if(drawBuffer!=NULL)
		error("drawBuffer!=NULL.");
	}

void	BrushSensor::down(Coord p)
	{
	prepareBuffer();
	for(int i=0;i<cursorTrackQueueMax;i++)
		cursorTrackIndex[i]=0;
	cursorTrackQueueIndex=0;
	if(index!=-1)
		color=actingColor[index];
	drawedCursor=p;
	lastLightPos=zeroCoord;
	lastLight=NULL;
	}
void	BrushSensor::press(Coord p)
	{
	static int skip=0;
	int qi=cursorTrackQueueIndex;
	cursorTrackQueueIndex=(cursorTrackQueueIndex+1)%cursorTrackQueueMax;
	MonoBitmap *nowLight;
	for(int i=0;i<cursorTrackIndex[qi];i++)
		{
		skip=(skip+1)%10;
		if(skip!=0)
			;//continue;
		Coord &cursorPos=cursorTrack[qi][i];
		Coord lt=imgCoord(cursorPos&drawedCursor)-Coord(brushRadius,brushRadius)/imgScale-monoCoord;
		Coord rb=imgCoord(cursorPos|drawedCursor)+Coord(brushRadius,brushRadius)/imgScale+monoCoord*2;
		lt|=zeroCoord;
		rb&=imgSize;
		/*if(!(rb>lt))
			{
			if(lastLight!=NULL)
				delete lastLight;
			lastLight=NULL;
			drawedCursor=cursorPos;
			continue;
			}*/
			
		DSBitmap *bmp=editingLayer->getEditableBitmap();
		if(!(rb>lt))
			nowLight=NULL;
		else
			nowLight=new MonoBitmap(rb-lt);
		
		Coord div=cursorPos-drawedCursor;
		Coord right=div.right();
		int rd=drawedCursor.dot(right);
		double rl=right.absDouble();
		int da=cursorPos.dot(div);
		int db=drawedCursor.dot(div);
		
		Coord c;
		for(c.y=lt.y;c.y<rb.y;c.y++)
		for(c.x=lt.x;c.x<rb.x;c.x++)
			{
			double f=-1.0;
			if(rl>0)
				{
				int rc=rawCoord(c).dot(right);
				f=brushRadius-abs(rc-rd)/rl;
				if(f<=0.0)
					f=0.0;
					//continue;
				f=f/brushRadius*brushDeep;
							
				int dc=rawCoord(c).dot(div);
				if(((dc>da)+(dc>db))%2==0)
					f=-1.0;
				}
			
			if(f==-1.0)
				{
				f=(double)brushRadius-(rawCoord(c)-drawedCursor).absDouble();
				f=f*brushDeep/brushRadius;
				int r;
				r=(double)brushRadius-(rawCoord(c)-cursorPos).absDouble();
				r=r*brushDeep/brushRadius;
				if(r>f)
					f=r;
				}
			
			if(f<0.0)
				f=0.0;
			if(f>32767.0)
				f=32767.0;
			int fi=f+0.5;
			takeMax(fi,0);
			takeMin(fi,32767);
			
			if(lastLight!=NULL)
				if(c-lastLightPos>=zeroCoord&&c-lastLightPos<lastLight->getSize())
					takeMax(fi,lastLight->get(c-lastLightPos));
			nowLight->put(c-lt,fi);
			//f+=fillBuffer->get(c);
			//takeMin(f,255);
			//if(fillBuffer->get(c)<f)
				{
				//fillBuffer->put(c,f);
				int o=((32767-fi)*(32767-fillBuffer->get(c))+16383)/32767;
				if(index==-1)
					{
					DS_RGB putColor=drawBuffer->get(c);
					putColor.a=(putColor.a*o+16383)/32767;
					bmp->put(c,putColor);
					}
				else
					{
					DS_RGB putColor=color;
					putColor.a=(putColor.a*(32767-o)+16383)/32767;
					bmp->put(c,drawBuffer->get(c));
					bmp->drawPixel(c,putColor);
					}
				}
			}
		if(lastLight!=NULL)
			{
			Coord lastLightEnd=lastLightPos+lastLight->getSize();
			for(c.y=lastLightPos.y;c.y<lastLightEnd.y;c.y++)
			for(c.x=lastLightPos.x;c.x<lastLightEnd.x;c.x++)
				{
				if(c>=lt&&c<rb)
					continue;
				int f=fillBuffer->get(c);
				int e=lastLight->get(c-lastLightPos);
				//if(e<20&&e<f)
				//	continue;
				int o=((32767-f)*(32767-e)+16383)/32767;
				fillBuffer->put(c,32767-o);
				if(index==-1)
					{
					DS_RGB putColor=drawBuffer->get(c);
					putColor.a=(putColor.a*o+16383)/32767;
					bmp->put(c,putColor);
					}
				else
					{
					DS_RGB putColor=color;
					putColor.a=(putColor.a*(32767-o)+16383)/32767;
					bmp->put(c,drawBuffer->get(c));
					bmp->drawPixel(c,putColor);
					}
				}
			}
		
		
		if(lastLight!=NULL)
			delete lastLight;
		lastLight=nowLight;
		lastLightPos=lt;
		drawedCursor=cursorPos;
		}
	cursorTrackIndex[qi]=0;
	imgReshow();
	}
void	BrushSensor::up(Coord p)
	{
	cursorTrackQueueIndex=-1;
	if(lastLight!=NULL)
		delete lastLight;
	lastLight=NULL;
	//drawThread->terminate();
	//delayTime=10;
	stepContainer->addStep(new DrawStep(drawBuffer));
	drawBuffer=NULL;
	delete fillBuffer;
	fillBuffer=NULL;
	}









	LineSensor::LineSensor(int i)
	{
	index=i;
	drawBuffer=NULL;
	}
	
void	LineSensor::prepareBuffer()
	{
	if(drawBuffer!=NULL)
		error("drawBuffer!=NULL.");
	drawBuffer=editingLayer->getEditableBitmap()->clone();
	}
	
	LineSensor::~LineSensor()
	{
	}

void	LineSensor::down(Coord p)
	{
	prepareBuffer();
	editingLayer->getEditableBitmap()->put(imgCoord(p),actingColor[index]);
	start=imgCoord(p);
	}
	
void	LineSensor::press(Coord p)
	{
	editingLayer->getEditableBitmap()->blit(drawBuffer,zeroCoord);
	editingLayer->getEditableBitmap()->drawLineC(start,imgCoord(p),actingColor[index]);
	imgReshow();
	}
	
void	LineSensor::up(Coord p)
	{
	stepContainer->addStep(new DrawStep(drawBuffer));
	drawBuffer=NULL;
	}




void	EraserSensor::down(Coord p)
	{
	//imgData->putPixel(imgCoord(p),white);
	imgReshow();
	}
void	EraserSensor::press(Coord p)
	{
	//imgData->drawLineC(imgCoord(p-dCursor),imgCoord(p),white);
	imgReshow();
	}



void	MoverSensor::down(Coord p)
	{
	start=imgCoord(p);
	}
void	MoverSensor::press(Coord p)
	{
	p=imgCoord(p);
	if(p==start)
		return;
	imgPos-=p-start;
	imgReshow();
	}


	BucketSensor::BucketSensor(int i)
	{
	index=i;
	}
void	BucketSensor::down(Coord p)
	{
	if(!imgData->inside(Position(imgCoord(p),Position::RealLeftUp)))
		return;
	if(editingLayer->getBitmap()->get(imgCoord(p)).same(actingColor[index]))
		return;
	BucketStep *s=new BucketStep(imgCoord(p),actingColor[index]);
	stepContainer->addStep(s);
	//imgData->bucketFill(imgCoord(p),actingColor[index]);
	//imgReshow();
	}


	ColorPickerSensor::ColorPickerSensor(int i)
	{
	index=i;
	}
void	ColorPickerSensor::press(Coord p)
	{
	if(!imgData->inside(Position(imgCoord(p),Position::RealLeftUp)))
		return;
	setColor(imgData->getPixel(Position(imgCoord(p),Position::RealLeftUp)),index);
	manager->reShow();
	}



	EnclosureSensor::EnclosureSensor()
	{
	mode=0;
	backBuffer=NULL;
	frontBuffer=NULL;
	drawBuffer=NULL;
	}
	
	EnclosureSensor::~EnclosureSensor()
	{
	if(backBuffer!=NULL)
		delete backBuffer;
	if(frontBuffer!=NULL)
		delete frontBuffer;
	if(drawBuffer!=NULL)
		delete drawBuffer;
	}

void	EnclosureSensor::down(Coord p)
	{
	argCheck(mode!=0);
	base=imgCoord(p);
	basePos=enclosurePos;
	
	if(enclosureSize==zeroCoord)
		mode=1;
	else if(base>=enclosurePos&&base<=enclosurePos+enclosureSize)
		{
		mode=2;
		if(!editingLayer->bitmapEditable())
			mode=3;
		}
	else
		mode=1;

	switch(mode)
		{
	case 1:
		if(backBuffer!=NULL)
			{
			delete backBuffer;
			delete frontBuffer;
			backBuffer=NULL;
			frontBuffer=NULL;
			}
		break;
	case 2:
		drawBuffer=editingLayer->getEditableBitmap()->clone();
		if(backBuffer==NULL)
			{
			backBuffer=editingLayer->getEditableBitmap()->clone();
			frontBuffer=new DSBitmap(enclosureSize);
			frontBuffer->blit(zeroCoord,backBuffer,enclosurePos,enclosureSize);
			FOR_COORD_D(c,enclosurePos+enclosureSize,enclosurePos,monoCoord)
				backBuffer->put(c,transparent);
			END_FOR_COORD
			}
		break;
	case 3:
		break;
		}
	}

void	EnclosureSensor::press(Coord p)
	{
	switch(mode)
		{
	case 1:
		enclosurePos=imgCoord(p)&base;
		enclosureSize=(imgCoord(p)|base)-enclosurePos+monoCoord;
		imgReshow();
		break;
	case 2:
		if(imgCoord(p)==base)
			break;
		mode=4;
	case 4:
		enclosurePos=basePos+imgCoord(p)-base;
		editingLayer->getEditableBitmap()->blit(backBuffer,zeroCoord);
		editingLayer->getEditableBitmap()->draw(frontBuffer,enclosurePos);
		imgReshow();
		break;
	case 3:
		break;
		}
	}

void	EnclosureSensor::up(Coord p)
	{
	switch(mode)
		{
	case 1:
		if(!(enclosureSize>monoCoord))
			enclosureSize=zeroCoord;
		break;
	case 2:
		delete backBuffer;
		delete frontBuffer;
		backBuffer=NULL;
		frontBuffer=NULL;
		delete drawBuffer;
		drawBuffer=NULL;
		enclosureSize=zeroCoord;
		imgReshow();
		break;
	case 4:
		stepContainer->addStep(new DrawStep(drawBuffer));
		drawBuffer=NULL;
		break;
	case 3:
		break;
		}
	mode=0;
	}

void	EnclosureSensor::paste(DSBitmap *bmp)
	{
	if(backBuffer!=NULL)
		{
		delete backBuffer;
		delete frontBuffer;
		}
	if(drawBuffer!=NULL)
		delete drawBuffer;
	backBuffer=editingLayer->getBitmap()->clone();
	drawBuffer=editingLayer->getBitmap()->clone();
	frontBuffer=bmp;
	editingLayer->getEditableBitmap()->draw(frontBuffer,zeroCoord);
	enclosurePos=zeroCoord;
	enclosureSize=bmp->getSize();
	stepContainer->addStep(new DrawStep(drawBuffer));
	drawBuffer=NULL;
	}




	CursorSensor::CursorSensor()
	{
	}

void	CursorSensor::down(Coord pos)
	{
	pos=imgCoord(pos);
	if(pos>=enclosurePos&&pos<enclosurePos+enclosureSize)
		{
		Popup *p=new Popup(Position(pos),base);
		p->addContext("Copy",ResponseIndex_copyMulti);
		p->addContext("Clone as Layer",ResponseIndex_cloneMulti);
		p->addFolder("Set Size");
			p->addContext("Real",ResponseIndex_setSize);
			p->addContext("Fix",ResponseIndex_setFixSize);
			p->addContext("Square",ResponseIndex_setSquareSize);
			p->endFolder();
		p->endFolder();
		addWindow(p);
		}
	else
		{
		clickPos=pos;
		Popup *p=new Popup(Position(pos),base);
		p->addContext("Copy",ResponseIndex_copy);
		if(isOSClipboardBitmapAvailable())
		p->addContext("Paste",ResponseIndex_paste);
		p->addFolder("Set Size");
			p->addContext("Real Head",ResponseIndex_setHeadSize);
			p->addContext("Real End",ResponseIndex_setEndSize);
			p->addContext("Fix Head",ResponseIndex_setFixHeadSize);
			p->addContext("Fix End",ResponseIndex_setFixEndSize);
			p->addContext("Square Head",ResponseIndex_setSquareHeadSize);
			p->addContext("Square End",ResponseIndex_setSquareEndSize);
			p->endFolder();
		p->endFolder();
		addWindow(p);
		}
	}


	DeviateSensor::DeviateSensor()
	{
	base=zeroCoord;
	originDeviation=zeroCoord;
	}
void	DeviateSensor::down(Coord p)
	{
	base=imgCoord(p);
	originDeviation=editingLayer->getDeviation();
	}
void	DeviateSensor::press(Coord p)
	{
	if(originDeviation+imgCoord(p)-base==editingLayer->getDeviation())
		return;
	editingLayer->setDeviation(originDeviation+imgCoord(p)-base);
	imgReshow();
	}

void	DeviateSensor::up(Coord p)
	{
	stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_deviation,originDeviation));
	}
