


template<>
void	Step<Datafile>::setBackupRaw(Datafile *f)
	{
	}




	AddDataStep::AddDataStep(Data *d,int p,bool a)
	{
	argCheck(d==NULL);
	addType(Type_alwaysReady);
	addType(Type_reverse);
	if(a&&fileData->getNumber()==1)
		legal=false;
	data=d;
	pos=p;
	added=a;
	}
	
	AddDataStep::~AddDataStep()
	{
	if(!added)
		delete data;
	}
	
void	AddDataStep::stepInRaw()
	{
	if(!added)
		{
		fileData->addData(data,pos);
		added=true;
		}
	else
		{
		if(pos==dataIndex)
			setData(pos-1<0?pos+1:pos-1);
		fileData->loseData(pos);
		added=false;
		}
	refreshWindow();
	}
	





	AddFrameStep::AddFrameStep(Frame **f,int *p,int m,bool a)
	{
	argCheck(f==NULL||p==NULL);
	addType(Type_alwaysReady);
	addType(Type_reverse);
	if(a&&pic->getFrameNumber()<=m)
		legal=false;
	addFrame=f;
	pos=p;
	max=m;
	added=a;
	}

	AddFrameStep::AddFrameStep(Frame *f,int p,bool a)
	{
	argCheck(f==NULL);
	if(a&&pic->getFrameNumber()==1)
		legal=false;
	addFrame=dalloc<Frame*>(1);
	addFrame[0]=f;
	pos=dalloc<int>(1);
	pos[0]=p;
	max=1;
	added=a;
	}
	
	AddFrameStep::~AddFrameStep()
	{
	if(!added)
		for(int i=0;i<max;i++)
			delete addFrame[i];
	dfree(addFrame);
	dfree(pos);
	}

void	AddFrameStep::stepInRaw()
	{
	if(!added)
		{
		for(int i=0;i<max;i++)
			pic->addFrame(addFrame[i],pos[i]);
		added=true;
		}
	else
		{
		for(int i=max-1;i>=0;i--)
			{
			if(pos[i]==frameIndex)
				setFrame(pos[i]-1<0?pos[i]+1:pos[i]-1);
			pic->loseFrame(pos[i]);
			frameIndex=imgData->getFrameIndex(frameData);
			}
		added=false;
		}
	frameIndex=imgData->getFrameIndex(frameData);
	refreshWindow();
	}




	AddLayerStep::AddLayerStep(Layer **l,FolderLayer **t,int *p,int m,bool a)
	{
	argCheck(l==NULL||t==NULL||p==NULL||m<=0);
	addType(Type_alwaysReady);
	addType(Type_reverse);
	addLayer=l;
	targetLayer=t;
	pos=p;
	max=m;
	added=a;
	
	if(added)
		for(int i=0;i<m;i++)
			if(!addLayer[i]->linker.empty())
				legal=false;
	}

	AddLayerStep::AddLayerStep(Layer *l,Layer *t,bool downClick,bool a)
	{
	argCheck(l==NULL||t==NULL);
	addType(Type_alwaysReady);
	addType(Type_reverse);
	//if(a&&pic->getFrameNumber()==1)
	//	legal=false;
	addLayer=dalloc<Layer*>(1);
	targetLayer=dalloc<FolderLayer*>(1);
	pos=dalloc<int>(1);
	max=1;
	addLayer[0]=l;
	added=a;
	
	if(t->getOwner(true)==NULL ||!l->linker.empty())
		{
		legal=false;
		return;
		}
		
	if(isType(t,FolderLayer)&&downClick)
		{
		targetLayer[0]=(FolderLayer*)t;
		pos[0]=0;
		}
	else
		{
		targetLayer[0]=t->getOwner();
		pos[0]=targetLayer[0]->getIndex(t)+downClick;
		}
	}
	
	AddLayerStep::~AddLayerStep()
	{
	if(!added)
		for(int i=0;i<max;i++)
			delete addLayer[i];
	dfree(addLayer);
	dfree(targetLayer);
	dfree(pos);
	}

void	AddLayerStep::stepInRaw()
	{
	if(!added)
		{
		for(int i=0;i<max;i++)
			targetLayer[i]->addLayer(addLayer[i],pos[i]);
		added=true;
		}
	else
		{
		for(int i=max-1;i>=0;i--)
			{
			if(clipboard->isCopyed(addLayer[i]))
				clipboard->clear(0);
			if(addLayer[i]==editingLayer)
				{
				if(targetLayer[i]->getNumber()==1)
					setLayer(targetLayer[i]);
				else
					setLayer(targetLayer[i]->getLayer(pos[i]-1<0?pos[i]+1:pos[i]-1));
				}
			targetLayer[i]->loseLayer(pos[i]);
			}
		added=false;
		}
	refreshWindow();
	}



	AddNextFrameStep::AddNextFrameStep(Frame *host,Frame **f,int *p,int m,bool a)
	{
	argCheck(host==NULL||f==NULL||p==NULL||m<=0);
	addType(Type_alwaysReady);
	addType(Type_reverse);
	frame=host;
	nextPic=dalloc<Picture*>(m);
	nextIndex=dalloc<int>(m);
	pos=p;
	max=m;
	added=a;
	
	for(int i=0;i<max;i++)
		{
		for(int pi=0;pi<fileData->getNumber();pi++)
			{
			Picture *pic=fileData->getData(pi)->getPic(true);
			if(pic==NULL)
				continue;
			for(int fi=0;fi<pic->getFrameNumber();fi++)
				if(pic->getFrame(fi)==f[i])
					{
					nextPic[i]=pic;
					nextIndex[i]=fi;
					break;
					}
			if(nextPic[i]!=NULL)
				break;
			}
		if(nextPic[i]==NULL)
			{
			legal=false;
			break;
			}
		}
	dfree(f);
	}
	AddNextFrameStep::~AddNextFrameStep()
	{
	dfree(nextPic);
	dfree(nextIndex);
	dfree(pos);
	}
void	AddNextFrameStep::stepInRaw()
	{
	if(!added)
		{
		for(int i=0;i<max;i++)
			frame->addNextFrame(nextPic[i],nextIndex[i],pos[i]);
		added=true;
		}
	else
		{
		for(int i=max-1;i>=0;i--)
			frame->deleteNextFrame(pos[i]);
		added=false;
		}
	//refreshWindow();
	}
	



	SetDataNameStep::SetDataNameStep(Data *d,const char *str)
	{
	argCheck(d==NULL||str==NULL);
	addType(Type_alwaysReady);
	addType(Type_reverse);
	data=d;
	target=origin=NULL;
	added=false;
	
	int len=getStringLength(str)+1;
	if(len<=0||len>300)
		{
		legal=false;
		return;
		}
	target=dclone(str,len);

	len=d->getName().getLength()+1;
	if(len<=0||len>300)
		{
		legal=false;
		return;
		}
	origin=dclone<char>(d->getName(),len);
	}
	
	SetDataNameStep::~SetDataNameStep()
	{
	if(target!=NULL)
		dfree(target);
	if(origin!=NULL)
		dfree(origin);
	}
	
void	SetDataNameStep::stepInRaw()
	{
	if(!added)
		{
		data->setName(target);
		added=true;
		}
	else
		{
		data->setName(origin);
		added=false;
		}
	refreshWindow();
	}





	MoveFrameStep::MoveFrameStep(int *f,int *t,int m)
	{
	argCheck(f==NULL||t==NULL);
	addType(Type_alwaysReady|Type_reverse);
	from=f;
	to=t;
	max=m;
	}
	
	MoveFrameStep::~MoveFrameStep()
	{
	dfree(from);
	dfree(to);
	}

void	MoveFrameStep::stepInRaw()
	{
	Frame **stack=dalloc<Frame*>(max);
	int i;
	for(i=max-1;i>=0;i--)
		{
		if(pic->getFrameNumber()==1)
			break;
		stack[i]=pic->getFrame(from[i]);
		pic->loseFrame(from[i]);
		}
	for(i++;i<max;i++)
		pic->addFrame(stack[i],to[i]);
	
	dfree(stack);
	int *temp=from;
	from=to;
	to=temp;
	
	frameIndex=imgData->getFrameIndex(frameData);
	refreshWindow();
	}





	PictureStep::PictureStep()
	{
	if(dataType!=Data::Type_Picture)
		{
		legal=false;
		return;
		}
	pic=imgData;
	frame=frameData;
	layer=editingLayer;
	}

	PictureStep::~PictureStep()
	{
	}


	BitmapStep::BitmapStep()
	{
	if(legal)
		if(!layer->bitmapEditable())
			legal=false;
	backupBmp=NULL;
	backupBuffer=NULL;
	backupNeeded=true;
	privateBackup=NULL;
	backupPos=zeroCoord;
	}
	
	BitmapStep::~BitmapStep()
	{
	clearBackup();
	}

void	BitmapStep::setBackupRaw(Datafile *d)
	{
	if(privateBackup!=NULL)
		{
		backupBmp=privateBackup;
		privateBackup=NULL;
		}
	else if(backupNeeded&&layer->getEditableBitmap()->getSize().area()<5000000)
		{
		backupBmp=layer->getEditableBitmap()->clone();
		}
	}

void	BitmapStep::compressBuffer()
	{
	if(!backupNeeded)
		return;
	if(backupBuffer!=NULL)
		return;
	if(backupBmp==NULL)
		return;
	
	DSBitmap *diff=backupBmp->cloneDiffer(layer->getEditableBitmap(),&backupPos);
	delete backupBmp;
	backupBmp=NULL;

	if(diff==NULL)
		{
		legal=false;
		return;
		}
	Picture *pic=new Picture(diff);
	backupBuffer=new IOBuffer(pic->getDataSize().area()*4+1000);
	pic->savePNG(new OutputEmptyMask(backupBuffer),(new PictureOption())->setStableTransparent(true));
	delete pic;
	backupBuffer->cutBuffer();
	}

void	BitmapStep::clearBackup()
	{
	if(backupBmp!=NULL)
		delete backupBmp;
	if(backupBuffer!=NULL)
		delete backupBuffer;
	}

bool	BitmapStep::stepOutReadyRaw()
	{
	if(backupNeeded&&backupBmp==NULL&&backupBuffer==NULL)
		return false;
	return true;
	}

void	BitmapStep::stepOutRaw()
	{
	argCheck(!backupNeeded);
	if(backupBmp!=NULL)
		layer->getEditableBitmap()->blit(backupBmp,zeroCoord);
	else if(backupBuffer!=NULL)
		{
		backupBuffer->setPosition(0);
		DSBitmap *bmp=loadBitmap(new InputEmptyMask(backupBuffer),bitmapPNG);
		layer->getEditableBitmap()->blit(bmp,backupPos);
		delete bmp;
		}
	else
		;
	}


	DrawStep::DrawStep(DSBitmap *t)
	{
	argCheck(t==NULL);
	privateBackup=t;
	trace=layer->getEditableBitmap()->cloneDiffer(privateBackup,&traceCoord);
	if(trace==NULL)
		legal=false;
	}

	DrawStep::~DrawStep()
	{
	if(trace!=NULL)
		delete trace;
	if(privateBackup!=NULL)
		delete privateBackup;
	}

void	DrawStep::stepInRaw()
	{
	if(trace==NULL)
		return;
	layer->getEditableBitmap()->blit(trace,traceCoord);
	compressBuffer();
	}







	BucketStep::BucketStep(Coord p,DS_RGB c)
	{
	pos=p;
	color=c;
	if(legal)
		if(layer->getEditableBitmap()->get(pos).same(color))
			legal=false;
	}

void	BucketStep::stepInRaw()
	{
	layer->getEditableBitmap()->bucketFill(pos,color);
	compressBuffer();
	}






	ChangeSizeStep::ChangeSizeStep(int t,Coord size)
	{
	type=t;
	legal=false;
	Coord space=pic->getDataSize()-pic->getFixHeadSize()-pic->getFixEndSize();
	if(pic->getType()==Picture::Type_square)
		space-=pic->getSquareHeadSize()+pic->getSquareEndSize();
	switch(type)
		{
	case ChangeSizeProcess_headSize:
		targetSize=size;
		originSize=pic->getDataSize();
		if(targetSize<space)
			legal=true;
		for(int fi=0;fi<pic->getFrameNumber();fi++)
			if(!pic->getFrame(fi)->getLayer()->bitmapEditable())
				legal=false;
		break;
	case ChangeSizeProcess_fixHeadSize:
		targetSize=size;
		originSize=pic->getFixHeadSize();
		if(targetSize>=zeroCoord&&targetSize-originSize<=space)
			legal=true;
		break;
	case ChangeSizeProcess_fixEndSize:
		targetSize=pic->getDataSize()-size;
		originSize=pic->getFixEndSize();
		if(targetSize>=zeroCoord&&targetSize-originSize<=space)
			legal=true;
		break;
	case ChangeSizeProcess_endSize:
		targetSize=size;
		originSize=pic->getDataSize();
		if(targetSize>zeroCoord&&originSize-targetSize<=space)
			legal=true;
		break;
	case ChangeSizeProcess_squareHeadSize:
		if(pic->getType()!=Picture::Type_square)
			break;
		targetSize=size-pic->getFixHeadSize();
		originSize=pic->getSquareHeadSize();
		if(targetSize>=zeroCoord&&targetSize-originSize<=space)
			legal=true;
		break;
	case ChangeSizeProcess_squareEndSize:
		if(pic->getType()!=Picture::Type_square)
			break;
		targetSize=pic->getDataSize()-size-pic->getFixEndSize();
		originSize=pic->getSquareEndSize();
		if(targetSize>=zeroCoord&&targetSize-originSize<=space)
			legal=true;
		break;
	
	case ChangeSizeProcess_deviation:
		targetSize=layer->getDeviation();
		originSize=size;
		legal=true;
		break;
	default:
		legal=false;
		break;
		}
	}

void	ChangeSizeStep::setBackupRaw(Datafile *d)
	{
	}

void	ChangeSizeStep::clearBackup()
	{
	}

void	ChangeSizeStep::stepInRaw()
	{
	switch(type)
		{
	case ChangeSizeProcess_headSize:
		for(int fi=0;fi<pic->getFrameNumber();fi++)
			pic->getFrame(fi)->getLayer()->getEditableBitmap()->move((-targetSize)&zeroCoord,transparent);
		pic->reSize(Size(pic->getDataSize()-targetSize,Size::Real));
		for(int fi=0;fi<pic->getFrameNumber();fi++)
			pic->getFrame(fi)->getLayer()->getEditableBitmap()->move(-(targetSize&zeroCoord),transparent);
		imgSize=pic->getDataSize();
		imgPos-=targetSize;
		enclosurePos-=targetSize;
		break;
	case ChangeSizeProcess_fixHeadSize:
		pic->setFixSize(targetSize,pic->getFixEndSize());
		break;
	case ChangeSizeProcess_fixEndSize:
		pic->setFixSize(pic->getFixHeadSize(),targetSize);
		break;
	case ChangeSizeProcess_endSize:
		pic->reSize(Size(targetSize,Size::Real));
		imgSize=pic->getDataSize();
		break;
	case ChangeSizeProcess_squareHeadSize:
		pic->setSquare(targetSize,pic->getSquareEndSize());
		break;
	case ChangeSizeProcess_squareEndSize:
		pic->setSquare(pic->getSquareHeadSize(),targetSize);
		break;
	case ChangeSizeProcess_deviation:
		layer->setDeviation(targetSize);
		break;
		}
	}

bool	ChangeSizeStep::stepOutReadyRaw()
	{
	if(type==ChangeSizeProcess_headSize)
		return false;
	if(type==ChangeSizeProcess_deviation)
		if(layer->getDeviation()!=targetSize)
			return false;
	return true;
	}

void	ChangeSizeStep::stepOutRaw()
	{
	switch(type)
		{
	case ChangeSizeProcess_fixHeadSize:
		pic->setFixSize(originSize,pic->getFixEndSize());
		break;
	case ChangeSizeProcess_fixEndSize:
		pic->setFixSize(pic->getFixHeadSize(),originSize);
		break;
	case ChangeSizeProcess_endSize:
		pic->reSize(Size(originSize,Size::Real));
		imgSize=pic->getDataSize();
		break;
	case ChangeSizeProcess_squareHeadSize:
		pic->setSquare(originSize,pic->getSquareEndSize());
		break;
	case ChangeSizeProcess_squareEndSize:
		pic->setSquare(pic->getSquareHeadSize(),originSize);
		break;
	case ChangeSizeProcess_deviation:
		layer->setDeviation(originSize);
		break;
		}
	}


	SetPicNextFrameStep::SetPicNextFrameStep(Frame *f)
	{
	argCheck(f==NULL);
	addType(Type_alwaysReady|Type_reverse);
	for(int i=0;i<fileData->getNumber();i++)
		{
		Picture *pic=fileData->getData(i)->getPic(true);
		if(pic==NULL)
			continue;
		for(int j=0;j<pic->getFrameNumber();j++)
			if(pic->getFrame(j)==f)
				{
				nextPic=pic;
				nextIndex=j;
				return;
				}
		}
	warning("Frame not found.");
	legal=false;
	}
void	SetPicNextFrameStep::stepInRaw()
	{
	Picture *p=pic->getNextPic();
	int i=pic->getNextIndex();
	pic->setNextFrame(nextPic,nextIndex);
	nextPic=p;
	nextIndex=i;
	}


















//==========================================================================================


void	addProcessStep(int r)
	{
	if		(r>DatafileProcess_start&&r<DatafileProcess_end)
		{
		stepContainer->addStep(new DatafileProcessStep(r));
		}
	else if	(r>PictureProcess_start&&r<PictureProcess_end)
		{
		if(dataType!=Data::Type_Picture)
			error("not Picture");
		stepContainer->addStep(new PictureProcessStep(r));
		}
	else if	(r>BitmapProcess_start&&r<BitmapProcess_end)
		{
		if(dataType!=Data::Type_Picture)
			error("not Picture");
		stepContainer->addStep(new BitmapProcessStep(r));
		}
	}

	DatafileProcessStep::DatafileProcessStep(int n)
	{
	number=n;
	}

void	DatafileProcessStep::stepInRaw()
	{
	switch(number)
		{
	case DatafileProcess_addPicture:
		fileData->addData(new Picture(Coord(64,64)));
		break;
	case DatafileProcess_addText:
		{
		Text *text=new Text();
		text->addLine("test",4);
		fileData->addData(text);
		}
		break;
	case DatafileProcess_addSound:
		break;
		}
	if(manager->datafileManager!=NULL)
		refreshWindow(manager->datafileManager);
	}



	PictureProcessStep::PictureProcessStep(int n)
	{
	number=n;
	switch(number)
		{
	case PictureProcess_enableLayer:
		if(isType(frame->getLayer(),FolderLayer))
			legal=false;
		layer=new FolderLayer(layer->getSize());
		break;
		}
	}

	PictureProcessStep::~PictureProcessStep()
	{
	switch(number)
		{
	case PictureProcess_enableLayer:
		if(layer!=NULL)
			delete layer;
		break;
		}
	}
	
void	PictureProcessStep::stepInRaw()
	{
	switch(number)
		{
	case PictureProcess_enableLayer:
		((FolderLayer*)layer)->addLayer(frame->getLayer());
		frame->setLayer(layer);
		layer=NULL;
		refreshWindow();
		break;
	case PictureProcess_reverseFrame:
		imgData->reverseFrame();
		frameIndex=imgData->getFrameIndex(frameData);
		refreshWindow();
		break;
	case PictureProcess_setTypeDefault:
		imgData->destroyType();
		break;
	case PictureProcess_setTypeSquare:
		imgData->setSquare(monoCoord,monoCoord);
		break;
		}
	}

bool	PictureProcessStep::stepOutReadyRaw()
	{
	switch(number)
		{
	case PictureProcess_setTypeDefault:
	case PictureProcess_setTypeSquare:
		return false;
		}
	return true;
	}
	
void	PictureProcessStep::stepOutRaw()
	{
	switch(number)
		{
	case PictureProcess_enableLayer:
		{
		if(!isType(frame->getLayer(),FolderLayer)||layer!=NULL)
			error("PictureProcess::disableLayer");
		layer=frame->getLayer();
		FolderLayer *fl=(FolderLayer*)layer;
		frame->setLayer(fl->getLayer(0));
		fl->loseLayer(0);
		refreshWindow();
		}
		break;
	case PictureProcess_reverseFrame:
		stepInRaw();
		break;
	case PictureProcess_addFrame:
		break;
	default:
		error("switchOut.");
		break;
		}
	}



	FrameProcessStep::FrameProcessStep(int n,int fi,bool r)
	{
	number=n;
	frameIndex=fi;
	frame=pic->getFrame(frameIndex);
	rightClick=r;
	backupFrame=NULL;
	backupDelay=NULL;
	}

	FrameProcessStep::~FrameProcessStep()
	{
	if(backupFrame!=NULL)
		delete backupFrame;
	if(backupDelay!=NULL)
		dfree(backupDelay);
	}

void	FrameProcessStep::stepInRaw()
	{
	switch(number)
		{
	case FrameProcess_addFrame:
		pic->addFrame(new Frame(new DSBitmap(pic->getDataSize()),frame->getDelay()),frameIndex+rightClick);
		pic->setTimeType(Picture::Realtime);
		break;
	case FrameProcess_copyFrame:
		pic->addFrame(frame->clone(),frameIndex+rightClick);
		pic->setTimeType(Picture::Realtime);
		break;
	case FrameProcess_deleteFrame:
		backupFrame=pic->getFrame(frameIndex);
		pic->loseFrame(frameIndex);
		break;
	case FrameProcess_unifyDelay:
		if(backupDelay==NULL)
			{
			backupDelay=dalloc<int>(pic->getFrameNumber());
			for(int fi=0;fi<pic->getFrameNumber();fi++)
				backupDelay[fi]=pic->getFrame(fi)->getDelay();
			}
		for(int fi=0;fi<pic->getFrameNumber();fi++)
			pic->getFrame(fi)->setDelay(frame->getDelay());
		break;
	case FrameProcess_diff:
		{
		if(frameIndex==0)
			break;
		const DSBitmap *back=pic->getBitmapIndex(frameIndex-1);
		DSBitmap *fron=pic->getFrame(frameIndex)->getLayer()->getEditableBitmap();
		FOR_COORD(c,fron->getSize())
			if(!back->get(c).same(fron->get(c)))
				fron->put(c,transparent);
		END_FOR_COORD
		}
		break;
		}
	::frameIndex=imgData->getFrameIndex(frameData);
	if(manager->animationBar!=NULL)
		refreshWindow(manager->animationBar);
	}

void	FrameProcessStep::stepOutRaw()
	{
	switch(number)
		{
	case FrameProcess_addFrame:
	case FrameProcess_copyFrame:
		pic->deleteFrame(frameIndex+rightClick);
		break;
	case FrameProcess_deleteFrame:
		pic->addFrame(backupFrame,frameIndex);
		backupFrame=NULL;
		break;
	case FrameProcess_unifyDelay:
		if(backupDelay==NULL)
			error("Missing backupDelay.");
		for(int fi=0;fi<pic->getFrameNumber();fi++)
			pic->getFrame(fi)->setDelay(backupDelay[fi]);
		break;
		}
	::frameIndex=imgData->getFrameIndex(frameData);
	if(manager->animationBar!=NULL)
		refreshWindow(manager->animationBar);
	}




	BitmapProcessStep::BitmapProcessStep(int n)
	{
	number=n;

	switch(number)
		{
	case BitmapProcess_reverseRGB:
	case BitmapProcess_reverseHue:
	case BitmapProcess_reverseHorizontal:
	case BitmapProcess_reverseVertical:
		backupNeeded=false;
		break;
		}
	}

void	BitmapProcessStep::stepInRaw()
	{
	switch(number)
		{
	case BitmapProcess_grayScalizeRGB:
		FOR_COORD(c,imgData->getSize(Size::Real))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			color.r=color.g=color.b=color.getAverage();
			layer->getEditableBitmap()->put(c,color);
		END_FOR_COORD
		break;

	case BitmapProcess_grayScalizeHSL:
		FOR_COORD(c,imgData->getSize(Size::Real))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			color.r=color.g=color.b=(color.getMax()+color.getMin())/2;
			layer->getEditableBitmap()->put(c,color);
		END_FOR_COORD
		break;

	case BitmapProcess_grayScalizeHSV:
		FOR_COORD(c,imgData->getSize(Size::Real))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			color.r=color.g=color.b=color.getMax();
			layer->getEditableBitmap()->put(c,color);
		END_FOR_COORD
		break;

	case BitmapProcess_grayScalizeYCC:
		FOR_COORD(c,imgData->getSize(Size::Real))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			color.r=color.g=color.b=(color.r*299+color.g*587+color.b*114+500)/1000;
			layer->getEditableBitmap()->put(c,color);
		END_FOR_COORD
		break;

	case BitmapProcess_binarizeSimple:
		FOR_COORD(c,imgData->getSize(Size::Real))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			if(color.getAverage()>=128)
				color=white;
			else
				color=black;
			layer->getEditableBitmap()->put(c,color);
		END_FOR_COORD
		break;

	case BitmapProcess_binarizeIntegral:
		{
		long long sum=0;
		FOR_COORD(c,imgData->getSize(Size::Real))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			sum+=(color.r+color.g+color.b)*color.a/255;
		END_FOR_COORD
		sum/=layer->getEditableBitmap()->getSize().area();
		sum/=3;
		FOR_COORD(c,imgData->getSize(Size::Real))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			if(color.getAverage()>=sum)
				color=white;
			else
				color=black;
			layer->getEditableBitmap()->put(c,color);
		END_FOR_COORD
		}
		break;

	case BitmapProcess_deMask:
		FOR_COORD_D(c,imgData->getSize(Size::Real),Coord(1,1),Coord(2,2))
			int r=0,g=0,b=0,d=0;
			if(c.y>0)
				{
				r+=layer->getEditableBitmap()->get(c-Coord(0,1)).r;
				g+=layer->getEditableBitmap()->get(c-Coord(0,1)).g;
				b+=layer->getEditableBitmap()->get(c-Coord(0,1)).b;
				d++;
				}
			if(c.x>0)
				{
				r+=layer->getEditableBitmap()->get(c-Coord(1,0)).r;
				g+=layer->getEditableBitmap()->get(c-Coord(1,0)).g;
				b+=layer->getEditableBitmap()->get(c-Coord(1,0)).b;
				d++;
				}
			if(c.y<imgData->getSize(Size::Real).y-1)
				{
				r+=layer->getEditableBitmap()->get(c+Coord(0,1)).r;
				g+=layer->getEditableBitmap()->get(c+Coord(0,1)).g;
				b+=layer->getEditableBitmap()->get(c+Coord(0,1)).b;
				d++;
				}
			if(c.x<imgData->getSize(Size::Real).x-1)
				{
				r+=layer->getEditableBitmap()->get(c+Coord(1,0)).r;
				g+=layer->getEditableBitmap()->get(c+Coord(1,0)).g;
				b+=layer->getEditableBitmap()->get(c+Coord(1,0)).b;
				d++;
				}
			layer->getEditableBitmap()->put(c,DS_RGB(r/d,g/d,b/d));
		END_FOR_COORD
		break;

	case BitmapProcess_showMask:
		FOR_COORD_D(c,imgData->getSize(Size::Real),Coord(1,1),Coord(2,2))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			layer->getEditableBitmap()->put(c-Coord(1,1),color);
			layer->getEditableBitmap()->put(c-Coord(0,1),color);
			layer->getEditableBitmap()->put(c-Coord(1,0),color);
		END_FOR_COORD
		break;

	case BitmapProcess_opacifyIgnore:
		layer->getEditableBitmap()->ignoreAlpha();
		break;

	case BitmapProcess_clearWhite:
		layer->getEditableBitmap()->clear(white);
		break;

	case BitmapProcess_clearBlack:
		layer->getEditableBitmap()->clear(black);
		break;

	case BitmapProcess_clearNoise:
		FOR_COORD(c,imgData->getSize(Size::Real))
			layer->getEditableBitmap()->put(c,DS_RGB(rand()%256,rand()%256,rand()%256));
		END_FOR_COORD
		break;

	case BitmapProcess_clearTransparent:
		layer->getEditableBitmap()->clear(transparent);
		break;

	case BitmapProcess_reverseRGB:
		FOR_COORD(c,imgData->getSize(Size::Real))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			layer->getEditableBitmap()->put(c,DS_RGB(color.getReverse(),color.a));
		END_FOR_COORD
		break;

	case BitmapProcess_reverseHue:
		FOR_COORD(c,imgData->getSize(Size::Real))
			DS_RGB color=layer->getEditableBitmap()->get(c);
			int mm=color.getMin()+color.getMax();
			color.r=mm-color.r;
			color.g=mm-color.g;
			color.b=mm-color.b;
			layer->getEditableBitmap()->put(c,color);
		END_FOR_COORD
		break;
	case BitmapProcess_reverseHorizontal:
		{
		DSBitmap *back=imgData->getEditableBitmap()->clone();
		FOR_COORD(c,imgData->getSize(Size::Real))
			layer->getEditableBitmap()->put(c,back->get(imgData->getSize(Size::Real).x-1-c.x,c.y));
		END_FOR_COORD
		delete back;
		}
		break;
	case BitmapProcess_reverseVertical:
		{
		DSBitmap *back=imgData->getEditableBitmap()->clone();
		FOR_COORD(c,imgData->getSize(Size::Real))
			layer->getEditableBitmap()->put(c,back->get(c.x,imgData->getSize(Size::Real).y-1-c.y));
		END_FOR_COORD
		delete back;
		}
		break;
		}
	compressBuffer();
	}

void	BitmapProcessStep::stepOutRaw()
	{
	switch(number)
		{
	default:
		BitmapStep::stepOutRaw();
		break;
	case BitmapProcess_reverseRGB:
	case BitmapProcess_reverseHue:
	case BitmapProcess_reverseHorizontal:
	case BitmapProcess_reverseVertical:
		stepInRaw();
		break;
		}
	}
