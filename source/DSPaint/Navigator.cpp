

static const Coord	naviSize(130,130),
					naviBorder(10,10);
static const int	scrollWidth=naviSize.x-16-8;
static const int	scrollMax=scrollWidth-48;


	Navigator::Navigator():Window(Position(8,48,Position::EffectiveRightUp),DSData->getPic(BDF_Window),Size(naviSize+naviBorder*2+Coord(0,8+16)))
	{
	SENSE->left=new MoveSensor();
	SENSE->right=new SuicideSensor();
	naviSense=new RegularSense(new NaviSensor());
	scaleIndex=scrollMax/2;
	addEtcObject(scroll=new Scroll(Position(naviBorder+Coord(0,naviSize.y+8))
				,Size(scrollWidth,16),DSData->getPic(BDF_Scroll),scaleIndex,0,scrollMax,2));
	addEtcObject(new Button(Position(naviBorder+Coord(0,naviSize.y+8),Position::EffectiveRightUp)
				,DSData->getPic(BDF_Button),1));
	}

	Navigator::~Navigator()
	{
	delete naviSense;
	}

void	Navigator::response(int i)
	{
	switch(i)
		{
		case 1:
			resetSight();
			response(3);
			break;
		case 2:
			imgScale=pow(2.0,((double)scaleIndex/scrollMax-0.5)*12);
			imgReshow();
			break;
		case 3:
			scaleIndex=((log2(imgScale)/12.0+0.5)*scrollMax);
			break;
		}
	}

void	Navigator::NaviSensor::press(Coord p)
	{
	origin=Position(window_size/2,Position::RealLeftUp);
	imgPos=Position((p-naviBorder)*imgSize/naviSize);
	imgReshow();
	}

Sense*	Navigator::sense(Coord p)
	{
	if(p<naviBorder+naviSize&&p>=naviBorder)
		return naviSense;
	else
		return defaultSense;
	}

void	Navigator::input()
	{
	}

void	Navigator::refresh()
	{
	
	}

void	Navigator::show()
	{
	switch(dataType)
		{
	case Data::Type_nothing:
		break;
	case Data::Type_Text:
		break;
	case Data::Type_Picture:
		wBuffer->drawScale(imgData,Position(naviBorder,Position::RealLeftUp,Position::EffectiveLeftUp),Size(naviSize,Size::Real));
		break;
		}
	}
