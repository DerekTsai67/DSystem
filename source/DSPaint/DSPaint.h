#include "../DSystem.h"
#include "Sensor.h"
#include "Toolbar.h"
#include "Navigator.h"
#include "ColorBox.h"
#include "DatafileManager.h"
#include "AnimationBar.h"
#include "SaveMessage.h"
#include "LoadMessage.h"
#include "LayerManager.h"
#include "ColorPanel.h"
#include "StepManager.h"
#include "MainManager.h"
#include "Step.h"

extern Datafile	*fileData;
extern int		dataIndex;
extern Data		*typeData;
extern int		dataType;

extern Picture	*imgData;
extern int		frameIndex;
extern Frame	*frameData;
extern Layer	*editingLayer;
extern Coord	enclosurePos,enclosureSize;
extern EnclosureSensor	*enclosureSensor;
extern Coord	clickPos;

extern Coord	imgPos,origin;
extern Coord	imgSize;
extern double	imgScale;

extern StepContainer<Datafile>	*stepContainer;

extern Clipboard	*clipboard;
enum {
	Clipboard_Data=1,
	Clipboard_Frame,
	Clipboard_Layer,
	Clipboard_count
	};

enum {
	ResponseIndex_close=1000,
	ResponseIndex_step,
	ResponseIndex_reShow,
	ResponseIndex_selectAll,
	ResponseIndex_shiftSelect,
	ResponseIndex_copy,
	ResponseIndex_copyMulti,
	ResponseIndex_paste,
	ResponseIndex_pasteLink,
	ResponseIndex_clone,
	ResponseIndex_cloneMulti,
	ResponseIndex_delete,
	ResponseIndex_deleteMulti,
	ResponseIndex_reverse,
	ResponseIndex_setNextFrame,
	ResponseIndex_rename,
	ResponseIndex_setSize,
	ResponseIndex_setHeadSize,
	ResponseIndex_setEndSize,
	ResponseIndex_setFixSize,
	ResponseIndex_setFixHeadSize,
	ResponseIndex_setFixEndSize,
	ResponseIndex_setSquareSize,
	ResponseIndex_setSquareHeadSize,
	ResponseIndex_setSquareEndSize,
	
	ResponseIndex_add		=1050,
	ResponseIndex_filterType=1070,
//	ResponseIndex_reShow,
	
	ResponseIndex_toolBar	=2000,
	ResponseIndex_navigator,
	ResponseIndex_colorBox,
	ResponseIndex_datafileManager,
	ResponseIndex_animationBar,
	ResponseIndex_saveMessage,
	ResponseIndex_layerManager,
	ResponseIndex_loadMessage,
	ResponseIndex_stepManager,

	ResponseIndex_count
	};

extern Datafile	*DSPaintData,*iconData,*foldingArrow,*toolData;
extern DS_RGB	actingColor[2];
extern bool		colorSkip;
extern MainManager *manager;

class DSPaintBase:public Base{
	public:
	DSPaintBase();
	~DSPaintBase();
void	input();
void	response(int ri);
Sense*	sense(Coord c);
void	show();
	private:

};

void	setData(int index);
void	setFrame(int fi);
void	setLayer(Layer *l);
void	setColor(DS_RGB c,int index);
void	resetSight();
void	imgReshow();
Coord	imgCoord(Coord p,bool center=false);
Coord	rawCoord(Coord p,bool center=false);
