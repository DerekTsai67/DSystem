


class AddDataStep:public Step<Datafile>{
	public:
	AddDataStep(Data *d,int pos,bool added=false);
	~AddDataStep();
void	stepInRaw();
	private:
Data	*data;
int 	pos;
bool	added;
	};

class SetDataNameStep:public Step<Datafile>{
	public:
	SetDataNameStep(Data *d,const char *str);
	~SetDataNameStep();
void	stepInRaw();
	private:
Data	*data;
char	*target,*origin;
bool	added;
	};



class PictureStep:public Step<Datafile>{
	public:
	PictureStep();
virtual	~PictureStep();
	protected:
Picture	*pic;
Frame	*frame;
Layer	*layer;
	};

class BitmapStep:public PictureStep{
	public:
	BitmapStep();
virtual	~BitmapStep();
void	setBackupRaw(Datafile *d);
void	compressBuffer();
void	clearBackup();
bool	stepOutReadyRaw();
void	stepOutRaw();
	protected:
bool		backupNeeded;
DSBitmap	*backupBmp;
IOBuffer	*backupBuffer;
DSBitmap	*privateBackup;
Coord		backupPos;
	};


class DrawStep:public BitmapStep{
	public:
	DrawStep(DSBitmap *t);
	~DrawStep();
void	stepInRaw();
	private:
DSBitmap	*trace;
Coord		traceCoord;
	};

class BucketStep:public BitmapStep{
	public:
	BucketStep(Coord pos,DS_RGB color);
void	stepInRaw();
	private:
Coord	pos;
DS_RGB	color;
	};

class ChangeSizeStep:public PictureStep{
	public:
	ChangeSizeStep(int t,Coord size);
void	setBackupRaw(Datafile *d);
void	clearBackup();
void	stepInRaw();
bool	stepOutReadyRaw();
void	stepOutRaw();
	private:
int		type;
Coord	targetSize,originSize;
	};

class SetPicNextFrameStep:public PictureStep{
	public:
	SetPicNextFrameStep(Frame *f);
void	stepInRaw();
	private:
Picture	*nextPic;
int		nextIndex;
	};
	
class AddNextFrameStep:public PictureStep{
	public:
	AddNextFrameStep(Frame *host,Frame **f,int *pos,int max,bool added=false);
	~AddNextFrameStep();
void	stepInRaw();
	private:
Picture	**nextPic;
int		*nextIndex;
int 	*pos;
int		max;
bool	added;
	};

class AddFrameStep:public PictureStep{
	public:
	AddFrameStep(Frame **f,int *pos,int max,bool added=false);
	AddFrameStep(Frame *f,int pos,bool added=false);
	~AddFrameStep();
void	stepInRaw();
	private:
Frame	**addFrame;
int 	*pos;
int		max;
bool	added;
	};

class MoveFrameStep:public PictureStep{
	public:
	MoveFrameStep(int *from,int *to,int max);
	~MoveFrameStep();
void	stepInRaw();
	private:
int 	*from,*to;
int		max;
	};
	
class AddLayerStep:public PictureStep{
	public:
	AddLayerStep(Layer **l,FolderLayer **t,int *p,int m,bool a=false);
	AddLayerStep(Layer *l,Layer *t,bool downClick,bool added=false);
	~AddLayerStep();
void	stepInRaw();
	private:
Layer	**addLayer;
FolderLayer	**targetLayer;
int		*pos;
int		max;
bool	added;
	};
	
	

class DatafileProcessStep:public Step<Datafile>{
	public:
	DatafileProcessStep(int number);
void	stepInRaw();
	private:
int		number;
	};

class PictureProcessStep:public PictureStep{
	public:
	PictureProcessStep(int number);
	~PictureProcessStep();
void	stepInRaw();
bool	stepOutReadyRaw();
void	stepOutRaw();
	private:
int		number;
	};

class FrameProcessStep:public PictureStep{
	public:
	FrameProcessStep(int number,int fi,bool rightClick);
	~FrameProcessStep();
void	stepInRaw();
void	stepOutRaw();
	private:
int		number;
Frame	*frame,*backupFrame;
int 	frameIndex;
bool	rightClick;
int 	*backupDelay;
	};
	
class BitmapProcessStep:public BitmapStep{
	public:
	BitmapProcessStep(int number);
void	stepInRaw();
void	stepOutRaw();
	private:
int		number;
	};
	
enum {
	 Process_start=30000

	,DatafileProcess_start
	,DatafileProcess_addPicture
	,DatafileProcess_addText
	,DatafileProcess_addSound
	,DatafileProcess_end

	,PictureProcess_start
	,PictureProcess_enableLayer
	,PictureProcess_reverseFrame
	,PictureProcess_addFrame
	,PictureProcess_setTypeDefault
	,PictureProcess_setTypeSquare
	,PictureProcess_end
	
	,BitmapProcess_start
	,BitmapProcess_grayScalizeRGB
	,BitmapProcess_grayScalizeHSL
	,BitmapProcess_grayScalizeHSV
	,BitmapProcess_grayScalizeYCC
	,BitmapProcess_binarizeSimple
	,BitmapProcess_binarizeIntegral
	,BitmapProcess_deMask
	,BitmapProcess_showMask
	,BitmapProcess_opacifyIgnore
	,BitmapProcess_clearWhite
	,BitmapProcess_clearBlack
	,BitmapProcess_clearNoise
	,BitmapProcess_clearTransparent
	,BitmapProcess_reverseRGB
	,BitmapProcess_reverseHue
	,BitmapProcess_reverseHorizontal
	,BitmapProcess_reverseVertical
	,BitmapProcess_end
	
	,ChangeSizeProcess_start
	,ChangeSizeProcess_headSize
	,ChangeSizeProcess_fixHeadSize
	,ChangeSizeProcess_fixEndSize
	,ChangeSizeProcess_endSize
	,ChangeSizeProcess_squareHeadSize
	,ChangeSizeProcess_squareEndSize
	,ChangeSizeProcess_deviation
	,ChangeSizeProcess_end
	
	,FrameProcess_start
	,FrameProcess_addFrame
	,FrameProcess_copyFrame
	,FrameProcess_deleteFrame
	,FrameProcess_unifyDelay
	,FrameProcess_diff
	,FrameProcess_end

	,Process_end

	};

void	addProcessStep(int r);



