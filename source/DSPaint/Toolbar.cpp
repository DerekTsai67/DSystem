



Tool	***actingTool,**tool;
int		**acceptTool;
int		*toolIndex;


enum{
	ResponseIndex_brushRadius=300
	};

int		brushRadius,brushDeep,brushInner;

void	initTool()
	{
	tool=dalloc<Tool*>(Tool_count);
	actingTool=dalloc<Tool**>(Data::Type_count);
	acceptTool=dalloc<int*>(Data::Type_count);
	toolIndex=dalloc<int>(Data::Type_count);
	for(int i=0;i<Data::Type_count;i++)
		{
		actingTool[i]=dalloc<Tool*>(3);
		acceptTool[i]=dalloc<int>(Tool_count);
		}

	tool[Tool_mover]		=new Tool(toolData->getPic("Deviate")		,Tool::nonEditing		,new MoverSensor());
	tool[Tool_pencil]		=new Tool(toolData->getPic("Draw")			,Tool::bitmapDrawing	,new DrawSensor(0),new DrawSensor(1));
	tool[Tool_eraser]		=new Tool(toolData->getPic("Eraser")		,Tool::bitmapDrawing	,new DrawSensor(-1),new DrawSensor(-1));
	tool[Tool_brush]		=new Tool(toolData->getPic("Draw")			,Tool::bitmapDrawing	,new BrushSensor(0),new BrushSensor(1));
	tool[Tool_brushEraser]	=new Tool(toolData->getPic("Eraser")		,Tool::bitmapDrawing	,new BrushSensor(-1),new BrushSensor(-1));
	tool[Tool_bucket]		=new Tool(toolData->getPic("Bucket")		,Tool::bitmapDrawing	,new BucketSensor(0),new BucketSensor(1));
	tool[Tool_colorPicker]	=new Tool(toolData->getPic("ColorPicker")	,Tool::nonEditing		,new ColorPickerSensor(0),new ColorPickerSensor(1));
	tool[Tool_line]			=new Tool(toolData->getPic("Line")			,Tool::bitmapDrawing	,new LineSensor(0),new LineSensor(1));
	tool[Tool_enclosure]	=new Tool(toolData->getPic("Enclosure")		,Tool::bitmapDrawing	,enclosureSensor=new EnclosureSensor(),new CursorSensor());
	tool[Tool_deviate]		=new Tool(toolData->getPic("Deviate")		,Tool::nonEditing		,new DeviateSensor());

	int ti=0;
	acceptTool[Data::Type_Picture][ti++]=Tool_mover;
	acceptTool[Data::Type_Picture][ti++]=Tool_enclosure;
	acceptTool[Data::Type_Picture][ti++]=Tool_colorPicker;
	acceptTool[Data::Type_Picture][ti++]=Tool_deviate;
	acceptTool[Data::Type_Picture][ti++]=Tool_pencil;
	acceptTool[Data::Type_Picture][ti++]=Tool_eraser;
	acceptTool[Data::Type_Picture][ti++]=Tool_line;
	acceptTool[Data::Type_Picture][ti++]=Tool_bucket;
	acceptTool[Data::Type_Picture][ti++]=Tool_brush;
	acceptTool[Data::Type_Picture][ti++]=Tool_brushEraser;
	actingTool[Data::Type_Picture][0]=tool[Tool_enclosure];
	actingTool[Data::Type_Picture][1]=tool[Tool_enclosure];
	actingTool[Data::Type_Picture][2]=tool[Tool_mover];
	toolIndex[Data::Type_Picture]=Tool_enclosure;
	
	ti=0;
	acceptTool[Data::Type_Text][ti++]=Tool_mover;
	actingTool[Data::Type_Text][0]=tool[Tool_mover];
	actingTool[Data::Type_Text][1]=tool[Tool_mover];
	actingTool[Data::Type_Text][2]=tool[Tool_mover];
	toolIndex[Data::Type_Text]=Tool_mover;
	
	ti=0;
	acceptTool[Data::Type_Sound][ti++]=Tool_mover;
	actingTool[Data::Type_Sound][0]=tool[Tool_mover];
	actingTool[Data::Type_Sound][1]=tool[Tool_mover];
	actingTool[Data::Type_Sound][2]=tool[Tool_mover];
	toolIndex[Data::Type_Sound]=Tool_mover;
	
	brushRadius=30;
	brushDeep=32767*5;
	brushInner=1024*4/5;
	//response(ResponseIndex_brushRadius);
	}
void	destroyTool()
	{
	for(int di=0;di<Data::Type_count;di++)
		{
		dfree(actingTool[di]);
		dfree(acceptTool[di]);
		}
	dfree(toolIndex);
	dfree(actingTool);
	dfree(acceptTool);
	for(int ti=0;ti<Tool_count;ti++)
		delete tool[ti];
	dfree(tool);
	}



void	setTool(int mi,int ti)
	{
	argCheck(mi<0||mi>=3||ti<0||ti>=Tool_count);
	
	int i;
	for(i=0;i<Tool_count;i++)
		if(acceptTool[dataType][i]==ti)
			break;
	if(i==Tool_count)
		error("!accept");
	
	actingTool[dataType][mi]=tool[ti];
	if(mi==0)
		toolIndex[dataType]=ti;

	refreshWindow(manager->toolbar);
	//if(manager->toolbar!=NULL)
	//	manager->toolbar->reShow();
	manager->reShow();
	}










	Tool::Tool(Picture *p,int st,Sensor *l,Sensor *r,Sensor *m)
	{
	argCheck(l==NULL);
	pic=p;
	senseType=st;
	sensor[0]=l;
	sensor[1]=r;
	sensor[2]=m;
	}

	Tool::~Tool()
	{
	for(int i=0;i<3;i++)
		if(sensor[i]!=NULL)
			delete sensor[i];
	}

Picture*	Tool::getPic()
	{
	return pic;
	}

Sensor*	Tool::getSensor(int i)
	{
	argCheck(i<0||i>=3);
	if(sensor[i]==NULL)
		i=0;

	switch(senseType)
		{
	case bitmapDrawing:
		if(!editingLayer->bitmapEditable())
			return NULL;
		break;
		}
	return sensor[i];
	}


	
//==========================================================================


	Toolbar::Toolbar():Window(Position(0,48),DSData->getPic(BDF_Window),Size(160,160))
	{
	SENSE->left=new MoveSensor();
	SENSE->right=new SuicideSensor();
	addObject(toolButton);

	for(int di=0;di<Data::Type_count;di++)
	//for(int i=0;i<toolMax[di];i++)
		//toolButton[di].addItem(new ToolButton(Position(2,22)+Coord(i%4,i/4)*Coord(20,20),tool[di][i]));
		;//toolButton[di].addItem(new Button(Position(2,22)+Coord(i%4,i/4)*Coord(20,20),buttonData,Size(16,16),tool[di][i]->getPic(),i+10,i+110));
	//toolButton.setPage(dataType);
	}
	Toolbar::~Toolbar()
	{
	toolButton.clear();
	}

void	Toolbar::refresh()
	{
	etcObject.clear();
	toolButton.clear();
	for(int i=0;i<Tool_count;i++)
		{
		int ti=acceptTool[dataType][i];
		if(ti==0)
			break;
		toolButton.addItem(new Button(Position(2,22)+Coord(i%4,i/4)*Coord(20,20),buttonData,Size(16,16),tool[ti]->getPic(),ti+10,ti+110));
		}
	report("refresh %d",toolIndex[dataType]);
	switch(toolIndex[dataType])
		{
	case Tool_brush:
		addEtcObject(new Scroll(Position(12,100),Size(1320,16),DSData->getPic(BDF_Scroll),brushRadius,1,128,ResponseIndex_brushRadius));
		addEtcObject(new Scroll(Position(12,120),Size(1320,16),DSData->getPic(BDF_Scroll),brushInner,0,1023,ResponseIndex_brushRadius));
		report("refresh2");
		break;
	default:
		break;
		}
	}

void	Toolbar::input()
	{
	if(keySignal.down(KEY_X))
		kill();
	}

void	Toolbar::response(int ri)
	{
	if(ri>=10&&ri<200)
		{
		setTool(ri/100,ri%100-10);
		}
	switch(ri)
		{
	case ResponseIndex_brushRadius:
		brushDeep=32767*1024/(1024-brushInner);
		break;
		}
	}



void	Toolbar::show()
	{
	}


