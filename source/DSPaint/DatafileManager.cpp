




	DatafileManager::DatafileManager():Window(Position(0,208),DSData->getPic(BDF_Window),Size(16+128+16,16+120+16))
	{
	if(fileData==NULL)
		error("No datafile");
	SENSE->left=new MoveSensor();
	dataBlock=new LinkedList<DataBlock>;
	addEtcObject(menu=new Menu(Position(16,16),Size(128,120),(LinkedList<MenuObject>*)dataBlock));
	menu->setDirection(Menu::vertical);
	clickTime=0;
	refresh();
	}

	DatafileManager::~DatafileManager()
	{
	}

void	DatafileManager::input()
	{
	if(keySignal.down(KEY_HOME))
		setData(0);
	if(keySignal.down(KEY_END))
		setData(-1);
	if(keySignal.down(KEY_UP))
		setData(dataIndex-1);
	if(keySignal.down(KEY_DOWN))
		setData(dataIndex+1);
		
	if(keySignal.down(KEY_UP)||keySignal.down(KEY_DOWN))
		clickTime=clock();
	if(keySignal.press(KEY_UP)||keySignal.press(KEY_DOWN))
		while((clock()-clickTime)*1000/CLOCKS_PER_SEC>500)
			{
			if(keySignal.press(KEY_UP))
				setData(dataIndex-1);
			else
				setData(dataIndex+1);
			clickTime+=100;
			}
	}

void	DatafileManager::rclick(Coord c)
	{
	kill();
	return;
	Popup *popup=new Popup(getPosition()+c,this);
	/*popup->addFolder("Add data");
		popup->addContext("Picture",DatafileProcess_addPicture);
		popup->addContext("Text",DatafileProcess_addText);
		popup->addContext("Sound",DatafileProcess_addSound);
		popup->endFolder();*/
	popup->addContext("Close",1);
	popup->endFolder();
	addWindow(popup);
	}

void	DatafileManager::response(int index)
	{
	switch(index)
		{
	case 1:
		kill();
		break;
		}
	addProcessStep(index);
	}

void	DatafileManager::refresh()
	{
	menu->refreshStart();
	for(int i=0;i<fileData->getNumber();i++)
		{
		DataBlock *now=(DataBlock *)menu->refreshNext(fileData->getData(i));
		if(now==NULL)
			menu->refreshAdd(now=new DataBlock(i));
		now->setIndex(i);
		now->reShow();
		}
	menu->refreshEnd();
	menu->fitSize();
	
	reSize(menu->getSize()+Coord(16+16,16+16));
	}












	DatafileManager::DataBlock::DataBlock(int i):MenuObject(DSData->getPic(BDF_DataButton),Size(192,24))
	{
	index=i;
	data=fileData->getData(index);
	downClick=false;
	}

	DatafileManager::DataBlock::~DataBlock()
	{
	}

bool	DatafileManager::DataBlock::isData(void *d)const
	{
	return d==data;
	}

void	DatafileManager::DataBlock::setIndex(int i)
	{
	index=i;
	if(data!=fileData->getData(index))
		error("Wrong data.");
	}

void	DatafileManager::DataBlock::lclick(Coord c)
	{
	setData(index);
	}

void	DatafileManager::DataBlock::rclick(Coord c)
	{
	Popup *p=new Popup(getPosition()+c,this);
	p->addFolder("Add");
		p->addContext("Picture",ResponseIndex_add+1);
		p->addContext("Text",ResponseIndex_add+2);
		p->addContext("Sound",ResponseIndex_add+3);
		p->endFolder();
	p->addContext("Raname",ResponseIndex_rename);
	p->addContext("Copy",ResponseIndex_clone);
	p->addContext("Delete",ResponseIndex_delete);
	p->endFolder();
	addWindow(p);
	downClick=c.y>getSize().y/2;
	}

void	DatafileManager::DataBlock::response(int ri)
	{
	switch(ri)
		{
	case ResponseIndex_add+1:
		stepContainer->addStep(new AddDataStep(new Picture(Coord(64,64),blue),index+downClick));
		break;
	case ResponseIndex_add+2:
		stepContainer->addStep(new AddDataStep(new Picture(Coord(64,64),red),index+downClick));
		break;
	case ResponseIndex_add+3:
		stepContainer->addStep(new AddDataStep(new Picture(Coord(64,64),green),index+downClick));
		break;
	case ResponseIndex_clone:
		stepContainer->addStep(new AddDataStep(fileData->getData(index)->clone(),index+downClick));
		break;
	case ResponseIndex_delete:
		stepContainer->addStep(new AddDataStep(fileData->getData(index),index,true));
		break;
	
	
	case ResponseIndex_rename:
		addWindow(new TextPopup(this,getPosition(),200,100,10));
		break;
	case 10:
		{
		extern TextPopup *textPopup;
		if(textPopup->getString()[0]!='\0')
			stepContainer->addStep(new SetDataNameStep(data,textPopup->getString()));
		break;
		}
		}
	}

void	DatafileManager::DataBlock::show()
	{
	ConstString str=format("%c%2d %S",index==dataIndex?'>':' ',index,data->getName());
	font->putString(wBuffer,str,Position(0,0));
	}



