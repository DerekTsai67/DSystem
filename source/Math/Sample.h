



struct Sample{
	public:
	Sample(double x,double y);
	Sample(int ym);
	Sample(int xm,int ym);
	Sample(const Sample &s);
	//Sample(int c,...);
	~Sample();
void	init();
Sample*	clone();
void	setAxis(int axis,double d);
double	getAxis(int si,int axis=0);
double	getX(int xi=0);
double	getY(int yi=0);
double	*data;
int		xMax,yMax,max;
	};


class SampleMeasure:public Measure{
	public:
	SampleMeasure();
	SampleMeasure(const SampleMeasure &sm);
	~SampleMeasure();
SampleMeasure&	operator=(const SampleMeasure &sm);
SampleMeasure	clone(int min=0,int max=-1);
SampleMeasure	getSingleAxis(int sampleIndex);
void	clear();


void	addSample(Sample *s);
Sample*	getSample(int i);
double	getX(int i,int xi=0);
double	getY(int i,int yi=0);
int		getNumber();

double	getAmplitude(int si);
double	getBiggestAmplitude(int rank=0,bool back=false,int sampleIndex=1);

void	plus(const Polynomial *p,int si=1);
void	minus(const Polynomial *p,int si=1);
void	times(double d,int si=1);
void	resetAxis();

SampleMeasure	operator+(const Polynomial &p);
SampleMeasure	operator-(const Polynomial &p);
SampleMeasure	operator*(double d);


Polynomial		getRegress(int rank=10,int min=0,int max=-1);
SampleMeasure	getDiffer();
SampleMeasure	getPeakSample(int sampleIndex=1);
SampleMeasure	blurMax(double radius,int range,int si=1);
SampleMeasure	blurMin(double radius,int range,int si=1);
SampleMeasure	intergral(double zeroPoint,int si=0,bool minus=false);

void	fitScaler(Scaler *s,int a);
void	fitScaler(Scaler *s,int i,int a);
void	show(Picture *buffer,Scaler *xScaler,Scaler *yScaler,DS_RGB color);
void	show(Picture *buffer,int xi,int yi,Scaler *xScaler,Scaler *yScaler,DS_RGB color,int type=0);
void	show2D(Picture *buffer,int xi,int yi,int zi,Scaler *xScaler,Scaler *yScaler,Scaler *zScaler,int type=0);
	private:
Array<Sample*>	data;
	};




