



	ConstString::ConstString()
	{
	length=0;
	data=NULL;
	}
	ConstString::ConstString(const ConstString &str)
	{
	data=str.data;
	length=str.length;
	}
	ConstString::ConstString(const char *raw)
	{
	if(raw==NULL)
		{
		//error("NULL string");
		length=0;
		data=NULL;
		}
	else
		{
		length=getStringLength(raw);
		data=raw;
		}
	}
	ConstString::ConstString(const ConstString &str,int len)
	{
	if(len>str.getLength())
		error("Length overflow.");
	data=str.data;
	length=len;
	}
	ConstString::ConstString(const char *s,int len)
	{
	argCheck(s==NULL||len<0)
	data=s;
	length=len;
	}


	ConstString::~ConstString()
	{
	}






char	ConstString::operator[](int index)
	{
	return data[index];
	}
ConstString	ConstString::operator+(int index)
	{
	return ConstString(data+index,length-index);
	}
ConstString	ConstString::operator-(int index)
	{
	return ConstString(data,length-index);
	}
ConstString	ConstString::cut(int index)
	{
	return ConstString(data,index);
	}
void	ConstString::operator+=(int index)
	{
	data+=index;
	length-=index;
	}
void	ConstString::operator-=(int index)
	{
	length-=index;
	}

bool	ConstString::operator==(const char *s)
	{
	//report("==* %08X",s);
	//if(s==NULL)
	//	return data==NULL;
	return (*this)==ConstString(s);
	}
bool	ConstString::operator!=(const char *s)
	{
	//report("!=* %08X",s);
	//if(s==NULL)
	//	return data!=NULL;
	return (*this)!=ConstString(s);
	}

bool	ConstString::operator==(const ConstString &s)
	{
	//report("==& %08X",s.data);
	if(length!=s.length)
		return false;
	if(data==s.data)
		return true;
	if(data==NULL||s.data==NULL)
		return false;
	for(int i=0;i<length;i++)
		if(data[i]!=s.data[i])
			return false;
	return true;
	}
bool	ConstString::operator!=(const ConstString &s)
	{
	//report("!=& %08X",s.data);
	return !(*this==s);
	}
	ConstString::operator const char*()
	{
	if(data==NULL)
		error("NULL string.");
	if(data[length]!=0)
		error("Not zero ended.");
	return data;
	}
const char*	ConstString::getRaw()const
	{
	if(data==NULL)
		error("NULL string.");
	return data;
	}
int		ConstString::getLength()const
	{
	return length;
	}


int		ConstString::search(ConstString candi,bool back)
	{
	if(!back)
		{
		for(int i=0;i<length;i++)
			for(int j=0;j<candi.getLength();j++)
				if(data[i]==candi[j])
					return i;
		}
	else
		{
		for(int i=length-1;i>=0;i--)
			for(int j=0;j<candi.getLength();j++)
				if(data[i]==candi[j])
					return i;
		}
	return -1;
	}
	
int		ConstString::searchNot(ConstString candi,bool back)
	{
	if(!back)
		{
		for(int i=0;i<length;i++)
			if(candi.search(ConstString(data+i,1))==-1)
				return i;
		}
	else
		{
		for(int i=length-1;i>=0;i--)
			if(candi.search(ConstString(data+i,1))==-1)
				return i;
		}
	return -1;
	}

void	ConstString::takeSpace(ConstString candi)
	{
	int searchPos=searchNot(candi);
	if(searchPos==-1)
		return;
	*this+=searchPos;
	}
	
bool	ConstString::takeVerify(ConstString v)
	{
	if(!verify(*this,v))
		return false;
	*this+=v.getLength();
	return true;
	}

/*String	ConstString::copy()
	{
	char *nd=dalloc<char>(length+1);
	memcpy(nd,data,length);
	return String(nd,length);
	}*/

void	ConstString::print()
	{
	fwrite(data,1,length,stdout);
	}







	String::String():ConstString()
	{
	data=nonConstData=NULL;
	length=0;
	del=true;
	}
	String::String(int len):ConstString(nonConstData=dalloc<char>(len+1),len)
	{
	del=true;
	}
	String::String(const char *raw):ConstString(raw)
	{
	data=nonConstData=dclone<char>(raw,length+1);
	del=true;
	}
	String::String(char *d,int len):ConstString(d,len)
	{
	argCheck(d==NULL||len<0);
	data=nonConstData=d;
	length=len;
	del=true;
	}
	String::String(const Array<char> &arr):ConstString()
	{
	data=nonConstData=arr.cloneAlloc();
	length=arr.getNumber();
	del=true;
	}
	String::String(const String &str):ConstString()
	{
	error("Copying String.");
	}

	String::~String()
	{
	if(nonConstData!=NULL)
	if(del)
		dfree(nonConstData);
	}


char&	String::operator[](int index)
	{
	return nonConstData[index];
	}
	
void	String::cut(int index)
	{
	if(length<index)
		error("cut longer.");
	if(index<length)
		nonConstData[index]=0;
	length=index;
	}
char*	String::getRaw()
	{
	argCheck(nonConstData==NULL);
	return nonConstData;
	}

void	String::copyFrom(ConstString str)
	{
	if(length<str.getLength())
		{
		if(!del)
			error("!del.");
		if(nonConstData!=NULL)
			dfree(nonConstData);
		data=nonConstData=dalloc<char>(str.getLength()+1);
		}
	memcpy(nonConstData,str.getRaw(),str.getLength());
	length=str.getLength();
	}



void	String::setDelete()
	{
	//del=true;
	}

void	String::regisTemp()
	{
	if(nonConstData!=NULL)
		::regisTemp(nonConstData);
	del=false;
	}
/*void	String::free()
	{
	if(nonConstData!=NULL)
		dfree(nonConstData);
	data=nonConstData=NULL;
	length=0;
	}*/






