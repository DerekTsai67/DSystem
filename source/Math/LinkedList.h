#define FOR_LL_CONST_BACK(x,ll,type,back) \
			{\
			const LinkedList<type> &ll_=(ll);\
			const bool back_=back;\
			if(ll_.getHead(back_)!=NULL)\
				{\
				const List< type > *now_=ll_.getHead(back_);\
				while(now_!=NULL)\
					{const type *x=NULL;\
					x=now_->data;

#define FOR_LL_CONST(x,ll,type) \
			{\
			const LinkedList<type> &ll_=(ll);\
			const bool back_=false;\
			if(ll_.getHead()!=NULL)\
				{\
				const List< type > *now_=ll_.getHead();\
				while(now_!=NULL)\
					{const type *x=NULL;\
					x=now_->data;


#define FOR_LL_BACK(x,ll,type,b) \
			{\
			LinkedList<type> &ll_=(ll);\
			bool back_=b;\
			if(ll_.getHead(back_)!=NULL)\
				{\
				List< type > *now_=ll_.getHead(back_);\
				while(now_!=NULL)\
					{type *x=NULL;\
					x=now_->data;


#define FOR_LL(x,ll,type) \
			{\
			LinkedList<type> &ll_=(ll);\
			const bool back_=false;\
			if(ll_.getHead()!=NULL)\
				{\
				List< type > *now_=ll_.getHead();\
				while(now_!=NULL)\
					{type *x=NULL;\
					x=now_->data;

#define CONTINUE_LL	{now_=now_->near[back_];\
					continue;}
#define DELECON_LL(Type)	{List<Type> *temp=now_->near[back_];\
					ll_.deleteItem(now_);\
					now_=temp;\
					continue;\
					}
#define DELECON_LL_ND(Type,nd)	{List<Type> *temp=now_->near[back_];\
					ll_.deleteItem(now_,nd);\
					now_=temp;\
					continue;\
					}
#define END_FOR_LL \
					now_=now_->near[back_];}\
				}\
			}

template<class Type>
struct List{
	Type	*data;
	List<Type>	*near[2];
	List<Type>	*&next,*&last;
	List():next(near[0]),last(near[1]){}
	};

template<class Type>
class LinkedList{
   public:
	 LinkedList(bool noDelete=false);
	 LinkedList(Type *arr,int num,bool noDelete=false);
virtual	~LinkedList();
bool	operator==(LinkedList<Type> l);
Type*	operator[](int i);
Type*	getItem(int i,bool back=false);
void	setNoDel(bool b);
virtual	List<Type>*	getHead(bool back=false)const;
virtual	void	act();
int 	getNumber();
int 	getIndex(Type *target);
List<Type>*	getList(Type *target);
void	addItem(Type *newItem,bool back=false);
void	addItem(Type *newItem,List<Type> *l,bool back=false);
void	deleteItem(Type *delItem,int nd=-1);
void	deleteItem(List<Type> *delItem,int nd=-1);
void	cut(List<Type> *t,bool back);
void	move(List<Type> *des,List<Type> *src,bool back);
void	sort(Compare compare);
void	clear();
void	kill();
bool	empty();
    protected:
void	quickRec(Compare compare,List<Type> *&head,List<Type> *&tail);
List<Type>	*border[2];
List<Type>	*&head,*&tail;
bool	noDel;
int 	num;
	};


template<class Type>
 	LinkedList<Type>::LinkedList(bool b):head(border[0]),tail(border[1])
	{
	head=tail=NULL;
	noDel=b;
	num=0;
	}

template<class Type>
	LinkedList<Type>::LinkedList(Type *arr,int ai,bool noDelete):head(border[0]),tail(border[1])
	{
	if(arr==NULL||ai<1)
		error("construct LL with NULL array.");

	head=new List<Type>;
	head->data=arr;
	head->last=NULL;
	List<Type> *l=head;
	for(int i=1;i<ai;i++)
		{
		List<Type> *n=new List<Type>;
		n->data=arr+i;
		n->last=l;
		l->next=n;
		l=n;
		}
	l->next=NULL;
	tail=l;
	num=ai;
	noDel=true;
	}


template<class Type>
 	LinkedList<Type>::~LinkedList()
	{
	clear();
	}

template<class Type>
List<Type>*	LinkedList<Type>::getHead(bool back)const
	{
	if(!back)
		return head;
	else
		return tail;
	}

template<class Type>
void	LinkedList<Type>::setNoDel(bool b)
	{
	noDel=b;
	}

template<class Type>
void	LinkedList<Type>::act()
	{
	}

template<class Type>
Type*	LinkedList<Type>::operator[](int index)
	{
	return this->getItem(index);
	}

template<class Type>
Type*	LinkedList<Type>::getItem(int index,bool back)
	{
	if(index<0||index>=getNumber())
		error("LinkedList::getItem overflow. [%d]",index);

	if(index>getNumber()/2)
		{
		index=getNumber()-1-index;
		back=!back;
		}
	List<Type> *now=getHead(back);
	for(int i=0;i<index;i++)
		{
		if(now==NULL)
			error("LL overflow.");
		now=now->near[back];
		}
	if(now==NULL)
		error("LL overflow.");
	return now->data;
	}

template<class Type>
int 	LinkedList<Type>::getNumber()
	{
	int ret=0;
	List<Type> *now=getHead();
	while(now!=NULL)
		{
		ret++;
		now=now->next;
		}
	//return ret;
	if(ret!=num)
		error("ret!=num %d %d",ret,num);
	return num;
	}

template<class Type>
int 	LinkedList<Type>::getIndex(Type *target)
	{
	int i=0;
	FOR_LL(now,*this,Type)
		if(now==target)
			return i;
		i++;
	END_FOR_LL
	return -1;
	}

template<class Type>
List<Type>*	LinkedList<Type>::getList(Type *target)
	{
	FOR_LL(now,*this,Type)
		if(now==target)
			return now_;
	END_FOR_LL
	return NULL;
	}

template<class Type>
void	LinkedList<Type>::addItem(Type *newItem,bool back)
	{
	addItem(newItem,getHead(back),!back);
	}

template<class Type>
void	LinkedList<Type>::addItem(Type *newItem,List<Type> *list,bool back)
	{
	List<Type> *n=new List<Type>;
	if(n==NULL)//return false;
		error("LL::addItem new List failed.");
	n->data=newItem;
	if(list==NULL)
		{
		list=getHead(back);
		back=!back;/*
		error("LL:addItem list==NULL.");
		n->next=head;
		head=n;*/
		}

	if(head==NULL)
		{
		head=tail=n;
		n->next=n->last=NULL;
		}
	else
		{
		n->near[back]=list->near[back];
		if(list->near[back]!=NULL)
			list->near[back]->near[!back]=n;
		else
			border[!back]=n;
		list->near[back]=n;
		n->near[!back]=list;
		}
	num++;
	}


template<class Type>
void	LinkedList<Type>::deleteItem(Type *target,int nd)
	{
	if(target==NULL||head==NULL)
		error("LL::deleteItem NULL");
	List<Type> *del=NULL;
	FOR_LL(now,*this,Type)
		if(now==target)
			{
			del=now_;
			break;
			}
	END_FOR_LL
	if(del==NULL)
		error("not found.");
	deleteItem(del,nd);
	}

template<class Type>
void	LinkedList<Type>::deleteItem(List<Type> *target,int nd)
	{
	if(target==NULL||head==NULL)
		error("LL::deleteItem NULL");
	if(nd==-1)
		nd=noDel;
	//report(1,"LL deleting item%s...",(nd?" without del":""));

	for(int back=0;back<2;back++)
		{
		if(target->near[back]!=NULL)
			target->near[back]->near[!back]=target->near[!back];
		else
			border[!back]=target->near[!back];
		}
	if(!nd)
		delete target->data;
	delete target;
	num--;
	//report(-1,"deleted.");
	}

template<class Type>
void	LinkedList<Type>::cut(List<Type> *t,bool back)
	{
	int i=0;
	FOR_LL_BACK(now,*this,Type,back)
		now=NULL;
		if(now_->near[!back]!=NULL)
			{
			if(!noDel)
				delete now_->near[!back]->data;
			delete now_->near[!back];
			}
		if(now_==t)
			break;
		i++;
	END_FOR_LL
	if(i==num)
		error("LL::cut with list not exist.");
	border[back]=t;
	t->near[!back]=NULL;
	num-=i;
	}

template<class Type>
void	LinkedList<Type>::move(List<Type> *des,List<Type> *src,bool back)
	{
	if(des==NULL||src==NULL)
		error("LL::move with NULL argument.");
	if(des==src)
		return;
	for(int i=0;i<2;i++)
		{
		if(src->near[i]!=NULL)
			src->near[i]->near[!i]=src->near[!i];
		else
			border[!i]=src->near[!i];
		}
	if(des->near[!back]!=NULL)
		des->near[!back]->near[back]=src;
	else
		border[back]=src;
	src->near[!back]=des->near[!back];
	des->near[!back]=src;
	src->near[back]=des;
	}

template<class Type>
void	LinkedList<Type>::sort(Compare compare)
	{
	if(head==NULL)
		return;
	quickRec(compare,head,tail);
	List<Type> *now=head;
	head->last=NULL;
	tail->next=NULL;
	while(now->next!=NULL)
		{
		now->next->last=now;
		now=now->next;
		}
	}

template<class Type>
void	LinkedList<Type>::quickRec(Compare compare,List<Type> *&head,List<Type> *&tail)
	{
	if(head==NULL||tail==NULL)
		error("LL::quickRec with NULL boundary.");
	List<Type> *begin[3],*end[3]={NULL,NULL,NULL},*now=head;
	begin[1]=end[1]=head;
	while(now!=tail)
		{
		now=now->next;
		int index=compare(now->data,head->data)+1;
		if(end[index]==NULL)
			begin[index]=end[index]=now;
		else
			{
			end[index]->next=now;
			end[index]=now;
			}
		}
	if(end[0]!=NULL)
		{
		quickRec(compare,begin[0],end[0]);
		head=begin[0];
		end[0]->next=begin[1];
		}
	else
		head=begin[1];
	if(end[2]!=NULL)
		{
		quickRec(compare,begin[2],end[2]);
		end[1]->next=begin[2];
		tail=end[2];
		}
	else
		tail=end[1];
	}

template<class Type>
void	LinkedList<Type>::clear()
	{
	List<Type> *now=head;
	while(now!=NULL)
		{
		List<Type> *temp=now->next;
		if(!noDel)
			delete now->data;
		delete now;
		now=temp;
		}
	head=tail=NULL;
	num=0;
	}

template<class Type>
void	LinkedList<Type>::kill()
	{
	List<Type> *now=head;
	while(now!=NULL)
		{
		extern LinkedList<Object>	*trashObject;
		List<Type> *temp=now->next;
		trashObject->addItem(now->data);
		delete now;
		now=temp;
		}
	head=tail=NULL;
	num=0;
	extern bool	trashSignatureShow;
	trashSignatureShow=false;
	}

template<class Type>
bool	LinkedList<Type>::empty()
	{
	return getHead()==NULL;
	}


template<class Type>
bool	LinkedList<Type>::operator==(LinkedList<Type> l)
	{
	List<Type> *n1,*n2;
	if(num!=l.num)
		return false;
	n1=getHead();n2=l.getHead();
	if(n1==NULL)
		return n2==NULL;
	if(n2==NULL)
		return false;
	while((n1!=NULL)&&(n2!=NULL))
		{
		if((n1->data)!=(n2->data))
			return false;
		n1=n1->next;n2=n2->next;
		}
	return n1==NULL&&n2==NULL;
	}




