




struct Neuron{
	double	input,output;
	double	bias,*weight;
	double	coe,dBias,*dWeight;
	int		weightStart,weightEnd;
	};

class NeuralNetwork{
	public:
	NeuralNetwork(int inNum,int hidNum,int outNum);
	~NeuralNetwork();

void	setInput(int index,double value);
double	getOutput(int index);
void	setAnswer(int index,double value);
void	setTrainingData(double **in,double **out,int num);
void	freeTrainingData();
void	calValue();
int		getHighestOutputIndex();
double	getCost();
double	getEntropyCost();
void	trainData(int epoch,int batchSize,double learningLevel,double regularLevel);

void	saveNN(ConstString path);
void	loadNN(ConstString path);
	private:
Neuron	*neuron;
double	*answer;
int		inputMax,hiddenMax,outputMax,totalMax;
int		trained;

double	**trainInput,**trainOutput,**validInput,**validOutput,**testInput,**testOutput;
int		trainMax,validMax,testMax;
double	**dWeight;
void	train();

bool	useEntropy;
	};














class NeuralLayer{
	public:
	NeuralLayer(int am,int bm=1,int cm=1,int dm=1);
virtual	~NeuralLayer();
void	initNeuron(int fromNum,int neuronMax=-1);
	

void	setInputArray(double *in,int inNum=-1);
virtual void	calOutput()=0;
Neuron&	getNeuron(int ai,int bi,int ci=0,int di=0);
Neuron&	getNeuron(int index);
double	getOutput(int ai,int bi,int ci=0,int di=0);
double	getOutput(int index);
int		getNumber();
virtual int		getInputNumber()=0;
int		getHighestOutputIndex();
double	getCost(int ans);

void			train(double ***in,int *inMax,int inNum,int outNum,int sampleNum,int epochMax,int batchSize,double learningLevel,double regularLevel);
virtual void	preBatch(double regularLevel);
virtual void	preTrain(double *in,int& inIndex);
void			trainSample(double *in,int ans);
virtual void	postTrain();
virtual void	postBatch(double learningLevel);

virtual void	printStructure(char *str,int& index)=0;
	//protected:
int		aMax,bMax,cMax,dMax;
Neuron	*neuron;
int		max;
	};
	
	
	
class InputNeuralLayer:public NeuralLayer{
	public:
	InputNeuralLayer(int am,int bm=1,int cm=1,int dm=1);
	~InputNeuralLayer();

void	setInput(double in,int ai,int bi=0,int ci=0,int di=0);
void	calOutput();
int		getInputNumber();

void	preTrain(double *in,int& inIndex);

void	printStructure(char *str,int& index);
	};

class FeatureNeuralLayer:public NeuralLayer{
	public:
	FeatureNeuralLayer(NeuralLayer *nl,int am,int bm,int cm,int dm);
	~FeatureNeuralLayer();

void	calOutput();
int		getInputNumber();

void	preBatch(double regularLevel);
void	preTrain(double *in,int& inIndex);
void	postTrain();
void	postBatch(double learningLevel);

void	printStructure(char *str,int& index);

	private:
NeuralLayer	*from;
int		fromNumber;
int		aSize,bSize,cSize,dSize;
//double	bias,*weight;
//double	coe,dBias,*dWeight;
	};
	
	
class FullNeuralLayer:public NeuralLayer{
	public:
	FullNeuralLayer(int num,NeuralLayer *fa,NeuralLayer *fb=NULL,NeuralLayer *fc=NULL,NeuralLayer *fd=NULL);
	~FullNeuralLayer();


void	calOutput();
int		getInputNumber();

void	preBatch(double regularLevel);
void	preTrain(double *in,int& inIndex);
void	postTrain();
void	postBatch(double learningLevel);

void	printStructure(char *str,int& index);

	private:
Array<NeuralLayer*>	fromLayer;
int		fromNumber;
	};




















