







	Sample::Sample(double x,double y)
	{
	xMax=1;
	yMax=1;
	init();
	data[0]=x;
	data[1]=y;
	}
	
	Sample::Sample(int ym)
	{
	xMax=1;
	yMax=ym;
	init();
	}
	
	Sample::Sample(int xm,int ym)
	{
	xMax=xm;
	yMax=ym;
	init();
	}
	
	Sample::Sample(const Sample &s)
	{
	xMax=s.xMax;
	yMax=s.yMax;
	max=s.max;
	data=dclone<double>(s.data,max);
	}
void	Sample::init()
	{
	max=xMax+yMax;
	data=dalloc<double>(max);
	}

Sample*	Sample::clone()
	{
	return new Sample(*this);
	}
	
	/*Sample::Sample(int c,...)
	{
	max=c+1;
	data=dalloc<double>(max);
	
	va_list va;
	va_start(va,c);
	for(int i=0;i<max;i++)
		{
		data[i]=va_arg(va,double);
		report("arg[%d]=%f",i,data[i]);
		}
	va_end(va);
	}*/
	
	Sample::~Sample()
	{
	dfree(data);
	}
	
void	Sample::setAxis(int axis,double d)
	{
	argCheck(axis<0||axis>=max);
	data[axis]=d;
	}
	
double	Sample::getAxis(int si,int axis)
	{
	if(axis==0)
		return data[si];
	else
		return getY(si);
	}
double	Sample::getX(int xi)
	{
	argCheck(xi<0||xi>=xMax);
	return data[xi];
	}
double	Sample::getY(int yi)
	{
	argCheck(yi<0||yi>=yMax);
	return data[yi+xMax];
	}




	Measure::Measure()
	{
	}
	Measure::~Measure()
	{
	}
double	Measure::getAmplitude(int si)
	{
	error("Not implemented.");
	return 0.0;
	}
void	Measure::fitScaler(Scaler *s,int a)
	{
	}


//================================================================================================

	SampleMeasure::SampleMeasure():data(300)
	{
	}
	
	SampleMeasure::SampleMeasure(const SampleMeasure &sm):data(sm.data.getNumber())
	{
	FOR_ARRAY(i,sm.data)
		data.addItem(sm.data.getItem(i)->clone());
	}

	SampleMeasure::~SampleMeasure()
	{
	clear();
	}

SampleMeasure	SampleMeasure::clone(int min,int max)
	{
	if(max==-1)
		max=data.getNumber();
	takeMax(min,0);
	takeMin(max,data.getNumber());
	argCheck(min<0||max>data.getNumber()||min>max);
	SampleMeasure	ret;
	for(int i=min;i<max;i++)
		ret.addSample(data.getItem(i)->clone());
	return ret;
	}
	
SampleMeasure&	SampleMeasure::operator=(const SampleMeasure &sm)
	{
	clear();
	FOR_ARRAY(i,sm.data)
		data.addItem(sm.data.getItem(i)->clone());
	return *this;
	}
	
SampleMeasure	SampleMeasure::getSingleAxis(int si)
	{
	SampleMeasure sm;
	FOR_ARRAY(i,data)
		{
		Sample *s=new Sample(data.getItem(i)->getAxis(0),data.getItem(i)->getAxis(si));
		sm.addSample(s);
		}
	return sm;
	}
	
void	SampleMeasure::clear()
	{
	data.deleteAll();
	}
	
void	SampleMeasure::addSample(Sample *s)
	{
	data.addItem(s);
	}
Sample*	SampleMeasure::getSample(int i)
	{
	return data.getItem(i);
	}
double	SampleMeasure::getX(int i,int xi)
	{
	return data.getItem(i)->getX(xi);
	}
double	SampleMeasure::getY(int i,int yi)
	{
	return data.getItem(i)->getY(yi);
	}
int		SampleMeasure::getNumber()
	{
	return data.getNumber();
	}

double	SampleMeasure::getAmplitude(int si)
	{
	argCheck(si<=0)
	double sum=0.0;
	FOR_ARRAY(x,data)
		sum+=pow(data.getItem(x)->getAxis(si),2.0);
	sum/=data.getNumber();
	sum=pow(sum,0.5);
	return sum;
	}

static int compareSampleIndex=1;
static int compareAbsSampleY(const void *a,const void *b)
	{
	Sample &A=**((Sample**)a);
	Sample &B=**((Sample**)b);
	double AY=abs(A.getAxis(compareSampleIndex));
	double BY=abs(B.getAxis(compareSampleIndex));
	return (AY>BY)-(AY<BY);
	}

double			SampleMeasure::getBiggestAmplitude(int rank,bool back,int si)
	{
	if(data.getNumber()<rank)
		return 0.0;
	Array<Sample*>	*s=data.clone();
	compareSampleIndex=si;
	s->sort(compareAbsSampleY,!back);
	compareSampleIndex=1;
	
	if(rank>=s->getNumber())
		{
		delete s;
		return 0.0;
		}
	double ret=abs(s->getItem(rank)->getAxis(si));
	delete s;
	return ret;
	}



void	SampleMeasure::plus(const Polynomial *p,int si)
	{
	argCheck(p==NULL||si<=0);
	FOR_ARRAY(i,data)
		data.getItem(i)->setAxis(si,data.getItem(i)->getAxis(si)+p->getY(data.getItem(i)->getAxis(0)));
	}
void	SampleMeasure::minus(const Polynomial *p,int si)
	{
	argCheck(p==NULL||si<=0);
	FOR_ARRAY(i,data)
		data.getItem(i)->setAxis(si,data.getItem(i)->getAxis(si)-p->getY(data.getItem(i)->getAxis(0)));
	}
void	SampleMeasure::times(double d,int si)
	{
	argCheck(si<=0);
	FOR_ARRAY(i,data)
		data.getItem(i)->setAxis(si,data.getItem(i)->getAxis(si)*d);
	}


void	SampleMeasure::resetAxis()
	{
	FOR_ARRAY(i,data)
		data.getItem(i)->setAxis(0,i);
	}




SampleMeasure	SampleMeasure::operator+(const Polynomial &p)
	{
	SampleMeasure sm=*this;
	sm.plus(&p,1);
	return sm;
	}
SampleMeasure	SampleMeasure::operator-(const Polynomial &p)
	{
	SampleMeasure sm=*this;
	sm.minus(&p,1);
	return sm;
	}
SampleMeasure	SampleMeasure::operator*(double d)
	{
	SampleMeasure sm=*this;
	sm.times(d,1);
	return sm;
	}

Polynomial	SampleMeasure::getRegress(int rank,int min,int max)
	{
	Polynomial p;
	p.regress(this,rank,min,max);
	return p;
	}
	
SampleMeasure	SampleMeasure::getDiffer()
	{
	return *this-getRegress();
	}
	
SampleMeasure	SampleMeasure::getPeakSample(int sampleIndex)
	{
	SampleMeasure ret;
	int n=0;
	FOR_ARRAY(i,data)
		{
		if(data[i]->getAxis(sampleIndex)*data[n]->getAxis(sampleIndex)<0)
			{
			ret.addSample(new Sample(data[n]->getAxis(0),data[n]->getAxis(sampleIndex)));
			n=i;
			}
		else
			if(abs(data[i]->getAxis(sampleIndex))>abs(data[n]->getAxis(sampleIndex)))
				n=i;
		}
	return ret;
	}
	
SampleMeasure	SampleMeasure::blurMax(double radius,int range,int si)
	{
	SampleMeasure ret=*this;
	FOR_ARRAY(i,data)
		{
		Sample *s0=getSample(i);
		int j=-range;
		if(j+i<0)
			j=-i;
		for(;j<=range&&i+j<ret.data.getNumber();j++)
			{
			//report("%f,%d,%d %d,%d,%d",radius,range,si,i,j,i+j);
			Sample *s1=ret.getSample(i+j);
			double y=s0->getAxis(si)*exp((double)-j*j/2/radius/radius);
			if(y>s1->getAxis(si))
				s1->setAxis(si,y);
			}
		}
	return ret;
	}
SampleMeasure	SampleMeasure::blurMin(double radius,int range,int si)
	{
	SampleMeasure ret=*this;
	FOR_ARRAY(i,data)
		{
		Sample *s0=getSample(i);
		int j=-range;
		if(j+i<0)
			j=-i;
		for(;j<=range&&i+j<ret.data.getNumber();j++)
			{
			//report("%f,%d,%d %d,%d,%d",radius,range,si,i,j,i+j);
			Sample *s1=ret.getSample(i+j);
			double y=s0->getAxis(si)*exp((double)j*j/2/radius/radius);
			if(y<s1->getAxis(si))
				s1->setAxis(si,y);
			}
		}
	return ret;
	}

SampleMeasure	SampleMeasure::intergral(double zeroPoint,int xi,bool back)
	{
	SampleMeasure ret;
	//int xm=data[0]->xMax;
	int ym=data[0]->yMax;
	
	double *now=dalloc<double>(ym);
	FOR_ARRAY(si,data)
		{
		double x=data[si]->getAxis(xi,0);
		if((!back)&&x>zeroPoint)
			break;
		if(back&&x>=zeroPoint)
			break;
		
		if(!back)
		for(int ai=0;ai<ym;ai++)
			now[ai]+=data[si]->getAxis(ai,1);
		
		if(back)
		for(int ai=0;ai<ym;ai++)
			now[ai]-=data[si]->getAxis(ai,1);
		}
	for(int i=0;i<ym;i++)
		now[i]*=-1;
		
	if(!back)
		{
		Sample *s=new Sample(ym);
		s->setAxis(0,-INFINITY);
		for(int ai=0;ai<ym;ai++)
			s->setAxis(1+ai,now[ai]);
		ret.addSample(s);
		}
	FOR_ARRAY(si,data)
		{
		Sample *s=new Sample(ym);
		if(!back)
		for(int ai=0;ai<ym;ai++)
			now[ai]+=data[si]->getAxis(ai,1);
			
		s->setAxis(0,data[si]->getAxis(0,0));
		for(int ai=0;ai<ym;ai++)
			s->setAxis(1+ai,now[ai]);
		ret.addSample(s);
		
		if(back)
		for(int ai=0;ai<ym;ai++)
			now[ai]-=data[si]->getAxis(ai,1);
		}
	if(back)
		{
		Sample *s=new Sample(ym);
		s->setAxis(0,INFINITY);
		for(int ai=0;ai<ym;ai++)
			s->setAxis(1+ai,now[ai]);
		ret.addSample(s);
		}

	dfree(now);
	return ret;
	}





void	SampleMeasure::fitScaler(Scaler *s,int a)
	{
	fitScaler(s,0,a);
	}

void	SampleMeasure::fitScaler(Scaler *s,int si,int a)
	{
	argCheck(s==NULL);
	for(int i=0;i<getNumber();i++)
		{
		double sample=getSample(i)->getAxis(si,a);
		if(sample==INFINITY||sample==-INFINITY)
			continue;
		s->fitSample(sample);
		}
	}

void	SampleMeasure::show(Picture *buffer,Scaler *xScaler,Scaler *yScaler,DS_RGB color)
	{
	show(buffer,0,0,xScaler,yScaler,color);
	}

void	SampleMeasure::show(Picture *buffer,int xi,int yi,Scaler *xScaler,Scaler *yScaler,DS_RGB color,int type)
	{
	for(int si=0;si<data.getNumber()-1;si++)
		{
		buffer->drawLineC(
			Position(
				xScaler->convert(getX(si,xi)	,buffer->getSize().x),
				yScaler->convert(getY(si,yi)	,buffer->getSize().y)),
			Position(
				xScaler->convert(getX(si+1,xi)	,buffer->getSize().x),
				yScaler->convert(getY(si+1,yi)	,buffer->getSize().y)),
			color,color);
		}
	for(int si=0;si<data.getNumber();si++)
		{/*
		buffer->drawCircle(
			Position(
				xScaler->convert(data.getItem(si)->getAxis(0)			,buffer->getSize().x),
				yScaler->convert(data.getItem(si)->getAxis(sampleIndex)	,buffer->getSize().y))
			,0,2
			,color);*/
		}
	}



void	SampleMeasure::show2D(Picture *buffer,int xi,int yi,int zi,Scaler *xScaler,Scaler *yScaler,Scaler *zScaler,int type)
	{
	//report("show2D");
	
	for(int si=0;si<data.getNumber();si++)
		{
		buffer->drawCircle(
			Position(
				xScaler->convert(getX(si,xi),buffer->getSize().x),
				yScaler->convert(getX(si,yi),buffer->getSize().y))
			,0,2
			,makeHWC(zScaler->convert(getY(si,zi),240),0,255));
		}
	}



