


int		toAngle(Coord w)
	{
	if(w==zeroCoord)
		return 0;
	if(w.x==0)
		return 90+(w.y<0)*180;
	int a=atan(abs((double)w.y/w.x))*180/PI;
	if(w.x<0)
		a=180-a;
	if(w.y<0)
		a=360-a;
	return a;
	}
/*
Coord rotate(Coord c,Angle r)
 {
 double ph=PI*r/128;
 return Coord((int)(c.x*cos(ph)-c.y*sin(ph)) , (int)(c.x*sin(ph)+c.y*cos(ph)));
 }
Coord randCoord(Coord a,Coord b)
 {
 Coord d=b-a;
 Coord r=Coord(rand()%d.x,rand()%d.y);
 return r+a;
 }


Coord speedEqualize(Coord w,int s)
{
if(w==Coord(0,0))
   return w;
double r=w.x*w.x+w.y*w.y;
r=s/pow(r,0.5);
w=w*r;
return w;
}

Coord speedCut(Coord w,int l,int h)
 {
 if(w.abs()>h && h!=0)
  return speedEqualize(w,h);
 if(w.abs()<l)
  return speedEqualize(w,l);
 return w;
 }


Coord exclude(Coord s,Coord t,int d)
 {
 int c=(s-t).abs();
 double a;
 if(d>c)
   a=64.0;
 else
   a=asin((double)d/c)*128.0/PI;
 return rotate(t-s,rand()%(int)(256.0-2*a)+a);
 }
*/
