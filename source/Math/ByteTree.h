

template<class Type>
class ByteNode{
	public:
	ByteNode();
	ByteNode(Type d);
	~ByteNode();
void	setData(Type d);
Type	getData()const;
void	setNext(ByteNode<Type> *n,Byte index);
ByteNode<Type>*	getNext(Byte *index,int num,bool create=false);
ByteNode<Type>*	getNext(Byte index,bool create=false);
	private:
Type		data;
ByteNode	***next;
	};





