
class Scaler;
class SampleMeasure;

class Measure{
	public:
	Measure();
virtual ~Measure();
virtual double	getAmplitude(int si);
virtual void	fitScaler(Scaler *s,int a);
virtual void	show  (Picture *buffer,Scaler *xScaler,Scaler *yScaler,DS_RGB color)=0;
//virtual void	show2D(Picture *buffer,int xi,int yi,int zi,Scaler *xScaler,Scaler *yScaler,Scaler *zScaler,int type=0)=0;
	private:
	};





class Polynomial:public Measure{
	public:
	Polynomial(double a=0.0);
	Polynomial(const Polynomial &p);
Polynomial*	clone();
Polynomial&	operator=(const Polynomial &p);
double	getY(double x)const;

void	plus(const Polynomial *p);
void	multiplicate(double d);

void	regress(SampleMeasure *m,int rank=10,int min=0,int max=-1);
void	linearRegress(SampleMeasure *m);


void	show(Picture *buffer,Scaler *xScaler,Scaler *yScaler,DS_RGB color);
	private:
Array<double>	coefficient;
	};


