


int		compareHuffInfoData(const void *A,const void *B);
int		compareHuffInfoLength(const void *A,const void *B);
int		compareHuffInfoCount(const void *A,const void *B);


struct HuffInfo{
	HuffInfo();
	int			data,length,count,code[30];
	HuffInfo	*left,*right,*next;
	};


struct HuffCode{
	int code[30],length;
	};


class HuffTree{
	public:
	 HuffTree(HuffInfo *huffInfo,int hi,int maxLength=99);
	~HuffTree();
void	countToLength(HuffInfo *huffInfo,int a,int b,int totalCount,int rank,int maxRank);
HuffInfo*	decodeHuffInfoLength(HuffInfo *huffInfo,int &hi,int rank);
void	buildDecoder(HuffInfo *now,int index,int rank);
void	buildBitLeftDecoder(HuffInfo *now,int index,int rank);
void	buildEncoder(HuffInfo *now,int *c,int cl);
void	clear(HuffInfo *n);
int 	getData(Input *in,int endianSetting=0);
void	putCode(int data,Output *o);
int 	getCodeLength(int data);
void	print();

	private:
void	printRec(char *printBuffer,int ci,HuffInfo *now);
int 	dataMax,decoderMax;
HuffInfo	*head,**decoder,**bitLeftDecoder;
HuffCode	*code;
	};


