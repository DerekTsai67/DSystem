



int		compareHuffInfoData(const void *A,const void *B)
	{
	HuffInfo *a=(HuffInfo*)A;
	HuffInfo *b=(HuffInfo*)B;
	return (a->data>b->data)-(a->data<b->data);
	}

int		compareHuffInfoLength(const void *A,const void *B)
	{
	HuffInfo *a=(HuffInfo*)A;
	HuffInfo *b=(HuffInfo*)B;
	if(a->length>b->length)return 1;
	if(a->length<b->length)return -1;
	return 0;
	}

int		compareHuffInfoCount(const void *A,const void *B)
	{
	HuffInfo *a=(HuffInfo*)A;
	HuffInfo *b=(HuffInfo*)B;
	if(a->count>b->count)return 1;
	if(a->count<b->count)return -1;
	return (a->data>b->data)-(a->data<b->data);
	}



	HuffInfo::HuffInfo():data(0),length(0),count(0),left(NULL),right(NULL),next(NULL)
		{
		for(int i=0;i<30;i++)
			code[i]=0;
		}




	HuffTree::HuffTree(HuffInfo *huffInfo,int hi,int maxLength)
	{
	argCheck(huffInfo==NULL||hi<1||maxLength<1);
	int lengthType=0,countType=0,codeType=0;
	dataMax=decoderMax=0;
	code=NULL;
	decoder=NULL;
	bitLeftDecoder=NULL;
	head=NULL;
	for(int i=0;i<hi;i++)
		{
		if(huffInfo[i].data<0)
			error("Negative HuffData.");
		if(huffInfo[i].data>dataMax)
			dataMax=huffInfo[i].data;

		if(huffInfo[i].length<0)
			error("Negative HuffLen.");
		if(huffInfo[i].length!=0)
			lengthType=1;

		if(huffInfo[i].count<0)
			error("Negative HuffCount.");
		if(huffInfo[i].count!=0)
			countType=1;

		if(huffInfo[i].code[0]!=0)
			codeType=1;
		}
	if(lengthType+countType+codeType>1)
		error("Multiple HuffInfo. %d %d %d",lengthType,countType,codeType);

	if(lengthType+countType+codeType==0)
		{
		head=dalloc<HuffInfo>(1);
		code=dalloc<HuffCode>(dataMax+1);
		}

	if(countType)
		{
		sort(huffInfo,hi,sizeof(HuffInfo),compareHuffInfoCount,true);
		while(huffInfo[hi-1].count==0&&hi>1)hi--;

		if(hi==1)
			huffInfo[0].length=1;
		else
			{
			int count=0;
			for(int i=0;i<hi;i++)
				count+=huffInfo[i].count;
			countToLength(huffInfo,0,hi,count,0,maxLength);
			}
		lengthType=1;
		sort(huffInfo,hi,sizeof(HuffInfo),compareHuffInfoData);
		}
	if(lengthType)
		{
		sort(huffInfo,hi,sizeof(HuffInfo),compareHuffInfoLength);
		hi--;
		head=decodeHuffInfoLength(huffInfo,hi,0);
		if(hi>=0&&huffInfo[hi].length!=0)
			error("huffInfoLength remained");

		decoderMax=10;
		decoder=dalloc<HuffInfo*>(1<<decoderMax);
		bitLeftDecoder=dalloc<HuffInfo*>(1<<decoderMax);
		buildDecoder(head,0,0);
		buildBitLeftDecoder(head,0,0);
		code=dalloc<HuffCode>(dataMax+1);
		int *c=dalloc<int>(30);
		buildEncoder(head,c,0);
		dfree(c);
		}
	}

	HuffTree::~HuffTree()
	{
	clear(head);
	if(code!=NULL)
		dfree(code);
	if(decoder!=NULL)
		dfree(decoder);
	if(bitLeftDecoder!=NULL)
		dfree(bitLeftDecoder);
	}

void HuffTree::countToLength(HuffInfo *huffInfo,int a,int b,int totalCount,int rank,int maxRank)
	{
	if(huffInfo==NULL||a>=b||totalCount<0||rank>maxRank)
		error("Huff::countToLength unexpected argument. %d %d %d %d %d",a,b,totalCount,rank,maxRank);
	if(b-a>(1<<(maxRank-rank)))
		error("Huff::countToLength num>maxNum.");
	if(b==a+1)
		{
		huffInfo[a].length=rank;
		return;
		}
	int minPart=1<<(maxRank-rank-1);
	int i,count=0;
	for(i=a;i<b-minPart+1;i++)
		count+=huffInfo[i].count;
	for(;i<b;i++)
		{
		if(count*2>=totalCount)
			break;
		count+=huffInfo[i].count;
		}
	if(count*2-totalCount>huffInfo[i-1].count)
		{
		i--;
		count-=huffInfo[i].count;
		}
	countToLength(huffInfo,a,i,count,rank+1,maxRank);
	countToLength(huffInfo,i,b,totalCount-count,rank+1,maxRank);
	}

HuffInfo* HuffTree::decodeHuffInfoLength(HuffInfo *huffInfo,int &hi,int rank)
	{
	if(hi<0)
		error("decodeHuffInfoLen overflow %d",hi);
	if(huffInfo[hi].length<rank)
		error("decodeHuffInfoLen len[%d]%d < rank %d",hi,huffInfo[hi].length,rank );
	HuffInfo *n=dalloc<HuffInfo>(1);
	if(huffInfo[hi].length==rank)
		{
		n->data=huffInfo[hi].data;
		n->length=huffInfo[hi].length;
		hi--;
		if(n->length==0)
			{
			HuffInfo *m=dalloc<HuffInfo>(1);
			m->left=n;
			m->right=dalloc<HuffInfo>(1);
			m->left->length=1;
			m->right->length=1;
			if(n->data==0)
				m->right->data=1;
			n=m;
			}
		return n;
		}
	n->right=decodeHuffInfoLength(huffInfo,hi,rank+1);
	if(rank==0&&hi==-1)
		hi=0;
	n->left=decodeHuffInfoLength(huffInfo,hi,rank+1);
	return n;
	}

void	HuffTree::buildDecoder(HuffInfo *now,int index,int rank)
	{
	if(rank==decoderMax)
		{
		decoder[index]=now;
		return;
		}
	if(now->left==NULL)
		{
		for(int i=(1<<(decoderMax-rank))-1;i>=0;i--)
			decoder[index|(i<<rank)]=now;
		return;
		}
	buildDecoder(now->left,index,rank+1);
	buildDecoder(now->right,index|(1<<rank),rank+1);
	}

void	HuffTree::buildBitLeftDecoder(HuffInfo *now,int index,int rank)
	{
	if(rank==decoderMax)
		{
		bitLeftDecoder[index]=now;
		return;
		}
	if(now->left==NULL)
		{
		for(int i=(1<<(decoderMax-rank))-1;i>=0;i--)
			bitLeftDecoder[index|i]=now;
		return;
		}
	buildBitLeftDecoder(now->left,index,rank+1);
	buildBitLeftDecoder(now->right,index|(1<<(decoderMax-1-rank)),rank+1);
	}

void	HuffTree::buildEncoder(HuffInfo *now,int *c,int cl)
	{
	if(now->left==NULL)
		{
		if(now->right!=NULL)
			error("Asymmetric HuffNode.");
		if(now->data>dataMax)
			error("HuffData > dataMax.");
		memcpy(code[now->data].code,c,30);
		code[now->data].length=cl;
		}
	else
		{
		buildEncoder(now->left,c,cl+1);
		c[cl/20]|=(1<<cl%20);
		buildEncoder(now->right,c,cl+1);
		c[cl/20]&=~(1<<cl%20);
		}
	}


void	HuffTree::clear(HuffInfo *n)
	{
	if(n->right!=NULL)
		clear(n->right);
	if(n->left!=NULL)
		clear(n->left);
	dfree(n);
	}

int 	HuffTree::getData(Input *in,int endianSetting)
	{
	if(in==NULL)
		error("HuffTree::getData from NULL Input.");
	HuffInfo **selectedDecoder=decoder;
	if(endianSetting&dlib::bitLeft)
		selectedDecoder=bitLeftDecoder;

	if(selectedDecoder==NULL)
		error("HuffTree::getData with NULL decoder.");
	HuffInfo *now=selectedDecoder[in->getPeekBits(decoderMax)];
	if(now->left==NULL)
		{
		in->getIgnoreBits(now->length);
		return now->data;
		}
	in->getIgnoreBits(decoderMax);
	while(now->left!=NULL)
		if(in->getBits(1))
			now=now->right;
		else
			now=now->left;
	return now->data;
	}

void	HuffTree::putCode(int data,Output *o)
	{
	if(code==NULL)
		error("putCode with NULL codeInfo.");
	if(data>dataMax||data<0||code[data].length==0)
		error("putCode with unexpected data %d.",data);
	if(o==NULL)
		error("putCode with NULL Output.");

	int cl=code[data].length;
	for(int i=0;i*20<cl;i++)
		o->putBits(code[data].code[i],cl-i*20>20?20:cl-i*20);
	}

int 	HuffTree::getCodeLength(int data)
	{
	if(code==NULL)
		error("getCodeLength with NULL codeInfo.");
	if(data>dataMax||data<0)
		error("Unexpected data %d",data);
	return code[data].length;
	}

void	HuffTree::print()
	{
	char printBuffer[200];
	printRec(printBuffer,0,head);
	}

void	HuffTree::printRec(char *printBuffer,int ci,HuffInfo *now)
	{
	if(now->left==NULL)
		{
		printBuffer[ci]='\0';
		//if(ci<8)
			report("%06X : %s",now->data,printBuffer);
		if(now->right!=NULL)
			error("right!=NULL while left==NULL.");
		return;
		}
	printBuffer[ci]='0';
	printRec(printBuffer,ci+1,now->left);
	printBuffer[ci]='1';
	printRec(printBuffer,ci+1,now->right);
	}







