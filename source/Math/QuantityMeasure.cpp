





	QuantityNode::QuantityNode(double tag,double quan)
	{
	min=max=tag;
	quantity=quan;
	left=right=NULL;
	}
	
	QuantityNode::~QuantityNode()
	{
	if(left!=NULL)
		{
		delete left;
		delete right;
		}
	}



void	QuantityNode::addSample(double tag,double quan)
	{
	if(left==NULL)
		{
		left=new QuantityNode(min,quantity);
		right=new QuantityNode(tag,quan);
		if(left->min>right->min)
			{
			QuantityNode *temp=left;
			left=right;
			right=temp;
			}
		}
	else
		{
		if(tag<=left->max)
			left->addSample(tag,quan);
		else if(tag>=right->min)
			right->addSample(tag,quan);
		else if(left->quantity<=right->quantity)
			left->addSample(tag,quan);
		else
			right->addSample(tag,quan);
		}
	min=left->min;
	max=right->max;
	quantity+=quan;
	}

double	QuantityNode::getQuantity(double mi,double ma)
	{
	if(mi>max||ma<=min)
		return 0.0;
	if(mi<=min&&ma>max)
		return quantity;
	return left->getQuantity(mi,ma)+right->getQuantity(mi,ma);
	}

void	QuantityNode::sampling(SampleMeasure *sm)
	{
	if(left!=NULL)
		{
		left->sampling(sm);
		right->sampling(sm);
		}
	else
		{
		sm->addSample(new Sample(min,quantity));
		}
	}






	QuantityMeasure::QuantityMeasure()
	{
	head=NULL;
	}

	QuantityMeasure::~QuantityMeasure()
	{
	clear();
	}
	
void	QuantityMeasure::clear()
	{
	if(head!=NULL)
		delete head;
	head=NULL;
	}





void	QuantityMeasure::addSample(double tag,double quantity)
	{
	if(head==NULL)
		head=new QuantityNode(tag,quantity);
	else
		head->addSample(tag,quantity);
	}

double	QuantityMeasure::getMinTag()
	{
	if(head==NULL)
		return 0.0;
	return head->min;
	}

double	QuantityMeasure::getMaxTag()
	{
	if(head==NULL)
		return 10.0;
	return head->max;
	}

double	QuantityMeasure::getQuantity(double mi,double ma)
	{
	//argCheck(mi>=ma);
	if(mi>=ma)
		return 0.0;
	if(head==NULL)
		return 0.0;
	return head->getQuantity(mi,ma);
	}
	
	
SampleMeasure	QuantityMeasure::getSampleMeasure()
	{
	SampleMeasure sm;
	if(head!=NULL)
		head->sampling(&sm);
	return sm;
	}



void	QuantityMeasure::fitScaler(Scaler *s,int a)
	{
	argCheck(s==NULL);
	double mi=getMinTag();
	double ma=getMaxTag();
	if(a==0)
		{
		s->fitSample(mi);
		s->fitSample(ma);
		}
	else
		{
		s->fitSample(0.0);
		s->fitSample(getQuantity(mi,ma+1.0)/10);
		}
	}
	
void	QuantityMeasure::show(Picture *buffer,Scaler *xScaler,Scaler *yScaler,DS_RGB color)
	{
	Array<double> grade;
	double min=xScaler->min;
	double max=xScaler->max;
	int num=100;
	for(int i=0;i<=num;i++)
		grade.addItem(min+(max-min)*i/num);
	double last=grade[0];
	for(int i=1;i<grade.getNumber();i++)
		{
		double q=getQuantity(last,grade[i]);
		//report("%f~%f 0~%f",last,grade[i],q);
		int x0=xScaler->convert(last,buffer->getSize().x);
		int x1=xScaler->convert(grade[i],buffer->getSize().x);
		int y0=yScaler->convert(q,buffer->getSize().y);
		int y1=yScaler->convert(0.0,buffer->getSize().y);
		//report("%d,%d %d,%d",x0,y0,x1,y1);
		buffer->drawFilledRect(Position(x0,y0),Coord(x1-x0,y1-y0),color);
		last=grade[i];
		}
	}








