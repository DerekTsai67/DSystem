

	
	
	
	
template<class Type>
	Array<Type>::Array(int num)
	{
	if(num<=0)
		num=10;
	arrayMax=num;
	arrayData=dalloc<Type>(arrayMax);
	itemMax=0;
	next=NULL;
	sorted=false;
	}

template<class Type>
	Array<Type>::Array(const Array<Type> &a)
	{
	arrayMax=a.arrayMax;
	arrayData=dclone<Type>(a.arrayData,arrayMax);
	itemMax=a.itemMax;
	if(a.next==NULL)
		next=NULL;
	else
		next=new Array<Type>(*(a.next));
	sorted=false;
	}
	
template<class Type>
	Array<Type>::~Array()
	{
	if(next!=NULL)
		delete next;
	dfree(arrayData);
	}


template<class Type>
void	Array<Type>::addItem(Type item,int pos)
	{
	argCheck(pos<-1);
	if(sorted)
		pos=searchSorted(item,pos);
	if(itemMax>=arrayMax&&next==NULL)
		next=new Array<Type>(arrayMax*2);
	if(pos==-1)
		{
		if(next!=NULL)
			{
			next->addItem(item,-1);
			return;
			}
		pos=itemMax;
		}
	if(pos>=arrayMax||pos>itemMax)
		{
		if(next==NULL)
			error("Overflow.");
		next->addItem(item,pos-itemMax);
		return;
		}
	if(itemMax>=arrayMax)
		{
		next->addItem(arrayData[itemMax-1],0);
		itemMax--;
		}

	for(int i=itemMax;i>pos;i--)
		arrayData[i]=arrayData[i-1];
	arrayData[pos]=item;
	itemMax++;
	}

template<class Type>
void	Array<Type>::deleteItem(Type item,bool noDel)
	{
	deleteItemIndex(getIndex(item),noDel);
	}
template<class Type>
void	Array<Type>::deleteItemIndex(int pos,bool noDel)
	{
	if(!noDel)
		delete getItem(pos);
	loseItemIndex(pos);
	}
template<class Type>
void	Array<Type>::loseItem(Type item)
	{
	loseItemIndex(getIndex(item));
	}
template<class Type>
void	Array<Type>::loseItemIndex(int pos)
	{
	argCheck(pos<0);
	if(pos>=itemMax)
		{
		if(next==NULL)
			error("Overflow.");
		next->loseItemIndex(pos-itemMax);
		return;
		}
	
	for(int i=pos;i<itemMax-1;i++)
		arrayData[i]=arrayData[i+1];
	itemMax--;
	}


template<class Type>
void	Array<Type>::clear()
	{
	if(next!=NULL)
		{
		delete next;
		next=NULL;
		}
	itemMax=0;
	}

template<class Type>
void	Array<Type>::deleteAll()
	{
	for(int i=0;i<itemMax;i++)
		delete arrayData[i];
	if(next!=NULL)
		next->deleteAll();
	clear();
	}

template<class Type>
void	Array<Type>::extend(int index)
	{
	if(index==-1)
		index=getNumber()+1;
	index-=itemMax;
	if(index<=0)
		return;
	
	if(next!=NULL)
		{
		next->extend(index);
		return;
		}
	
	int n=arrayMax-itemMax;
	if(n>index)n=index;
	itemMax+=n;
	index-=n;
	if(index<=0)
		return;

	next=new Array<Type>(index);
	next->extend(index);
	}


template<class Type>
Type*	Array<Type>::cloneAlloc()const
	{
	Type*	ret=dalloc<Type>(getNumber());
	
	int n=0;
	const Array<Type>	*now=this;
	while(now!=NULL)
		{
		memcpy(ret+n,now->arrayData,sizeof(Type)*now->itemMax);
		n+=now->itemMax;
		now=now->next;
		}
	return ret;
	}

template<class Type>
Array<Type>&	Array<Type>::operator=(const Array<Type> &a)
	{
	clear();
	extend(a.getNumber());
	FOR_ARRAY(i,a)
		getItem(i)=a.getItem(i);
	return *this;
	}


template<class Type>
Array<Type>*	Array<Type>::clone()const
	{
	Array<Type>	*ret=new Array<Type>(getNumber());
	for(int i=0;i<getNumber();i++)
		ret->addItem(getItem(i));
	return ret;
	}



template<class Type>
Type&	Array<Type>::getItem(int index)const
	{
	argCheck(index<0);
	/*if(index==itemMax&&next==NULL)
		{
		if(itemMax>=arrayMax)
			next=new Array<Type>(arrayMax*2);
		else
			itemMax++;
		}*/
	if(index>=itemMax)
		{
		if(next==NULL)
			error("Overflow.");
		return next->getItem(index-itemMax);
		}
	return arrayData[index];
	}
template<class Type>
Type&	Array<Type>::operator[](int index)const
	{
	return getItem(index);
	}

template<class Type>
int		Array<Type>::getNumber()const
	{
	argCheck(itemMax<0||itemMax>arrayMax);
	if(next!=NULL)
		return next->getNumber()+itemMax;
	else
		return itemMax;
	}

template<class Type>
int		Array<Type>::getIndex(Type item,bool safeFail)const
	{
	if(sorted)
		return getIndexSorted(item,safeFail);
	
	for(int i=0;i<itemMax;i++)
		if(arrayData[i]==item)
			return i;
	if(next!=NULL)
		{
		int r=next->getIndex(item,safeFail);
		if(r!=-1)
			return r+itemMax;
		}
	if(!safeFail)
		error("No item.");
	return -1;
	}



template<class Type>
void		Array<Type>::cutBuffer()
	{
	if(getNumber()<=0)
		return;
	Type *newData=cloneAlloc();
	int newMax=getNumber();

	delete next;
	next=NULL;
	dfree(arrayData);
	arrayData=newData;
	itemMax=arrayMax=newMax;
	}


template<class Type>
void		Array<Type>::sort(Compare comp,bool ba)
	{
	cutBuffer();
	
	::sort(arrayData,itemMax,sizeof(Type),comp,ba);
	}






template<class Type>
void	Array<Type>::setSorted(Compare c,bool b)
	{
	argCheck(c==NULL||(sorted==true));
	if(getNumber()!=0)
		sort(c,b);
	compare=c;
	back=b;
	sorted=true;
	}
	
template<class Type>
int		Array<Type>::getIndexSorted(Type item,bool safeFail)const
	{
	int pos=searchSorted(item,-1)-1;
	while(compare(arrayData+pos,&item)==0)
		{
		if(arrayData[pos]==item)
			return pos;
		pos--;
		}
	if(!safeFail)
		error("No item.");
	return -1;
	}

template<class Type>
int		Array<Type>::searchSorted(Type item,int pos)
	{
	int min=-1,max=getNumber();
	/*if(pos!=-1)
		{
		
		}*/
	while(min+1!=max)
		{
		int t=(min+max)/2;
		int r=compare(&getItem(t),&item);
		if(back)
			r*=-1;
		if(r==-1||r==0)
			min=t;
		else
			max=t;
		}
	return max;
	}






