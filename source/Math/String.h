
class String;


class ConstString{
	public:
	ConstString();
	ConstString(const ConstString &str);
	ConstString(const char *raw);
	ConstString(const ConstString &str,int len);
	ConstString(const char *s,int len);
	~ConstString();

char		operator[](int index);
ConstString	operator+(int index);
ConstString	operator-(int index);
ConstString	cut(int index);
void		operator+=(int index);
void		operator-=(int index);
bool		operator==(const char *s);
bool		operator!=(const char *s);
bool		operator==(const ConstString &s);
bool		operator!=(const ConstString &s);
operator	const char*();
const char*	getRaw()const;
int			getLength()const;

int		search(ConstString candidate,bool back=false);
int		searchNot(ConstString candidate,bool back=false);
void	takeSpace(ConstString candi);
bool	takeVerify(ConstString v);
//String	copy();
void	print();

	//private:
const char	*data;
int		length;
	};


class String:public ConstString{
	public:
			String();
explicit	String(const char *raw);
			String(char *raw,int len);
			String(int len);
			String(const Array<char> &arr);
			String(const String &str);
			~String();

void	cut(int index);
char&	operator[](int index);
char*	getRaw();
void	copyFrom(ConstString str);
	//private:
char	*nonConstData;
	
	public:
void	setDelete();
void	regisTemp();
//void	free();
	private:
bool	del;
	};






