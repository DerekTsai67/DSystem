
#define FOR_COORD(i,m)		\
			{Coord i,coordMax=m;		\
			for(i.y=0;i.y<coordMax.y;i.y++)\
			for(i.x=0;i.x<coordMax.x;i.x++){
#define FOR_COORD_D(i,m,s,d)		\
			{Coord i,coordMax=m,s_=s,d_=d;		\
			for(i.y=s_.y;i.y<coordMax.y;i.y+=d_.y)\
			for(i.x=s_.x;i.x<coordMax.x;i.x+=d_.x){
#define END_FOR_COORD }}



class Coord{
	public:
	Coord()				:x(0),y(0){}
	Coord(int a)		:x(a),y(a){}
	Coord(int a,int b)	:x(a),y(b){}
int	x,y;

Coord operator=(Coord b);
Coord operator+=(Coord b);
Coord operator-=(Coord b);
Coord operator*=(Coord b);
Coord operator/=(Coord b);
Coord operator%=(Coord b);
Coord operator|=(Coord b);
Coord operator&=(Coord b);
Coord operator*=(int b);
Coord operator/=(int b);
Coord operator%=(int b);
Coord operator*=(double b);
Coord operator/=(double b);

Coord operator+(Coord b)const;
Coord operator-(Coord b)const;
Coord operator*(Coord b)const;
Coord operator/(Coord b)const;
Coord operator%(Coord b)const;
Coord operator|(Coord b)const;
Coord operator&(Coord b)const;
Coord operator*(int b)const;
Coord operator/(int b)const;
Coord operator%(int b)const;
Coord operator*(double b)const;
Coord operator/(double b)const;
friend Coord operator*(int b,Coord c);
friend Coord operator*(double b,Coord c);
Coord operator-()const;

bool operator==(Coord b)const;
bool operator!=(Coord b)const;
bool operator<(Coord b)const;
bool operator>(Coord b)const;
bool operator<=(Coord b)const;
bool operator>=(Coord b)const;

Coord	flipX()const;
Coord	flipY()const;
Coord	onlyX()const;
Coord	onlyY()const;
Coord	right()const;
Coord	left()const;
int		abs()const;
double	absDouble()const;
int		area()const;
friend Coord	abs(Coord c);

int		dot(Coord c)const;
Coord	cut(int l)const;
	};

const Coord zeroCoord(0,0),monoCoord(1,1);




class Position : public Coord{
	public:
	enum Type{
		RealLeftUp,
		RealLeftDown,
		RealRightUp,
		RealRightDown,
		EffectiveLeftUp,
		EffectiveLeftDown,
		EffectiveRightUp,
		EffectiveRightDown,
		EffectiveMiddleUp,
		EffectiveMiddleDown,
		EffectiveMiddleLeft,
		EffectiveMiddleRight,
		Center,
		Type_count,
		Default
		};
explicit	Position(Coord c=zeroCoord	,Type t=EffectiveLeftUp,Type t2=Default);
explicit	Position(int x,int y		,Type t=EffectiveLeftUp,Type t2=Default);
Position	change(const Picture *p1		,const Picture *p2			,Type outFromType=EffectiveLeftUp,Type outToType=Default)const;
Position	change(Coord s,Coord s2,Coord s3,const Picture *p2			,Type outFromType=EffectiveLeftUp,Type outToType=Default)const;
Position	change(const Picture *p1		,Coord d,Coord d2,Coord d3	,Type outFromType=EffectiveLeftUp,Type outToType=Default)const;
Position	change(Coord s,Coord s2,Coord s3,Coord d,Coord d2,Coord d3	,Type outFromType=EffectiveLeftUp,Type outToType=Default)const;
Coord	fix(Coord p1f,Coord p1s,Coord p1e,Coord p2f,Coord p2s,Coord p2e,Type outFromType=RealLeftUp,Type outToType=Default)const;
Coord	fix(Coord s,Coord s2,Coord s3,const Picture *p2	,Type outFromType=RealLeftUp,Type outToType=Default)const;
Coord	fix(const Picture *p1,Coord d,Coord d2,Coord d3	,Type outFromType=RealLeftUp,Type outToType=Default)const;
Coord	fix(const Picture *p1,const Picture *p2			,Type outFromType=RealLeftUp,Type outToType=Default)const;
	private:
Type	fromType,toType;

	public:
Position operator+(Coord b)const;
Position operator-(Coord b)const;
Position operator*(Coord b)const;
Position operator/(Coord b)const;
Position operator*(double b)const;
Position operator/(double b)const;
Position operator-()const;
Position operator+(Position b)const;
	};

const Position zeroRealPosition(0,0,Position::RealLeftUp);
const Position zeroEffectivePosition(0,0,Position::EffectiveLeftUp);




class Size : public Coord{
	public:
	enum Type{
		Real,
		Effective,
		Type_count
		};
explicit	Size(Coord c=zeroCoord,Type t=Effective);
explicit	Size(int x,int y,Type t=Effective);
Size	change(const Picture *p,Type outType=Effective);
Coord	fix(Coord s,Type outType=Real);
Coord	fix(const Picture *p,Type outType=Real);
	private:
Type	type;

	public:
Size operator+(Coord b)const;
Size operator-(Coord b)const;
Size operator*(Coord b)const;
Size operator/(Coord b)const;
Size operator*(double b)const;
Size operator/(double b)const;
Size operator-()const;
	};

const Size zeroRealSize(0,0,Size::Real);
const Size zeroEffectiveSize(0,0,Size::Effective);


int		toAngle(Coord w);
/*typedef double Angle;
Coord rotate(Coord c,Angle r);
Coord randCoord(Coord a,Coord b);
Coord speedEqualize(Coord w,int s);
Coord speedCut(Coord w,int l,int h);
Coord exclude(Coord s,Coord t,int d);*/
