



double	Mapping::getValue(int xu,int xd)const
	{
	argCheck(xu<0||xd<=0);
	return getValue((double)xu/xd);
	}
double	Mapping::getValue(double x)const
	{
	argCheck(x<0.0||x>1.0);
	double ret=getValueRaw(x);
	argCheck(ret<0.0||ret>1.0);
	return ret;
	}


double	SimpleMapping::getValueRaw(double x)const
	{
	return x;
	}

	ExponentialMapping::ExponentialMapping(double d)
	{
	argCheck(d<=0.0);
	base=2.71828;
	decay=d;
	}

double	ExponentialMapping::getValueRaw(double x)const
	{
	return (pow(base,x*decay)-1.0)/(pow(base,decay)-1.0);
	}





	InverseMapping::InverseMapping(bool x,bool y,Mapping *m)
	{
	argCheck(m==NULL);
	inverseX=x;
	inverseY=y;
	mapping=m;
	}
	
	InverseMapping::~InverseMapping()
	{
	delete mapping;
	}

double	InverseMapping::getValueRaw(double x)const
	{
	if(inverseX)
		x=1.0-x;
	double y=mapping->getValue(x);
	if(inverseY)
		y=1.0-y;
	return y;
	}






















