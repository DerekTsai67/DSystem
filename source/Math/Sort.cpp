




static void	quickRec(Byte *arr,int max,int size,Compare compare,int back,Byte *buf,int *compareBuf)
	{
	if(max<=1)
		{
		if(max==1)
			memcpy(buf,arr,size);
		return;
		}
	int count_[3]={0,0,0},bi_[3]={0,0,0};
	int *count=count_+1;
	int *bi=bi_+1;

	for(int i=1;i<max;i++)
		count[(compareBuf[i]=compare(arr+i*size,arr)*back)]++;
	
	if(count[-1]==max-1)
		{
		int i;
		for(i=0;i<max-1;i++)
			if(compare(arr+(i+1)*size,arr+i*size)*back!=-1)
				break;
		if(i==max-1)
			{
			for(int i=0;i<max;i++)
				memcpy(buf+i*size,arr+(max-1-i)*size,size);
			memcpy(arr,buf,max*size);
			return;
			}
		}
	else if(count[1]==max-1)
		{
		int i;
		for(i=0;i<max-1;i++)
			if(compare(arr+(i+1)*size,arr+i*size)*back==-1)
				break;
		if(i==max-1)
			{
			memcpy(buf,arr,max*size);
			return;
			}
		}
	else if(count[0]==max-1)
		{
		memcpy(buf,arr,max*size);
		return;
		}

	bi[0]=count[-1]+1;
	bi[1]=bi[0]+count[0];

	for(int i=1;i<max;i++)
		memcpy(buf+bi[compareBuf[i]]++*size,arr+i*size,size);

	memcpy(buf+count[-1]*size,arr,size);

	quickRec(buf					,count[-1]	,size,compare,back,arr						,compareBuf);
	memcpy	(arr+count[-1]*size		,buf+count[-1]*size		,(count[0]+1)*size);
	quickRec(buf+(max-count[1])*size,count[1]	,size,compare,back,arr+(max-count[1])*size	,compareBuf);
	}

void	sort(void *arr,int max,int size,Compare compare,bool back)
	{
	argCheck(arr==NULL||compare==NULL);
	if(max<=0||size<=0)
		return;
	
	Byte *buf=dalloc<Byte>(max*size);
	int *compareBuf=dalloc<int>(max);
	quickRec((Byte*)arr,max,size,compare,back?-1:1,buf,compareBuf);
	dfree(compareBuf);
	dfree(buf);
	}






int*	getRandomIndex(int max)
	{
	int *ret=dalloc<int>(max);
	for(int i=0;i<max;i++)
		ret[i]=i;
	for(int i=0;i<max-2;i++)
		{
		int j=random(max-i-1);
		int temp=ret[i+j+1];
		ret[i+j+1]=ret[i];
		ret[i]=temp;
		}
	return ret;
	}

void	randomSort(void *arr,int max,int size,int *index)
	{
	argCheck(arr==NULL);
	if(max<=0||size<=0)
		return;
	
	Byte *buf=dalloc<Byte>(max*size);
	bool deleteIndex=false;
	if(index==NULL)
		{
		index=getRandomIndex(max);
		deleteIndex=true;
		}
	for(int i=0;i<max;i++)
		memcpy((Byte*)buf+i*size,(Byte*)arr+index[i]*size,size);
	memcpy(arr,buf,max*size);
	if(deleteIndex)
		dfree(index);
	dfree(buf);
	}


int		compareInt(const void *a,const void *b)
	{
	return ((*(int*)a)>(*(int*)b))-((*(int*)a)<(*(int*)b));
	}
	
int		compareDouble(const void *a,const void *b)
	{
	return ((*(double*)a)>(*(double*)b))-((*(double*)a)<(*(double*)b));
	}




