


int		random(int max)
	{
	return random(0,max);
	}

int		random(int min,int max)
	{
	int m=max-min;
	argCheck(m<=0);
	int randMax=RAND_MAX+1;
	int ret=0;
	if(m<=randMax)
		{
		int rm=(randMax/m)*m;
		while((ret=rand())>=rm);
		}
	else if(m<=randMax*randMax)
		{
		int rm=(randMax*randMax/m)*m;
		while((ret=rand()*randMax+rand())>=rm);
		}
	else
		error("Too big max =%d",m);
	ret%=m;
	return min+ret;
	}




double	gaussianRandom(double devi,int precision)
	{
	return gaussianRandom(0.0,devi,precision);
	}

double	gaussianRandom(double mean,double devi,int precision)
	{
	argCheck(RAND_MAX!=0x7FFF||precision<=0);
	double sum[16];
	for(int s=0;s<16;s++)
		{
		int a=0;
		for(int i=0;i<4;i++)
			if(s&(1<<i))
				a++;
		sum[s]=a;
		}

	double ret=0.0;
	for(int i=0;i<precision;i++)
		{
		int r=rand();
		ret+=sum[(r)&0xF];
		ret+=sum[(r>>4)&0xF];
		ret+=sum[(r>>8)&0xF];
		ret+=sum[(r>>12)&0xF];
		}
	ret-=7.5*precision;
	return mean+ret*2.0/pow(15.0*precision,0.5)*devi;
	}






