

#define ARRAY(Type)\
	public:\
Type*	get##Type(int index)const\
	{return Type##Array.getItem(index);}\
void	add##Type(Type *item,int pos=-1)\
	{Type##Array.addItem(item,pos);}\
void	delete##Type(Type *item,bool noDel=false)\
	{Type##Array.deleteItem(item,noDel);}\
void	delete##Type(int pos,bool noDel=false)\
	{Type##Array.deleteItem(pos,noDel);}\
void	lose##Type(Type *item)\
	{Type##Array.loseItem(item);}\
void	lose##Type(int pos)\
	{Type##Array.loseItem(pos);}\
	private:\
Array<Type>	Type##Array;

#define FOR_ARRAY(x,y) for(int x=0;x<(y).getNumber();x++)


template<class Type>
class Array{
	public:
	Array(int num=100);
	Array(const Array<Type> &a);
	~Array();
	
void	addItem(Type item,int pos=-1);
void	deleteItem(Type item,bool noDel=false);
void	deleteItemIndex(int pos,bool noDel=false);
void	loseItem(Type item);
void	loseItemIndex(int pos);

void	clear();
void	deleteAll();
void	extend(int index=-1);
Type*	cloneAlloc()const;
Array<Type>*	clone()const;
Array<Type>&	operator=(const Array<Type> &a);

Type&	getItem(int index)const;
Type&	operator[](int i)const;
int		getNumber()const;
int		getIndex(Type item,bool safeFail=false)const;

void	cutBuffer();
void	sort(Compare compare,bool back=false);

	private:
int		arrayMax;
Type	*arrayData;
int		itemMax;
Array<Type>	*next;
	
	public:
void	setSorted(Compare c,bool b=false);
	private:
int		getIndexSorted(Type item,bool safeFail)const;
int		searchSorted(Type item,int pos);
bool	sorted;
Compare	compare;
bool	back;
	};
	
	




















