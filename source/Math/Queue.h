



template<class Type>
class	Queue{
	public:
	Queue(int m);
	~Queue();

void	addItem(Type item);

void	clear();
void	deleteAll();

Type&	peekItem(int index=0)const;
Type&	takeItem();
Type&	operator[](int i)const;
int		getNumber()const;
	private:
Type*	data;
int		max,startPoint,index;
	};








