



class QuantityNode{
	public:
	QuantityNode(double tag,double quantity);
	~QuantityNode();
void	addSample(double tag,double quantity);
double	getQuantity(double min,double max);

void	sampling(SampleMeasure *sm);
	//private:
double	min,max;
double	quantity;
QuantityNode	*left,*right;
	};



class QuantityMeasure:public Measure{
	public:
	QuantityMeasure();
	~QuantityMeasure();
void	clear();

void	addSample(double tag,double quantity);
double	getMinTag();
double	getMaxTag();
double	getQuantity(double mi,double ma);

SampleMeasure	getSampleMeasure();

void	fitScaler(Scaler *s,int a);
void	show(Picture *buffer,Scaler *xScaler,Scaler *yScaler,DS_RGB color);
	private:
QuantityNode	*head;
	};

















