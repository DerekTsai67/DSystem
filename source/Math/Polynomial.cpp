


	Polynomial::Polynomial(double a):coefficient(30)
	{
	coefficient.addItem(a);
	}
	
	Polynomial::Polynomial(const Polynomial &p):coefficient(p.coefficient)
	{
	}


Polynomial*	Polynomial::clone()
	{
	return new Polynomial(*this);
	}

Polynomial&	Polynomial::operator=(const Polynomial &p)
	{
	coefficient=p.coefficient;
	return *this;
	}


double	Polynomial::getY(double x)const
	{
	double y=0.0;
	FOR_ARRAY(i,coefficient)
		y+=coefficient.getItem(i)*pow(x,i);
	return y;
	}

void	Polynomial::plus(const Polynomial *p)
	{
	coefficient.extend(p->coefficient.getNumber());
	FOR_ARRAY(i,coefficient)
		{
		if(i>=p->coefficient.getNumber())
			break;
		coefficient.getItem(i)+=p->coefficient.getItem(i);
		}
	}

void	Polynomial::multiplicate(double d)
	{
	FOR_ARRAY(i,coefficient)
		coefficient.getItem(i)*=d;
	}











void	Polynomial::regress(SampleMeasure *m,int rank,int min,int max)
	{
	argCheck(m==NULL);
	argCheck(m->getNumber()==0);
	if(max==-1)
		max=m->getNumber();
	argCheck(max<=min+1||min<0||max>m->getNumber());
	double **multiTable=dalloc<double*>(max);
	for(int i=min;i<max;i++)
		{
		multiTable[i]=dalloc<double>(rank+1);
		multiTable[i][0]=1.0;
		multiTable[i][1]=m->getSample(i)->getAxis(0);
		for(int j=2;j<=rank;j++)
			multiTable[i][j]=pow(multiTable[i][1],j);
		}
	
	double **coeTable=dalloc<double*>(rank+1);
	for(int i=0;i<=rank;i++)
		{
		coeTable[i]=dalloc<double>(rank+1);
		}
	
	for(int li=0;li<=rank;li++)
		{
		double dt=0.0;
		for(int si=min;si<max;si++)
			{
			double xd=multiTable[si][li];
			for(int ri=0;ri<li;ri++)
				xd-=coeTable[ri][li]*multiTable[si][ri];
				
			for(int ui=li+1;ui<=rank;ui++)
				{
				double yd=multiTable[si][ui];
				for(int ri=0;ri<li;ri++)
					yd-=coeTable[ri][ui]*multiTable[si][ri];
				coeTable[li][ui]+=xd*yd;
				}
				
			double yd=m->getSample(si)->getAxis(1);
			for(int ri=0;ri<li;ri++)
				yd-=coeTable[ri][0]*multiTable[si][ri];
			coeTable[li][0]+=xd*yd;
			
			dt+=xd*xd;
			}
		if(dt==0.0)
			dt=1.0;
		for(int ui=0;ui<=rank;ui++)
			coeTable[li][ui]/=dt;
		for(int ri=0;ri<li;ri++)
		for(int ui=0;ui<=rank;ui++)
			coeTable[ri][ui]-=coeTable[li][ui]*coeTable[ri][li];
		}
		
	for(int li=rank;li>=0;li--)
		{
		for(int ui=li+1;ui<=rank;ui++)
			;//coeTable[li][0]-=coeTable[li][ui]*coeTable[ui][0];
		}
	
	coefficient.clear();
	for(int li=0;li<=rank;li++)
		coefficient.addItem(coeTable[li][0]);
	
	
	for(int i=0;i<=rank;i++)
		dfree(coeTable[i]);
	dfree(coeTable);
	
	for(int i=min;i<max;i++)
		dfree(multiTable[i]);
	dfree(multiTable);
	}

void	Polynomial::linearRegress(SampleMeasure *m)
	{
	argCheck(m==NULL);
	argCheck(m->getNumber()==0);
	
	coefficient.clear();
	
	double xb=0.0,yb=0.0;
	for(int si=0;si<m->getNumber();si++)
		{
		xb+=m->getSample(si)->getAxis(0);
		yb+=m->getSample(si)->getAxis(1);
		}
	xb/=m->getNumber();
	yb/=m->getNumber();

	double n=0.0,d=0.0;
	for(int si=0;si<m->getNumber();si++)
		{
		double xd=m->getSample(si)->getAxis(0)-xb;
		double yd=m->getSample(si)->getAxis(1)-yb;
		n+=xd*yd;
		d+=xd*xd;
		}
	double kx;
	if(d==0.0)
		kx=9999.0;
	else
		kx=n/d;
	coefficient.addItem(yb-kx*xb);
	coefficient.addItem(kx);
	}

void	Polynomial::show(Picture *buffer,Scaler *xScaler,Scaler *yScaler,DS_RGB color)
	{
	int lastY=-1;
	for(int px=0;px<buffer->getSize().x;px++)
		{
		double x=xScaler->convertBack(px,buffer->getSize().x);
		double y=getY(x);
		int py=yScaler->convert(y,buffer->getSize().y);
		if(lastY==-1)
			lastY=py;
		if(py!=-1)
		buffer->drawLineC(
			Position(px,lastY),
			Position(px,py),
			color
			);
		lastY=py;
		}
	}

























