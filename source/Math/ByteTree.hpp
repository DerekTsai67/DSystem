



template<class Type>
	ByteNode<Type>::ByteNode()
	{
	memset(&data,0,sizeof(Type));
	next=NULL;
	}

template<class Type>
	ByteNode<Type>::ByteNode(Type d)
	{
	data=d;
	next=NULL;
	}
	
template<class Type>
	ByteNode<Type>::~ByteNode()
	{
	if(next!=NULL)
		{
		for(int x=0;x<16;x++)
		if(next[x]!=NULL)
			{
			for(int y=0;y<16;y++)
			if(next[x][y]!=NULL)
				delete next[x][y];
			dfree(next[x]);
			}
		dfree(next);
		}
	}
	
template<class Type>
void	ByteNode<Type>::setData(Type d)
	{
	data=d;
	}

template<class Type>
Type	ByteNode<Type>::getData()const
	{
	return data;
	}

template<class Type>
void	ByteNode<Type>::setNext(ByteNode<Type> *n,Byte index)
	{
	argCheck(n==NULL||index<0||index>=256);
	//report("sN %08X %d",n,index);
	if(next==NULL)
		next=dalloc<ByteNode**>(16);
	if(next[index>>4]==NULL)
		next[index>>4]=dalloc<ByteNode*>(16);
	if(next[index>>4][index&0xF]!=NULL)
		error("reSet.");
	
	next[index>>4][index&0xF]=n;
	}
	
template<class Type>
ByteNode<Type>*	ByteNode<Type>::getNext(Byte *index,int num,bool create)
	{
	argCheck(index==NULL||num<=0);
	//report("gN %08X %d %02X %02X %02X %02X",index,num,index[0],index[1],index[2],index[3]);
	ByteNode<Type> *now=getNext(*index,create);
	if(num==1)
		return now;
	if(now==NULL)
		return NULL;
	return now->getNext(index+1,num-1,create);
	}

template<class Type>
ByteNode<Type>*	ByteNode<Type>::getNext(Byte index,bool create)
	{
	argCheck(index<0||index>=256);
	ByteNode<Type> *ret=NULL;
	if(next!=NULL)
	if(next[index>>4]!=NULL)
		ret=next[index>>4][index&0xF];
	if(ret==NULL&&create)
		{
		ret=new ByteNode<Type>();
		setNext(ret,index);
		}
	return ret;
	}















