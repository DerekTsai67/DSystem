


	/*  Coord::Coord()
	:x(0),y(0){}
	  Coord::Coord(int a,int b)
	:x(a),y(b){}
	*/

Coord Coord::operator=(Coord b)
	{x=b.x;y=b.y;	return *this;}
Coord Coord::operator+=(Coord b)
	{x+=b.x;y+=b.y;	return *this;}
Coord Coord::operator-=(Coord b)
	{x-=b.x;y-=b.y;	return *this;}
Coord Coord::operator*=(Coord b)
	{*this=*this*b;	return *this;}
Coord Coord::operator/=(Coord b)
	{*this=*this/b;	return *this;}
Coord Coord::operator%=(Coord b)
	{*this=*this%b;	return *this;}
Coord Coord::operator|=(Coord b)
	{*this=*this|b;	return *this;}
Coord Coord::operator&=(Coord b)
	{*this=*this&b;	return *this;}
Coord Coord::operator*=(int b)
	{*this=*this*b;	return *this;}
Coord Coord::operator/=(int b)
	{*this=*this/b;	return *this;}
Coord Coord::operator%=(int b)
	{*this=*this%b;	return *this;}
Coord Coord::operator*=(double b)
	{*this=*this*b;	return *this;}
Coord Coord::operator/=(double b)
	{*this=*this/b;	return *this;}

Coord Coord::operator+(Coord b)const
	{return Coord(x+b.x,y+b.y);}
Coord Coord::operator-(Coord b)const
	{return Coord(x-b.x,y-b.y);}
Coord Coord::operator*(Coord b)const
	{return Coord(x*b.x,y*b.y);}
Coord Coord::operator/(Coord b)const
	{return Coord(x/b.x,y/b.y);}
Coord Coord::operator%(Coord b)const
	{return Coord(x%b.x,y%b.y);}
Coord Coord::operator|(Coord b)const
	{return Coord(x>b.x?x:b.x,y>b.y?y:b.y);}
Coord Coord::operator&(Coord b)const
	{return Coord(x<b.x?x:b.x,y<b.y?y:b.y);}
Coord Coord::operator*(int b)const
	{return Coord(x*b,y*b);}
Coord Coord::operator/(int b)const
	{return Coord(x/b,y/b);}
Coord Coord::operator%(int b)const
	{return Coord(x%b,y%b);}
Coord Coord::operator*(double b)const
	{return Coord((int)(x*b),(int)(y*b));}
Coord Coord::operator/(double b)const
	{return Coord((int)(x/b),(int)(y/b));}
Coord operator*(int b,Coord c)
	{return Coord(c.x*b,c.y*b);}
Coord operator*(double b,Coord c)
	{return Coord((int)(c.x*b),(int)(c.y*b));}
Coord Coord::operator-()const
	{return Coord(-x,-y);}

bool Coord::operator==(Coord b)const
	{return x==b.x && y==b.y;}
bool Coord::operator!=(Coord b)const
	{return x!=b.x || y!=b.y;}
bool Coord::operator>(Coord b)const
	{return x>b.x && y>b.y;}
bool Coord::operator<(Coord b)const
	{return x<b.x && y<b.y;}
bool Coord::operator>=(Coord b)const
	{return x>=b.x && y>=b.y;}
bool Coord::operator<=(Coord b)const
	{return x<=b.x && y<=b.y;}


Coord	Coord::flipX()const
	{return Coord(-x,y);}
Coord	Coord::flipY()const
	{return Coord(x,-y);}
Coord	Coord::onlyX()const
	{return Coord(x,0);}
Coord	Coord::onlyY()const
	{return Coord(0,y);}
Coord	Coord::right()const
	{return Coord(y,-x);}
Coord	Coord::left()const
	{return Coord(-y,x);}
int		Coord::abs()const
	{return (int)(pow((double)(x*x+y*y),0.5)+0.5);}
double	Coord::absDouble()const
	{return pow((double)(x*x+y*y),0.5);}
int		Coord::area()const
	{return x*y;}
Coord	abs(Coord c)
	{return Coord(c.x>=0?c.x:-c.x,c.y>=0?c.y:-c.y);}
	
int		Coord::dot(Coord c)const
	{return x*c.x+y*c.y;}
Coord	Coord::cut(int l)const
	{
	int a=abs();
	if(a<l)
		return *this;
	return Coord(x*l/a,y*l/a);
	}


//==========================================================================
Position Position::operator+(Coord b)const
	{return Position(x+b.x,y+b.y,fromType,toType);}
Position Position::operator-(Coord b)const
	{return Position(x-b.x,y-b.y,fromType,toType);}
Position Position::operator*(Coord b)const
	{return Position(x*b.x,y*b.y,fromType,toType);}
Position Position::operator/(Coord b)const
	{return Position(x/b.x,y/b.y,fromType,toType);}
Position Position::operator*(double b)const
	{return Position((int)(x*b),(int)(y*b),fromType,toType);}
Position Position::operator/(double b)const
	{return Position((int)(x/b),(int)(y/b),fromType,toType);}
Position operator*(double b,Position c)
	{return c*b;}
Position Position::operator-()const
	{return Position(-x,-y,fromType,toType);}

Size Size::operator+(Coord b)const
	{return Size(x+b.x,y+b.y,type);}
Size Size::operator-(Coord b)const
	{return Size(x-b.x,y-b.y,type);}
Size Size::operator*(Coord b)const
	{return Size(x*b.x,y*b.y,type);}
Size Size::operator/(Coord b)const
	{return Size(x/b.x,y/b.y,type);}
Size Size::operator*(double b)const
	{return Size((int)(x*b),(int)(y*b),type);}
Size Size::operator/(double b)const
	{return Size((int)(x/b),(int)(y/b),type);}
Size operator*(double b,Size c)
	{return c*b;}
Size Size::operator-()const
	{return Size(-x,-y,type);}

Position Position::operator+(Position b)const
	{
	if(b.toType!=fromType)
		error("Position + Position with different type");
	return Position(x+b.x,y+b.y,b.fromType,toType);
	}
//==========================================================================

	Position::Position(Coord c,Type t,Type t2):Coord(c),fromType(t),toType(t2)
		{if(t2==Default)toType=fromType;}
	Position::Position(int x,int y,Type t,Type t2):Coord(x,y),fromType(t),toType(t2)
		{if(t2==Default)toType=fromType;}

Position	Position::change(const Picture *p1			,const Picture *p2			,Type outFromType,Type outToType)const
	{
	return Position(fix(p1,p2,outFromType,outToType),outFromType,outToType);
	}
Position	Position::change(Coord s,Coord s2,Coord s3	,const Picture *p2			,Type outFromType,Type outToType)const
	{
	return Position(fix(s,s2,s3,p2,outFromType,outToType),outFromType,outToType);
	}
Position	Position::change(const Picture *p1			,Coord d,Coord d2,Coord d3	,Type outFromType,Type outToType)const
	{
	return Position(fix(p1,d,d2,d3,outFromType,outToType),outFromType,outToType);
	}
Position	Position::change(Coord s,Coord s2,Coord s3	,Coord d,Coord d2,Coord d3	,Type outFromType,Type outToType)const
	{
	return Position(fix(s,s2,s3,d,d2,d3,outFromType,outToType),outFromType,outToType);
	}


Coord	Position::fix(const Picture *p1,const Picture *p2,Type outFromType,Type outToType)const
	{
	if(p1==NULL)
		return fix(zeroCoord,zeroCoord,zeroCoord,p2,outFromType,outToType);
	else
		return fix(p1->fixHeadSize,p1->showSize,p1->fixEndSize,p2,outFromType,outToType);
	}
	
//Coord	Position::fix(Coord s,Coord s2,const Picture *p2,Type outFromType,Type outToType)const
//	{
//	return fix(s,s2,zeroCoord,p2,outFromType,outToType);
//	}
	
Coord	Position::fix(Coord s,Coord s2,Coord s3,const Picture *p2,Type outFromType,Type outToType)const
	{
	if(p2==NULL)
		return fix(s,s2,s3,zeroCoord,zeroCoord,zeroCoord,outFromType,outToType);
	else
		return fix(s,s2,s3,p2->fixHeadSize,p2->showSize,p2->fixEndSize,outFromType,outToType);
	}

Coord	Position::fix(const Picture *p1,Coord d,Coord d2,Coord d3,Type outFromType,Type outToType)const
	{
	if(p1==NULL)
		return fix(zeroCoord,zeroCoord,zeroCoord,d,d2,d3,outFromType,outToType);
	else
		return fix(p1->fixHeadSize,p1->showSize,p1->fixEndSize,d,d2,d3,outFromType,outToType);
	}

Coord	Position::fix(Coord p1f,Coord p1s,Coord p1e,Coord p2f,Coord p2s,Coord p2e,Type outFromType,Type outToType)const
	{
	Coord c=(Coord)*this;
	Coord p1r=p1f+p1s+p1e,p2r=p2f+p2s+p2e;
	if(outToType==Default)
		outToType=outFromType;
	switch(toType)
		{
		case EffectiveLeftUp:
			c+=p2f;
			break;
		case EffectiveLeftDown:
			c.y=p2s.y-c.y-1;
			c+=p2f;
			break;
		case EffectiveRightUp:
			c.x=p2s.x-c.x-1;
			c+=p2f;
			break;
		case EffectiveRightDown:
			c.x=p2s.x-c.x-1;
			c.y=p2s.y-c.y-1;
			c+=p2f;
			break;
		case RealLeftUp:
			break;
		case RealLeftDown:
			c.y=p2r.y-c.y-1;
			break;
		case RealRightUp:
			c.x=p2r.x-c.x-1;
			break;
		case RealRightDown:
			c.x=p2r.x-c.x-1;
			c.y=p2r.y-c.y-1;
			break;
		default:
			error("Position::fix toType switchOut.");
		}
	switch(fromType)
		{
		case EffectiveLeftUp:
			c-=p1f;
			break;
		case EffectiveLeftDown:
			c-=p1f;
			c.y-=p1s.y;
			break;
		case EffectiveRightUp:
			c-=p1f;
			c.x-=p1s.x;
			break;
		case EffectiveRightDown:
			c-=p1f+p1s;
			break;
		case EffectiveMiddleUp:
			c-=p1f;
			c.x-=p1s.x/2;
			break;
		case EffectiveMiddleDown:
			c-=p1f;
			c.x-=p1s.x/2;
			c.y-=p1s.y;
			break;
		case EffectiveMiddleLeft:
			c-=p1f;
			c.y-=p1s.y/2;
			break;
		case EffectiveMiddleRight:
			c-=p1f;
			c.x-=p1s.x;
			c.y-=p1s.y/2;
			break;
		case Center:
			c-=p1f+p1s/2;
			break;
		case RealLeftUp:
			break;
		case RealLeftDown:
			c.y-=p1r.y;
			break;
		case RealRightUp:
			c.x-=p1r.x;
			break;
		case RealRightDown:
			c-=p1r;
			break;
		default:
			error("Position::fix fromType switchOut.");
		}
	switch(outFromType)
		{
		case EffectiveLeftUp:
			c+=p1f;
			break;
		case EffectiveLeftDown:
			c+=p1f;
			c.y+=p1s.y;
			break;
		case EffectiveRightUp:
			c+=p1f;
			c.x+=p1s.x;
			break;
		case EffectiveRightDown:
			c+=p1f+p1s;
			break;
		case EffectiveMiddleUp:
			c+=p1f;
			c.x+=p1s.x/2;
			break;
		case EffectiveMiddleDown:
			c+=p1f;
			c.x+=p1s.x/2;
			c.y+=p1s.y;
			break;
		case EffectiveMiddleLeft:
			c+=p1f;
			c.y+=p1s.y/2;
			break;
		case EffectiveMiddleRight:
			c+=p1f;
			c.x+=p1s.x;
			c.y+=p1s.y/2;
			break;
		case Center:
			c+=p1f+p1s/2;
			break;
		case RealLeftUp:
			break;
		default:
			error("Position::fix outFromType switchOut.");
		}
	switch(outToType)
		{
		case EffectiveLeftUp:
			c-=p2f;
			break;
		case EffectiveLeftDown:
			c-=p2f;
			c.y=p2s.y-c.y-1;
			break;
		case EffectiveRightUp:
			c-=p2f;
			c.x=p2s.x-c.x-1;
			break;
		case EffectiveRightDown:
			c-=p2f;
			c.x=p2s.x-c.x-1;
			c.y=p2s.y-c.y-1;
			break;
		case RealLeftUp:
			break;
		default:
			error("Position::fix outToType switchOut.");
		}
	return c;
	}

//==========================================================================

	Size::Size(Coord c,Type t):Coord(c),type(t){}

	Size::Size(int x,int y,Type t):Coord(x,y),type(t){}

Size	Size::change(const Picture *p,Type outType)
	{
	return Size(fix(p,outType),outType);
	}

Coord	Size::fix(const Picture *p,Type outType)
	{
	return fix(p->fixHeadSize+p->fixEndSize,outType);
	}

Coord	Size::fix(Coord s,Type outType)
	{
	Coord c=(Coord)*this;
	switch(type)
		{
		case Effective:
			c+=s;
			break;
		case Real:
			break;
		default:
			error("Size::fix switch out.");
		}
	switch(outType)
		{
		case Effective:
			c-=s;
			break;
		case Real:
			break;
		default:
			error("Size::fix switch out.");
		}
	return c;
	}

