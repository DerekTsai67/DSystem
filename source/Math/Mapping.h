



class Mapping{
	public:
virtual	~Mapping(){}
double	getValue(int xu,int xd)const;
double	getValue(double x)const;
	protected:
virtual	double	getValueRaw(double x)const=0;
	};


class SimpleMapping:public Mapping{
	protected:
double	getValueRaw(double x)const;
	};

class ExponentialMapping:public Mapping{
	public:
	ExponentialMapping(double decay);
	protected:
double	getValueRaw(double x)const;
double	base,decay;
	};



class InverseMapping:public Mapping{
	public:
	InverseMapping(bool x,bool y,Mapping *m);
	~InverseMapping();
	protected:
double	getValueRaw(double x)const;
bool	inverseX,inverseY;
Mapping	*mapping;
	};

















