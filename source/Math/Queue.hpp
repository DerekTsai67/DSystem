





template<class Type>
	Queue<Type>::Queue(int m)
	{
	argCheck(m<=1);
	max=m;
	startPoint=index=0;
	data=dalloc<Type>(max);
	}
	
template<class Type>
	Queue<Type>::~Queue()
	{
	dfree(data);
	}

	
	
	
template<class Type>
void	Queue<Type>::addItem(Type item)
	{
	if(index>=max)
		error("Queue overflow.");
	data[(startPoint+index)%max]=item;
	index++;
	}
	
	
	
template<class Type>
void	Queue<Type>::clear()
	{
	startPoint=index=0;
	}

template<class Type>
void	Queue<Type>::deleteAll()
	{
	for(int i=0;i<index;i++)
		delete peekItem(i);
	clear();
	}



template<class Type>
Type&	Queue<Type>::peekItem(int i)const
	{
	argCheck(i<0||i>=index);
	return data[(startPoint+i)%max];
	}

template<class Type>
Type&	Queue<Type>::takeItem()
	{
	Type &ret=data[startPoint];
	startPoint=(startPoint+1)%max;
	index--;
	return ret;
	}
	
template<class Type>
Type&	Queue<Type>::operator[](int i)const
	{
	return peekItem(i);
	}

template<class Type>
int		Queue<Type>::getNumber()const
	{
	return index;
	}



















