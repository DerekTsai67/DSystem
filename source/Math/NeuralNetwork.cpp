



		int c0,c1,c2,c3,testClock;

	NeuralNetwork::NeuralNetwork(int inNum,int hidNum,int outNum)
	{
	inputMax=inNum;
	hiddenMax=hidNum;
	outputMax=outNum;
	totalMax=1+inputMax+hiddenMax+outputMax;

	neuron=dalloc<Neuron>(totalMax);
	for(int i=0;i<totalMax;i++)
		{
		if(i<1+inputMax)
			neuron[i].weightStart=neuron[i].weightEnd=1;
		else if(i<1+inputMax+hiddenMax)
			{
			neuron[i].weightStart=1;
			neuron[i].weightEnd=1+inputMax;
			}
		else//out
			{
			neuron[i].weightStart=1+inputMax;
			neuron[i].weightEnd=1+inputMax+hiddenMax;
			}
		neuron[i].weight=dalloc<double>(i);
		neuron[i].weight[0]=gaussianRandom(0.0,1.0);
		for(int j=neuron[i].weightStart;j<neuron[i].weightEnd;j++)
			neuron[i].weight[j]=gaussianRandom(0.0,2.0/(neuron[i].weightEnd-neuron[i].weightStart));
		}
	neuron[0].output=1.0;
	answer=dalloc<double>(outputMax);
	
	trainInput=trainOutput=validInput=validOutput=testInput=testOutput=NULL;
	trainMax=validMax=testMax=0;
	
	trained=0;
	useEntropy=false;
	}
	NeuralNetwork::~NeuralNetwork()
	{
	for(int i=0;i<totalMax;i++)
		dfree(neuron[i].weight);
	dfree(neuron);
	dfree(answer);
	freeTrainingData();
	}

void	NeuralNetwork::setInput(int index,double value)
	{
	argCheck(index<0||index>=inputMax);
	neuron[1+index].output=value;
	}
	
double	NeuralNetwork::getOutput(int index)
	{
	argCheck(index<0||index>=outputMax);
	return neuron[1+inputMax+hiddenMax+index].output;
	}
	
void	NeuralNetwork::setAnswer(int index,double value)
	{
	argCheck(index<0||index>=outputMax);
	answer[index]=value;
	}
	
void	NeuralNetwork::setTrainingData(double **in,double **out,int num)
	{
	argCheck(trainInput!=NULL||in==NULL||out==NULL||num<=5);
	trainInput=in;
	trainOutput=out;
	trainMax=num;
	
	validMax=trainMax/5;
	if(validMax>10000)
		validMax=10000;
	testMax=validMax;
	trainMax-=validMax+testMax;
	
	validInput=trainInput+trainMax;
	validOutput=trainOutput+trainMax;
	testInput=trainInput+trainMax+validMax;
	testOutput=trainOutput+trainMax+validMax;
	}
void	NeuralNetwork::freeTrainingData()
	{
	if(trainInput==NULL)
		return;
	
	for(int i=0;i<trainMax+validMax+testMax;i++)
		{
		dfree(trainInput[i]);
		dfree(trainOutput[i]);
		}
	dfree(trainInput);
	dfree(trainOutput);
	
	trainInput=trainOutput=validInput=validOutput=testInput=testOutput=NULL;
	trainMax=validMax=testMax=0;
	}

void	NeuralNetwork::calValue()
	{
	for(int i=1+inputMax;i<totalMax;i++)
		{
		double *weight=neuron[i].weight;
		double v=weight[0]*neuron[0].output;
		for(int j=neuron[i].weightStart;j<neuron[i].weightEnd;j++)
			v+=weight[j]*neuron[j].output;
		neuron[i].input=v;
		neuron[i].output=1/(1+exp(-neuron[i].input));
		}
	}

int		NeuralNetwork::getHighestOutputIndex()
	{
	int rep=0;
	for(int i=1;i<outputMax;i++)
		if(getOutput(i)>getOutput(rep))
			rep=i;
	return rep;
	}

double	NeuralNetwork::getCost()
	{
	double cost=0.0;
	for(int i=0;i<outputMax;i++)
		{
		double er=neuron[1+inputMax+hiddenMax+i].output-answer[i];
		cost+=er*er;
		}
	return cost/outputMax*10;
	}
	
double	NeuralNetwork::getEntropyCost()
	{
	double cost=0.0;
	for(int i=0;i<outputMax;i++)
		cost+=answer[i]*log(neuron[1+inputMax+hiddenMax+i].output)+(1-answer[i])*log(1-neuron[1+inputMax+hiddenMax+i].output);
	return cost;
	}
	
void	NeuralNetwork::train()
	{
	//calValue();
	double *coe=dalloc<double>(totalMax);
	double *ans=answer-(1+inputMax+hiddenMax);
	
	if(!useEntropy)
	for(int i=1+inputMax+hiddenMax;i<totalMax;i++)
		coe[i]=(neuron[i].output-ans[i]);
	
	for(int y=totalMax-1;y>=1+inputMax;y--)
		{
		if(neuron[y].input<-200.0)
			coe[y]=0.0;
		else
			{
			double e=exp(-neuron[y].input);
			coe[y]*=e/(1+e)/(1+e);
			}
			
		if(useEntropy)
		if(y>=1+inputMax+hiddenMax)
			coe[y]+=(neuron[y].output-ans[y]);
			
		for(int x=neuron[y].weightStart>1+inputMax?neuron[y].weightStart:1+inputMax;x<neuron[y].weightEnd;x++)
			coe[x]+=coe[y]*neuron[y].weight[x];
		
		dWeight[y][0]-=coe[y]*neuron[0].output;
		for(int x=neuron[y].weightStart;x<neuron[y].weightEnd;x++)
			dWeight[y][x]-=coe[y]*neuron[x].output;
		}
	dfree(coe);
	trained++;
	}


void	NeuralNetwork::trainData(int epoch,int batchSize,double learningLevel,double regularLevel)
	{
	argCheck(trainInput==NULL);
	int lastTime=time(NULL);
	int startClock=clock();
	
	double bestCorrect=-1.0,bestCost=9999.9;
	//double lastRate=0.0,lastCost=-1.0;
	int stillCount=0,stillThreshold=1;
	//int freezeCount=0,freezeThreshold=20;
	
	//loadNN("E:\\back.nn");
	useEntropy=true;
	
	FILE *frep=fopen("E:\\rep.txt","at");
	fprintf(frep,"%s hidden:%d Batch:%d Learning:%.2f Regular:%f\n",useEntropy?"Entropy":"Quadrat",hiddenMax,batchSize,learningLevel,regularLevel);
	fclose(frep);
	
	dWeight=dalloc<double*>(totalMax);
	for(int i=0;i<totalMax;i++)
		dWeight[i]=dalloc<double>(i);
	
	for(int e=0;e<epoch;e++)
		{
		int trainCorrect=0;
		double trainCost=0.0;
		//if(e!=0)
		for(int t=0;t<1000;t++)
			{
			for(int y=1+inputMax;y<totalMax;y++)
			for(int x=neuron[y].weightStart;x<neuron[y].weightEnd;x++)
				neuron[y].weight[x]*=1.0-regularLevel*learningLevel;
			
			for(int b=0;b<batchSize;b++)
				{
				int n=random(0,trainMax);
				for(int i=0;i<inputMax;i++)
					neuron[1+i].output=trainInput[n][i];
				memcpy(answer,trainOutput[n],outputMax*sizeof(double));
				calValue();
				train();
				
				//if(getOutput(getHighestOutputIndex())>0.9)
				if(answer[getHighestOutputIndex()]>0.9)
					trainCorrect++;
				trainCost+=getCost();
				}
				
			/*
			double l=learningLevel;
			double speed=0.0;
			for(int y=1+inputMax;y<totalMax;y++)
			for(int x=0;x<y;x++)
				speed+=dWeight[y][x]*dWeight[y][x];
			speed*=2;
			speed*=l;
			totalCost/=batchIndex;
			if(speed>totalCost)
				l=totalCost/speed*0.7;
			//report("grow: *%d ct%f sp%f %f",batchIndex,totalCost,speed,l);*/
			for(int y=1+inputMax;y<totalMax;y++)
				{
				neuron[y].weight[0]+=dWeight[y][0]*learningLevel/batchSize;
				dWeight[y][0]=0.0;
				for(int x=neuron[y].weightStart;x<neuron[y].weightEnd;x++)
					{
					neuron[y].weight[x]+=dWeight[y][x]*learningLevel/batchSize;
					dWeight[y][x]=0.0;
					}
				}
				
			if(time(NULL)!=lastTime)
				{
				report("training %d %3d/1000",e,t);
				lastTime=time(NULL);
				}
			}
		trainCost/=1000*batchSize;
		report("clock %.2f",(double)(clock()-startClock)/CLOCKS_PER_SEC);
		
		
		int validCorrect=0;
		double validCost=0.0;
		for(int n=0;n<validMax;n++)
			{
			for(int i=0;i<inputMax;i++)
				neuron[1+i].output=validInput[n][i];
			memcpy(answer,validOutput[n],outputMax*sizeof(double));
			calValue();
			
			//if(getOutput(getHighestOutputIndex())>0.8)
			if(answer[getHighestOutputIndex()]>0.9)
				validCorrect++;
			validCost+=getCost();
			}
		validCost/=validMax;


		int testCorrect=0;
		double testCost=0.0;
		for(int n=0;n<testMax;n++)
			{
			for(int i=0;i<inputMax;i++)
				neuron[1+i].output=testInput[n][i];
			memcpy(answer,testOutput[n],outputMax*sizeof(double));
			calValue();

			//if(getOutput(getHighestOutputIndex())>0.8)
			if(answer[getHighestOutputIndex()]>0.9)
				testCorrect++;
			testCost+=getCost();
			/*if(n%1000==0)
				{
				printf("[%5d] %d %c %f %f",n,rep,testOutput[n][rep]>0.9?' ':'X',getOutput(rep),getCost());
				printf("\n");
				}*/
			}
		testCost/=testMax;
		
		double corrRate=(double)validCorrect/validMax;
		if(corrRate<=bestCorrect&&validCost>=bestCost)
			stillCount++;
		else
			stillCount=0;
		/*if(corrRate==bestCorrect&&validCost==bestCost)
			freezeCount++;
		else
			freezeCount=0;*/
		
		FILE *frep=fopen("E:\\rep.txt","at");
		report(			"epoch[%3d] %f %f %4d/%5d %f %2d %4d/%5d %f"			,e+1,learningLevel,trainCost,validCorrect,validMax,validCost,stillCount,testCorrect,testMax,testCost);
		fprintf(frep,	"epoch[%3d] %f %f %4d/%5d %f %2d %4d/%5d %f %7.2f\n"	,e+1,learningLevel,trainCost,validCorrect,validMax,validCost,stillCount,testCorrect,testMax,testCost,(double)(clock()-startClock)/CLOCKS_PER_SEC);
		fclose(frep);

		if(stillCount>=stillThreshold)
			{
			learningLevel/=3;
			stillThreshold=stillThreshold*3/2+1;
			if(stillThreshold>=20)
				break;
			}
		//if(freezeCount>=freezeThreshold)
		//	break;

		//lastRate=corrRate;
		//lastCost=validCost;
		if(corrRate>bestCorrect)
			{
			bestCorrect=corrRate;
			saveNN("E:\\BestCorrect.nn");
			}
		if(validCost<bestCost)
			{
			bestCost=validCost;
			saveNN("E:\\BestCost.nn");
			}
		report("clock %.2f",(double)(clock()-startClock)/CLOCKS_PER_SEC);
		}
		
	for(int i=0;i<totalMax;i++)
		dfree(dWeight[i]);
	dfree(dWeight);
	}






void	NeuralNetwork::saveNN(ConstString path)
	{
	argCheck(path==NULL);
	Output *out=new OutputFile(path);
	out->putInt(0x0000,4);
	out->putInt(inputMax,4);
	out->putInt(hiddenMax,4);
	out->putInt(outputMax,4);
	for(int y=1+inputMax;y<totalMax;y++)
	for(int x=0;x<y;x++)
		out->putDouble(neuron[y].weight[x]);
	delete out;
	}
void	NeuralNetwork::loadNN(ConstString path)
	{
	argCheck(path==NULL);
	Input *in=new InputFile(path);
	if(in->getInt(4)!=0x0000)
		error("Unsupported version.");
	if(in->getInt(4)!=inputMax)
		error("Wrong inputMax");
	if(in->getInt(4)!=hiddenMax)
		error("Wrong hiddenMax");
	if(in->getInt(4)!=outputMax)
		error("Wrong outputMax");
	for(int y=1+inputMax;y<totalMax;y++)
	for(int x=0;x<y;x++)
		neuron[y].weight[x]=in->getDouble();
	delete in;
	}















	NeuralLayer::NeuralLayer(int am,int bm,int cm,int dm)
	{
	argCheck(am<=0||bm<=0||cm<=0||dm<=0);
	aMax=am;
	bMax=bm;
	cMax=cm;
	dMax=dm;
	max=aMax*bMax*cMax*dMax;
	neuron=dalloc<Neuron>(max);
	}

void	NeuralLayer::initNeuron(int fromNum,int neuronMax)
	{
	if(neuronMax==-1)
		neuronMax=max;
	for(int i=0;i<neuronMax;i++)
		{
		neuron[i].input=neuron[i].output=0.0;

		neuron[i].bias=gaussianRandom(0.0,1.0);
		neuron[i].weight=dalloc<double>(fromNum);
		for(int j=0;j<fromNum;j++)
			neuron[i].weight[j]=gaussianRandom(0.0,1.0/fromNum);

		neuron[i].coe=neuron[i].dBias=0.0;
		neuron[i].dWeight=dalloc<double>(fromNum);
		}
	}

	NeuralLayer::~NeuralLayer()
	{
	for(int i=0;i<max;i++)
		{
		if(neuron[i].weight!=NULL)
			dfree(neuron[i].weight);
		if(neuron[i].dWeight!=NULL)
			dfree(neuron[i].dWeight);
		}
	dfree(neuron);
	}


void	NeuralLayer::setInputArray(double *in,int inNum)
	{
	if(inNum!=-1)
	if(inNum!=getInputNumber())
		error("Incorrect inNum.");
	int inIndex=0;
	preTrain(in,inIndex);
	}


Neuron&	NeuralLayer::getNeuron(int ai,int bi,int ci,int di)
	{
	argCheck(ai<0||bi<0||ci<0||di<0);
	argCheck(ai>=aMax||bi>=bMax||ci>=cMax||di>=dMax);
	return getNeuron(((di*cMax+ci)*bMax+bi)*aMax+ai);
	}
Neuron&	NeuralLayer::getNeuron(int index)
	{
	argCheck(index<0||index>=max);
	return neuron[index];
	}
double	NeuralLayer::getOutput(int ai,int bi,int ci,int di)
	{
	return getNeuron(ai,bi,ci,di).output;
	}
double	NeuralLayer::getOutput(int index)
	{
	return getNeuron(index).output;
	}
int		NeuralLayer::getNumber()
	{
	return max;
	}

int		NeuralLayer::getHighestOutputIndex()
	{
	int rep=0;
	for(int i=1;i<max;i++)
		if(getOutput(i)>getOutput(rep))
			rep=i;
	return rep;
	}

double	NeuralLayer::getCost(int ans)
	{
	double cost=0.0;
	for(int i=0;i<max;i++)
		{
		double er=(neuron[i].output+1.0)/2.0-(ans==i);
		cost+=er*er;
		}
	return cost/max*10;
	}








void	NeuralLayer::preBatch(double rl){}
void	NeuralLayer::preTrain(double *in,int& inIndex){}
void	NeuralLayer::postTrain(){}
void	NeuralLayer::postBatch(double ll){}




	InputNeuralLayer::InputNeuralLayer(int am,int bm,int cm,int dm):NeuralLayer(am,bm,cm,dm)
	{
	initNeuron(0);
	}
	InputNeuralLayer::~InputNeuralLayer()
	{
	}

void	InputNeuralLayer::setInput(double in,int ai,int bi,int ci,int di)
	{
	argCheck(ai<0||bi<0||ci<0||di<0);
	argCheck(ai>=aMax||bi>=bMax||ci>=cMax||di>=dMax);
	int index=((di*cMax+ci)*bMax+bi)*aMax+ai;
	neuron[index].input=neuron[index].output=in;
	}

void	InputNeuralLayer::calOutput()
	{
	}
int		InputNeuralLayer::getInputNumber()
	{
	return max;
	}


void	InputNeuralLayer::preTrain(double *in,int& inIndex)
	{
	argCheck(in==NULL);
	in+=inIndex;
	for(int ni=0;ni<max;ni++)
		/*neuron[ni].input=*/neuron[ni].output=in[ni];
	inIndex+=max;
	}









	FeatureNeuralLayer::FeatureNeuralLayer(NeuralLayer *nl,int am,int bm,int cm,int dm):NeuralLayer(am,bm,cm,dm)
	{
	argCheck(nl==NULL);
	from=nl;
	aSize=from->aMax-am+1;
	bSize=from->bMax-bm+1;
	cSize=from->cMax-cm+1;
	dSize=from->dMax;
	argCheck(aSize<=0||bSize<=0||cSize<=0||dSize<=0);
	fromNumber=aSize*bSize*cSize*dSize;
	
	//initNeuron(-)
	initNeuron(fromNumber,dMax);
	/*bias=gaussianRandom(0.0,1.0);
	weight=dalloc<double>(fromNumber);
	for(int j=0;j<fromNumber;j++)
		weight[j]=gaussianRandom(0.0,2.0/fromNumber);
	coe=dBias=0.0;
	dWeight=dalloc<double>(fromNumber);
	
	for(int i=0;i<max;i++)
		{
		neuron[i].input=neuron[i].output=0.0;
		neuron[i].weight=NULL;
		neuron[i].coe=neuron[i].dBias=0.0;
		neuron[i].dWeight=NULL;
		}*/
	}
	FeatureNeuralLayer::~FeatureNeuralLayer()
	{
	delete from;
	}


void	FeatureNeuralLayer::calOutput()
	{
	from->calOutput();
	for(int ami=0;ami<aMax;ami++)
	for(int bmi=0;bmi<bMax;bmi++)
	for(int cmi=0;cmi<cMax;cmi++)
	for(int dmi=0;dmi<dMax;dmi++)
		{
		int ni=((dmi*cMax+cmi)*bMax+bmi)*aMax+ami;
		neuron[ni].input=neuron[dmi].bias;
		for(int asi=0;asi<aSize;asi++)
		for(int bsi=0;bsi<bSize;bsi++)
		for(int csi=0;csi<cSize;csi++)
		for(int dsi=0;dsi<dSize;dsi++)
			{
			neuron[ni].input+=
				from->neuron[((dsi*from->cMax+cmi+csi)*from->bMax+bmi+bsi)*from->aMax+ami+asi].output
				*neuron[dmi].weight[((dsi*cSize+csi)*bSize+bsi)*aSize+asi];
			}
		neuron[ni].output=2.0/(1+exp(-neuron[ni].input))-1.0;
		}
	}
int		FeatureNeuralLayer::getInputNumber()
	{
	return from->getInputNumber();
	}

void	FeatureNeuralLayer::preBatch(double regularLevel)
	{
	for(int dmi=0;dmi<dMax;dmi++)
	for(int wi=0;wi<fromNumber;wi++)
		neuron[dmi].weight[wi]*=regularLevel;
	from->preBatch(regularLevel);
	}
void	FeatureNeuralLayer::preTrain(double *in,int& inIndex)
	{
	from->preTrain(in,inIndex);
	}
void	FeatureNeuralLayer::postTrain()
	{
	for(int ami=0;ami<aMax;ami++)
	for(int bmi=0;bmi<bMax;bmi++)
	for(int cmi=0;cmi<cMax;cmi++)
	for(int dmi=0;dmi<dMax;dmi++)
		{
		int ni=((dmi*cMax+cmi)*bMax+bmi)*aMax+ami;
		double inCoe;
		if(neuron[ni].input<-200.0)
			inCoe=0.0;
		else
			{
			double e=exp(-neuron[ni].input);
			inCoe=neuron[ni].coe*e/(1+e)/(1+e)*2.0;
			}
		neuron[ni].coe=0.0;
			
		neuron[dmi].dBias-=inCoe;
		for(int asi=0;asi<aSize;asi++)
		for(int bsi=0;bsi<bSize;bsi++)
		for(int csi=0;csi<cSize;csi++)
		for(int dsi=0;dsi<dSize;dsi++)
			{
			Neuron &n=from->neuron[((dsi*from->cMax+cmi+csi)*from->bMax+bmi+bsi)*from->aMax+ami+asi];
			n.coe+=
				inCoe*neuron[dmi].weight[((dsi*cSize+csi)*bSize+bsi)*aSize+asi];
			neuron[dmi].dWeight[((dsi*cSize+csi)*bSize+bsi)*aSize+asi]-=inCoe*n.output;
			}
		}
	from->postTrain();
	}

void	FeatureNeuralLayer::postBatch(double learningLevel)
	{
	double ll=learningLevel/12;
	for(int dmi=0;dmi<dMax;dmi++)
		{
		neuron[dmi].bias+=neuron[dmi].dBias*ll;
		neuron[dmi].dBias=0.0;
		for(int wi=0;wi<fromNumber;wi++)
			{
			neuron[dmi].weight[wi]+=neuron[dmi].dWeight[wi]*ll;
			neuron[dmi].dWeight[wi]=0.0;
			}
		}
	from->postBatch(learningLevel*2);
	}





	FullNeuralLayer::FullNeuralLayer(int num,NeuralLayer *fa,NeuralLayer *fb,NeuralLayer *fc,NeuralLayer *fd):NeuralLayer(num)
	{
	argCheck(fa==NULL);
	fromLayer.addItem(fa);
	if(fb!=NULL)
		fromLayer.addItem(fb);
	if(fc!=NULL)
		fromLayer.addItem(fc);
	if(fd!=NULL)
		fromLayer.addItem(fd);
	
	fromNumber=0;
	FOR_ARRAY(i,fromLayer)
		fromNumber+=fromLayer[i]->getNumber();
	initNeuron(fromNumber);
	}
	FullNeuralLayer::~FullNeuralLayer()
	{
	fromLayer.deleteAll();
	}

	
void	FullNeuralLayer::calOutput()
	{
	FOR_ARRAY(i,fromLayer)
		fromLayer[i]->calOutput();
	for(int ni=0;ni<max;ni++)
		{
		neuron[ni].input=neuron[ni].bias;
		int wi=0;
		FOR_ARRAY(li,fromLayer)
			{
			NeuralLayer *from=fromLayer[li];
			int fromNum=from->getNumber();
			for(int lwi=0;lwi<fromNum;lwi++)
				neuron[ni].input+=from->neuron[lwi].output*neuron[ni].weight[wi+lwi];
			wi+=fromNum;
			}
		neuron[ni].output=2.0/(1+exp(-neuron[ni].input))-1.0;
		}
	}

int		FullNeuralLayer::getInputNumber()
	{
	int ret=0;
	FOR_ARRAY(i,fromLayer)
		ret+=fromLayer[i]->getInputNumber();
	return ret;
	}



	
void	FullNeuralLayer::preBatch(double regularLevel)
	{
	for(int ni=0;ni<max;ni++)
	for(int wi=0;wi<fromNumber;wi++)
		neuron[ni].weight[wi]*=regularLevel;
	
	FOR_ARRAY(i,fromLayer)
		fromLayer[i]->preBatch(regularLevel);
	}

void	FullNeuralLayer::preTrain(double *in,int& inIndex)
	{
	FOR_ARRAY(i,fromLayer)
		fromLayer[i]->preTrain(in,inIndex);
	}

void	NeuralLayer::trainSample(double *in,int ans)
	{
	argCheck(in==NULL);
	int inIndex=0;
	preTrain(in,inIndex);
	calOutput();
	for(int i=0;i<max;i++)
		neuron[i].coe=(neuron[i].output+1.0)/2.0-(ans==i);
	postTrain();
	}

void	FullNeuralLayer::postTrain()
	{
	for(int ni=0;ni<max;ni++)
		{
		double inCoe;
		if(neuron[ni].input<-200.0)
			inCoe=0.0;
		else
			{
			double e=exp(-neuron[ni].input);
			inCoe=neuron[ni].coe*e/(1+e)/(1+e)*2.0;
			}
		neuron[ni].coe=0.0;
			
		int wi=0;
		neuron[ni].dBias-=inCoe;
		FOR_ARRAY(li,fromLayer)
			{
			NeuralLayer *from=fromLayer[li];
			int fromNum=from->getNumber();
			for(int lwi=0;lwi<fromNum;lwi++)
				{
				from->neuron[lwi].coe+=inCoe*neuron[ni].weight[wi+lwi];
				neuron[ni].dWeight[wi+lwi]-=inCoe*from->neuron[lwi].output;
				}
			wi+=fromNum;
			}
		}
	FOR_ARRAY(i,fromLayer)
		fromLayer[i]->postTrain();
	}

void	FullNeuralLayer::postBatch(double learningLevel)
	{
	for(int ni=0;ni<max;ni++)
		{
		neuron[ni].bias+=neuron[ni].dBias*learningLevel;
		neuron[ni].dBias=0.0;
		for(int wi=0;wi<fromNumber;wi++)
			{
			neuron[ni].weight[wi]+=neuron[ni].dWeight[wi]*learningLevel;
			neuron[ni].dWeight[wi]=0.0;
			}
		}
	FOR_ARRAY(i,fromLayer)
		fromLayer[i]->postBatch(learningLevel*2);
	}










void	NeuralLayer::train(double ***in,int *inMax,int inNum,int outNum,int sampleNum,int epochMax,int batchSize,double learningLevel,double regularLevel)
	{
	argCheck(in==NULL||inMax==NULL||sampleNum<=5||batchSize<1||learningLevel<=0.0||regularLevel<0.0);
	if(inNum!=-1&&inNum!=getInputNumber())
		error("Incorrect inNum.");
	if(outNum!=-1&&outNum!=max)
		error("Incorrect outNum.");
	
	int	validMax=inMax[0];
	for(int oi=1;oi<max;oi++)
		takeMin(validMax,inMax[oi]);
	validMax/=5;
	takeMin(validMax,10000/max);
	
	int	*trainMax=dalloc<int>(max);
	double ***trainInput=dalloc<double**>(max);
	double ***validInput=dalloc<double**>(max);
	double ***testInput =dalloc<double**>(max);
	for(int oi=0;oi<max;oi++)
		{
		trainMax	[oi]=inMax[oi]-validMax*2;
		trainInput	[oi]=in[oi];
		validInput	[oi]=in[oi]+trainMax[oi];
		testInput 	[oi]=in[oi]+trainMax[oi]+validMax;
		}
	
	double bestCorrect=-1.0,bestCost=9999.9;
	//double lastRate=0.0,lastCost=-1.0;
	int stillCount=0,stillThreshold=1;
	bool batchGrow=true;
	
	FILE *frep=fopen("E:\\rep.txt","at");
	fprintf(frep,"\nDeepLearn Batch:%d Learning:%.2f Regular:%f\n",batchSize,learningLevel,regularLevel);
	char stru[2000];
	int struIndex=0;
	printStructure(stru,struIndex);
	fprintf(frep,"%s\n",stru);
	fclose(frep);
	
	int lastTime=time(NULL);
	int startClock=clock();
	
	for(int e=0;e<epochMax;e++)
		{
		//int trainCorrect=0;
		double trainCost=0.0;
		c0=0;c1=0;c2=0;c3=0;testClock=clock();
		for(int t=0;t<1000;t++)
			{
			preBatch(1.0-regularLevel*learningLevel);
			
			for(int b=0;b<batchSize;b++)
			for(int oi=0;oi<max;oi++)
				{
				int n=random(0,trainMax[oi]);
				trainSample(trainInput[oi][n],oi);
				
				//if(getOutput(getHighestOutputIndex())>0.9)
				//if(trainOutput[n][getHighestOutputIndex()]>0.9)
				//	trainCorrect++;
				trainCost+=getCost(oi);
				}
			
			postBatch(learningLevel/batchSize/max);
			
			
			if(time(NULL)!=lastTime)
				{
				report("training %d %3d/1000",e,t);
				lastTime=time(NULL);
				}
			}
		trainCost/=1000*batchSize*max;
		report("clock %.2f",(double)(clock()-startClock)/CLOCKS_PER_SEC);
		//report("c0=%d c1=%d c2=%d c3=%d",c0*1000/CLOCKS_PER_SEC,c1*1000/CLOCKS_PER_SEC,c2*1000/CLOCKS_PER_SEC,c3*1000/CLOCKS_PER_SEC);
		//c0+=clock()-testClock;testClock=clock();
		
		
		int validNum=validMax;
		//if(validCost>=0.05)
		//	validNum/=10;
		if(validNum<=0)
			validNum=validMax;
		int validCorrect=0;
		double validCost=0.0;
		for(int n=0;n<validNum;n++)
		for(int oi=0;oi<max;oi++)
			{
			setInputArray(validInput[oi][n]);
			calOutput();
			
			//if(getOutput(getHighestOutputIndex())>0.8)
			if(getHighestOutputIndex()==oi)
				validCorrect++;
			validCost+=getCost(oi);
			/*if(n==validMax/2&&2.0*validCorrect/validMax<0.95)
				{
				validCorrect*=2;
				validCost*=2;
				break;
				}*/
			}
		validNum*=max;
		validCost/=validNum;


		int testNum=validMax;
		if(validCost>=0.05)
			testNum/=10;
		if(testNum<=0)
			testNum=validMax;
		int testCorrect=0;
		double testCost=0.0;
		for(int n=0;n<testNum;n++)
		for(int oi=0;oi<max;oi++)
			{
			setInputArray(testInput[oi][n]);
			calOutput();

			//if(getOutput(getHighestOutputIndex())>0.8)
			if(getHighestOutputIndex()==oi)
				testCorrect++;
			testCost+=getCost(oi);
			}
		testNum*=max;
		testCost/=testNum;
		
		double corrRate=(double)validCorrect/validNum;
		if(corrRate<=bestCorrect&&validCost>=bestCost)
			stillCount++;
		else
			stillCount=0;
		/*if(corrRate==bestCorrect&&validCost==bestCost)
			freezeCount++;
		else
			freezeCount=0;*/
		
		FILE *frep=fopen("E:\\rep.txt","at");
		report(			"batchSize %d learningLevel %f threshold %d",batchSize,learningLevel,stillThreshold);
		report(			"epoch[%3d] %f %4d/%5d %f %2d %4d/%5d %f"			,e+1,trainCost,validCorrect,validNum,validCost,stillCount,testCorrect,testNum,testCost);
		fprintf(frep,	"epoch[%3d] %f %4d/%5d %f %2d %4d/%5d %f %7.2f\n"	,e+1,trainCost,validCorrect,validNum,validCost,stillCount,testCorrect,testNum,testCost,(double)(clock()-startClock)/CLOCKS_PER_SEC);
		fclose(frep);
		
		if(stillCount==3&&batchGrow==true)
			{
			batchSize*=2;
			batchGrow=false;
			FILE *frep=fopen("E:\\rep.txt","at");
			report(			"batchSize grow %d",batchSize);
			fprintf(frep,	"batchSize grow %d\n",batchSize);
			fclose(frep);
			}
		
		if(stillCount>=stillThreshold)
			{
			learningLevel/=3;
			stillThreshold=stillThreshold*3/2+1;
			if(stillThreshold>=20)
				break;
			FILE *frep=fopen("E:\\rep.txt","at");
			report(			"learningLevel decay %f threshold %d",learningLevel,stillThreshold);
			fprintf(frep,	"learningLevel decay %f threshold %d\n",learningLevel,stillThreshold);
			fclose(frep);
			}
		//if(freezeCount>=freezeThreshold)
		//	break;

		//lastRate=corrRate;
		//lastCost=validCost;
		if(corrRate>bestCorrect)
			{
			bestCorrect=corrRate;
			//saveNN("E:\\BestCorrect.nn");
			}
		if(validCost<bestCost)
			{
			bestCost=validCost;
			//saveNN("E:\\BestCost.nn");
			}
		report("clock %.2f",(double)(clock()-startClock)/CLOCKS_PER_SEC);
		}
	dfree(trainMax);
	dfree(trainInput);
	dfree(validInput);
	dfree(testInput);
	}





void	InputNeuralLayer::printStructure(char *str,int& index)
	{
	index+=sprintf(str+index,"Input[%d]",aMax);
	if(bMax>1)
		index+=sprintf(str+index,"[%d]",bMax);
	if(cMax>1)
		index+=sprintf(str+index,"[%d]",cMax);
	if(dMax>1)
		index+=sprintf(str+index,"[%d]",dMax);
	}
void	FeatureNeuralLayer::printStructure(char *str,int& index)
	{
	from->printStructure(str,index);
	index+=sprintf(str+index,"->Feature[%d]",aMax);
	if(bMax>1)
		index+=sprintf(str+index,"[%d]",bMax);
	if(cMax>1)
		index+=sprintf(str+index,"[%d]",cMax);
	if(dMax>1)
		index+=sprintf(str+index,"[%d]",dMax);
	}
void	FullNeuralLayer::printStructure(char *str,int& index)
	{
	if(fromLayer.getNumber()==1)
		fromLayer[0]->printStructure(str,index);
	else
		FOR_ARRAY(i,fromLayer)
			{
			if(i!=0)
				index+=sprintf(str+index,"+");
			index+=sprintf(str+index,"(");
			fromLayer[i]->printStructure(str,index);
			index+=sprintf(str+index,")");
			}
	index+=sprintf(str+index,"->Full[%d]",max);
	}






