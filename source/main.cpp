

char		savePath[500];
Datafile	*fileData=NULL;
int			dataIndex=0;
Data		*typeData=NULL;
int			dataType=Data::Type_nothing;

Picture		*imgData;
int			frameIndex;
Frame		*frameData;
Layer		*editingLayer;
Coord		enclosurePos,enclosureSize;
EnclosureSensor	*enclosureSensor;
Coord		clickPos;
Text		*textData;

Coord		imgPos,origin;
Coord		imgSize;
double		imgScale;


StepContainer<Datafile>	*stepContainer;

Clipboard	*clipboard;

Datafile	*DSPaintData,*iconData,*foldingArrow,*toolData;
DS_RGB		actingColor[2];
bool		colorSkip;
MainManager	*manager;


void	enableLayer()
	{
	stepContainer->addStep(new PictureProcessStep(PictureProcess_enableLayer));
	}


int main(int argc,char *argv[])
	{
	window_size=Coord(1600,900);
	initDSystem(argv[0]);
	

	copyString(savePath,"D:\\temp.dsd",500);
	typeData=NULL;
	fileData=NULL;
	if(argc>=2)
		{
		if(isSuffix(argv[1],".dsd"))
			copyString(savePath,argv[1],500);
		fileData=loadFile(argv[1],true);
		}
	if(fileData==NULL)
		fileData=new Datafile();

	if(fileData->getNumber()==0)
		{
		DSBitmap *bmp=getOSClipboardBitmap(true);
		if(bmp!=NULL)
			fileData->addData(new Picture(bmp));
		else
			fileData->addData(new Picture(Coord(32,32),gray));
		}


	DSPaintData=new Datafile("datafile\\DSPaint.dsd");
	iconData=new Datafile("datafile\\Icon.dsd");
	foldingArrow=new Datafile("datafile\\FoldingArrow.dsd");
	toolData=new Datafile("datafile\\Tool.dsd");



	addWindow(base=new DSPaintBase());
	
	stepContainer=new StepContainer<Datafile>(fileData);
	stepContainer->setResponse(base,ResponseIndex_step);
	
	clipboard=new Clipboard();
	
	enclosurePos=enclosureSize=clickPos=zeroCoord;
	
	actingColor[0]=red;
	actingColor[1]=red;
	colorSkip=true;
	

	initTool();
	
	addWindow(manager=new MainManager());
	
	int picIndex=fileData->getPicIndex();
	takeMax(picIndex,0);
	setData(picIndex);


	DSystem();

	destroyTool();
	delete clipboard;
	delete stepContainer;
	delete toolData;
	delete foldingArrow;
	delete iconData;
	delete DSPaintData;
	delete fileData;

	endCheck();
	return 0;
	}




void	setData(int index)
	{
	report("settingData from %d",dataType);

	switch(dataType)
		{
	case Data::Type_Text:
		//textData=NULL;
		break;
	case Data::Type_Picture:
		//imgData=NULL;
		break;
		}

	index%=fileData->getNumber();
	while(index<0)
		index+=fileData->getNumber();
	dataIndex=index;
	typeData=fileData->getData(dataIndex);
	bool typeChanged=(dataType!=typeData->type);
	dataType=typeData->type;

	report("settingData to %d",dataType);

	switch(dataType)
		{
	case Data::Type_nothing:
		break;
	case Data::Type_Text:
		textData=typeData->getText();
		imgSize=Coord(1000,1000);
		break;
	case Data::Type_Picture:
		imgData=typeData->getPic();
		imgSize=imgData->getDataSize();
		setFrame(0);
		if(imgData->getFrameNumber()>1)
			manager->response(ResponseIndex_animationBar);
		manager->response(ResponseIndex_layerManager);
		break;
	default:
		error("Unsupported type.");
		}
	
	refreshWindow();
	if(typeChanged)
		resetSight();
	report("settedData to %d",dataType);
	}


void	setFrame(int fi)
	{
	argCheck(dataType!=Data::Type_Picture);
	fi%=imgData->getFrameNumber();
	while(fi<0)
		fi+=imgData->getFrameNumber();
	frameIndex=fi;
	imgData->setFrameIndex(fi);
	frameData=imgData->nowFrame;
	setLayer(frameData->getLayer());
	//report("SetFrame 3");
	if(manager->animationBar!=NULL)
		manager->animationBar->refreshIndex(frameIndex);
	//report("SetFrame 4");
	if(manager->layerManager!=NULL)
		refreshWindow(manager->layerManager);
	imgReshow();
	}

void	setLayer(Layer *l)
	{
	editingLayer=l;
	if(manager->layerManager!=NULL)
		manager->layerManager->select(l);
	}

void	setColor(DS_RGB c,int index)
	{
	if(colorSkip)
		{
		if(index!=0)
			return;
		actingColor[1]=c;
		}
	actingColor[index]=c;
	if(manager->colorPanel!=NULL)
		manager->colorPanel->response(9);
	manager->reShow();
	}


void	resetSight()
	{
	switch(dataType)
		{
	case Data::Type_nothing:
	case Data::Type_Text:
		origin=Position(100,80,Position::RealLeftUp);
		imgPos=Position(zeroCoord,Position::RealLeftUp);
		imgScale=1.0;
		break;
	case Data::Type_Picture:
		origin=Position(window_size/2,Position::RealLeftUp);
		imgPos=Position(imgSize/2,Position::RealLeftUp);
		imgScale=1.0;
		break;
		}
	imgReshow();
	}

void	imgReshow()
	{
	if(manager->navigator!=NULL)
		manager->navigator->reShow();
	base->reShow();
	}

Coord	imgCoord(Coord p,bool center)
	{
	if(imgScale<=0.01)
		return zeroCoord;
	p-=origin;
	if(center)
		p+=Coord(imgScale/2);
	if(p.x<0)
		p.x-=imgScale-1.0;
	if(p.y<0)
		p.y-=imgScale-1.0;
	p/=imgScale;

	return imgPos+p;
	}
	
Coord	rawCoord(Coord p,bool center)
	{
	if(imgScale<=0.01)
		return zeroCoord;
	p-=imgPos;
	p.x=(0.5*center+p.x)*imgScale+0.999*(1-2*(p.x<0));
	p.y=(0.5*center+p.y)*imgScale+0.999*(1-2*(p.y<0));
	return origin+p;
	}


	DSPaintBase::DSPaintBase()
	{
	}

	DSPaintBase::~DSPaintBase()
	{
	SENSE->left=NULL;
	SENSE->middle=NULL;
	SENSE->right=NULL;
	}

void	DSPaintBase::input()
	{
	if(keySignal.down(KEY_CTRL|KEY_Q))
		{
		imgData->getEditableBitmap()->convertAlpha(DS_RGB(255,0,255));
		}
	
	if(keySignal.down(KEY_CTRL|KEY_S))
		{
		fileData->save(savePath);
		}
	if(keySignal.down(KEY_CTRL|KEY_Z))
		{
		stepContainer->stepBack();
		}
	if(keySignal.down(KEY_CTRL|KEY_Y))
		{
		stepContainer->stepForward();
		}
		
	if(!keySignal.press(KEY_CTRL))
		{
		
		
		const double scalingSpeed=0.03;
		if(keySignal.press(KEY_Z))
			{
			Coord oldImgCoord=imgCoord(cursorPos,true);
			imgScale*=1.0+scalingSpeed;
			origin+=cursorPos-rawCoord(oldImgCoord);
			if(manager->navigator!=NULL)
				manager->navigator->response(3);
			refreshWindow(manager->navigator);
			imgReshow();
			}
		if(keySignal.press(KEY_X))
			{
			Coord oldImgCoord=imgCoord(cursorPos,true);
			imgScale*=1.0-scalingSpeed;
			origin+=cursorPos-rawCoord(oldImgCoord);
			if(manager->navigator!=NULL)
				manager->navigator->response(3);
			refreshWindow(manager->navigator);
			imgReshow();
			}
			
			
		}
		

	switch(dataType)
		{
	case Data::Type_Picture:
		
		if(keySignal.down(KEY_CTRL|KEY_A))
			{
			response(ResponseIndex_selectAll);
			}
		if(keySignal.down(KEY_CTRL|KEY_C))
			{
			response(ResponseIndex_copyMulti);
			}
		if(keySignal.down(KEY_CTRL|KEY_V))
			{
			response(ResponseIndex_paste);
			}
		
		if(keySignal.down(KEY_COMMA)||keySignal.down(KEY_DOT))
			{
			if(keySignal.down(KEY_COMMA))
				setFrame(imgData->getFrameIndex()-1);
			if(keySignal.down(KEY_DOT))
				setFrame(imgData->getFrameIndex()+1);
			AnimationBar::playTime=0;
			AnimationBar::clickTime=clock();
			}
		if(keySignal.press(KEY_COMMA)||keySignal.press(KEY_DOT))
			{
			if(AnimationBar::playTime==0)
				{
				if((clock()-AnimationBar::clickTime)*1000/CLOCKS_PER_SEC>500)
					AnimationBar::playTime=imgData->getTime();
				}
			else
				{
				int time=imgData->getTime();
				if(time-AnimationBar::playTime>frameData->getDelay())
					{
					time-=frameData->getDelay();
					AnimationBar::playTime+=frameData->getDelay();
					if(keySignal.press(KEY_COMMA))
						setFrame(imgData->getFrameIndex()-1);
					if(keySignal.press(KEY_DOT))
						setFrame(imgData->getFrameIndex()+1);
					}
				}
			}

		if(keySignal.down(KEY_N1))
			{
			Coord c=imgCoord(cursorPos);
			stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_headSize,c));
			}
		if(keySignal.down(KEY_N2))
			{
			Coord c=imgCoord(cursorPos);
			stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_fixHeadSize,c));
			}
		if(keySignal.down(KEY_N3))
			{
			Coord c=imgCoord(cursorPos);
			if(c>zeroCoord)
				{
				showThread->waitOnce();
				stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_fixEndSize,c));
				}
			}
		if(keySignal.down(KEY_N4))
			{
			Coord c=imgCoord(cursorPos);
			if(c>zeroCoord)
				{
				showThread->waitOnce();
				stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_endSize,c));
				}
			}
		if(keySignal.down(KEY_N5))
			{
			Coord c=imgCoord(cursorPos);
			stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_squareHeadSize,c));
			}
		if(keySignal.down(KEY_N6))
			{
			Coord c=imgCoord(cursorPos);
			stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_squareEndSize,c));
			}
		break;
	case Data::Type_Text:
		break;
		}
	
	//imgReshow();
	}

void	DSPaintBase::response(int ri)
	{
	switch(ri)
		{
	case ResponseIndex_step:
		if(manager->stepManager!=NULL)
			refreshWindow(manager->stepManager);
		imgReshow();
		break;
	
	case ResponseIndex_setSize:
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_endSize,enclosurePos+enclosureSize));
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_headSize,enclosurePos));
		break;
	case ResponseIndex_setFixSize:
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_fixEndSize,enclosurePos+enclosureSize));
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_fixHeadSize,enclosurePos));
		break;
	case ResponseIndex_setSquareSize:
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_squareEndSize,enclosurePos+enclosureSize));
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_squareHeadSize,enclosurePos));
		break;
		
	case ResponseIndex_setHeadSize:
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_headSize,clickPos));
		break;
	case ResponseIndex_setEndSize:
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_endSize,clickPos));
		break;
	case ResponseIndex_setFixHeadSize:
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_fixHeadSize,clickPos));
		break;
	case ResponseIndex_setFixEndSize:
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_fixEndSize,clickPos));
		break;
	case ResponseIndex_setSquareHeadSize:
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_squareHeadSize,clickPos));
		break;
	case ResponseIndex_setSquareEndSize:
		stepContainer->addStep(new ChangeSizeStep(ChangeSizeProcess_squareEndSize,clickPos));
		break;

	case ResponseIndex_cloneMulti:
		{
		enableLayer();
		Layer *layer=new SimpleLayer(imgSize);
		layer->getEditableBitmap()->blit(enclosurePos,editingLayer->getBitmap(),enclosurePos,enclosureSize);
		stepContainer->addStep(new AddLayerStep(layer,editingLayer,false));
		}
		break;
	
	case ResponseIndex_selectAll:
		enclosurePos=zeroCoord;
		enclosureSize=imgSize;
		reShow();
		break;
	case ResponseIndex_copyMulti:
		{
		if(enclosureSize==zeroCoord)
			break;
		DSBitmap *bmp=new DSBitmap(enclosureSize);
		bmp->blit(zeroCoord,editingLayer->getBitmap(),enclosurePos,enclosureSize);
		setOSClipboardBitmap(bmp);
		clipboard->clear(0);
		delete bmp;
		}
		break;
	case ResponseIndex_paste:
		{
		DSBitmap *bmp=getOSClipboardBitmap(true);
		if(bmp==NULL)
			break;
		if(!editingLayer->bitmapEditable())
			{
			SimpleLayer *layer=new SimpleLayer(imgSize);
			stepContainer->addStep(new AddLayerStep(layer,editingLayer,false));
			setLayer(layer);
			}
		enclosureSensor->paste(bmp);
		}
		break;
		}
	}

Sense*	DSPaintBase::sense(Coord c)
	{
	SENSE->left		=actingTool[dataType][0]->getSensor(0);
	SENSE->right	=actingTool[dataType][1]->getSensor(1);
	SENSE->middle	=actingTool[dataType][2]->getSensor(2);
	return defaultSense;
	}


void	DSPaintBase::show()
	{
	fullRender();
	Coord p0=origin-imgPos*imgScale;
	//Coord s9=Coord(100,100);

	wBuffer->clear(DS_RGB(25,25,25));

	switch(dataType)
		{
	case Data::Type_nothing:
		wBuffer->blitSquare(DSData->getPic(BDF_hollowBG),Position(p0),Size(100,10));
		break;
	case Data::Type_Text:
		wBuffer->blitSquare(DSData->getPic(BDF_hollowBG),Position(p0),Size(imgSize*imgScale));
		for(int i=0;i<textData->getNumber();i++)
			font->putString(wBuffer,textData->getViewLine(i),Position(origin+(Coord(0,i*font->getSize().y)-imgPos)*imgScale));
		break;
	case Data::Type_Picture:
		{
		wBuffer->blitSquare(DSData->getPic(BDF_hollowBG),Position(p0),Size(imgSize*imgScale));
		wBuffer->drawScale(imgData,Position(origin+(imgData->nowFrame->getLayer()->getDeviation()-imgPos)*imgScale
							,Position::RealLeftUp,Position::EffectiveLeftUp),Size(imgSize*imgScale,Size::Real));
		}
		break;
	default:
		error("switchOut");
		}
	
	if(enclosureSize!=zeroCoord)
		wBuffer->drawRect(Position(p0+enclosurePos*imgScale,Position::RealLeftUp),enclosureSize*imgScale,blue);
	//return;
	//wBuffer->blitSquare(DSData->getPic(BDF_hollowBG),Position(p0),Size(s9));
	//wBuffer->drawScale(imgData,Position(p0,Position::RealLeftUp),Size(s9,Size::Real));
	//return;
	if(imgScale>=3.0&&dataType==Data::Type_Picture)
		{
		Coord s0=imgData->getFixHeadSize()*imgScale;
		Coord s1=imgData->getSize()*imgScale;
		Coord s2=imgData->getFixEndSize()*imgScale;
		Coord p1=p0+s0;
		Coord p2=p1+s1;
		Coord p3=p2+s2;
		wBuffer->drawRect(Position(p1),Size(s1),green);

		s0-=Coord(1,1);
		s2-=Coord(1,1);
		p1-=Coord(1,1);
		wBuffer->drawLineD(Position(p1.x,p1.y),Size(0,-s0.y),red);
		wBuffer->drawLineD(Position(p1.x,p1.y),Size(-s0.x,0),red);
		wBuffer->drawLineD(Position(p2.x,p1.y),Size(0,-s0.y),red);
		wBuffer->drawLineD(Position(p2.x,p1.y),Size( s2.x,0),red);
		wBuffer->drawLineD(Position(p1.x,p2.y),Size(0, s2.y),red);
		wBuffer->drawLineD(Position(p1.x,p2.y),Size(-s0.x,0),red);
		wBuffer->drawLineD(Position(p2.x,p2.y),Size(0, s2.y),red);
		wBuffer->drawLineD(Position(p2.x,p2.y),Size( s2.x,0),red);
		switch(imgData->getType())
			{
		case Picture::Type_square:
			s0=imgData->getSquareHeadSize()*imgScale;
			s2=imgData->getSquareEndSize()*imgScale;
			p0=p1+s0;
			p3=p2-s2;
			s0-=Coord(2,2);
			s2-=Coord(2,2);
			wBuffer->drawLineD(Position(p0.x,p0.y),Size(0,-s0.y),orange);
			wBuffer->drawLineD(Position(p0.x,p0.y),Size(-s0.x,0),orange);
			wBuffer->drawLineD(Position(p3.x,p0.y),Size(0,-s0.y),orange);
			wBuffer->drawLineD(Position(p3.x,p0.y),Size( s2.x,0),orange);
			wBuffer->drawLineD(Position(p0.x,p3.y),Size(0, s2.y),orange);
			wBuffer->drawLineD(Position(p0.x,p3.y),Size(-s0.x,0),orange);
			wBuffer->drawLineD(Position(p3.x,p3.y),Size(0, s2.y),orange);
			wBuffer->drawLineD(Position(p3.x,p3.y),Size( s2.x,0),orange);
			break;
		case Picture::Type_fractile:
			s0=imgData->getFractileSize()*imgScale;
			p0=p0+s0;
			wBuffer->drawLineD(Position(p0.x,p0.y),Size(0,-s0.y),blue);
			wBuffer->drawLineD(Position(p0.x,p0.y),Size(-s0.x,0),blue);
			break;
			}
		}
	}

