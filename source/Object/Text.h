

class TextBar:public Object{
	 public:
	 TextBar(Position p,int w,char *s,int max,Font *f=::font);
	~TextBar();
	

	public:
virtual void	insert(char *ns);
void	input();
void	show();
	 protected:
class TextBarSensor:public PullSensor{
		 public:
		void	down(Coord cursor);
		SENSOR_OWNER(TextBar);
		};
char	*str;
bool	del;
int 	keyCursor,max,limit;
Displayer	*cursorDisplayer;
Font	*font;


	public:
TextBar*	setResponse(int enterRes,int tabRes=0);
	private:
int		enterResponseIndex,tabResponseIndex;
	};


class TextBarInt:public TextBar{
	 public:
	 TextBarInt(Position p,int w,int *d,int max,Font *f=::font);
	~TextBarInt();
void	insert(char *ns);
void	show();

	private:
int *data;
	};




class TextPopup:public Window{
	public:
	TextPopup(Object *ro,Position p,int w,int max,int r);
	~TextPopup();
void	act();
void	senseOut();
void	response(int ri);
char*	getString();
	private:
TextBar	*textBar;
char	*str;
Object	*responseObject;
int		responseIndex;
	};




