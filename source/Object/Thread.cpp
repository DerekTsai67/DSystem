


LinkedList<Thread> thread;



void addThread(VoidFunction f)
	{
	thread.addItem(new Thread(f),true);
	}

void addThread(Thread *t)
	{
	thread.addItem(t,true);
	}



void	voidProcess(void *f)
	{
	VoidFunction vf=*((VoidFunction*)f);
	vf();
	}


	Thread::Thread(VoidFunction f,bool prepare)
	{
	fpsCount=0;
	delay=10;
	voidFunc=f;
	func=voidProcess;
	data=&voidFunc;
	status=Terminated;
	if(!prepare)
		launch();
	}

	Thread::Thread(VoidPointerFunction f,void *d,bool prepare)
	{
	fpsCount=0;
	delay=10;
	func=f;
	data=d;
	status=Terminated;
	if(!prepare)
		launch();
	}

	Thread::~Thread()
	{
	terminate(true);
	}

void	Thread::launch(bool p)
	{
	if(status!=Terminated)
		error("Launch while not terminated.");
	if(p)
		status=Paused;
	else
		status=Process;
	launchOS(p);
	}

void	Thread::terminate(bool wait)
	{
	if(status==Process)
		status=Terminate;
	else if(status==Terminated)
		return;
	else if(status==ProcessOnce)
		waitOnce();
	else if(status==Pause)
		waitPause();
	else if(status==Paused)
		;
	else if(status==Terminate)
		;
	else
		error("Unknown status.");
		
	if(status==Paused)
		{
		status=Terminate;
		resumeOS();
		}
	if(wait)
		waitTerminated();
	}

void	Thread::waitTerminated()
	{
	if(status!=Terminate)
	if(status!=Terminated)
		error("waitTerminated while not Terminating.");
	while(status!=Terminated)
		sleep(1);
	}


void	Thread::once()
	{
	if(status==Terminated)
		return;
	if(status!=Paused)
		error("Once while not Paused.");
	status=ProcessOnce;
	resumeOS();
	}

void	Thread::pause()
	{
	if(status==Terminated)
		return;
	if(status!=Process)
		error("Pause while not Processing.");
	status=Pause;
	waitPause();
	}

void	Thread::resume()
	{
	if(status==Terminated)
		return;
	if(status!=Paused)
		error("Resume while not Paused.");
	status=Process;
	resumeOS();
	}

bool	Thread::isPaused()
	{
	return status==Paused;
	}

bool	Thread::isTerminated()
	{
	return status==Terminated;
	}

void	Thread::waitOnce()
	{
	if(status==Terminated)
		return;
	if(status!=ProcessOnce)
	if(status!=Pause)
	if(status!=Paused)
		error("WaitOnce while not in ProcessOnce. %d",status);
	while(status!=Paused)
		sleep(1);
	}

void	Thread::waitPause()
	{
	if(status!=Pause)
	if(status!=Paused)
			error("Wrong status.");
	while(status!=Paused)
		sleep(1);
	}

int 	Thread::getFps()
	{
	int ret=fpsCount;
	fpsCount-=ret;
	return ret;
	}

void	Thread::setDelay(int d)
	{
	delay=d;
	}



















