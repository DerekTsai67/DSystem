



	 TextBar::TextBar(Position p,int w,char *s,int m,Font *f):Object(p,
	 		DSData->getPic(BDF_TextBG)->makeSquare(Size(w,f->getSize().y+6)) )
	{
	if(m==0)
		m=100;
	if(s==NULL)
		{
		str=new char[m];
		str[0]=0;
		del=true;
		}
	else
		{
		str=s;
		//str[0]=0;
		del=false;
		}
	limit=m;
	SENSE->left=new TextBarSensor();
	font=f;
	enterResponseIndex=tabResponseIndex=0;
	max=0;
	while(str[max]!=0)max++;
	keyCursor=max;
	addDisplayer(cursorDisplayer=new Displayer(DSData->getPic(BDF_TextCursor)));
	}

	 TextBar::~TextBar()
	{
	if(del)
		delete str;
	}

TextBar*	TextBar::setResponse(int enterRes,int tabRes)
	{
	enterResponseIndex=enterRes;
	tabResponseIndex=tabRes;
	return this;
	}

void	TextBar::insert(char *ns)
	{
	max=0;
	while(str[max]!=0)max++;
	int m=0;
	while(ns[m]!=0)m++;
	if(m>limit-max)
		m=limit-max;
	if(m==0)
		return;
	
	for(int i=max;i>=keyCursor;i--)
		str[i+m]=str[i];
	for(int i=0;i<m;i++)
		str[keyCursor+i]=ns[i];
	keyCursor+=m;
	//max+=m;
	reShow();
	}

void	TextBar::input()
	{
	char ns[100];
	keySignal.getString(ns);
	insert(ns);


	if(keySignal.down(KEY_LEFT)){
		keyCursor-=keyCursor>0;
		reShow();}
	if(keySignal.down(KEY_RIGHT)){
		keyCursor+=keyCursor<max;
		reShow();}
	if(keySignal.down(KEY_HOME)){
		keyCursor=0;
		reShow();}
	if(keySignal.down(KEY_END)){
		keyCursor=max;
		reShow();}

	if(keySignal.down(KEY_DELETE))
		if(keyCursor<max)
		{
		for(int i=keyCursor;i<max;i++)
			str[i]=str[i+1];
		max--;
		str[max]=0;
		reShow();
		}
	if(keySignal.down(KEY_BACK))
		if(keyCursor>0)
		{
		keyCursor--;
		for(int i=keyCursor;i<max;i++)
			str[i]=str[i+1];
		max--;
		str[max]=0;
		reShow();
		}
	
	if(keySignal.down(KEY_ENTER))
		getOwner()->response(enterResponseIndex);
	if(keySignal.down(KEY_TAB))
		getOwner()->response(tabResponseIndex);
	
	//if(keySignal.down(KEY_SPACE))
	//	warning("up.");


	}


void	TextBar::TextBarSensor::down(Coord p)
	{
	TextBar *o=(TextBar*)owner;
	o->max=0;
	while(o->str[o->max]!=0)o->max++;
	Coord size=o->font->getSize();
	o->keyCursor=p.x/size.x;
	if(o->keyCursor>o->max)
		o->keyCursor=o->max;
	typeOwner()->reShow();
	}


void	TextBar::show()
	{
	Coord c;
	c=font->putString(wBuffer,str,Position(3,3));
	//wBuffer->draw(DSData->getPic(4),Coord(3+keyCursor*font->getSize().x,3));
	cursorDisplayer->setPos(Position(3+keyCursor*font->getSize().x,3));
	cursorDisplayer->setTime();
	}


//===========================================================================Int




	 TextBarInt::TextBarInt(Position p,int w,int *d,int m,Font *f):TextBar(p,w,NULL,m==0?m=10:m,f)
	{
	/*if(m==0)
		m=10;*/
	if(d==NULL)
		{
		data=new int;
		*data=0;
		del=true;
		}
	else
		{
		data=d;
		del=false;
		}
	sprintf(str,"%d",*data);
	while(str[max]!=0)max++;
	}
	 TextBarInt::~TextBarInt()
	{
	if(del)
		delete data;
	del=true;
	}

void	TextBarInt::insert(char *ns)
	{
	int m=0;
	//while(ns[m]!=0)m++;
	for(int i=0;ns[i]!=0;i++)
		{
		switch(ns[i])
			{
			default:
				if(ns[i]>='0'&&ns[i]<='9');
				else break;
			case '.':
			case '-':
			ns[m++]=ns[i];
			}
		}
	if(m>limit-max)m=limit-max;
	if(m==0)return;
	for(int i=max-1;i>=keyCursor;i--)
		str[i+m]=str[i];
	for(int i=0;i<m;i++)
		str[keyCursor+i]=ns[i];
	keyCursor+=m;
	max+=m;
	reShow();
	}



void	TextBarInt::show()
	{
	sscanf(str,"%d",data);
	TextBar::show();
	}





TextPopup	*textPopup;


	TextPopup::TextPopup(Object *ro,Position p,int w,int max,int r):Window(p,zeroCoord)
	{
	responseObject=ro;
	responseIndex=r;
	str=dalloc<char>(max+10);
	addEtcObject(textBar=(new TextBar(zeroEffectivePosition,w,str,max))->setResponse(5));
	addBehind(responseObject->getOwner());
	}
	
	TextPopup::~TextPopup()
	{
	dfree(str);
	}

void	TextPopup::act()
	{
	if(focusObject!=this)
		textBar->focus();
	}

void	TextPopup::senseOut()
	{
	response(5);
	}
	
void	TextPopup::response(int ri)
	{
	switch(ri)
		{
	case 5:
		textPopup=this;
		responseObject->response(responseIndex);
		kill();
		break;
		}
	}

char*	TextPopup::getString()
	{
	return str;
	}







