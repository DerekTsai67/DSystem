
enum SensorRet{
	SENSOR_hold 	=0x0001,
	SENSOR_push 	=0x0002,
	SENSOR_default	=0x0000
	};


class Sensor;

class Sense {
	 public:
	 Sense();
virtual	~Sense();
static int count;
virtual int 	process(Coord cursorPos,KeySignal keySignal);
virtual void	pointed(Coord p,bool on);
void	show();

	 //protected:

Object	*senseObject;
#define senseWindow (senseObject->getOwner())

#define SENSE_OWNER(TYPE) \
TYPE	*typeOwner(){return(TYPE*)senseObject;}
#define SENSOR_OWNER(TYPE) \
TYPE	*typeOwner(){return(TYPE*)owner;}

	};



class RegularSense:public Sense{
	 public:
	 RegularSense(Sensor *l=NULL,Sensor *r=NULL,Sensor *m=NULL);
	~RegularSense();
int 	process(Coord cursorPos,KeySignal keySignal);

Sensor *right,*middle,*left;
int 	boot[3];

	};



/*class MoveSense:public Sense{
	 public:
	 MoveSense();
	~MoveSense();

	};*/


class NonSense:public Sense{
	 public:
int 	process(Coord cursorPos,KeySignal keySignal);
void	pointed(Coord p,bool on);

	};

//==============================================================================

class Sensor{
	 public:
	Sensor();
virtual ~Sensor();
virtual int	process(Coord cursor,int b);
	 protected:
Object	*owner;
	};


class FreeSensor:public Sensor{
	 public:
	 FreeSensor(void(*click)());
	 FreeSensor(void(*click)(),void(*doubleClick)(),void(*down)(),void(*press)(),void(*up)());

int 	process(Coord cursor,int b);

	 protected:
void	(*click)();
void	(*dclick)();
void	(*down)();
void	(*press)();
void	(*up)();
	};

class ResponseSensor:public Sensor{
	 public:
	 ResponseSensor(int c);
	 ResponseSensor(int c,int dc,int d,int p,int u);
int 	process(Coord cursor,int b);
	 protected:
int 	click,dclick,down,press,up;
	};

class PullSensor:public Sensor{
	 public:
	PullSensor();
int 	process(Coord sensePos,int b);
virtual void down(Coord sensePos);
virtual void press(Coord sensePos);
virtual void up(Coord sensePos);
bool	isPulling();
	 protected:
bool	pulling;
Coord	startPos;
	};

class MoveSensor:public PullSensor{
	 public:
void	press(Coord cursor);
	};

class SuicideSensor:public PullSensor{
	public:
void	down(Coord c);
	};

//============================================================================


class React{
	public:
	React();
virtual ~React();
void	setSense(Sense *s);
virtual bool	check(Coord p,Sense *s);
virtual void	pointed(Coord p,bool on);
virtual void	press(Coord p);
virtual int 	process(Coord p);
	protected:
Object	*reactObject;
Sense	*sense;
#define REACT_OWNER(TYPE) \
virtual TYPE	*typeOwner(){return(TYPE*)reactObject;}
	};

#define IF_SENSOR_CAST(s,n,Type) Type *n=sensorCast<Type>(dynamic_cast<RegularSense*>(s));if(n!=NULL)

template<class Type>
Type*	sensorCast(RegularSense *s)
	{
	Type	*ret;
	if(s==NULL)
		return NULL;
	if(s->boot[MOUSE_LEFT])
		if((ret=dynamic_cast<Type*>(s->left))!=NULL)
			return ret;
	if(s->boot[MOUSE_RIGHT])
		if((ret=dynamic_cast<Type*>(s->right))!=NULL)
			return ret;
	if(s->boot[MOUSE_MIDDLE])
		if((ret=dynamic_cast<Type*>(s->middle))!=NULL)
			return ret;
	return NULL;
	}



