



template<class Type>
class Page:public LinkedList<Type>{
	 public:
	Page(int m,bool b=false);
	~Page();
LinkedList<Type>&	operator[](int i);
List<Type>*	getHead(bool back=false)const;
void	act();
void	setPage(int i);
	 private:
int 	index,max;
LinkedList<Type>	*arr;
	};

template<class Type>
 	Page<Type>::Page(int m,bool b):LinkedList<Type>(b)
	{
	max=m;
	index=0;
	arr=new LinkedList<Type>[max];
	}

template<class Type>
 	Page<Type>::~Page()
	{
	for(int i=0;i<max;i++)
		{
		arr[i].clear();
		}
	delete arr;
	}


template<class Type>
LinkedList<Type>&	Page<Type>::operator[](int i)
	{
	if(i>=max||i<0)
		error("Page overflow.");
	return arr[i];
	}


template<class Type>
List<Type>*	Page<Type>::getHead(bool back)const
	{
	return arr[index].getHead(back);
	}


template<class Type>
void	Page<Type>::act()
	{/*
	if(index>=max)
		error("Page overflow.");
	this->head=arr[index];*/
	}

template<class Type>
void	Page<Type>::setPage(int i)
	{
	if(i>=max||i<0)
		error("Page overflow.");
	if(i==index)
		return;

	FOR_LL(now,*this,Type)
		if(dynamic_cast<Object*>(now)==focusObject)
			{focusObject=focusObject->getObjectOwner();break;}
	END_FOR_LL
	index=i;
	}













