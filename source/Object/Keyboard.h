enum Key{
KEY_A=1,
KEY_B,
KEY_C,
KEY_D,
KEY_E,
KEY_F,
KEY_G,
KEY_H,
KEY_I,
KEY_J,
KEY_K,
KEY_L,
KEY_M,
KEY_N,
KEY_O,
KEY_P,
KEY_Q,
KEY_R,
KEY_S,
KEY_T,
KEY_U,
KEY_V,
KEY_W,
KEY_X,
KEY_Y,
KEY_Z,
KEY_U0,
KEY_U1,
KEY_U2,
KEY_U3,
KEY_U4,
KEY_U5,
KEY_U6,
KEY_U7,
KEY_U8,
KEY_U9,
KEY__,
KEY_COMMA,
KEY_DOT,
KEY_SLASH,
KEY_COLON,
KEY_QUOTE,
KEY_LBRACKET,
KEY_RBRACKET,
KEY_TILDE,
KEY_EQUAL,
KEY_BACKSLASH,
KEY_UP,
KEY_DOWN,
KEY_RIGHT,
KEY_LEFT,
KEY_BACK,
KEY_ENTER,
KEY_SPACE,
KEY_ESC,
KEY_TAB,
KEY_CAPS,
KEY_F1,
KEY_F2,
KEY_F3,
KEY_F4,
KEY_F5,
KEY_F6,
KEY_F7,
KEY_F8,
KEY_F9,
KEY_F10,
KEY_F11,
KEY_F12,
KEY_DELETE,
KEY_INSERT,
KEY_HOME,
KEY_END,
KEY_PUP,
KEY_PDOWN,
KEY_N0,
KEY_N1,
KEY_N2,
KEY_N3,
KEY_N4,
KEY_N5,
KEY_N6,
KEY_N7,
KEY_N8,
KEY_N9,
KEY_PLUS,
KEY_MINUS,
KEY_TIMES,
KEY_DEVIDE,
KEY_NDOT,
KEY_NUMLOCK,
KEY_PAUSE,
KEY_SCROLL,
KEY_MAX,
KEY_CTRL	=0x0100,
KEY_ALT		=0x0200,
KEY_SHIFT	=0x0400,
KEY_MAX2	=3,
	};

enum Mouse{
MOUSE_LEFT=0,
MOUSE_RIGHT,
MOUSE_MIDDLE,
MOUSE_MAX
	};

class KeySignal{
	 public:
	KeySignal();
bool	press	(int x);
bool	down	(int x);
bool	up		(int x);
bool	mousePress(int x);
bool	mouseDown(int x);
bool	mouseUp(int x);
void	renew(int *mouseState,int *keyState,int (*keyTrans)(int i),int (*keyTrans2)(int i));
int 	getMouseState(int x);
void	getString(char *ret);
void	reduceFocus();
	 private:
int 	keys[KEY_MAX],keys2[KEY_MAX2],mouse[MOUSE_MAX];
	};



















