



	Displayer::Displayer()
	{
	init();
	setPos(zeroEffectivePosition);
	}

	Displayer::Displayer(Picture *a,Position b,int t)
	{
	init();
	setPic(a);
	setPos(b);
	setTime(t);
	}

void	Displayer::init()
	{
	pic=NULL;
	pos=zeroEffectivePosition;
	startTime=0;
	nowAct=0;
	branchStepIndex=-1;
	branchFrameIndex=branchNextIndex=0;
	lastBmp=NULL;
	lastPos=lastEnd=zeroCoord;
	}

	Displayer::~Displayer()
	{
	}

void	Displayer::setTime(int t)
	{
	if(t==-1)
		{
		if(pic==NULL)
			t=0;
		else
			t=pic->getTime();
		}
	startTime=t;
	}

void	Displayer::setPic(Picture *p,bool forceCut)
	{
	if(pic!=NULL&&!forceCut)
		{
		if(pic==p||pic->getNextPic()==p)
			return;
		for(int fi=frameIndex;fi<pic->getFrameNumber();fi++)
			{
			Frame *frame=pic->getFrame(fi);
			for(int ni=0;ni<frame->getNextFrameNumber();ni++)
				if(frame->getNextPic(ni)==p||frame->getNextPic(ni)->getNextPic()==p)
					{
					if(pic->getTime()==-1)
						{
						pic=frame->getNextPic(ni);
						frameIndex=frame->getNextIndex(ni);
						if(pic!=NULL)
							startTime=pic->getTime();
						return;
						}
					else
						{
						branchStepIndex=0;
						branchFrameIndex=fi;
						branchNextIndex=ni;
						return;
						}
					}
			}
		}
	pic=p;
	frameIndex=0;
	if(pic!=NULL)
		startTime=pic->getTime();
	}

Picture*	Displayer::getPic()const
	{
	return pic;
	}
int		Displayer::getFrameIndex()const
	{
	return frameIndex;
	}

void	Displayer::setPos(Position p)
	{
	pos=p;
	}
	
Position	Displayer::getPos()const
	{
	return pos;
	}
	
bool	Displayer::showCheck()
	{
	while(true)
		{
		if(pic==NULL)
			return true;
		int time=pic->getTime()-startTime;
		if(time<pic->getFrame(frameIndex)->getDelay())
			return false;
		startTime+=pic->getFrame(frameIndex)->getDelay();
		if(branchStepIndex!=-1)
		if(frameIndex==branchFrameIndex)
			{
			pic=pic->getFrame(frameIndex)->getNextPic(branchNextIndex);
			frameIndex=pic->getFrame(frameIndex)->getNextIndex(branchNextIndex);
			branchStepIndex=-1;
			continue;
			}
		pic->goNextFrame(&pic,&frameIndex);
		}
	}










