

extern LinkedList<Thread> thread;

void addThread(VoidFunction f);
void addThread(Thread *t);





class Thread{
	public:
	Thread(VoidFunction f,bool prepare=false);
	Thread(VoidPointerFunction f,void *data,bool prepare=false);
	~Thread();
int 	getFps();
void	setDelay(int d);

	public:
void	launch(bool pause=false);
void	launchOS(bool pause=false);
void	terminate(bool wait=false);
void	waitTerminated();
void*	OSHandle;

	public:
void	once();
void	pause();
void	resume();
void	resumeOS();

bool	isPaused();
bool	isTerminated();
void	waitOnce();
void	waitPause();

int 	fpsCount;
int		delay;
VoidFunction voidFunc;
VoidPointerFunction func;
void	*data;
enum ThreadStatus{
	Process,
	ProcessOnce,
	Pause,
	Paused,
	Terminate,
	Terminated
	} status;
	};







class	ThreadLock{
	public:
	ThreadLock(ConstString name);
	ThreadLock(int i);
	~ThreadLock();
int		index;
	};








