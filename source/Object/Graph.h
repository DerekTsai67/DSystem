
class Scaler;


class GraphLine{
	public:
	GraphLine();
virtual	~GraphLine();
virtual void	show(Picture *wBuffer)=0;
Scaler	*scaler[3],*&xScaler,*&yScaler,*&zScaler;
DS_RGB	color;
	};

class SimpleMeasureLine:public GraphLine{
	public:
	SimpleMeasureLine(Measure *m,DS_RGB c);
void	show(Picture *wBuffer);
Measure	*measure;
	};

class SampleMeasureLine:public GraphLine{
	public:
	SampleMeasureLine(SampleMeasure &m,int xi,int yi,DS_RGB c);
	SampleMeasureLine(SampleMeasure &m,int xi,int yi,int zi);
void	show(Picture *wBuffer);
SampleMeasure	measure;
int		xIndex,yIndex,zIndex;
	};
	
class StrightLine:public GraphLine{
	public:
	StrightLine(double data,int axis,DS_RGB c);
	~StrightLine();
void	show(Picture *wBuffer);
double	data;
int		axis;
	};


class Graph:public Object{
	public:
	Graph(Position p,Size s);
	~Graph();
void	addXLine(double x,DS_RGB color,int yScalerIndex=-1,int xScalerIndex=-1);
void	addYLine(double y,DS_RGB color,int yScalerIndex=-1,int xScalerIndex=-1);
void	addMeasure(Measure *m,DS_RGB color,int xScalerIndex=-1,int yScalerIndex=-1);
void	addSampleMeasure(SampleMeasure sm,int xi,int yi,DS_RGB color,int xScalerIndex=-1,int yScalerIndex=-1);
void	addSampleMeasure2D(SampleMeasure sm,int xi,int yi,int zi,int xScalerIndex=-1,int yScalerIndex=-1,int zScalerIndex=-1);
void	addLine(GraphLine *gl,int xScalerIndex=-1,int yScalerIndex=-1,int zScalerIndex=-3);

void	addScaler(Scaler *s);
Scaler*	getScaler(int axis,int index=0);

void	clearLine();
	//scalerIndex: -1=Auto search  -2=Create new
void	show();
	private:
Array<Scaler*>		scaler[3],&xScaler,&yScaler,&zScaler;
Array<GraphLine*>	line;
	};







class Scaler{
	public:
	Scaler(int a,double min,double max);
	Scaler(int axis);
void	setMin(double min);
void	setMax(double max);
//void	addFitMeasure(SampleMeasure *m,int si,int dep=0);
void	fitSample(double s);
int		convert(double s,int l);
double	getDistance();
int		convertDiff(double s,int l);
double	convertBack(int p,int l);
	//private:
int		axis;
double	min,max;
bool	fixed;


Scaler*	setLogarithm();
void	refreshLogarithm();
double	lmin,lmax;
bool	logarithm,logarithmReady;
	};























