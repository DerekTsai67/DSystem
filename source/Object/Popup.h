

class Popup;

class PopupButton:public MenuObject{
	public:
	PopupButton(ConstString title,int res,bool available);
virtual void	lclick(Coord c);
void	show();
	protected:
String	title;
int		res;
bool	available;
	};

class PopupFolderButton:public PopupButton{
	public:
	PopupFolderButton(ConstString title,bool available,Object *target);
	~PopupFolderButton();
void	lclick(Coord c);
Popup*	getFolder();
	private:
Popup	*folderWindow;
	};


class Popup:public Window{
	public:
	Popup(Position pos,Object *t,Popup *origin=NULL);
void	addContext(ConstString title,int res,bool available=true);
void	addFolder(ConstString title,bool available=true);
bool	endFolder();
void	refresh();
void	senseOut();
void	response(int r);
	private:
LinkedList<PopupButton>	*button;
LinkedList<Popup>	folder;
ObjectLinker<Popup,Object>	resTarget;
Popup	*origin;
Popup	*actingFolder;
Menu	*menu;
	};












