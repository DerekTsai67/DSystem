


	Button::Button(Position p,Datafile *df,Size s,Picture *ic,int r0,int r1,int r2):Object(p,df->getPic(0)->makeSquare(s))
	{
	argCheck(df==NULL);
	datafile=df;
	setIcon(ic);
	
	buttonType=leftAvalible;
	responseIndex[0]=r0;
	responseIndex[1]=r1;
	responseIndex[2]=r2;
	if(r1!=0)
		addButtonType(rightAvalible);
	if(r2!=0)
		addButtonType(middleAvalible);
	bg=NULL;
	looks=0;
	}

	Button::Button(Position p,int r0,int r1,int r2):Object(p,Size(20,20))
	{
	buttonType=leftAvalible;
	responseIndex[0]=r0;
	responseIndex[1]=r1;
	responseIndex[2]=r2;
	if(r1!=0)
		addButtonType(rightAvalible);
	if(r2!=0)
		addButtonType(middleAvalible);
	bg=NULL;
	looks=0;
	datafile=NULL;
	icon=NULL;
	}

	Button::Button(Position p,const Picture *pic,int r0,int r1,int r2):Object(p,pic->getSize())
	{
	buttonType=leftAvalible;
	responseIndex[0]=r0;
	responseIndex[1]=r1;
	responseIndex[2]=r2;
	if(r1!=0)
		addButtonType(rightAvalible);
	if(r2!=0)
		addButtonType(middleAvalible);
	bg=pic;
	looks=0;
	datafile=NULL;
	icon=NULL;
	}

	Button::~Button()
	{
	if(datafile!=NULL)
		for(int i=0;i<3;i++)
			;//delete buffer[i];
	}

void	Button::addButtonType(int t)
	{
	buttonType|=t;
	}
void	Button::deleteButtonType(int t)
	{
	buttonType&=~t;
	}
	
void	Button::setIcon(Picture *ic)
	{
	icon=ic;
	reShow();
	}

void	Button::lclick(Coord c,bool b)
	{
	if(buttonType&leftAvalible)
		click(0,b);
	}
void	Button::rclick(Coord c,bool b)
	{
	if(buttonType&rightAvalible)
		click(1,b);
	}
void	Button::mclick(Coord c,bool b)
	{
	if(buttonType&middleAvalible)
		click(2,b);
	}
	
void	Button::setDatafile(Datafile *df)
	{
	datafile=df;
	setLooks(looks);
	}

void	Button::setLooks(int i)
	{
	looks=i;
	if(datafile!=NULL)
		setSquareBg(datafile->getPic(looks));
	}

void	Button::click(int i,bool b)
	{
	if(b)
		{
		setLooks(2);
		reShow();
		}
	else
		{
		this->button(i);
		setLooks(1);
		reShow();
		}
	}
void	Button::button(int i)
	{
	owner->response(responseIndex[i]);
	}
void	Button::pointed(Coord c,bool on)
	{
	if(on)
		{
		setLooks(1);
		reShow();
		}
	else
		{
		setLooks(0);
		reShow();
		}
	}
void	Button::show()
	{
	if(datafile!=NULL)
		{
		wDisplayer->setPic(icon);
		wDisplayer->setPos(zeroEffectivePosition+(looks==2?monoCoord:zeroCoord));
		}
	else if(bg==NULL)
		wBuffer->clear(DS_RGB(94,104,216));
	else
		wBuffer->drawFractile(bg,zeroEffectivePosition,Coord(looks,0));
	}
















