


class Displayer{
	public:
	Displayer();
	Displayer(Picture *a,Position b=zeroEffectivePosition,int t=-1);
	~Displayer();
void	init();
void	setPic(Picture *p,bool forceCut=false);
Picture*	getPic()const;
int			getFrameIndex()const;
void		setPos(Position p);
Position	getPos()const;
void	setTime(int t=-1);
bool	showCheck();
void	show(Coord p);
	private:
Picture		*pic;
int			frameIndex;
Position	pos;
int 		startTime,nowAct;
int			branchStepIndex,branchFrameIndex,branchNextIndex;

const DSBitmap	*lastBmp;
Coord	lastPos,lastEnd;
	};








