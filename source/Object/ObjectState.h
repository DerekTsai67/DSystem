



class ObjectState{
	public:
	ObjectState(int time);
virtual	~ObjectState();

	public:
int		getRemainTime()const;
int		getElapsedTime()const;
int		getDuration()const;
	protected:
int		startTime;
int		duration;

	public:
void	setOwner(Object *o);
Object*	getOwner()const;
	protected:
Object	*owner;

	public:
virtual Coord	getPositionDeviation()const;
virtual Coord	getSize(Coord originSize)const;
virtual bool	getReShow()const;
	};





class PositionDeviation:public ObjectState{
	public:
	PositionDeviation(Coord c,Mapping *m,int t);
	~PositionDeviation();
Coord	getPositionDeviation()const;
	private:
Coord	deviation;
Mapping	*mapping;
	};


class ShakeState:public ObjectState{
	public:
	ShakeState(int amplitude,Mapping *timeMapping,int t,Mapping *ampMapping=NULL);
	~ShakeState();
Coord	getPositionDeviation()const;
	private:
int		amplitude;
Mapping	*timeMapping,*ampMapping;
	};



class SizeDeviation:public ObjectState{
	public:
	SizeDeviation(Coord c,Mapping *m,int t);
	~SizeDeviation();
Coord	getSize(Coord originSize)const;
bool	getReShow()const;
	private:
Coord	deviation;
Mapping	*mapping;
	};
	
	
