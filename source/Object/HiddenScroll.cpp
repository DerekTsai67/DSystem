


void	verticalScrollConverter(Coord c,int xm,int ym,int zm,int *x,int *y,int *z)
	{
	argCheck(x==NULL);
	if(c.y<=0)
		*x=0;
	else if(c.y>=xm)
		*x=xm;
	else
		*x=c.y;
	}


	ScrollSensor::ScrollSensor(ScrollConverter cv,Coord oc,bool rl,int xm_,int ym_,int zm_,int xi,int yi,int zi,int dr,int pr,int ur)
	{
	converter=cv;
	originCoord=oc;
	fixCoord=zeroCoord;
	relative=rl;
	xm=xm_;
	ym=ym_;
	zm=zm_;
	x=xi;
	y=yi;
	z=zi;
	downResponse=dr;
	pressResponse=pr;
	upResponse=ur;
	}

void	ScrollSensor::down(Coord c)
	{
	owner->response(downResponse);
	}

void	ScrollSensor::press(Coord c)
	{
	converter(c-originCoord+fixCoord,xm,ym,zm,&x,&y,&z);
	owner->response(pressResponse);
	}

void	ScrollSensor::up(Coord c)
	{
	owner->response(upResponse);
	}

void	ScrollSensor::setFixCoord(Coord f)
	{
	fixCoord=f;
	}

int		ScrollSensor::getX()const
	{return x;}
int		ScrollSensor::getY()const
	{return y;}
int		ScrollSensor::getZ()const
	{return z;}

void	ScrollSensor::setX(int a)
	{x=a;}
void	ScrollSensor::setY(int a)
	{y=a;}
void	ScrollSensor::setZ(int a)
	{z=a;}





	HiddenScroll::HiddenScroll(Position pos,int m0,int m1,int init,int r):Object(pos,Coord(50,20))
	{
	SENSE->left=scrollSensor=new ScrollSensor(verticalScrollConverter,Coord(0,-200),true,401,0,0,50,0,0,1,2,3);
	min=m0;
	max=m1;
	originValue=value=init;
	responseIndex=r;
	defaultReShow=true;
	}

	HiddenScroll::~HiddenScroll()
	{

	}

void	HiddenScroll::response(int r)
	{
	switch(r)
		{
	case 1:
		scrollSensor->setFixCoord(zeroCoord-(cursorPos-getPosition()));
		originValue=value;
		break;
	case 2:
		value=originValue+scrollSensor->getX()*61/401-30;
		if(value>=max)
			value=max;
		if(value<min)
			value=min;
		getOwner()->response(responseIndex);
		break;
		}
	}

void	HiddenScroll::setValue(int v)
	{
	value=v;
	}

int		HiddenScroll::getValue()const
	{
	return value;
	}

void	HiddenScroll::show()
	{
	wBuffer->clear(green);
	char str[50];
	sprintf(str,"%3d",value);
	font->putString(wBuffer,str,zeroEffectivePosition);
	}



