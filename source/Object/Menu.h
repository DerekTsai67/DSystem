
class Menu;

class MenuObject:public Object{
	public:
	MenuObject(Picture *sbg,Size s);
	MenuObject(Picture *pic);
	MenuObject(Coord size);
void	init();
virtual	~MenuObject();
Menu*	getMenuOwner()const;
virtual bool	isData(void *data)const;
	//private:
int		refreshStatus;

	public:
void	setMenuSelected(bool b);
bool	isMenuSelected()const;
virtual void	menuSelected();
	private:
bool	menuSelectedB;

	public:
void	setIndent(Coord c);
Coord	getIndent()const;
	private:
Coord	menuIndent;
	};

class Menu:public Object{
	public:
	Menu(Position p,Size s,LinkedList<MenuObject> *LL);
	~Menu();
friend class MenuSensor;
void	setSR(Sensor *s,React *r);
void	refresh();
void	fitSize(const Picture *square=NULL);
Sense*	sense(Coord p);
void	addItem(MenuObject *o);
void	focused();
void	focusOut();
List<MenuObject>	*focusList;
int 	focusY;
void	show();
//void	showOut(Coord p);

	protected:
LinkedList<MenuObject> *data;
Picture	*line;
RegularSense	*menuSense;

	public:
void	setDirection(int t);
enum{
	vertical	=0x0001,
	horizontal	=0x0002
	};
	private:
int		direction;

	public:
void	clearSelect();
bool	isMultiSelected()const;
void	senseOut();

	public:
void		refreshStart();
MenuObject*	refreshNext(void *data);
void		refreshAdd(MenuObject *obj);
void		refreshEnd();
	private:
bool		skipEntryDeform;
Position	refreshAddPosition;
	};


class MenuSensor:public PullSensor{
	public:
	MenuSensor();
SENSOR_OWNER(Menu);
//void	press(Coord p);
virtual void	up(Coord p);
virtual MenuObject*	getData();
List<MenuObject>	*selected;
Menu	*lastObject;
	};

class MenuReact:public React{
	public:
REACT_OWNER(Menu);
virtual bool	check(Coord p,Sense *s);
void	pointed(Coord p,bool on);
void	press(Coord p);
int 	process(Coord p);
	};
