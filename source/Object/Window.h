







enum ObjectType{
	 OBJECT_none			=0x0000
	,OBJECT_centered		=0x0001
	,OBJECT_rotatable		=0x0002
	,OBJECT_line			=0x0004
	,OBJECT_movable			=0x0008
	,OBJECT_Xreshapable		=0x0010
	,OBJECT_Yreshapable		=0x0020
	,OBJECT_reshapable		=0x0030
	,OBJECT_overReshapable	=0x0040
	,OBJECT_visible			=0x0080
	,OBJECT_alwaysDown		=0x0100
	//,OBJECT_alwaysUp		=0x0200
	,OBJECT_sensable		=0x0400
	,OBJECT_actable			=0x0800

	,OBJECT_default		=OBJECT_visible
						|OBJECT_sensable
						|OBJECT_movable
						|OBJECT_overReshapable
						|OBJECT_actable
	};


class Object{
	public:
	 Object(Position p,Picture *square,Size size);
	 Object(Position p,Picture *pic);
	 Object(Position p,Coord s);
void	init();
virtual	~Object();
friend void mainProcess();

	public:
void		setPosition(Position p);
Position	getPosition();
Position	getLocalPosition();
Position	getDeformedLocalPosition();
Position	getOriginPosition()const;
void	reSize(Size s);
void	reSize(Size s,int t,Mapping *m=NULL);
void	reSize(Picture *pic,bool changeSize=true);
//void	reSize(Coord s);
Size	getSize()const;
Size	getDeformedSize()const;
Size	getOriginSize()const;
void		setSquareBg(Picture *sbg);
Picture*	getSquareBg()const;
	private:
Coord	pos;
Coord	size;
Picture	*squareBg;

	public:
int 	getID()const;
static int 	count;
	private:
int 	objectID;

	public:
bool	isWindow()const;
void	setOwner(Object *o);
Window*	getOwner()const;
Object*	getObjectOwner()const;
void	focus();
	protected:
Object	*owner;

	public:
void	addObject_(void *obj);
void	addEtcObject(Object *obj);
	protected:
LinkedList<LinkedList<Object> >	object;
LinkedList<Object>	etcObject;
#define addObject(x) addObject_((LinkedList<Object>*)&(x))
//#define addEtcObject(x) etcObject.addItem((Object*)(x))

	public:
void	act_();
virtual void	act();

	public:
void	check(Coord cursor);

	public:
		Sense* 	sense_(Coord cursor);
virtual Sense* 	sense(Coord cursor);
virtual void	senseOut();
		React* 	react_(Coord cursor,Sense *s);
virtual React* 	react(Coord cursor,Sense *s);
void			addReact(React *a);
virtual void	lclick(Coord c);
virtual void	lclick(Coord c,bool d);
virtual void	rclick(Coord c);
virtual void	rclick(Coord c,bool d);
virtual void	mclick(Coord c);
virtual void	mclick(Coord c,bool d);
virtual void	ldclick(Coord c);
virtual void	rdclick(Coord c);
virtual void	mdclick(Coord c);
virtual void	pointed(Coord c,bool on);
virtual void	input();
	protected:
Sense	*defaultSense;
LinkedList<React>	reactLL;
#define SENSE ((RegularSense*)defaultSense)

	public:
virtual void	response(int index);
virtual bool	threwOut(Window*);

	public:
void	setType(int t);
void	addType(int t);
void	deleteType(int t);
bool	haveType(int t);
	private:
int 	type;

	public:
void	addState(ObjectState *s);
	private:
LinkedList<ObjectState>	objectState;

	public:
void	move(Coord v,Mapping *m=NULL,int time=0);
//void	reshape(Coord p,Sense s);
//virtual void	pull(Coord p);
	protected:
Coord	reshapeMin,reshapeMax,movePos;
int 	moveStartTime,moveTime;

	public:
void	kill();
bool	isDead()const;
enum {
	killAlive,
	killing,
	killed
	};
	private:
int		killState;

	public:
LinkedList<EtcLinker>	etcLinker;

	public:
void	addDisplayer(Displayer *d);
	protected:
LinkedList<Displayer>	displayer;
Displayer	*bgDisplayer,*wDisplayer;

	public:
void			reShow();
void			show_(Coord p=zeroCoord);
virtual void	show();
void			showOut_();
virtual void	showOut();
	protected:
Picture	*wBuffer;
bool	defaultReShow;
	private:
Picture	*bgBuffer;
bool	reShowB,reShowBg;
/*	All the Object draw images on wBuffer
	for purpose to optimize it

	showOut : redraw wBuffer every frame
	show : redraw all picture including all the buffer and wBuffer , when needed
	reShow : make Object call upper again

	*/
	};




class Window:public Object{
	public:
	 Window(Position p,Picture *square,Size size);
	 Window(Position p,Picture *bg);
	 Window(Position p,Coord s);
void	init();
virtual	~Window();
void	recDel();
friend bool addWindow(Window *n);
friend void deleteWindow(Window *n);

	public:
Window*	getNext();
	protected:
Window	*next;

	public:
void	addBehind(Window *b);
void	push_to_top(Window *top=NULL,Window *source=NULL);
	private:
static const int	frontMax=10;
static const int	behindMax=10;
ObjectLinker<Window,Window>	front [frontMax];
ObjectLinker<Window,Window>	behind[behindMax];
int		frontIndex,behindIndex;

	public:
virtual React* 	react(Coord cursor,Sense *s);

	public:
friend	void	refreshWindow(Window*);
friend	void	refreshWindow();
	protected:
virtual void	refresh();


	public:
void	showRec(bool focus);
void	showOutRec();
	};


class Base:public Window{
	public:
	Base();
	};





