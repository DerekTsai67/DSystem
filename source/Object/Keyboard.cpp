


	KeySignal::KeySignal()
	{
	for(int i=0;i<KEY_MAX;i++)
		keys[i]=0;
	for(int i=0;i<KEY_MAX2;i++)
		keys2[i]=0;
	for(int i=0;i<MOUSE_MAX;i++)
		mouse[i]=0;
	}

bool	KeySignal::press(int x)
	{
	int low=x&0xFF;
	
	for(int i=0;i<KEY_MAX2;i++)
		if(x&(0x100<<i))
			{
			if(keys2[i]<=1)
				return false;
			}
	if(low!=0)
		if(keys[low]<=1)
			return false;
	
	return true;
	}

bool	KeySignal::down(int x)
	{
	int low=x&0xFF;
	
	if(low==0)
		{
		bool ret=false;
		for(int i=0;i<KEY_MAX2;i++)
			if(x&(0x100<<i))
				{
				if(keys2[i]<=1)
					return false;
				if(keys2[i]>=3)
					{
					keys2[i]=3;
					ret=true;
					}
				}
		return ret;
		}
	
	for(int i=0;i<KEY_MAX2;i++)
		if(x&(0x100<<i))
			if(keys2[i]<=1)
				return false;
	if(keys[low]<=2)
		return false;
	keys[low]=3;
	return true;
	}

bool	KeySignal::up	(int x)
	{
	int low=x&0xFF;
	bool ret=false;
	
	if(low!=0)
		{
		if(keys[low]>=2)
			return false;
		if(keys[low]==0)
			ret=true;
		}
	for(int i=0;i<KEY_MAX2;i++)
		if(x&(0x100<<i))
			{
			if(keys2[i]>=2)
				return false;
			if(keys2[i]==0)
				ret=true;
			}
	
	return ret;
	}

bool	KeySignal::mousePress(int x)
	{
	return mouse[x]>=2;
	}
bool	KeySignal::mouseDown(int x)
	{
	return mouse[x]>=3;
	}
bool	KeySignal::mouseUp(int x)
	{
	return mouse[x]==0;
	}

void	KeySignal::renew(int *mouseState,int *keyState,int (*keyTrans)(int i),int (*keyTrans2)(int i))
	{
	keys[0]=4;
	for(int i=1;i<KEY_MAX;i++)
		if(keyState[keyTrans(i)]==1)
			if(keys[i]<=1)
				keys[i]=4;
			else
				keys[i]=2;
		else
			if(keys[i]>=2)
				keys[i]=0;
			else
				keys[i]=1;
	for(int i=0;i<KEY_MAX2;i++)
		if(keyState[keyTrans2(i)]==1)
			if(keys2[i]<=1)
				keys2[i]=4;
			else
				keys2[i]=2;
		else
			if(keys2[i]>=2)
				keys2[i]=0;
			else
				keys2[i]=1;
	for(int i=0;i<MOUSE_MAX;i++)
		if(mouseState[i]==1)
			if(mouse[i]<=1)
				mouse[i]=4;
			else
				mouse[i]=2;
		else
			if(mouse[i]>=2)
				mouse[i]=0;
			else
				mouse[i]=1;
	}

void	KeySignal::reduceFocus()
	{
	for(int i=1;i<KEY_MAX;i++)
		if(keys[i]==3)
			keys[i]=2;
	for(int i=0;i<KEY_MAX2;i++)
		if(keys2[i]==3)
			keys2[i]=2;
	for(int i=0;i<MOUSE_MAX;i++)
		if(mouse[i]==3)
			mouse[i]=2;
	}



int 	KeySignal::getMouseState(int x)
	{
	return mouse[x];
	}



void	KeySignal::getString(char *ret)
	{
	int max=0;
	for(int i=KEY_N0;i<=KEY_N9;i++)
		{
		if(down(i))
			ret[max++]='0'+i-KEY_N0;
		}
	if(down(KEY_PLUS))		ret[max++]='+';
	if(down(KEY_MINUS))		ret[max++]='-';
	if(down(KEY_TIMES))		ret[max++]='*';
	if(down(KEY_DEVIDE))	ret[max++]='/';
	if(down(KEY_NDOT))		ret[max++]='.';

	if(!press(KEY_SHIFT))
		{
		for(int i=KEY_U0;i<=KEY_U9;i++)
			{
			if(down(i))
				ret[max++]='0'+i-KEY_U0;
			}
		for(int i=KEY_A;i<=KEY_Z;i++)
			{
			if(down(i))
				ret[max++]='a'+i-KEY_A;
			}
		if(down(KEY__))			ret[max++]='-';
		if(down(KEY_COMMA))		ret[max++]=',';
		if(down(KEY_DOT))		ret[max++]='.';
		if(down(KEY_SLASH))		ret[max++]='/';
		if(down(KEY_COLON))		ret[max++]=';';
		if(down(KEY_QUOTE))		ret[max++]='\'';
		if(down(KEY_LBRACKET))	ret[max++]='[';
		if(down(KEY_RBRACKET))	ret[max++]=']';
		if(down(KEY_TILDE))		ret[max++]='`';
		if(down(KEY_EQUAL))		ret[max++]='=';
		if(down(KEY_BACKSLASH))	ret[max++]='\\';
		}
	else
		{
		if(down(KEY_U1))	ret[max++]='!';
		if(down(KEY_U2))	ret[max++]='@';
		if(down(KEY_U3))	ret[max++]='#';
		if(down(KEY_U4))	ret[max++]='$';
		if(down(KEY_U5))	ret[max++]='%';
		//if(down(KEY_U6))	ret[max++]='^';
		if(down(KEY_U7))	ret[max++]='&';
		if(down(KEY_U8))	ret[max++]='*';
		if(down(KEY_U9))	ret[max++]='(';
		if(down(KEY_U0))	ret[max++]=')';
		for(int i=KEY_A;i<=KEY_Z;i++)
			{
			if(down(i))
				ret[max++]='A'+i-KEY_A;
			}
		if(down(KEY__))			ret[max++]='_';
		if(down(KEY_COMMA))		ret[max++]='<';
		if(down(KEY_DOT))		ret[max++]='>';
		if(down(KEY_SLASH))		ret[max++]='?';
		if(down(KEY_COLON))		ret[max++]=':';
		if(down(KEY_QUOTE))		ret[max++]='\"';
		if(down(KEY_LBRACKET))	ret[max++]='{';
		if(down(KEY_RBRACKET))	ret[max++]='}';
		if(down(KEY_TILDE))		ret[max++]='~';
		if(down(KEY_EQUAL))		ret[max++]='+';
		if(down(KEY_BACKSLASH))	ret[max++]='|';
		}

	ret[max]=0;
	}



