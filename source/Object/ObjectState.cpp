












	ObjectState::ObjectState(int t)
	{
	owner=NULL;
	startTime=0;
	duration=t;
	}
	ObjectState::~ObjectState()
	{
	}



int		ObjectState::getRemainTime()const
	{
	if(ftime>=startTime+duration)
		return 0;
	return startTime+duration-ftime;
	}

int		ObjectState::getElapsedTime()const
	{
	if(ftime<=startTime)
		return 0;
	return ftime-startTime;
	}

int		ObjectState::getDuration()const
	{
	return duration;
	}


void	ObjectState::setOwner(Object *o)
	{
	argCheck(o==NULL);
	owner=o;
	startTime=ftime;
	}
Object*	ObjectState::getOwner()const
	{
	argCheck(owner==NULL);
	return owner;
	}


Coord	ObjectState::getPositionDeviation()const
	{
	return zeroCoord;
	}
Coord	ObjectState::getSize(Coord originSize)const
	{
	return originSize;
	}
bool	ObjectState::getReShow()const
	{
	return false;
	}















	PositionDeviation::PositionDeviation(Coord c,Mapping *m,int t):ObjectState(t)
	{
	argCheck(m==NULL);
	deviation=c;
	mapping=m;
	}
	
	PositionDeviation::~PositionDeviation()
	{
	delete mapping;
	}
	
Coord	PositionDeviation::getPositionDeviation()const
	{
	return deviation*mapping->getValue((double)getElapsedTime()/getDuration());
	}





	SizeDeviation::SizeDeviation(Coord c,Mapping *m,int t):ObjectState(t)
	{
	argCheck(m==NULL);
	deviation=c;
	mapping=m;
	}
	
	SizeDeviation::~SizeDeviation()
	{
	delete mapping;
	}
	
Coord	SizeDeviation::getSize(Coord originSize)const
	{
	return originSize+deviation*mapping->getValue((double)getElapsedTime()/getDuration());
	}

bool	SizeDeviation::getReShow()const
	{
	return true;
	}






	ShakeState::ShakeState(int a,Mapping *tm,int t,Mapping *am):ObjectState(t)
	{
	if(am==NULL)
		am=new SimpleMapping;
	argCheck(tm==NULL||am==NULL);
	amplitude=a;
	timeMapping=tm;
	ampMapping=am;
	}
	ShakeState::~ShakeState()
	{
	delete timeMapping;
	delete ampMapping;
	}
Coord	ShakeState::getPositionDeviation()const
	{
	return Coord(amplitude*ampMapping->getValue(rand()&0xFF,256)*timeMapping->getValue(getElapsedTime(),getDuration())*(1-rand()%2*2)
				,amplitude*ampMapping->getValue(rand()&0xFF,256)*timeMapping->getValue(getElapsedTime(),getDuration())*(1-rand()%2*2));
	}









