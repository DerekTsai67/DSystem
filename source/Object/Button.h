



class Button:public Object{
	public:
	Button(Position p,Datafile *df,Size size,Picture *icon,int r0=0,int r1=0,int r2=0);
	Button(Position p,const Picture *pic,int r0=0,int r1=0,int r2=0);
	Button(Position p,int r0=0,int r1=0,int r2=0);
	~Button();
void	addButtonType(int t);
void	deleteButtonType(int t);
void	setIcon(Picture *icon);
void	lclick(Coord c,bool b);
void	rclick(Coord c,bool b);
void	mclick(Coord c,bool b);
void	click(int i,bool b);
void	setLooks(int i);
void	setDatafile(Datafile *df);
virtual void	button(int i);
void	pointed(Coord c,bool On);
void	show();

enum ButtonType{
	leftAvalible	=0x0001,
	rightAvalible	=0x0002,
	middleAvalible	=0x0004,
	pressed			=0X0008
	};
	protected:
Datafile	*datafile;
Picture		*icon;
Picture		*buffer[3];
const Picture	*bg;
int 	looks;
int 	responseIndex[3];
int 	buttonType;
	};










