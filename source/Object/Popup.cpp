


static const Coord	border=Coord(30,100);


	PopupButton::PopupButton(ConstString t,int r,bool a):MenuObject(Coord(100,32))
	{
	title.copyFrom(t);
	res=r;
	available=a;
	}

void	PopupButton::lclick(Coord c)
	{
	getOwner()->response(res);
	}

void	PopupButton::show()
	{
	font->putString(wBuffer,title,zeroEffectivePosition);
	}



	PopupFolderButton::PopupFolderButton(ConstString title,bool available,Object *target):PopupButton(title,0,available)
	{
	folderWindow=new Popup(zeroEffectivePosition,target,(Popup*)getOwner());
	folderWindow->deleteType(OBJECT_visible);
	addWindow(folderWindow);
	}

	PopupFolderButton::~PopupFolderButton()
	{
	folderWindow->kill();
	}

void	PopupFolderButton::lclick(Coord c)
	{
	Coord p=getPosition()+getSize().onlyX();
	Coord maxPos=p+folderWindow->getSize()+border;
	if(maxPos.x>=window_size.x)
		p.x-=folderWindow->getSize().x+getSize().x;
	if(maxPos.y>=window_size.y)
		p.y-=folderWindow->getSize().y-getSize().y;
	folderWindow->setPosition(Position(p));
	folderWindow->push_to_top();
	folderWindow->addType(OBJECT_visible);
	}


Popup*	PopupFolderButton::getFolder()
	{
	return folderWindow;
	}





	Popup::Popup(Position pos,Object *t,Popup *o):Window(pos,Coord(10,10))
	{
	resTarget=t;
	origin=o;
	button=new LinkedList<PopupButton>(false);
	addEtcObject(menu=new Menu(zeroEffectivePosition,Size(100,20),(LinkedList<MenuObject>*)button));
	actingFolder=NULL;
	if(origin==NULL)
		{
		addBehind(resTarget->getOwner());
		}
	else
		addBehind(origin);
	}

void	Popup::addContext(ConstString title,int res,bool available)
	{
	if(actingFolder!=NULL)
		{
		actingFolder->addContext(title,res,available);
		return;
		}
	nowOwner=this;
	button->addItem(new PopupButton(title,res,available),true);
	}

void	Popup::addFolder(ConstString title,bool available)
	{
	if(actingFolder!=NULL)
		{
		actingFolder->addFolder(title,available);
		return;
		}
	nowOwner=this;
	PopupFolderButton *b=new PopupFolderButton(title,available,resTarget);
	actingFolder=b->getFolder();
	button->addItem(b,true);
	}

bool	Popup::endFolder()
	{
	if(actingFolder!=NULL)
		{
		if(actingFolder->endFolder())
			actingFolder=NULL;
		return false;
		}
	refresh();
	if(origin==NULL)
		{
		Coord maxPos=getPosition()+getSize()+border;
		setPosition(Position(cursorPos,
			maxPos.y>=window_size.y?
			(maxPos.x>=window_size.x?Position::EffectiveRightDown:Position::EffectiveLeftDown):
			(maxPos.x>=window_size.x?Position::EffectiveRightUp:Position::EffectiveLeftUp)
			,Position::EffectiveLeftUp));
		}
	return true;
	}

void	Popup::refresh()
	{
	menu->fitSize(DSData->getPic(BDF_DataButton));
	reSize(menu->getSize());
	}

void	Popup::senseOut()
	{
	if(origin==NULL)
		kill();
	else
		deleteType(OBJECT_visible);
	}

void	Popup::response(int r)
	{
	if(resTarget!=NULL)
		resTarget->response(r);

	Popup *p=this;
	while(p->origin!=NULL)
		p=p->origin;
	p->kill();
	}



