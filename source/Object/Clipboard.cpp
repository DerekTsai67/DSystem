

	Clipboard::Clipboard()
	{
	dataMax=50;
	pointerData=dalloc<void*>(dataMax);
	dataType=0;
	dataIndex=0;
	}

	Clipboard::~Clipboard()
	{
	dfree(pointerData);
	}

void	Clipboard::addCopy(void *data)
	{
	argCheck(data==NULL);
	if(dataIndex==dataMax)
		{
		void **n=dalloc<void*>(dataMax*2);
		memcpy(n,pointerData,sizeof(void*)*dataMax);
		dfree(pointerData);
		pointerData=n;
		dataMax*=2;
		}
	pointerData[dataIndex++]=data;
	}

void	Clipboard::clear(int t)
	{
	dataType=t;
	dataIndex=0;
	}

bool	Clipboard::avalible(int t)
	{
	if(dataType==t&&dataIndex>=1)
		return true;
	return false;
	}

bool	Clipboard::isCopyed(void *d)
	{
	for(int i=0;i<dataIndex;i++)
		if(pointerData[i]==d)
			return true;
	return false;
	}

void*	Clipboard::getData(int i)
	{
	argCheck(i<0||i>=dataIndex)
	return pointerData[i];
	}

int		Clipboard::getNumber()const
	{
	return dataIndex;
	}

