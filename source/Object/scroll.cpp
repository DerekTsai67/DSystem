



	Scroll::Scroll(Position p,Size s,const Picture *texture,int &d,int m,int M,int ri):Object(p,s)
	{
	data=&d;oldData=d;
	min=m;max=M;
	responseIndex=ri;
	SENSE->middle=new MoveSensor();
	barSense=new RegularSense(new BarSensor());
	pic=texture;
	line=pic->makeLine(0,s);
	node=pic->makeTile(Coord(3,0));
	valueMax=getSize().x-getSize().y*2-node->getSize().x;
	getValue();
	}

	Scroll::~Scroll()
	{
	delete line;
	delete node;
	delete barSense;
	}

void	Scroll::act()
	{
	if(*data!=oldData)
		getValue();
	}

void	Scroll::BarSensor::press(Coord p)
	{
	typeOwner()->setValue(p.x-typeOwner()->getSize().y-typeOwner()->node->getSize().x/2);
	}

void	Scroll::lclick(Coord p)
	{
	if		(p.x>=getSize().x-getSize().y)	value+=1;
	else if	(p.x<getSize().y)			value-=1;
	else if (p.x<getSize().y+value)		value-=16;
	else							value+=16;
	setValue(value);
	}

Sense*	Scroll::sense(Coord c)
	{
	int xm=getSize().y+value;
	if(c.x<xm||c.x>=xm+node->getSize().x)
		return defaultSense;
	else
		return barSense;
	}

void	Scroll::setValue(int v)
	{
	value=v;
	if(value>valueMax)	value=valueMax;
	if(value<0)			value=0;
	oldData=*data=min+((max-min)*value+valueMax/2)/valueMax;
	owner->response(responseIndex);
	reShow();
	}

void	Scroll::getValue()
	{
	oldData=*data;
	value=((*data-min)*valueMax+(max-min)/2)/(max-min);
	reShow();
	}

void	Scroll::show()
	{
	wBuffer->blit(line,zeroEffectivePosition);
	wBuffer->draw(node,Position(getSize().y+value,0));
	}

