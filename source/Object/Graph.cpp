


	GraphLine::GraphLine():
		xScaler(scaler[0]),yScaler(scaler[1]),zScaler(scaler[2])
	{
	}
	
	GraphLine::~GraphLine()
	{
	}



	SimpleMeasureLine::SimpleMeasureLine(Measure *m,DS_RGB c)
	{
	argCheck(m==NULL);
	measure=m;
	color=c;
	}
void	SimpleMeasureLine::show(Picture *wBuffer)
	{
	measure->show(wBuffer,xScaler,yScaler,color);
	}
	
	SampleMeasureLine::SampleMeasureLine(SampleMeasure &m,int xi,int yi,DS_RGB c)
	{
	argCheck(xi<0||yi<0);
	measure=m;
	xIndex=xi;
	yIndex=yi;
	zIndex=-3;
	color=c;
	}
	
	SampleMeasureLine::SampleMeasureLine(SampleMeasure &m,int xi,int yi,int zi)
	{
	argCheck(xi<0||yi<0||zi<0);
	measure=m;
	xIndex=xi;
	yIndex=yi;
	zIndex=zi;
	}
	
	
void	SampleMeasureLine::show(Picture *wBuffer)
	{
	if(zIndex==-3)
		measure.show(wBuffer,xIndex,yIndex,xScaler,yScaler,color);
	else
		measure.show2D(wBuffer,xIndex,yIndex,zIndex,xScaler,yScaler,zScaler);
	}
	
	
	StrightLine::StrightLine(double d,int a,DS_RGB c)
	{
	data=d;
	axis=a;
	color=c;
	}
	
	StrightLine::~StrightLine()
	{
	}
	
void	StrightLine::show(Picture *buffer)
	{
	if(axis==0)
		{
		int x=xScaler->convert(data,buffer->getSize().x);
		buffer->drawLineC(Position(x,0),Position(x,buffer->getSize().y),color);
		}
	else
		{
		int y=yScaler->convert(data,buffer->getSize().y);
		buffer->drawLineC(Position(0,y),Position(buffer->getSize().x,y),color);
		}
	}



//============================================================================

	Graph::Graph(Position p,Size s):Object(p,s),
		scaler({10,10,10}),xScaler(scaler[0]),yScaler(scaler[1]),zScaler(scaler[2]),line(10)
	{
	}
	Graph::~Graph()
	{
	xScaler.deleteAll();
	yScaler.deleteAll();
	zScaler.deleteAll();
	line.deleteAll();
	}
	
void	Graph::addXLine(double x,DS_RGB color,int yScalerIndex,int xScalerIndex)
	{
	addLine(new StrightLine(x,0,color),xScalerIndex,yScalerIndex);
	}

void	Graph::addYLine(double y,DS_RGB color,int yScalerIndex,int xScalerIndex)
	{
	addLine(new StrightLine(y,1,color),xScalerIndex,yScalerIndex);
	}
	
void	Graph::addMeasure(Measure *m,DS_RGB color,int xScalerIndex,int yScalerIndex)
	{
	argCheck(m==NULL);
	SimpleMeasureLine *ml=new SimpleMeasureLine(m,color);
	addLine(ml,xScalerIndex,yScalerIndex);
	m->fitScaler(ml->xScaler,0);
	m->fitScaler(ml->yScaler,1);
	}
	
void	Graph::addSampleMeasure(SampleMeasure m,int xi,int yi,DS_RGB color,int xScalerIndex,int yScalerIndex)
	{
	SampleMeasureLine *ml=new SampleMeasureLine(m,xi,yi,color);
	addLine(ml,xScalerIndex,yScalerIndex);
	m.fitScaler(ml->xScaler,xi,0);
	m.fitScaler(ml->yScaler,yi,1);
	}

void	Graph::addSampleMeasure2D(SampleMeasure m,int xi,int yi,int zi,int xScalerIndex,int yScalerIndex,int zScalerIndex)
	{
	SampleMeasureLine *ml=new SampleMeasureLine(m,xi,yi,zi);
	addLine(ml,xScalerIndex,yScalerIndex,zScalerIndex);
	m.fitScaler(ml->xScaler,0);
	m.fitScaler(ml->yScaler,0);
	m.fitScaler(ml->zScaler,1);
	}

void	Graph::addLine(GraphLine *gl,int xScalerIndex,int yScalerIndex,int zScalerIndex)
	{
	argCheck(gl==NULL);
	int arg[3]={xScalerIndex,yScalerIndex,zScalerIndex};
	for(int axis=0;axis<2+(zScalerIndex!=-3);axis++)
		{
		if(arg[axis]>=scaler[axis].getNumber())
			arg[axis]=-2;
		if(arg[axis]>=0)
			if(true)
				{
				gl->scaler[axis]=scaler[axis][arg[axis]];
				continue;
				}
		
		int i=scaler[axis].getNumber();
		if(arg[axis]!=-2)
		for(i=0;i<scaler[axis].getNumber();i++)
			{
			if(true)
				{
				gl->scaler[axis]=scaler[axis][i];
				break;
				}
			}
		if(i==scaler[axis].getNumber())
			{
			scaler[axis].addItem(new Scaler(axis));
			gl->scaler[axis]=scaler[axis][i];
			}
		}
	
	line.addItem(gl);
	reShow();
	}



void	Graph::addScaler(Scaler *s)
	{
	scaler[s->axis].addItem(s);
	}
	
Scaler*	Graph::getScaler(int axis,int index)
	{
	argCheck(axis<0||axis>=3);
	return scaler[axis][index];
	}



void	Graph::clearLine()
	{
	line.deleteAll();
	xScaler.deleteAll();
	yScaler.deleteAll();
	zScaler.deleteAll();
	reShow();
	}


void	Graph::show()
	{
	wBuffer->clear(white);
	if(xScaler.getNumber()<=0||yScaler.getNumber()<=0)
		return;
	int edgePoint=20;
	int xPoint=xScaler[0]->convert(0.0,wBuffer->getSize().x);
	int yPoint=yScaler[0]->convert(0.0,wBuffer->getSize().y);
	takeMax(xPoint,edgePoint);
	takeMax(yPoint,edgePoint);
	takeMin(xPoint,wBuffer->getSize().x-edgePoint);
	takeMin(yPoint,wBuffer->getSize().y-edgePoint);
	wBuffer->drawLineC(Position(xPoint  ,0),Position(xPoint  ,wBuffer->getSize().y),orange);
	wBuffer->drawLineC(Position(xPoint+1,0),Position(xPoint+1,wBuffer->getSize().y),orange);
	wBuffer->drawLineC(Position(0,yPoint  ),Position(wBuffer->getSize().x,yPoint  ),orange);
	wBuffer->drawLineC(Position(0,yPoint+1),Position(wBuffer->getSize().x,yPoint+1),orange);

	FOR_ARRAY(si,xScaler)
		{
		double ex=log10(xScaler[si]->getDistance())-log10(2);
		int exi=floor(ex);
		double exd=ex-exi;
		double base=1.0;
		if(exd>=log10(5))
			base=5.0;
		else if(exd>=log10(2))
			base=2.0;
		base*=pow(10.0,exi);
		//report("base %f %d =%f",ex,exi,base);
			
		for(int i=xScaler[si]->min/base;i<xScaler[si]->max/base+1;i++)
			{
			int point=xScaler[si]->convert(base*i,wBuffer->getSize().x);
			wBuffer->drawLineC(Position(point,yPoint-5),Position(point,yPoint+5),orange);
			
			char num[50];
			char format[50];
			if(exi<0)
				sprintf(format,"%%.%df",-exi);
			else
				sprintf(format,"%%.0f");
			sprintf(num,format,base*i);
			//report("[%s] at %d",num,point);
			smallFont->putString(wBuffer,num,Position(point,yPoint+2));
			}
		}
	FOR_ARRAY(si,yScaler)
		{
		double ex=log10(yScaler[si]->getDistance())-log10(6);
		int exi=floor(ex);
		double exd=ex-exi;
		double base=1.0;
		if(exd>=log10(5))
			base=5.0;
		else if(exd>=log10(2))
			base=2.0;
		base*=pow(10.0,exi);
		//report("base %f %d =%f",ex,exi,base);
			
		for(int i=yScaler[si]->min/base;i<yScaler[si]->max/base+1;i++)
			{
			int point=yScaler[si]->convert(base*i,wBuffer->getSize().y);
			wBuffer->drawLineC(Position(0,point),Position(wBuffer->getSize().x,point),orange);
			
			char num[50];
			char format[50];
			if(exi<0)
				sprintf(format,"%%.%df",-exi);
			else
				sprintf(format,"%%.0f");
			sprintf(num,format,base*i);
			//report("[%s] at %d",num,point);
			smallFont->putString(wBuffer,num,Position(xPoint,point,Position::EffectiveLeftUp,Position::EffectiveLeftUp));
			}
		}
	FOR_ARRAY(li,line)
		{
		GraphLine *gl=line[li];
		gl->show(wBuffer);
		}
	}














	Scaler::Scaler(int a,double min,double max)
	{
	argCheck(a<0||a>2);
	axis=a;
	logarithm=false;
	fixed=false;
	setMin(min);
	setMax(max);
	fixed=true;
	}

	Scaler::Scaler(int a)
	{
	argCheck(a<0||a>2);
	axis=a;
	logarithm=false;
	fixed=false;
	setMin(-10.0);
	setMax(10.0);
	}
	
Scaler*	Scaler::setLogarithm()
	{
	logarithm=true;
	logarithmReady=false;
	return this;
	}
void	Scaler::refreshLogarithm()
	{
	if(logarithm&&!logarithmReady)
		{
		lmin=log10(min);
		lmax=log10(max);
		logarithmReady=true;
		}
	}

void	Scaler::setMin(double mi)
	{
	if(fixed)
		return;
	min=mi;
	if(logarithm)
		logarithmReady=false;
	}

void	Scaler::setMax(double ma)
	{
	if(fixed)
		return;
	max=ma;
	if(logarithm)
		logarithmReady=false;
	}


/*void	Scaler::addFitMeasure(SampleMeasure *m,int si,int dep)
	{
	argCheck(m==NULL);
	if(fixed)
		return;
	if(min==-10.0&&max==10.0)
		{
		setMin(m->getSample(0)->getAxis(si,dep));
		setMax(m->getSample(0)->getAxis(si,dep));
		}
	for(int i=0;i<m->getNumber();i++)
		{
		double s=m->getSample(i)->getAxis(si,dep);
		if(s>max)
			setMax(s);
		if(s<min)
			setMin(s);
		}
	}*/
	
void	Scaler::fitSample(double s)
	{
	if(min==-10.0&&max==10.0)
		{
		setMin(s);
		setMax(s);
		}
	if(s>max)
		setMax(s);
	if(s<min)
		setMin(s);
	}

int		Scaler::convert(double s,int l)
	{
	refreshLogarithm();
	int t;
	if(logarithm)
		t=(log10(s)-lmin)*l/(lmax-lmin)+0.5;
	else
		t=(s-min)*l/(max-min)+0.5;
	
	if(s==INFINITY)
		t=l*2;
	takeMax(t,-l);
	takeMin(t,l*2);
	if(axis==1)
		t=l-t-1;
	return t;
	}
	
double	Scaler::getDistance()
	{
	return max-min;
	}

int		Scaler::convertDiff(double s,int l)
	{
	refreshLogarithm();
	if(logarithm)
		return log10(s)*l/(lmax-lmin)+0.5;
	else
		return s*l/(max-min)+0.5;
	}


double	Scaler::convertBack(int p,int l)
	{
	refreshLogarithm();
	if(axis==1)
		p=l-p-1;
	if(logarithm)
		return lmin+((lmax-lmin)*log10(p)/l);
	else
		return min+((max-min)*p/l);
	}














