
template<class A,class B>
class ObjectLinker{
	 public:
	 ObjectLinker(int r=0);
	~ObjectLinker();
friend class ObjectLinker<B,A>;
void	setOwner(A*);
void	clear();
void	setTo(ObjectLinker<B,A> &p,int ta,int tb,bool add=false);
void	setTo(B *p);
		operator B*();
void	operator=(ObjectLinker<B,A> &p);
B*		operator=(B *p);
B*		operator->();
bool	operator==(B *p);
bool	operator!=(B *p);
	 private:
B	*targetOwner;
ObjectLinker<B,A>	*target;
A	*owner;
int type,responseInfo;
	};

template<class A,class B>
	ObjectLinker<A,B>:: ObjectLinker(int r)
	{
	target=NULL;
	targetOwner=NULL;
	owner=(A*)nowOwner;
	type=0;
	responseInfo=r;
	}

template<class A,class B>
	ObjectLinker<A,B>::~ObjectLinker()
	{
	//report("Deleting linker %s to %s.",typeid(A).name(),typeid(B).name());
	if(targetOwner==NULL)
		return;

	if(target!=NULL)
		target->clear();
	else
		targetOwner->etcLinker.deleteItem((EtcLinker*)this,true);
	switch(type)
		{
		case 1:/*
			if(targetOwner->isWindow())
				deleteWindow((Window*)targetOwner);
			else
				delete targetOwner;*/
			targetOwner->kill();
			break;
		}
	}

template<class A,class B>
void	ObjectLinker<A,B>::setOwner(A *o)
	{
	owner=o;
	}



template<class A,class B>
void	ObjectLinker<A,B>::clear()
	{
	if(targetOwner==NULL)
		error("Clear when target==NULL.");
	owner->response(responseInfo);
	targetOwner=NULL;
	}


template<class A,class B>
void	ObjectLinker<A,B>::setTo(ObjectLinker<B,A> &p,int ta,int tb,bool add)
	{
	if(targetOwner!=NULL)
		error("ObjectLinker reaimmed.");

	target=&p;
	target->target=this;
	targetOwner=target->owner;
	target->targetOwner=owner;
	if(targetOwner==NULL||target->targetOwner==NULL)
		error("ObjectLinker = NULL. 1");
	type=ta;
	target->type=tb;
	if(add)
		addWindow(targetOwner);
	}

template<class A,class B>
void	ObjectLinker<A,B>::setTo(B *p)
	{
	if(targetOwner!=NULL)
		error("ObjectLinker reaimmed.");
	if(p==NULL)
		error("ObjectLinker = NULL. 2");

	target=NULL;
	targetOwner=p;
	targetOwner->etcLinker.addItem((EtcLinker*)this);
	}


template<class A,class B>
	ObjectLinker<A,B>::operator B*()
	{
	return targetOwner;
	}

template<class A,class B>
void	ObjectLinker<A,B>::operator=(ObjectLinker<B,A> &p)
	{
	setTo(p,0,0);
	}

template<class A,class B>
B*		ObjectLinker<A,B>::operator=(B *p)
	{
	setTo(p);
	return p;
	}

template<class A,class B>
B*	ObjectLinker<A,B>::operator->()
	{
	if(targetOwner==NULL)
		error("ObjectLinker NULL used. %s to %s.",typeid(A).name(),typeid(B).name());
	return targetOwner;
	}

template<class A,class B>
bool	ObjectLinker<A,B>::operator==(B *p)
	{
	return targetOwner==p;
	}

template<class A,class B>
bool	ObjectLinker<A,B>::operator!=(B *p)
	{
	return !((*this)==p);
	}
