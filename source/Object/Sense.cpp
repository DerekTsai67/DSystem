

	 Sense::Sense()
	{
	senseObject=nowObject;
	//senseWindow=nowObject->owner;
	count++;
	}

	Sense::~Sense()
	{
	if(sense==this)
		{
		sense=nonSense;
		//senseWindow=NULL;
		}
	count--;
	}


void	Sense::pointed(Coord p,bool on)
	{
	senseObject->pointed(p-senseObject->getPosition(),on);
	}

void	NonSense::pointed(Coord p,bool on){}


	RegularSense::RegularSense(Sensor *l,Sensor *r,Sensor *m)
	{
	right=r;
	middle=m;
	left=l;
	boot[0]=boot[1]=boot[2]=0;
	}

	RegularSense::~RegularSense()
	{
	if(right)	delete right;
	if(middle)	delete middle;
	if(left)	delete left;
	}

int 	Sense::process(Coord cursorPos,KeySignal keySignal)
	{
	return 0;
	}

int 	RegularSense::process(Coord cursorPos,KeySignal keySignal)
	{
	Coord sensePos=cursorPos-senseObject->getPosition();
	int ret=0;
	//Coord sensePos=cursorPos-senseObject->pos-(senseObject==senseWindow?zeroCoord:senseWindow->pos);
/*
	if(dCursor!=zeroCoord)
		if(!(boot[0]||boot[1]|+boot[2]))
			senseObject->pointed(sensePos);*/

	if(keySignal.mouseDown(MOUSE_LEFT))
		boot[MOUSE_LEFT]=1;
	if(boot[MOUSE_LEFT])
		{
		if(left!=NULL)
			ret|=left->process(sensePos,keySignal.getMouseState(MOUSE_LEFT));
		else
			{
			if(keySignal.mouseDown(MOUSE_LEFT))
				{
				ret|=SENSOR_push;
				senseObject->lclick(sensePos);
				senseObject->lclick(sensePos,true);
				}
			if(keySignal.mouseUp(MOUSE_LEFT))
				{
				senseObject->lclick(sensePos,false);
				}
			if(keySignal.mousePress(MOUSE_LEFT))
				ret|=SENSOR_hold;
			}
		}


	if(keySignal.mouseDown(MOUSE_RIGHT))
		boot[MOUSE_RIGHT]=1;
	if(boot[MOUSE_RIGHT])
		{
		if(right!=NULL)
			ret|=right->process(sensePos,keySignal.getMouseState(MOUSE_RIGHT));
		else{
			if(keySignal.mouseDown(MOUSE_RIGHT))
				{
				ret|=SENSOR_push;
				senseObject->rclick(sensePos);
				senseObject->rclick(sensePos,true);
				}
			if(keySignal.mouseUp(MOUSE_RIGHT))
				{
				senseObject->rclick(sensePos,false);
				}
			if(keySignal.mousePress(MOUSE_RIGHT))
				ret|=SENSOR_hold;
			}
		}


	if(keySignal.mouseDown(MOUSE_MIDDLE))
		boot[MOUSE_MIDDLE]=1;
	if(boot[MOUSE_MIDDLE])
		{
		if(middle!=NULL)
			ret|=middle->process(sensePos,keySignal.getMouseState(MOUSE_MIDDLE));
		else{
			if(keySignal.mouseDown(MOUSE_MIDDLE))
				{
				ret|=SENSOR_push;
				senseObject->mclick(sensePos);
				senseObject->mclick(sensePos,true);
				}
			if(keySignal.mouseUp(MOUSE_MIDDLE))
				{
				senseObject->mclick(sensePos,false);
				}
			if(keySignal.mousePress(MOUSE_MIDDLE))
				ret|=SENSOR_hold;
			}
		}

	if(ret&SENSOR_push)
		senseObject->focus();
	if(!(ret&SENSOR_hold))
		boot[0]=boot[1]=boot[2]=0;

	return ret;
	}

int 	NonSense::process(Coord cursorPos,KeySignal keySignal)
	{
	return 0;
	}



void	Sense::show()
	{
	}














//==============================================================================


	Sensor::Sensor()
	{
	owner=nowObject;
	}

	Sensor::~Sensor()
	{
	}

int 	Sensor::process(Coord cursor,int b)
	{
	return 0;
	}

	PullSensor::PullSensor()
	{
	startPos=zeroCoord;
	pulling=false;
	}
bool	PullSensor::isPulling()
	{
	return pulling;
	}
void	PullSensor::down	(Coord cursor){}
void	PullSensor::press	(Coord cursor){}
void	PullSensor::up  	(Coord cursor){}
int 	PullSensor::process(Coord sensePos,int b)
	{
	int ret=0;
	switch(b)
		{
		case 0:
			up(sensePos);
			pulling=false;
		case 1:
			return 0;
		case 4:
		case 3:
			startPos=sensePos;
			down(sensePos);
			pulling=true;
			ret|=SENSOR_push;
		case 2:
			press(sensePos);
			return ret|SENSOR_hold;
		}
	error("PullSensor::process end.");
	return 0;
	}


void	MoveSensor::press	(Coord sensePos)
	{
	owner->move(dCursor);
	}

void	SuicideSensor::down(Coord sensePos)
	{
	owner->kill();
	}



	FreeSensor::FreeSensor(void(*c)())
	{
	click=c;
	dclick=down=press=up=nonFunction;
	}
	FreeSensor::FreeSensor(void(*c)(),void(*dc)(),void(*d)(),void(*p)(),void(*u)())
	{
	down=d;
	press=p;
	up=u;
	click=c;
	dclick=dc;

	}
int 	FreeSensor::process(Coord cursor,int b)
	{
	return 0;
	}

	ResponseSensor::ResponseSensor(int c)
	{
	click=c;
	dclick=down=press=up=-1;
	}
	ResponseSensor::ResponseSensor(int c,int dc,int d,int p,int u)
	{
	down=d;
	press=p;
	up=u;
	click=c;
	dclick=dc;

	}
int 	ResponseSensor::process(Coord cursor,int b)
	{
	return 0;
	}

//======================================================================
	React::React()
	{
	reactObject=nowObject;
	sense=NULL;
	}

	React::~React()
	{
	}

void	React::setSense(Sense *s)
	{
	sense=s;
	}

bool	React::check(Coord p,Sense *s)
	{
	return false;
	}
void	React::pointed(Coord p,bool on)
	{

	}
void	React::press(Coord p)
	{

	}
int 	React::process(Coord p)
	{
	return 0;
	}
