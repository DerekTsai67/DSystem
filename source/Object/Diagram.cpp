

/*
	Graph::Graph(Position p,Size s,int n,double b):Object(p,s)
	{
	deleteType(OBJECT_sensable);
	bitIndex=dalloc<int>(n);
	color=dalloc<DS_RGB>(n);
	bitData=dalloc<double*>(n);
	for(int i=0;i<n;i++)
		{
		bitData[i]=dalloc<double>(s.x+1);
		color[i]=red;
		}
	dataNum=n;
	base=scaler=-1;
	modify=true;
	drawed=1;left=1;
	buf=b;
	wBuffer->clear(white);
	lmin=lmax=-2.31;
	}

	Graph::~Graph()
	{
	for(int i=0;i<dataNum;i++)
		dfree(bitData[i]);
	dfree(bitData);
	dfree(bitIndex);
	dfree(color);
	}
	
void Graph::switchModify()
	{
	modify=!modify;
	}
int Graph::getDataNum()
	{
	return dataNum;
	}

void Graph::setData(int index,DS_RGB c,int t)
	{
	argCheck(index<0||index>=dataNum);
	color[index]=c;
	switch(t)
		{
	case 1:
		base=index;
		break;
	case 2:
		if(base==-1)
			error("Graph setting scaler before base.");
		scaler=index;
		break;
		}
	}

void Graph::addLine(double d,DS_RGB c)
	{
	line.addItem(new Line(d,c));
	}

void Graph::addBit(double y,int index)
	{
	if(index>=dataNum)
		error("Graph::addBit dataIndex>num.");
	if(index<0)
		error("Graph::addBit dataIndex<0.");
	if(bitIndex[index]<0)
		error("Graph::addBit bitIndex<0.");
	//if(bitIndex>size.x)
	//	error("Graph::addBit overflow.");
	bitData[index][bitIndex[index]++%(getSize().x+1)]=y;
	reShow();
	}

double	Graph::getAmplitude(int index)
	{
	if(index>=dataNum)
		error("Graph::getAmplitude dataIndex>num.");
	if(index<0)
		error("Graph::getAmplitude dataIndex<0.");

	int maxIndex=bitIndex[0];
	for(int i=1;i<dataNum;i++)
		if(bitIndex[i]>maxIndex)
			maxIndex=bitIndex[i];
	int nl=maxIndex-getSize().x;
	if(nl<1)nl=1;

	double max,min;
	max=min=getData(index,nl%(getSize().x+1));
	for(int x=nl;x<bitIndex[index];x++)
		{
		if(getData(index,x%(getSize().x+1))>max)max=getData(index,x%(getSize().x+1));
		if(getData(index,x%(getSize().x+1))<min)min=getData(index,x%(getSize().x+1));
		}
	return max-min;

	}

double Graph::getData(int di,int bi)
	{
	double data=bitData[di][bi];
	if(!modify)
		return data;
	if(base!=-1)
		data-=bitData[base][bi];
	if(scaler!=-1)
		data=data/(bitData[scaler][bi]-bitData[base][bi])*100.0;
	return data;
	}

bool Graph::reScale(int nl)
	{
	max=min=getData(0,nl%(getSize().x+1));
	for(int di=0;di<dataNum;di++)
	for(int x=nl;x<bitIndex[di];x++)
		{
		if(getData(di,x%(getSize().x+1))>max)max=getData(di,x%(getSize().x+1));
		if(getData(di,x%(getSize().x+1))<min)min=getData(di,x%(getSize().x+1));
		}
	max+=buf;min-=buf;
	max-=min;

	if(max==lmax&&min==lmin)
		return false;
	else
		{
		lmax=max;lmin=min;
		return true;
		}
	}

int Graph::getScaledData(int di,int bi)
	{
	return getScaledData(getData(di,bi));
	}
int Graph::getScaledData(double d)
	{
	return getSize().y-1-getSize().y*(d-min)/max;
	return getSize().y/2-1-(d);
	}

void Graph::show()
	{
	int maxIndex=bitIndex[0];
	for(int i=1;i<dataNum;i++)
		if(bitIndex[i]>maxIndex)
			maxIndex=bitIndex[i];
	int nl=maxIndex-getSize().x;
	if(nl<1)nl=1;

	if(!reScale(nl)&&0)
		wBuffer->move(Coord(left-nl,0),white);
	else
		{
		wBuffer->clear(white);
		drawed=nl;
		}
	left=nl;

	wBuffer->drawLineD(Position(0,getScaledData(0.0)),Coord(getSize().x-1,0),blue);
	FOR_LL(now,line,Line)
		wBuffer->drawLineD(Position(0,getScaledData(now->y)),Coord(getSize().x-1,0),now->color);
	END_FOR_LL
	for(int y=-19;y<=19;y++)
		{
		if(max<100)
		wBuffer->drawLineD(Position(0,getScaledData(y)),Coord(5,0),black);
		if(max<1000)
		wBuffer->drawLineD(Position(0,getScaledData(y*10.0)),Coord(20,0),blue);
		if(max<10000)
		wBuffer->drawLineD(Position(0,getScaledData(y*100.0)),Coord(50,0),red);
		if(max<100000)
		wBuffer->drawLineD(Position(0,getScaledData(y*1000.0)),Coord(75,0),blue);
		if(max<1000000)
		wBuffer->drawLineD(Position(0,getScaledData(y*10000.0)),Coord(100,0),blue);
		}

	drawed=drawed>left?drawed:left;
	for(int di=0;di<dataNum;di++)
		{
		int now,last=getScaledData(di,(drawed-1)%(getSize().x+1));
		for(int d=drawed;d<bitIndex[di];d++)
			{
			now=getScaledData(di,(d)%(getSize().x+1));
			if(now==-1)now++;
			last+=(now>last)-(now<last);
			wBuffer->drawLineD(Position(d-left-1,last),Coord(0,now-last),color[di]);
			last=now;
			}
		}
	drawed=maxIndex;
	}
//========================================================================================================
	Diagram::Diagram(Position p,Size s):Window(p,DSData->getPic(6)->makeSquare(s+Coord(0,0)))
	{
	SENSE->left=new MoveSensor();
	addEtcObject(graph=new Graph(zeroEffectivePosition,s,1,0.0));
	}

	Diagram::Diagram(Position p,Graph *g):Window(p,DSData->getPic(6)->makeSquare(g->getSize()+Coord(0,g->getDataNum()*20)))
	{
	SENSE->left=new MoveSensor();
	g->setOwner(this);
	g->setPosition(zeroEffectivePosition);
	graph=g;
	addEtcObject(g);
	//defaultReShow=true;
	}

	Diagram::~Diagram()
	{
	}
void Diagram::mclick(Coord c)
	{
	graph->switchModify();
	}
void Diagram::rclick(Coord c)
	{
	kill();
	}

void Diagram::show()
	{
	char str[100];
	for(int i=0;i<graph->getDataNum();i++)
		{
		sprintf(str,"amp%d:%5.2f",i,graph->getAmplitude(i));
		font->putString(wBuffer,str,Position(0,graph->getSize().y+20*i));
		wBuffer->putPixel(Position(0,graph->getSize().y+20*i),red);
		}
	}
//===========================================================================================
	FunctionGraph::FunctionGraph(Position p,Size s
								,double (*func)(double x),double l,double r,double b
								):Graph(p,s,1,b)
	{
	f=func;
	left=l;
	right=r;


	//initGraph(s,5.0);
	double scale=right-left;
	for(int x=0;x<=getSize().x*3/2;x++)
		addBit(100*f(left+scale*x/getSize().x));

	}

//===========================================================================================
	SoundGraph::SoundGraph(Position p,Size s,Sound *so):Graph(p,s,so->getChannel(),10.0)
	{
	if(so==NULL)
		error("NULL SoundGraph");

	sound=so;
	si=0;
	}

	SoundGraph::~SoundGraph()
	{
	delete sound;
	}

void	SoundGraph::act()
	{
	if(si<sound->getLength())
		{
		for(int i=0;i<4;i++)
			{
			for(int ci=0;ci<sound->getChannel();ci++)
				addBit(sound->getSample(ci,si),ci);
			si++;
			}
		reShow();
		}
	}

*/

