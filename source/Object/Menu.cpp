


	Menu::Menu(Position p,Size s,LinkedList<MenuObject> *LL):Object(p,DSData->getPic(BDF_hollowBG)->makeSquare(s))
	{
	menuSense=new RegularSense();
	//menuSense->left=new MenuSensor();
	//defaultReShow=true;
	addObject(*(data=LL));
	focusY=-1;
	line=DSData->getPic(BDF_MenuCursor)->makeLine(0,Size(s.x,16));
	direction=horizontal;
	refresh();
	}

	Menu::~Menu()
	{
	delete data;
	delete menuSense;
	delete line;
	}

void	Menu::setSR(Sensor *s,React *r)
	{
	menuSense->left=s;
	addReact(r);
	}

void	Menu::refresh()
	{
	Position p=zeroEffectivePosition;
	int my=0;
	FOR_LL(now,*data,MenuObject)
		now->setOwner(this);
		Coord c=now->getOriginSize();
		if(p.x+c.x>getOriginSize().x)
			{
			p.x=0;
			p.y+=my;
			my=0;
			}
		now->setPosition(p);
		p.x+=c.x;
		if(c.y>my)
			my=c.y;
	END_FOR_LL
	}

void	Menu::fitSize(const Picture *square)
	{
	if(square==NULL)
		square=DSData->getPic(BDF_hollowBG);
	Position p=zeroEffectivePosition;
	int mx=0;
	FOR_LL(now,*data,MenuObject)
		if(now->isDead())
			CONTINUE_LL;
		now->setOwner(this);
		Coord c=now->getOriginSize();
		if(now->getIndent().x+c.x>mx)
			mx=now->getIndent().x+c.x;
		now->setPosition(p+now->getIndent());
		p.y+=c.y;
	END_FOR_LL
	reSize(square->makeSquare(Size(mx,p.y)));
	}

Sense*	Menu::sense(Coord p)
	{/*
	Sense *ret=NULL;
	int y=0;
	FOR_LL(now,*data,MenuObject)
		ret=now->sense_(p+now->pos-Coord(0,y));
		if(ret==nonSense)
			{
			ret=menuSense;
			MenuSensor *ms=(MenuSensor*)(menuSense->left);
			if(!ms->isPulling())
				ms->selected=now_;
			}
		if(ret!=NULL)
			return ret;
		y+=now->getSize().y;
	END_FOR_LL*/
	return defaultSense;
	}

bool	MenuReact::check(Coord p,Sense *s)
	{
	//if(dynamic_cast<MenuSensor*>(((RegularSense*)s)->left)!=NULL)
	IF_SENSOR_CAST(s,n,MenuSensor)
		return true;
	return false;
	}

void	Menu::show()
	{
	//wBuffer->clear(red);
	/*
	int y=0;
	setShowDest(wBuffer);
	FOR_LL(now,*data,MenuObject)
		now->show_(Position(0,y).fix(now->wBuffer,wBuffer)-now->getPosition().fix(now->wBuffer,NULL));
		y+=now->getSize().y;
	END_FOR_LL
	setShowDest(NULL);
	if(focusY!=-1)
		wBuffer->draw(line,Position(0,focusY,Position::EffectiveMiddleLeft,Position::EffectiveLeftUp));
		*/
	}

/*
void	Menu::showOut(Coord p)
	{
	wBuffer->show(getPosition(),getSize(),0,NULL);
	}
*/
void	Menu::focused()
	{
	Coord pos=cursorPos-getPosition();
	int newFocusY=0;
	do{
	if(data->empty())
		{
		focusList=NULL;
		newFocusY=0;
		break;
		}
	int ty=(*data)[0]->getSize().y;
	if(pos.y<ty/2)
		{
		focusList=NULL;
		newFocusY=0;
		break;
		}
	FOR_LL(now,*data,MenuObject)
		now=now;//disable warning
		if(now_->next==NULL)
			{
			focusList=now_;
			newFocusY=ty;
			break;
			}
		int y=now_->next->data->getSize().y;
		if(pos.y<ty+y/2)
			{
			focusList=now_;
			newFocusY=ty;
			break;
			}
		ty+=y;
	END_FOR_LL
	}while(false);
	if(newFocusY!=focusY)
		{
		focusY=newFocusY;
		reShow();
		}
	}
void	Menu::focusOut()
	{
	focusY=-1;
	reShow();
	}

void	Menu::addItem(MenuObject *o)
	{
	data->addItem(o,focusList);
	o->setOwner(this);
	reShow();
	}

	MenuSensor::MenuSensor()
	{
	lastObject=NULL;
	}/*
void	Menu::MenuSensor::press(Coord p)
	{
	Menu *sw=dynamic_cast<Menu*>(nowSense->senseObject);
	if(sw!=lastObject&&lastObject!=NULL)
		lastObject->focusOut();
	lastObject=sw;
	if(sw!=NULL)
		{
		sw->focused();
		}
	}*/
void	MenuReact::pointed(Coord c,bool b)
	{
	if(!b)
		{
		typeOwner()->focusOut();
		}

	}
void	MenuReact::press(Coord c)
	{
	typeOwner()->focused();
	}
int 	MenuReact::process(Coord c)
	{
	IF_SENSOR_CAST(sense,ms,MenuSensor)
		{
		MenuObject *n=ms->getData();
		n->setOwner(typeOwner()->getOwner());
		typeOwner()->addItem(n);
		typeOwner()->focusOut();
		return 1;
		}
	return 0;
	}
MenuObject*	MenuSensor::getData()
	{
	return selected->data;
	}
void	MenuSensor::up(Coord p)
	{
	Menu *owner=typeOwner();/*
	Menu *sw=dynamic_cast<Menu*>(nowSense->senseObject);
	if(sw!=NULL)
		{
		//if(sw==fb)break;
		report("Menu::sensed : %s",typeid(*sw).name());
		sw->addItem(selected->data);
		sw->focusOut();
		owner->data->deleteItem(selected,true);
		}
	else
		if(selected->data->threwOut(senseWindow))
			owner->data->deleteItem(selected,true);*/
	if(::react->process(p)==1)
		owner->data->deleteItem(selected,true);
	/*if(dynamic_cast<Base*>(senseWindow))
		{
		addWindow(new PictureBlock(db->data));
		}*/
	}




	MenuObject::MenuObject(Picture *sbg,Size s):Object(zeroEffectivePosition,sbg,s)
	{
	init();
	}

	MenuObject::MenuObject(Picture *pic):Object(zeroEffectivePosition,pic)
	{
	init();
	}

	MenuObject::MenuObject(Coord size):Object(zeroEffectivePosition,size)
	{
	init();
	}

void	MenuObject::init()
	{
	refreshStatus=0;
	menuSelectedB=false;
	menuIndent=zeroCoord;
	}

	MenuObject::~MenuObject()
	{
	}

Menu*	MenuObject::getMenuOwner()const
	{
	if(!isType(getObjectOwner(),Menu))
		error("Cast failed.");
	return (Menu*)getObjectOwner();
	}

bool	MenuObject::isData(void *data)const
	{
	error("Undefined.");
	return false;
	}
	
void	MenuObject::setMenuSelected(bool b)
	{
	menuSelectedB=b;
	if(menuSelectedB)
		menuSelected();
	reShow();
	}

bool	MenuObject::isMenuSelected()const
	{
	return menuSelectedB;
	}

void	MenuObject::menuSelected()
	{
	}

void	MenuObject::setIndent(Coord c)
	{
	menuIndent=c;
	}
Coord	MenuObject::getIndent()const
	{
	return menuIndent;
	}



void	Menu::clearSelect()
	{
	FOR_LL(now,*data,MenuObject)
		now->setMenuSelected(false);
	END_FOR_LL
	}

bool	Menu::isMultiSelected()const
	{
	bool multi=false;
	FOR_LL(now,*data,MenuObject)
		if(now->isMenuSelected())
			{
			if(!multi)
				multi=true;
			else
				return true;
			}
	END_FOR_LL
	return false;
	}

void	Menu::senseOut()
	{
	clearSelect();
	}




void	Menu::setDirection(int d)
	{
	argCheck(d!=vertical&&d!=horizontal);
	direction=d;
	}


void		Menu::refreshStart()
	{
	FOR_LL(now,*data,MenuObject)
		now->refreshStatus=0;
	END_FOR_LL
	skipEntryDeform=true;
	}
	
MenuObject*	Menu::refreshNext(void *d)
	{
	FOR_LL(now,*data,MenuObject)
		if(now->isData(d))
			{
			now->refreshStatus=1;
			data->move(data->getHead(true),now_,true);
			skipEntryDeform=false;
			return now;
			}
	END_FOR_LL
	return NULL;
	}
	
void		Menu::refreshAdd(MenuObject *obj)
	{
	data->addItem(obj,data->getHead(true),false);
	obj->refreshStatus=2;
	}
	
void		Menu::refreshEnd()
	{
	//FOR_LL(now,*data,MenuObject)
	//END_FOR_LL
	Position newPosition=zeroEffectivePosition;
	Position oldPosition=zeroEffectivePosition;
	int my=0;
	FOR_LL(now,*data,MenuObject)
		if(now->refreshStatus==0)
			{
			now->kill();
			CONTINUE_LL;
			}
		now->setOwner(this);
		
		if(now->refreshStatus==2)
			{
			if(oldPosition.x+now->getOriginSize().x>getOriginSize().x)
				{
				oldPosition.x=0;
				oldPosition.y+=now->getOriginSize().y;
				}
			now->setPosition(oldPosition+now->getIndent());
			}
		else
			{
			oldPosition=now->getOriginPosition();
			oldPosition.x+=now->getOriginSize().x;
			}
		
		Coord c=now->getOriginSize();
		if(newPosition.x+c.x>getOriginSize().x)
			{
			newPosition.x=0;
			newPosition.y+=my;
			my=0;
			}
		newPosition+=now->getIndent();
		
		if(!skipEntryDeform)
			{
			if(now->refreshStatus==2)
				{
				//now->setPosition(oldPosition);
				if(direction&vertical)
					now->addState(new SizeDeviation(-now->getSize().onlyY()+Coord(0,1),new InverseMapping(true,false,new SimpleMapping),30));
				if(direction&horizontal)
					now->addState(new SizeDeviation(-now->getSize().onlyX()+Coord(1,0),new InverseMapping(true,false,new SimpleMapping),30));
				}
			now->move(newPosition-now->getOriginPosition(),new SimpleMapping(),30);
			}
		else
			now->setPosition(newPosition);
			
		
		newPosition.x+=c.x;
		if(c.y>my)
			my=c.y;
	END_FOR_LL
	}






