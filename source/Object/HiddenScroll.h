

typedef void	(*ScrollConverter)(Coord c,int xm,int ym,int zm,int *x,int *y,int *z);

class	ScrollSensor:public PullSensor{
	public:
	ScrollSensor(ScrollConverter cv,Coord oc,bool rl,int xm_,int ym_,int zm_,int xi,int yi,int zi,int dr,int pr,int ur);
void	down(Coord c);
void	press(Coord c);
void	up(Coord c);
void	setFixCoord(Coord f);
int		getX()const;
int		getY()const;
int		getZ()const;
void	setX(int a);
void	setY(int a);
void	setZ(int a);

	private:
ScrollConverter	converter;
Coord	originCoord,fixCoord;
bool	relative;
int		xm,ym,zm,x,y,z;
int		downResponse,pressResponse,upResponse;
	};



class HiddenScroll:public Object{
	public:
	HiddenScroll(Position pos,int min,int max,int init,int r);
	~HiddenScroll();
void	response(int r);
void	setValue(int v);
int		getValue()const;
void	show();
	private:
ScrollSensor	*scrollSensor;
int		min,max,value,originValue;
int		responseIndex;
	};




















