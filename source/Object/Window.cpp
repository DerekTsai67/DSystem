

int objectIDSeed=0;

	Object::Object(Position p,Picture *sbg,Size s):object(true)
	{
	argCheck(sbg==NULL);
	init();
	squareBg=sbg;
	reSize(squareBg->makeSquare(s));
	setPosition(p);
	}
	
	Object::Object(Position p,Picture *bg):object(true)
	{
	init();
	reSize(bg);
	setPosition(p);
	}

	Object::Object(Position p,Coord s):object(true)
	{
	init();
	size=s;
	if(!(s>zeroCoord))
		wBuffer=new Picture(Coord(1,1),DS_RGB(255,255,255));
	else
		wBuffer=new Picture(size,DS_RGB(255,255,255));
	wDisplayer->setPic(wBuffer);
	bgBuffer=new Picture(wBuffer->getSize(),transparent);
	setPosition(p);
	}

void	Object::init()
	{
	//report("Creating Object %d",objectID);
	objectID=objectIDSeed++;

	pos=size=zeroCoord;
	type=OBJECT_default;
	movePos=zeroCoord;
	moveStartTime=moveTime=0;
	killState=killAlive;
	owner=nowOwner;
	nowObject=this;
	reshapeMin=Coord(30,30);
	reshapeMax=Coord(300,300);
	reShowB=true;reShowBg=false;defaultReShow=false;
	defaultSense=new RegularSense();
	squareBg=bgBuffer=wBuffer=NULL;
	bgDisplayer=NULL;
	etcLinker.setNoDel(true);
	addDisplayer(bgDisplayer=new Displayer(NULL));
	addDisplayer(wDisplayer=new Displayer(NULL));
	addObject(etcObject);
	count++;
	}

	Object::~Object()
	{
	//report(1 ,"Deleting Object %5d in %s %5d",getID(),typeid(*owner).name(),owner->getID());
	FOR_LL(now,etcLinker,EtcLinker)
		now->clear();
	END_FOR_LL
	if(defaultSense!=nonSense)
		delete defaultSense;
	delete wBuffer;
	if(bgBuffer!=NULL)
		delete bgBuffer;
	if(focusObject==this)
		focusObject=NULL;
	count--;
	//report(-1,"Object Deleted.");
	}
	
	Window::Window(Position p,Picture *sbg,Size s):Object(p,sbg,s)
	{
	init();
	setPosition(p);
	}
	
	Window::Window(Position p,Picture *bg):Object(p,bg)
	{
	init();
	setPosition(p);
	}
	
	Window::Window(Position p,Coord s):Object(p,s)
	{
	init();
	setPosition(p);
	}

void	Window::init()
	{
	owner=nowOwner=this;
	next=NULL;
	delete defaultSense;
	defaultSense=new RegularSense();
	behindIndex=frontIndex=0;
	for(int i=0;i<frontMax;i++)
		front[i].setOwner(this);
	for(int i=0;i<behindMax;i++)
		behind[i].setOwner(this);
	}
	
	Window::~Window()
	{
	}
	
void	Window::recDel()
	{
	if(next!=NULL)
		{
		next->recDel();
		delete next;
		}
	}


//==============================================================================

void	Object::setPosition(Position p)
	{
	if(isWindow()||owner==NULL)
		pos=p.fix(wBuffer,zeroCoord,window_size,zeroCoord,Position::EffectiveLeftUp);
	else
		pos=p.fix(wBuffer,owner->wBuffer,Position::EffectiveLeftUp);
	}

Position	Object::getPosition()
	{
	if(isWindow())
		return getLocalPosition();
	else
		return owner->getPosition()+getLocalPosition();
	}

Position	Object::getLocalPosition()
	{
	return getDeformedLocalPosition();
	}

Position	Object::getDeformedLocalPosition()
	{
	Coord moveFix=zeroCoord;
	FOR_LL(now,objectState,ObjectState)
		moveFix+=now->getPositionDeviation();
	END_FOR_LL
	return getOriginPosition()+moveFix;
	}
	
Position	Object::getOriginPosition()const
	{
	return Position(pos);
	}

void	Object::reSize(Size s)
	{
	if(/*bgBuffer==NULL&&*/squareBg==NULL)
		{
		delete wBuffer;
		wBuffer=new Picture(s,transparent);
		size=s;
		return;
		}
	argCheck(squareBg==NULL);
	size=s.fix(squareBg,Size::Effective);
	reShowBg=true;
	//reSize(squareBg->makeSquare(s));
	}
	
void	Object::reSize(Size s,int t,Mapping *m)
	{
	argCheck(t<=0);
	if(m==NULL)
		m=new SimpleMapping();
	Coord oldSize=size;
	size=s.fix(squareBg,Size::Effective);
	addState(new SizeDeviation(oldSize-size,new InverseMapping(true,false,m),t));
	}

void	Object::reSize(Picture *pic,bool changeSize)
	{
	argCheck(pic==NULL);
	if(bgBuffer!=NULL)
		delete bgBuffer;
	bgBuffer=pic;

	if(wBuffer!=NULL)
		delete wBuffer;
	wBuffer=new Picture(bgBuffer->getSize(Size::Real));
	wBuffer->setFixSize(bgBuffer->getFixHeadSize(),bgBuffer->getFixEndSize());

	bgDisplayer->setPic(bgBuffer,true);
	wDisplayer->setPic(wBuffer,true);

	if(changeSize)
		size=bgBuffer->getSize();
	
	reShow();
	}

Size	Object::getSize()const
	{
	return getDeformedSize();
	}

Size	Object::getDeformedSize()const
	{
	Coord s=size;
	FOR_LL_CONST(now,objectState,ObjectState)
		s=now->getSize(s);
	END_FOR_LL
	return Size(s,Size::Effective);
	}
	
Size	Object::getOriginSize()const
	{
	return Size(size,Size::Effective);
	}
	
void		Object::setSquareBg(Picture *sbg)
	{
	argCheck(sbg==NULL);
	squareBg=sbg;
	reShowBg=true;
	}

Picture*	Object::getSquareBg()const
	{
	argCheck(squareBg==NULL);
	return squareBg;
	}


//==============================================================================

int 	Object::getID()const
	{
	return objectID;
	}

bool	Object::isWindow()const
	{
	return owner==this;
	}

void	Object::setOwner(Object *o)
	{
	owner=o;
	}

Window*	Object::getOwner()const
	{
	if(isWindow())
		return (Window*)this;
	return owner->getOwner();
	}
	
Object*	Object::getObjectOwner()const
	{
	return owner;
	}

void	Object::focus()
	{
	getOwner()->push_to_top(head,NULL);
	focusObject=this;
	}

//==============================================================================

void	Object::addObject_(void *obj)
	{
	object.addItem((LinkedList<Object>*)obj);
	}
	
void	Object::addEtcObject(Object *obj)
	{
	etcObject.addItem(obj);
	}
	
void	Object::act_()
	{
	if(!haveType(OBJECT_actable))
		return;
	FOR_LL(n1,object,LinkedList<Object>)
	FOR_LL(now,*n1,Object)
		now->act_();
	END_FOR_LL
	END_FOR_LL
	act();
	}
	
void	Object::act()
	{
	}

void	Object::check(Coord cursor)
	{
	FOR_LL(now,objectState,ObjectState)
		if(now->getRemainTime()==0)
			{
			extern LinkedList<ObjectState> *trashObjectState;
			extern bool	trashSignatureShow;
			trashObjectState->addItem(now);
			trashSignatureShow=false;
			DELECON_LL_ND(ObjectState,true);
			}
	END_FOR_LL

	FOR_LL(n1,object,LinkedList<Object>)
	FOR_LL(now,*n1,Object)
		if(now->owner!=this)
			error("Owner asymmetry.\n%s %X \n%s %X \n%s %X"
				,typeid(*now).name(),now
				,typeid(*now->owner).name(),now->owner
				,typeid(*this).name(),this);
		now->check(cursor);
		if(ftime>=now->moveStartTime+now->moveTime)
			now->moveTime=0;

		if(now->killState==killing)
			{
			extern LinkedList<Object>	*trashObject;
			extern bool	trashSignatureShow;
			trashObject->addItem(now);
			trashSignatureShow=false;
			DELECON_LL_ND(Object,true);
			}
	END_FOR_LL
	END_FOR_LL
	}


//==============================================================================

Sense*	Object::sense_(Coord c)
	{
	if(!haveType(OBJECT_sensable)||!haveType(OBJECT_visible))
		return NULL;
	c=c-getLocalPosition().fix(wBuffer,isWindow()?NULL:owner->wBuffer,Position::EffectiveLeftUp);
	Sense *ret=NULL;
	FOR_LL(n1,object,LinkedList<Object>)
	FOR_LL(now,*n1,Object)
		if((ret=now->sense_(c))!=NULL)
			return ret;
	END_FOR_LL
	END_FOR_LL
	if(!(c>=zeroCoord&&c<getSize()))
		{
		if(keySignal.mouseDown(MOUSE_LEFT)||keySignal.mouseDown(MOUSE_RIGHT)||keySignal.mouseDown(MOUSE_MIDDLE))
			senseOut();
		return NULL;
		}
	return sense(c);
	}

Sense*  Object::sense(Coord c)
	{
	return defaultSense;
	}

void	Object::senseOut()
	{
	}

React*	Object::react_(Coord c,Sense *s)
	{
	if(!haveType(OBJECT_sensable)||!haveType(OBJECT_visible))
		return NULL;
	c=c-getLocalPosition().fix(wBuffer,isWindow()?NULL:owner->wBuffer,Position::EffectiveLeftUp);
	React *ret=NULL;
	FOR_LL(n1,object,LinkedList<Object>)
	FOR_LL(now,*n1,Object)
		if((ret=now->react_(c,s))!=NULL)
			return ret;
	END_FOR_LL
	END_FOR_LL
	if(!(c>=zeroCoord&&c<getSize()))
		return NULL;
	FOR_LL(now,reactLL,React)
		if(now->check(c,s))
			return now;
	END_FOR_LL
	return react(c,s);
	}

React*  Object::react(Coord c,Sense *s)
	{
	return NULL;
	}

React*  Window::react(Coord c,Sense *s)
	{
	return nonReact;
	}

void	Object::addReact(React *a)
	{
	reactLL.addItem(a);
	}

void	Object::lclick(Coord c){}
void	Object::lclick(Coord c,bool d){}
void	Object::rclick(Coord c){}
void	Object::rclick(Coord c,bool d){}
void	Object::mclick(Coord c){}
void	Object::mclick(Coord c,bool d){}
void	Object::ldclick(Coord c){}
void	Object::rdclick(Coord c){}
void	Object::mdclick(Coord c){}
void	Object::pointed(Coord c,bool on){}
void	Object::input(){}

void	Object::response(int index)
	{
	}
	
bool	Object::threwOut(Window* sw)
	{
	return false;
	}


//==============================================================================

void	Object::setType(int t)
	{
	type=t;
	}

void	Object::addType(int t)
	{
	type|=t;
	}

void	Object::deleteType(int t)
	{
	type&=~t;
	}

bool	Object::haveType(int t)
	{
	return (type&t)==t;
	}

void	Object::addState(ObjectState *s)
	{
	argCheck(s==NULL);
	s->setOwner(this);
	objectState.addItem(s);
	}


void	 Object::move(Coord v,Mapping *m,int time)
	{
	if(!haveType(OBJECT_movable))
		return;
	if(m!=NULL)
		addState(new PositionDeviation(-v,new InverseMapping(false,true,m),time));
	pos+=v;
	}
/*
void	Object::pull(Coord p)
	{
	}

void	Object::reshape(Coord p,Sense s_)
	{
	}*/

void	Object::kill()
	{
	if(isDead())
		return;
	killState=killing;
	deleteType(OBJECT_visible);
	deleteType(OBJECT_sensable);
	deleteType(OBJECT_actable);
	}

bool	Object::isDead()const
	{
	return killState!=killAlive;
	}

//==============================================================================

Window*	Window::getNext()
	{
	return next;
	}
	
void	Window::refresh()
	{
	}


	
void	Window::addBehind(Window *b)
	{
	argCheck(b==NULL);
	int fi,bi;
	for(fi=0;fi<b->frontIndex;fi++)
		if(b->front[fi]==NULL)
			break;
	for(bi=0;bi<behindIndex;bi++)
		if(behind[bi]==NULL)
			break;
	if(fi>=b->frontIndex)
		b->frontIndex++;
	if(bi>=behindIndex)
		behindIndex++;
	if(behindIndex>=behindMax||b->frontIndex>=b->frontMax)
		error("front/behind overflow.");
	behind[bi].setTo(b->front[fi],0,0);
	}

void	Window::push_to_top(Window *top,Window *source)
	{
	if(top==NULL)
		top=head;
	if(top==this)
		error("top->push_to_top.");
	if(haveType(OBJECT_alwaysDown))
		return;

	for(int i=0;i<frontIndex;i++)
		if(front[i]!=source&&front[i]!=NULL)
			{
			front[i]->push_to_top(top,this);
			top=front[i];
			}

	Window *pre=top;
	while(pre->next!=this)
		if(pre->next==NULL)
			return;
		else
			pre=pre->next;
	pre->next=this->next;
	this->next=top->next;
	top->next=this;

	top=this;
	for(int i=0;i<behindIndex;i++)
		if(behind[i]!=source&&behind[i]!=NULL)
			{
			behind[i]->push_to_top(top,this);
			top=behind[i];
			}
	}




//=================================================================================================

void	Object::addDisplayer(Displayer *d)
	{
	displayer.addItem(d);
	}

void	Window::showRec(bool focus)
	{
	if(next!=NULL)
		next->showRec(false);
	if(!haveType(OBJECT_visible))
		return;

	this->show_();
	}

void	Object::reShow()
	{
	reShowB=true;
	}

void	Object::show()
	{
	}

void	Object::show_(Coord p)
	{
	if(!haveType(OBJECT_visible))
		return;
	
	bool reShowState=false;
	FOR_LL(now,objectState,ObjectState)
		if(now->getReShow())
			{
			reShowState=reShowBg=reShowB=true;
			break;
			}
	END_FOR_LL
	
	if(reShowBg)
		{
		if(squareBg==NULL)
			error("ReShowBg without squareBg.");
		reSize(squareBg->makeSquare(getSize()),false);
		reShowBg=false;
		}
	if(reShowB)
		{
		if(bgDisplayer==NULL)
			error("Depreciated");
			//wBuffer->blit(bgBuffer,zeroRealPosition);
		else
			wBuffer->clear(transparent);
		show();
		}
	reShowB=defaultReShow;
	
	if(reShowState)
		reShowB=reShowBg=true;
	
	//showOut();
	FOR_LL(d,displayer,Displayer)
		if(d->showCheck())
			;//DELECON_LL(Displayer);
	END_FOR_LL

	FOR_LL(n1,object,LinkedList<Object>)
	FOR_LL(now,*n1,Object)
		now->show_();
	END_FOR_LL
	END_FOR_LL
	}

void	Window::showOutRec()
	{
	if(next!=NULL)
		next->showOutRec();

	this->showOut_();
	}

void	Object::showOut_()
	{
	if(!haveType(OBJECT_visible))
		return;

	showOut();

	FOR_LL(n1,object,LinkedList<Object>)
	FOR_LL(now,*n1,Object)
		now->showOut_();
	END_FOR_LL
	END_FOR_LL
	}

void	Object::showOut()
	{
	FOR_LL_BACK(d,displayer,Displayer,true)
		d->show(getPosition());
	END_FOR_LL
	}


//=================================================================================================
	Base::Base():Window(zeroRealPosition,window_size)
	{
	addType(OBJECT_alwaysDown);
	}
/*void	Base::show()
	{
	wBuffer->clear(BGC);
	}*/

