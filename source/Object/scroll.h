

class Scroll:public Object{
	public:
	 Scroll(Position p,Size s,const Picture *texture,int &d,int m,int M,int ri=0);
	~Scroll();
//void	pull(Coord p);
void	act();
Sense* 	sense(Coord cursor);
void	lclick(Coord p);
void	setValue(int v);
void	getValue();
void	show();

	private:
int		*data,oldData,min,max;
int 	value,valueMax,responseIndex;
Picture	*line,*node;
const Picture	*pic;
class BarSensor:public PullSensor{
		 public:
	void	press(Coord sensePos);
	SENSOR_OWNER(Scroll);
		};
RegularSense *barSense;
	};




